//
//  TrackOrderViewController.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 22/11/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import Foundation
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON

class TrackOrderViewController: parentViewController,UITableViewDataSource,UITableViewDelegate,NVActivityIndicatorViewable
{
    //for tablecell and data
    var MyRestcells:MyResListCell!
    var arrData: NSMutableArray! = ["Order Recevied","Being Prepared","On its way!","Delivered"]
    var arrMetadata: NSMutableArray! = ["Recevied your order","Your food is being prepared","Your food is on its way to your table !","Your food has been delivered"]

    //for Calling webservices
    var strWSURL: NSString!
    var strStatus: String!

    var strUSERID: AnyObject!
    @IBOutlet weak var tblTrackOrderData:UITableView!
    @IBOutlet weak var viewHeader:UIView!

    // MARK: - LIFECYCLES METHODS

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       simplenavigationBarUI()
       self.navigationItem.title = "Track Order"

        customBarButtons()

        viewHeader.layer.borderColor = UIColor.lightGray.cgColor
        viewHeader.layer.borderWidth = 0.5

    }
    func customBarButtons()
    {
        //side menu button
        
        let img = UIImage(named: "BackArrow")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(backPressed))
    }
    func backPressed(_ sender: UIButton)
    {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: - TABLEVIEW DATASOURCE AND DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return 79;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if arrData==nil
        {
            return 0
            
        }
        else
            
        {
            return arrData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        //display searching result
        
        MyRestcells = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyResListCell
        
        // Name
        
        MyRestcells.lblRestName.text=(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject) as? String
        
        // OrderID
        
        MyRestcells.lblOrderId.text=(self.arrMetadata.object(at: (indexPath as NSIndexPath).row) as AnyObject) as? String

        
        
        // Price
        
        MyRestcells.btnPrice.setTitle("2:10 PM ", for: .normal)
        MyRestcells.btnPrice.layer.borderColor = GlobalConstants.MAIN_BROWN_COLOR .cgColor
        MyRestcells.btnPrice.layer.borderWidth = 0.5
        MyRestcells.viewOrderStatus.layer.cornerRadius = MyRestcells.viewOrderStatus.frame.size.width / 2
        // Status
        
        MyRestcells.viewOrderStatus.layer.borderColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1).cgColor
        MyRestcells.viewOrderStatus.layer.borderWidth = 1.0
        
        if(strStatus == "Placed")
        {
            if(indexPath.row == 0)
            {
                MyRestcells.viewOrderStatus.backgroundColor = #colorLiteral(red: 0.3116906285, green: 0.08825992793, blue: 0.07663153857, alpha: 1)
            }
            else
            {

            }
        }
        else if(strStatus == "In Progress")
        {
            
            if(indexPath.row == 0)
            {
                MyRestcells.viewOrderStatus.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            }
            else  if(indexPath.row == 1)

            {
                MyRestcells.viewOrderStatus.backgroundColor = #colorLiteral(red: 0.3116906285, green: 0.08825992793, blue: 0.07663153857, alpha: 1)

            }
            else
            {
                
            }
            
        }
        else
        {
            MyRestcells.viewOrderStatus.backgroundColor = #colorLiteral(red: 0.3116906285, green: 0.08825992793, blue: 0.07663153857, alpha: 1)
            
        }
        
       
        
        return MyRestcells
    }
}
