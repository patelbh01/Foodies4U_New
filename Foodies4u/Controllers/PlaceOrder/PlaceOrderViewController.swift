//
//  PlaceOrderViewController.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 03/11/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON

let borderWidth: CGFloat = 1.0
let borderColor : UIColor =  #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)

class PlaceOrderViewController: parentViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, NVActivityIndicatorViewable, FloatRatingViewDelegate
{
    
    var dictRestaurantDetails: NSDictionary!
    var dialog = ZAlertView()
    
    var arrRatingList = NSMutableArray()
    var dictResult :NSDictionary!
    var strWSURL: NSString!
    var strUSERID: AnyObject!


    
    var strUserComment :String!
    var overallRating: Float!
    
    @IBOutlet weak var tblOrderDeatil: UITableView!
    @IBOutlet weak var ViewNoCart: UIView!

    @IBOutlet weak var lblDateandTime: UILabel!
    @IBOutlet weak var lblRestName: UILabel!
    
    
    var arrFoodItemDetail:NSMutableArray!
    var FoodItemdetail:FoodItemInfo!
    var arrMenuId:NSMutableArray! = []
    var arrBadge:NSMutableArray! = []
    var arrQty:NSMutableArray! = []
    var arrPrice:NSMutableArray! = []
    var arrCouponData:  NSDictionary!

    
    var strRestId:String!
    var strRestName:String!
    var strMenuId:String!
    var strQty:String!
    var strprice:String!
    var strDiscountPrice : String! = "0.0"

    
    var strDiscount:String!
    var strSubTotal:String!
    var strSubTotalIncTax:String!
    var strSubTotalIncDiscount:String! = "0.0"
    var strOrderTotal:String!

    var strComment: AnyObject!
    var arrData: NSMutableDictionary!
    var dictFinalMenu:NSMutableDictionary!
    var arrFinalMenu:NSMutableArray!
    var strResId: String!

    
    
    
    var foodQty : Int = 0
    var selRow : Int!
    var TotalPrice : Double!
    var DiscountPrice : Double!

    var ISDISOUNT : Bool! = false

    var ISCOPOUNAPPLIED : Bool! = false

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Title and back button
        simplenavigationBarUI()
        self.navigationItem.title = "Order Detail"
        
        let strStatus = UserDefaults.standard.value(forKey: "MYCARTFROMSIDEMENU")
        
        if (strStatus as? String != "YES")
        {
        
           let img = UIImage(named: "BackArrow")
           navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(btngoBack(sender:)))
            
        }
        else
        {
            let img = UIImage(named: "MenuBar.png")
            navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        }
        
        //keyboard appearence notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        
        
        //get data from database
        
        FoodItemdetail  = FoodItemInfo()
        arrFoodItemDetail = NSMutableArray()
        arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
        
        if (arrFoodItemDetail.count == 0)
        {
            
            self.ViewNoCart.isHidden = false
            self.tblOrderDeatil.isHidden =  true
                      /* let dialog:ZAlertView = ZAlertView(title: "Foodies4u",
                                               message: "Your cart is empty"
                ,
                                               closeButtonText: "Ok",
                                               closeButtonHandler: { (alertView) -> () in
                                                alertView.dismissAlertView()
                                                
                                                
                }
                
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true*/
        }
        else
        {
            self.tblOrderDeatil.isHidden =  false
            self.tblOrderDeatil.dataSource = self
            self.tblOrderDeatil.delegate = self
            self.ViewNoCart.isHidden = true



            
            // set restaurant name
            FoodItemdetail = arrFoodItemDetail.object(at: 0) as! FoodItemInfo
            lblRestName.text = FoodItemdetail.strRestName
            strRestId = FoodItemdetail.strRestId as! String
            
            //tableview border
            tblOrderDeatil!.addTopBorderWithColor(color: borderColor, width: borderWidth)
            
            //coupon code
            
            self.getCouponCode()
        }
    
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        //get userid value
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!

        if (arrFoodItemDetail.count == 0)
        {
           
        }
        else
        {
            arrPrice.removeAllObjects()
            
            
            for i in 0...arrFoodItemDetail.count - 1
            {
                
                FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                arrPrice.add(FoodItemdetail.strTotalPrice)
               
            }
            
            
            
            var sum : Double = 0.0
        
             print(arrPrice)
            
            for i in 0...arrPrice.count - 1
            {
                sum = sum + Double(arrPrice[i] as! String)!
            }
            
            print("sum",sum)
        
          strprice = String(sum)
            
        }
        
        
        //dismis keyboard any where you tap on the screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
            
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- ======DELEGATE METHOD======
    //MARK: Table delegate and data source
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return arrFoodItemDetail.count + 8
    }
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var cellHeight: CGFloat = 0
        
        if (indexPath.row == 0) //Restaurant name & current rating, reviews
        {
            cellHeight = 63
        }
        else if (indexPath.row == 1) //Message
        {
            cellHeight = 30
        }
        else if (indexPath.row == 2) //Rating header
        {
            
            cellHeight = 44
            
        }
        else if (indexPath.row == arrFoodItemDetail.count + 7) //Rating header
        {
            
            cellHeight = 60
            
        }
        /*else if (indexPath.row == arrFoodItemDetail.count + 7) //Rating header
        {
            
            cellHeight = 60
            
        }*/
            
        
        else
        {
            cellHeight = 44
        }
        
        
        return cellHeight
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        
        if (indexPath.row < 2 || indexPath.row > arrFoodItemDetail.count + 1 )
        {
            
        }
        else
        {
            FoodItemdetail = arrFoodItemDetail.object(at: (indexPath as NSIndexPath).row - 2 ) as! FoodItemInfo
            
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderD&TCell") as! Customcell
        
        if(indexPath.row % 2 == 0)
        {
            cell.backgroundColor = #colorLiteral(red: 0.9759238362, green: 0.9766622186, blue: 0.9760381579, alpha: 1)
        }
        else
            
        {
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
        
        if (indexPath.row == 0) // order Date and time
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderD&TCell") as! Customcell
            
            //sets restaurant name
            
            
            let formatter = DateFormatter()
            let date = NSDate()
            formatter.dateFormat = "yyyy-MM-dd HH:mm a"
            let dateString = formatter.string(from:(date as? Date)!)
            
            
            cell.lblDateandTime.text = dateString
            
            cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
            cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
            
            
            
            return cell
        }
            
        else if (indexPath.row == 1) //Order Header
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "rateHeader") as! Customcell
            cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
            cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
            return cell
        }
            
        else if (indexPath.row == arrFoodItemDetail.count - 1 || indexPath.row == arrFoodItemDetail.count || indexPath.row == arrFoodItemDetail.count + 1 ) //Order Detail
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDeatilCell") as! Customcell
            
            if(arrFoodItemDetail.count > 0)
            {
                
                cell.lblFoodItemName.text = FoodItemdetail.strMenuName
                
                cell.lblFoodQty.text = FoodItemdetail.streQuantity
                
                let UnitPrice:Float = Float(FoodItemdetail.strUnitPrice)!
                
                let myIntValue:Int = Int(UnitPrice)
                
                let Qty:Int = Int(FoodItemdetail.streQuantity)!
                
                foodQty = Qty
                
                if(FoodItemdetail.strFoodDiscount.characters.count == 0)
                    
                {
                    cell.lblFoodPrice.text = "$ " + String(myIntValue * Qty)
                    
                }
                else
                {
                    
                    let attributeString: NSMutableAttributedString = NSMutableAttributedString(string:  "$" +  String(myIntValue * Qty) )
                    
                    attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length) )
                    attributeString.addAttribute(NSForegroundColorAttributeName, value:UIColor.init(colorLiteralRed: 92/255.0, green: 94/255.0, blue: 102/255.0, alpha: 1.0), range: NSMakeRange(0, attributeString.length))
                    // attributeString.addAttribute([NSFontAttributeName : UIFont (name: "OpenSans-Bold", size: 9)])
                    let attributeString1: NSMutableAttributedString = NSMutableAttributedString(string: FoodItemdetail.strTotalPrice + "  " )
                    
                    
                    let combination = NSMutableAttributedString()
                    
                    combination.append(attributeString1)
                    combination.append(attributeString)
                    
                    cell.lblFoodPrice.attributedText = combination
                    
                    
                    
                    cell.lblDiscount.text = FoodItemdetail.strFoodDiscount
                    
                    
                }
                
                cell.btnDelete.addTarget(self, action: #selector(self.btnDeletePressed( _:)), for: UIControlEvents.touchUpInside);
                cell.btnDelete.tag = indexPath.row
                
                
                
                cell.contentView.addLeftBorderWithColor(color: borderColor, width: borderWidth)
                cell.contentView.addRightBorderWithColor(color: borderColor, width: borderWidth)
                
                cell.btnIncreaseQty.addTarget(self, action: #selector(self.btnIncreaseQtyPressed(_:)), for: UIControlEvents.touchUpInside);
                cell.btnIncreaseQty.tag = indexPath.row
                
                cell.btnDecreaseQty.addTarget(self, action: #selector(self.btnDecreaseQtyPressed(_:)), for: UIControlEvents.touchUpInside);
                cell.btnDecreaseQty.tag = indexPath.row
                
            }
            
            
            return cell
        }
            
        else if (indexPath.row == arrFoodItemDetail.count + 2) //Review rows
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddMoreCell") as! Customcell
            
            
            
            
            cell.btnAddMoreItem.addTarget(self, action: #selector(self.btngoBack(sender:)), for: UIControlEvents.touchUpInside);
            
            cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
            cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
            return cell
        }
            
        else if (indexPath.row == arrFoodItemDetail.count + 3) //Review rows
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalCell") as! Customcell
            
            
            cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
            cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
            cell.addBottomBorderWithColor(color: borderColor, width: borderWidth)
            
            let strTax:Double = Double(strprice)!
            cell.lblGrandTotalAmount.text = "TOTAL AMOUNT"
            
            
            if(arrFoodItemDetail.count > 0)
            {

            cell.lblTotalAmount.text = "$ " + strprice
            }
            else
            {
            
            cell.lblTotalAmount.text =  "$ 0.0 "
            }

            
            return cell
        }
        else if (indexPath.row == arrFoodItemDetail.count + 4) //Comment Row
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TaxCell") as! Customcell
            
            cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
            cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
            
            let strTax:Double = Double(strprice)!
            
            
            
            if(arrFoodItemDetail.count > 0)
            {
            
            cell.lblTax.text =  "$ " + String( /*strTax +*/ strTax * 7.00/100)
            
            strSubTotalIncTax = String( strTax + strTax * 7.00/100)
            }
            else
            {
                cell.lblTax.text =  "$ 0.0"

            }
            
            
            
            return cell
        }
        else if (indexPath.row == arrFoodItemDetail.count + 5) //Overall review Row
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountCell") as! Customcell
            
            cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
            cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
            
            
            if(arrFoodItemDetail.count == 0)
            {
                cell.lblDiscount.text =  "$ 0.0 "

            }
            return cell
        }
        else  if (indexPath.row == arrFoodItemDetail.count + 6) //Submit review Row
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalCell") as! Customcell
            
            
            cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
            cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
            cell.addBottomBorderWithColor(color: borderColor, width: borderWidth)
            
            cell.lblGrandTotalAmount.text = "GRAND TOTAL"
            
            if(arrFoodItemDetail.count > 0)
            {
            
            
            let strTax:Double = Double(strprice)!

            if(ISDISOUNT ==  true)
            {
                
                
                
                if(strDiscountPrice.contains(" %"))
                {
                    
                    cell.lblTotalAmount.text =  "$ " + String( (strTax + strTax * 7.00/100) * DiscountPrice
                    )
                    
                    TotalPrice = strTax + strTax * 7.00/100
                }
                else
                {
                    cell.lblTotalAmount.text =  "$ " + String( strTax + strTax * 7.00/100 - DiscountPrice
                    )
                    
                    TotalPrice = strTax + strTax * 7.00/100
                }
                
                strSubTotalIncDiscount = String(TotalPrice)
                
                
                strOrderTotal = strSubTotalIncDiscount

            

            }
            else
            {
                cell.lblTotalAmount.text =  "$ " + String( strTax + strTax * 7.00/100 )
                
                TotalPrice = strTax + strTax * 7.00/100
                
                strOrderTotal = String(TotalPrice)

            }
                
            }
            else
            {
                cell.lblTotalAmount.text =  "$ 0.0 "
                
            }
            

            
            return cell
        }
            
       /* else  if (indexPath.row == arrFoodItemDetail.count + 6) //Submit review Row
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlankCell") as! Customcell
            // cell.btnSubmitReview.addTarget(self, action: #selector(self.btnSubmitReview(sender:)), for: UIControlEvents.touchUpInside)
            
            return cell
        }*/
            
        /*else  if (indexPath.row == arrFoodItemDetail.count + 7) //Submit review Row
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CouponCell") as! Customcell
            // cell.btnSubmitReview.addTarget(self, action: #selector(self.btnSubmitReview(sender:)), for: UIControlEvents.touchUpInside)
            
            cell.viewCopoun.layer.borderColor = UIColor.init(colorLiteralRed: 205/255.0, green: 205/255.0, blue: 205/255.0, alpha: 1.0).cgColor
            cell.viewCopoun.layer.borderWidth = 1.0
            
            cell.btnapply.addTarget(self, action: #selector(self.btnApplyCouponCodePressed(_:)), for: UIControlEvents.touchUpInside);
            cell.btnapply.tag = indexPath.row
            
            return cell
        }*/
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceOrderCell") as! Customcell
             cell.btnPlaceOrder.addTarget(self, action: #selector(self.btnPlaceOrderPressed(_:)), for: UIControlEvents.touchUpInside)
            
            return cell
        }
        
        
        
        return cell
    }
    
    
    //MARK: Textview delegate
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if (text=="\n")
        {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    
    public func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text.characters.count != 0)
        {
            strUserComment = textView.text
        }
        else
        {
            if let strComment = self.dictResult["Comment"]
            {
                strUserComment = strComment as! String
            }
            tblOrderDeatil.reloadData()
        }
    }
    
    
    //MARK: Rating delegate
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float)
    {
        overallRating = rating
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //MARK:- ======BUTTON ACTIONS======
    func dbInsertFoorItemIntoCart()
    {
        self.view.endEditing(true)
        
        FoodItemdetail  = FoodItemInfo()
        
        arrFoodItemDetail = NSMutableArray()
        //menuid
        
        arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
        
        print("bhumika", arrFoodItemDetail)
        
        if (arrFoodItemDetail.count > 0)
        {
            
            for i in 0...arrFoodItemDetail.count - 1
            {
                FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                
                
                
                arrMenuId.add(FoodItemdetail.strMenuId)
                
                print(arrMenuId)
                
                
                
                
                
                
            }
        }
        
        
        
        
        
        let indexPath1 = IndexPath(row: selRow + 2, section: 0)
        
        let cell:Customcell = (self.tblOrderDeatil.cellForRow(at: indexPath1) as? Customcell)!
        
        let qnt:Int = Int(cell.lblFoodQty.text!)!
        
        FoodItemdetail.streQuantity =  String(qnt)
        
        let price:Double = Double(strprice)!
        
        let qty:Double = Double(cell.lblFoodQty.text!)!
        
        // totalprice
        FoodItemdetail.strTotalPrice = String(qty * price)
        
        //comment
        if(strComment != nil)
        {
            FoodItemdetail.strComment = strComment as! String
        }
        else
        {
            FoodItemdetail.strComment = ""
            
        }
        
        //menuid
        FoodItemdetail.strMenuId = strMenuId
        
        
        //Badge
        
        FoodItemdetail.strBadge = String(foodQty )
        
        //discount
        
        
        if(strDiscount.characters.count > 0)
        {
            FoodItemdetail.strFoodDiscount = strDiscount
            
            if(strDiscount.contains("%"))
            {
                strDiscount = strDiscount.replacingOccurrences(of: "%", with: "")
                
                let disc:Double = Double(strDiscount)!
                
                FoodItemdetail.strTotalPrice = String( (price  * qty) - (price  * qty) * disc/100)
                
            }
            else
            {
                
                let disc:Double = Double(strDiscount)!
                FoodItemdetail.strTotalPrice = String( (price  * qty) - disc)
                
            }
        }
        else
        {
            FoodItemdetail.strFoodDiscount = ""
            
        }
        
        
        
        let isUpdated = ModelManager.getInstance().updateStudentData(FoodItemdetail)
        
        if isUpdated {
            
            print(isUpdated)
            
            //get data from database
            
            arrFoodItemDetail.removeAllObjects()
            FoodItemdetail  = FoodItemInfo()
            arrFoodItemDetail = NSMutableArray()
            arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
            
            arrPrice.removeAllObjects()
            
            for i in 0...arrFoodItemDetail.count - 1
            {
                FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
            
                 arrPrice.add(FoodItemdetail.strTotalPrice)
            }
            
            
            var sum : Double = 0.0
            
            for i in 0...arrPrice.count - 1
            {
                sum = sum + Double(arrPrice[i] as! String)!
            }
            
            print("sum",sum)
            
            strprice = String(sum)
            
            tblOrderDeatil.reloadData()
            
        }
        
        
        
    }
    
     @IBAction func btnApplyCouponCodePressed(_ sender: UIButton)
      {
        view.endEditing(true)
        
        let indexPath1 = IndexPath(row: arrFoodItemDetail.count + 7, section: 0)
        var cell:Customcell = (self.tblOrderDeatil.cellForRow(at: indexPath1) as? Customcell)!
        
        
        
        
        if(((cell.txtfieldCopoun.text?.characters.count)! <= 0)  ||  arrCouponData == nil || cell.txtfieldCopoun.text !=  (arrCouponData.value(forKey: "CouponCode") as! String) )
        {
            
            ISDISOUNT = false
            let dialog = ZAlertView(title: "Foodies4u",
                                    message: "Please enter valid Coupon Code",
                                    closeButtonText: "Ok",
                                    closeButtonHandler: { alertView
                                        in
                                        alertView.dismissAlertView()
                                        
                }
                
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
            
            strSubTotalIncDiscount = ""
        }
        else
        {
            
            if(ISCOPOUNAPPLIED == true)
            {
                let dialog = ZAlertView(title: "Foodies4u",
                                        message: "You have already applied the copoun code",
                                        closeButtonText: "Ok",
                                        closeButtonHandler: { alertView
                                            in
                                            alertView.dismissAlertView()
                                            
                    }
                    
                )
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
            }
            else
            {
                ISDISOUNT = true
                
                 ISCOPOUNAPPLIED = true
                
                let indexPath1 = IndexPath(row: arrFoodItemDetail.count + 4, section: 0)
                cell = (self.tblOrderDeatil.cellForRow(at: indexPath1) as? Customcell)!
                
                let indexPath2 = IndexPath(row: arrFoodItemDetail.count + 5, section: 0)
                let cell1:Customcell = (self.tblOrderDeatil.cellForRow(at: indexPath2) as? Customcell)!
                
                if(arrCouponData.value(forKey: "DiscountAmount") as? Int != nil)
                {
                    cell.lblDiscount.text = "$ " +  String(describing: arrCouponData.value(forKey: "DiscountAmount") as! Float)
                    
                    
                    let Dis:Double = arrCouponData.value(forKey: "DiscountAmount") as! Double
                    let TAmout =  TotalPrice
                    cell1.lblTotalAmount.text = "$ " + String(TAmout! - Dis)
                    
                    DiscountPrice = Double(Dis)
                    
                    strDiscountPrice = String(describing: arrCouponData.value(forKey: "DiscountAmount") as! Float)
                    
                    strSubTotalIncDiscount = String(TAmout! - Dis)
                    
                    strOrderTotal = String(TAmout! - Dis)
                }
                else
                {
                    cell.lblDiscount.text =  String(describing: arrCouponData.value(forKey: "DiscountPercentage") as! Float) + " % OFF"
                    
                    let Dis:Double = arrCouponData.value(forKey: "DiscountPercentage") as! Double
                    let TAmout =  TotalPrice
                    cell1.lblTotalAmount.text =  "$ " + String(TAmout!  - TAmout! * Dis/100)
                    
                    DiscountPrice = Double(Dis/100)
                    
                    strDiscountPrice = String(describing: arrCouponData.value(forKey: "DiscountPercentage") as! Float)
                    strSubTotalIncDiscount = String(TAmout! * Dis/100)
                    
                    strOrderTotal = String(TAmout! * Dis/100)
                    
                    
                }
                
                
                
                
                print(strOrderTotal)
            }
            
            
            
            
        }
        
        
        
        
        
      
        
      }
    @IBAction func btnContinueShoppingPressed(_ sender: UIButton)
        
    {
        let HomeViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        self.navigationController?.pushViewController(HomeViewControllerObj!, animated: false)
    }
    
    @IBAction func btnPlaceOrderPressed(_ sender: UIButton)

    {
        //webservice to add preferences
        
        if InternetConnection.isConnectedToNetwork()
        {
            
          

            //add food type
            if(arrFoodItemDetail.count > 0 )
            {
                
                
                let size = CGSize(width: 80, height:80)
                
                startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
                
                strWSURL=GETWSURL()
                
                arrFinalMenu = NSMutableArray()
                
            for i in 0...arrFoodItemDetail.count - 1
            {
                
                
                
                
                FoodItemdetail = arrFoodItemDetail.object(at: i ) as! FoodItemInfo

                
                self.dictFinalMenu =
                    [ "MenuId" : FoodItemdetail.strMenuId, "Quantity":FoodItemdetail.streQuantity ,"UnitPrice":FoodItemdetail.strUnitPrice ,"Comment":FoodItemdetail.strComment, "TotalPrice":FoodItemdetail.strTotalPrice]
                
                
                arrFinalMenu.add(self.dictFinalMenu)
                
                
            }
            
            print(arrFinalMenu)
            
        let formatter = DateFormatter()
        let date = NSDate()
        formatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        let dateString = formatter.string(from:(date as? Date)!)
        strResId = UserDefaults.standard.value(forKey: "RESERVATIONID") as! String!

            let param1 : [ String : AnyObject] = [
                
                "ReservationId" : strResId as AnyObject,
                "RestaurantId":FoodItemdetail.strRestId as AnyObject,
                "CustomerId":strUSERID,
                "OrderDate":dateString as AnyObject,
                "BillingAddressId":0 as AnyObject,
                "DeliveryAddressId":0 as AnyObject,
                "ShippingAddressId":0 as AnyObject,
                "OrderTypeId":1 as AnyObject,
                "OrderSubTotal":strprice as AnyObject,
                "OrderTax":7.00 as AnyObject,
                "OrderSubtotalInclTax":strSubTotalIncTax as AnyObject,
                "OrderDiscount":strDiscountPrice as AnyObject,
                "OrderSubTotalInclDiscount":strSubTotalIncDiscount as AnyObject,
                "OrderTotal":strOrderTotal as AnyObject,
                "MenuItems":arrFinalMenu as AnyObject
            ]
            
            print(param1)
            
            Alamofire.request(strWSURL as String + "PlaceOrder?",method: .post, parameters: param1,  encoding: JSONEncoding.default, headers: nil).responseJSON
                { response in switch response.result {
                    
                    
                case .success(let JSON):
                    
                    print("Success with JSON: \(JSON)")
                    
                    let response = JSON as! NSDictionary
                    
                    self.stopActivityAnimating()
                    
                    if ((response.value(forKey: "Success") as? String) == "1")
                    {
                        
                        let arr:NSDictionary = response.value(forKey: "Data") as! NSDictionary
                        
                        
                        
                        
                        
                        self.dialog = ZAlertView(title: "Foodies4u", message: "Order placed successfully.",closeButtonText: "Ok",
                                                 closeButtonHandler: { (alertView) -> () in
                                                    alertView.dismissAlertView()
                                                    self.dbDeletefromItemIntoCart()
                                                    let MyOrderControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "MyOrderViewController") as? MyOrderViewController
                                                    self.navigationController?.pushViewController(MyOrderControllerObj!, animated: false)
                                                    
                            
                            
                            }
                            
                            
                        
                        )
                        self.dialog.show()
                        
                        
                        
                    }
                    else
                    {
                        self.stopActivityAnimating()

                        
                        let arr:NSDictionary = (response.value(forKey: "Data") as? NSDictionary)!
                        self.dialog = ZAlertView(title: "Foodies4u", message: arr.value(forKey: "status") as? String,
                                                 closeButtonText: "Ok",
                                                 closeButtonHandler: { (alertView) -> () in
                                                    alertView.dismissAlertView()
                                                    let editProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "AddCCDetailViewController") as? AddCCDetailViewController
                                                    
                                                    self.navigationController?.pushViewController(editProfileVC!, animated: true)

                        
                        
                        })
                        self.dialog.show()
                    }
                    
                    
                case .failure(let error):
                    self.stopActivityAnimating()
                    
                    print("Request failed with error: \(error)")
                    }
            }
            
        }
            else
            {
                self.dialog = ZAlertView(title: "Foodies4u", message: "Your cart is empty. Please insert Food Item in your cart",closeButtonText: "Ok",
                                         closeButtonHandler: { (alertView) -> () in
                                            alertView.dismissAlertView()
                                          
                                            
                                            
                    }
                    
                    
                    
                )
                self.dialog.show()
            }
        }
    }
    

    
    @IBAction func btnIncreaseQtyPressed(_ sender: UIButton)
    {
        
        selRow = sender.tag - 2
        
    
        
        let indexPath1 = IndexPath(row: sender.tag, section: 0)
        let cell:Customcell = (self.tblOrderDeatil.cellForRow(at: indexPath1) as? Customcell)!
        
        var qnt:Int = Int(cell.lblFoodQty.text!)!
        
        
        
                cell.lblFoodQty.text = String(qnt + 1 )
            
        
        

        // get menu id
        FoodItemdetail = arrFoodItemDetail.object(at: selRow) as! FoodItemInfo
        strMenuId = FoodItemdetail.strMenuId
        
        //get qty
        FoodItemdetail = arrFoodItemDetail.object(at: selRow) as! FoodItemInfo
        strQty = FoodItemdetail.streQuantity
        
        //price
        FoodItemdetail = arrFoodItemDetail.object(at: selRow) as! FoodItemInfo
        strprice = FoodItemdetail.strUnitPrice
        
        //discount
        FoodItemdetail = arrFoodItemDetail.object(at: selRow) as! FoodItemInfo
        strDiscount = FoodItemdetail.strFoodDiscount
        
        
        print(strMenuId)
        print(strQty)
        print(strprice)
        print(strDiscount)
        
        self.dbInsertFoorItemIntoCart()
        
        
        
    }
    
    func dbDeletefromItemIntoCart()   {
        
        
        let isDeleted = ModelManager.getInstance().deleteAllStudentData(FoodItemdetail)
        
        if isDeleted {
            
            //Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
            //Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
    }
    
    @IBAction func btnDecreaseQtyPressed(_ sender: UIButton)
    {
        
        
        selRow = sender.tag - 2
        
        let indexPath1 = IndexPath(row: sender.tag, section: 0)
        let cell:Customcell = (self.tblOrderDeatil.cellForRow(at: indexPath1) as? Customcell)!
        
        var qnt:Int = Int(cell.lblFoodQty.text!)!
        
        if (qnt == 1)
        {
            
            self.dialog = ZAlertView(title: "Foodies4u", message: "Qty cannot be zero. Press Delete to remove the item",closeButtonText: "Ok",
                                     closeButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()})
            self.dialog.show()
            qnt = 1
            cell.lblFoodQty.text = String(qnt)

        }
        else
        {      cell.lblFoodQty.text = String(qnt - 1 )

        }
        
        
        
        
        
        // get menu id
        FoodItemdetail = arrFoodItemDetail.object(at: selRow) as! FoodItemInfo
        strMenuId = FoodItemdetail.strMenuId
        
        //get qty
        FoodItemdetail = arrFoodItemDetail.object(at: selRow) as! FoodItemInfo
        strQty = FoodItemdetail.streQuantity
        
        //price
        FoodItemdetail = arrFoodItemDetail.object(at: selRow) as! FoodItemInfo
        strprice = FoodItemdetail.strUnitPrice
        
        //discount
        FoodItemdetail = arrFoodItemDetail.object(at: selRow) as! FoodItemInfo
        strDiscount = FoodItemdetail.strFoodDiscount
        
        
        print(strMenuId)
        print(strQty)
        print(strprice)
        print(strDiscount)
        
        
        self.dbInsertFoorItemIntoCart()
        
        
    }
    
    @IBAction func btngoBack(sender: UIButton)
    {
        
        
        let strStatus = UserDefaults.standard.value(forKey: "MYCARTFROMSIDEMENU")
        
        if (strStatus as? String == "YES")
        {
        let RViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "menuListViewController") as? menuListViewController
            
            RViewControllerObj!.arrRestImages = UserDefaults.standard.value(forKey: "restaurantimgs") as! NSArray!
              RViewControllerObj?.strRestId = FoodItemdetail.strRestId
            RViewControllerObj?.restName = FoodItemdetail.strRestName
        self.navigationController?.pushViewController(RViewControllerObj!, animated: true)
        }
        else
        {        self.navigationController!.popViewController(animated: true)

            
        }
    }
    
    
    @IBAction func btnDeletePressed(_ sender: UIButton)
    {
        
        
        let dialog = ZAlertView(title: "Foodies4u",
                                message: " Are you sure you want to Delete the Menu Item from your cart?",
                                isOkButtonLeft: true,
                                okButtonText: "Yes",
                                cancelButtonText: "No",
                                okButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                                    
                                    let indexPath1 = IndexPath(row: sender.tag, section: 0)
                                    let cell:Customcell = (self.tblOrderDeatil.cellForRow(at: indexPath1) as? Customcell)!
                                    
                                    // get menu id
                                    self.FoodItemdetail = self.arrFoodItemDetail.object(at: sender.tag - 2) as! FoodItemInfo
                                    self.strMenuId = self.FoodItemdetail.strMenuId
                                    
                                    let isDeleted = ModelManager.getInstance().deleteStudentData(self.FoodItemdetail)
                                    
                                    if isDeleted {
                                        //Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
                                        //get data from database
                                        
                                        self.arrFoodItemDetail.removeAllObjects()
                                        self.FoodItemdetail  = FoodItemInfo()
                                        self.arrFoodItemDetail = NSMutableArray()
                                        self.arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
                                        self.tblOrderDeatil.reloadData()
                                        //self.dbInsertFoorItemIntoCart()
                                        
                                        
                                        //update badges
                                        
                                        if(self.arrFoodItemDetail.count > 0 )
                                        {
                                            for i in 0...self.arrFoodItemDetail.count - 1
                                            {
                                                self.FoodItemdetail = self.arrFoodItemDetail.object(at: i) as! FoodItemInfo
                                                
                                                
                                                self.arrQty.add(self.FoodItemdetail.streQuantity)
                                            }
                                            
                                            
                                            
                                            var sum : Double = 0.0
                                            
                                            for i in 0...self.arrQty.count - 1
                                            {
                                                sum = sum + Double(self.arrQty[i] as! String)!
                                            }
                                            
                                            print("sum",sum)
                                            
                                            self.FoodItemdetail = self.arrFoodItemDetail.object(at: self.arrQty.count - 1 ) as! FoodItemInfo
                                            self.strMenuId = self.FoodItemdetail.strMenuId
                                            
                                            
                                            
                                            
                                            let isUpdated = ModelManager.getInstance().updateBadgeData(self.FoodItemdetail)
                                            
                                            if isUpdated
                                            {
                                                
                                                self.arrFoodItemDetail = NSMutableArray()
                                                 self.arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
                                                
                                                 self.arrPrice.removeAllObjects()
                                                
                                                if( self.arrFoodItemDetail.count > 0 )
                                                {
                                                    for i in 0...self.arrFoodItemDetail.count - 1
                                                    {
                                                        
                                                         self.FoodItemdetail =  self.arrFoodItemDetail.object(at: i) as! FoodItemInfo
                                                        
                                                         self.arrPrice.add( self.FoodItemdetail.strTotalPrice)
                                                        
                                                    }
                                                    
                                                    
                                                    var sum : Double = 0.0
                                                    
                                                    for i in 0...self.arrPrice.count - 1
                                                    {
                                                        sum = sum + Double( self.arrPrice[i] as! String)!
                                                    }
                                                    
                                                    print("sum",sum)
                                                    
                                                     self.strprice = String(sum)
                                                }
                                                
                                                
                                                
                                            }
                                        }
                                        else
                                        {
                                            
                                        }
                                        
                                        
                                    }
                                    
                                    
            },
                                cancelButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
            }
        )
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //MARK:- ======USER DEFINE METHODS======
    
    
    
    //MARK: Keyboard hide and unhide methods
    func keyboardWillShow (notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
            
            tblOrderDeatil.contentInset = contentInsets
            tblOrderDeatil.scrollIndicatorInsets = contentInsets
        }
    }
    
    
    func keyboardWillHide(notification: NSNotification)
    {
        tblOrderDeatil.contentInset = UIEdgeInsets.zero
        tblOrderDeatil.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    
    //MARK: Get rating data
    func getCouponCode()
    {
        let size = CGSize(width: 80, height:80)
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        let param1 =
            
            ["RestaurantId":strRestId as String,
            "CouponCode":"10OFF" as String]
        
        print(param1)
        
        
        strWSURL=GETWSURL()
        
        
        
        Alamofire.request(strWSURL as String + "GetCouponCode?",method: .post, parameters: param1,  encoding: JSONEncoding.default, headers: nil).responseJSON
            {
                
                response in switch response.result
            
            
            {
                    
                    
                case .success(let JSON):
                    
                    print("Success with JSON: \(JSON)")
                    
                    let response = JSON as! NSDictionary
                    
                    self.stopActivityAnimating()
                    
                    if ((response.value(forKey: "Success") as? String) == "1")
                    {
                        
                        let arrData:NSArray =  (response.value(forKey: "Data") as? NSArray)!
                        self.arrCouponData = arrData.object(at: 0) as! NSDictionary
                        print ("arrCouponData: \(self.arrCouponData) ")

                    }
                    else
                    {
                        
                    }
                    
                    
                case .failure(let error):
                    self.stopActivityAnimating()

                    print("Request failed with error: \(error)")
                }
        }
        
       
    }
    
    
    


    
    
}


