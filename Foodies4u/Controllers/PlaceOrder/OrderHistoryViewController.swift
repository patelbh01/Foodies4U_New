//
//  OrderHistoryViewController.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 03/11/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//
//GetOrderHistoryByCustomerId		http://qafoodies4you.intransure.com:86/Api/Foodies/GetOrderHistoryByCustomerId	{"CustomerId":"9139"}

import Foundation
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON
import MapKit


class OrderHistoryViewController: parentViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, NVActivityIndicatorViewable, FloatRatingViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UICollectionViewDelegate
{
    
    var dictRestaurantDetails: NSDictionary!
    var dialog = ZAlertView()
    var cell = Customcell()
    var arrRatingList = NSMutableArray()
    var arrDeatilRating:NSArray!
    var arrFinalRating = NSMutableArray()
    
    var dictResult :NSDictionary!
    var strWSURL: NSString!
    var strUSERID: AnyObject!
    
    
    
    var strUserComment :String!
    var strFoodItemName :String!
    var arrSelectedReceipe:NSMutableArray = []

    var overallRating: Float!
    
    @IBOutlet weak var tblOrderDeatil: UITableView!
    @IBOutlet weak var tblRateFoodItem: UITableView!
    @IBOutlet weak var currReview: FloatRatingView!
    
    @IBOutlet weak var ViewRateFoodItem: UIView!
    @IBOutlet weak var btnRateFoodItemName: UIButton!
    
    @IBOutlet weak var lblDateandTime: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblreview: UILabel!
    
    @IBOutlet weak var lblRestName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var arrFoodItemDetail:NSMutableArray!
    var FoodItemdetail:FoodItemInfo!
    var arrMenuId:NSMutableArray! = []
    var arrBadge:NSMutableArray! = []
    var arrQty:NSMutableArray! = []
    var arrPrice:NSMutableArray! = []
    var arrCouponData:  NSDictionary!
    
    
    var strRestId:String!
    var strRestName:String!
    var strReviewId:String!
    
    var strMenuId:String!
    var strQty:String!
    var strprice:String!
    var strDiscountPrice : String! = "0.0"
    
    
    var strDiscount:String!
    var strSubTotal:String!
    var strSubTotalIncTax:String!
    var strSubTotalIncDiscount:String! = "0.0"
    var strOrderTotal:String!
    
    var strComment: AnyObject!
    var arrData: NSMutableDictionary!
    var dictFinalMenu:NSMutableDictionary!
    var arrFinalMenu:NSMutableArray!
    
    var Tag : Int!
    var selRow : Int!
    var TotalPrice : Double!
    var DiscountPrice : Double!
    
    var ISDISOUNT : Bool! = false
    
    var ISCOPOUNAPPLIED : Bool! = false
    
    var Ccell:CollectionViewCell!
    let arrDefaultImage: NSMutableArray = ["Call","Location-1","Favourite-Icon-White","Share"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Title and back button
        
        
        collectionView.isUserInteractionEnabled = true
        
        collectionView.allowsSelection = true
        
        arrSelectedReceipe.removeAllObjects()

        
        simplenavigationBarUI()
        self.navigationItem.title = "Order History"
        
        
        let img = UIImage(named: "BackArrow")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(btngoBack(sender:)))
        
        
        //keyboard appearence notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        
        arrFinalMenu = self.arrData.value(forKey: "OrderItems") as! NSMutableArray!
        
        print("arrFinalMenu" ,self.arrFinalMenu)
        
        self.tblOrderDeatil.isHidden =  false
        self.tblOrderDeatil.dataSource = self
        self.tblOrderDeatil.delegate = self
        
        //tableview border
        tblOrderDeatil!.addTopBorderWithColor(color: borderColor, width: borderWidth)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        //get userid value
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!
        
        //dismis keyboard any where you tap on the screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        currReview.rating = Float((self.arrData.value(forKey: "RestaurantRating")  as AnyObject) as! NSNumber)
        
        
        
        lblRating.text = String(describing:(self.arrData.value(forKey: "RestaurantRating")  as AnyObject) as! NSNumber)
        lblreview.text = String(describing:(self.arrData.value(forKey: "TotalReview")  as AnyObject) as! Int) + " Reviews"
        lblRestName.text = (self.arrData.value(forKey: "RestaurantName")  as AnyObject) as! String
        
        
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Get rating data
    
    
    func WSgetFoodItemReview()
    {
        getFoodItemReview() { responseObject, error in
            if (responseObject != nil)
            {
                let json = JSON(responseObject!)
                if let string = json.rawString()
                {
                    //Do something you want
                    //print(string)
                    self.stopActivityAnimating()
                    
                    if let data = string.data(using: String.Encoding.utf8) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            print("json ",json)
                            
                            let successVal:String = json!["Success"] as! String
                            if (successVal == "1") {
                                
                                
                                self.arrRatingList = json!["Data"] as! NSMutableArray
                                
                                
                                let arrTemp = self.arrRatingList.value(forKey: "ExperienceRatingData") as! NSArray
                                
                                let arrTempComment = self.arrRatingList.value(forKey: "Comment") as! NSArray
                                
                                self.strUserComment = arrTempComment.object(at: 0 ) as! String
                                
                                
                                let arrReviewId = self.arrRatingList.value(forKey: "Id") as! NSArray
                                
                                self.strReviewId = String(describing: arrReviewId.object(at: 0 ) as! NSNumber)
                                
                                self.arrDeatilRating = arrTemp.object(at: 0 ) as! NSArray
                                
                                let arrTempRating = self.arrRatingList.value(forKey: "Rating") as! NSArray
                                
                                self.overallRating = arrTempRating.object(at: 0 ) as! Float
                                
                                self.Tag = 0
                                
                                self.ViewRateFoodItem.isHidden =  false
                                self.tblRateFoodItem.dataSource = self
                                self.tblRateFoodItem.delegate = self
                                self.tblRateFoodItem.reloadData()
                                
                                
                                
                            }else {
                                
                                
                                
                                let dialog = ZAlertView(title: "Foodies4u",
                                                        message:  "No data found",
                                                        closeButtonText: "Ok",
                                                        closeButtonHandler: { (alertView) -> () in
                                                            
                                                            alertView.dismissAlertView()
                                }
                                )
                                dialog.show()
                                dialog.allowTouchOutsideToDismiss = true
                            }
                            
                            
                            
                        } catch {
                            print("Something went wrong")
                        }
                    }
                    
                }
            }
            else
            {
                self.stopActivityAnimating()
                self.AlertMethod()
            }
        }
    }
    
    func getFoodItemReview(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        //let size = CGSize(width: 80, height:80)
        
        //startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        strWSURL=GETWSURL()
        
        let param1 : [ String : AnyObject] = [
            "RestaurantId":strRestId as AnyObject,
            "MenuId":strMenuId as AnyObject,
            "CustomerId":strUSERID as AnyObject
        ]
        
        makeCall(section: strURL as String + "GetMenuReviewByCustomerId?",param:param1,completionHandler: completionHandler)
        
    }
    
    //MARK:- ======DELEGATE METHOD======
    //MARK: Table delegate and data source
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if(tableView ==  tblRateFoodItem)
        {
            
            if(Tag == 3)
        {
            
            
            return arrFinalMenu.count + 1 
        }
        else
        {
            
            return self.arrDeatilRating.count + 5
            }
            return 0
        }
        else
        {
            
            return arrFinalMenu.count + 7
        }
        
        return  7
        
    }
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        var cellHeight: CGFloat = 0
        
        
        if(tableView ==  tblRateFoodItem)
        {
            if(Tag == 3)
            {
                if (indexPath.row == arrFinalMenu.count)
                {
                    cellHeight = 60
                }
                else
                {
                   cellHeight = 40
                }
                
            }
            else
            {
                
                
                
                if (indexPath.row == 0) //Rating header
                {
                    if (arrDeatilRating.count == 0)
                    {
                        cellHeight = 0
                    }
                    else
                    {
                        cellHeight = 30
                    }
                }
                else if (indexPath.row < arrDeatilRating.count+1) //Rating rows
                {
                    cellHeight = 30
                }
                else if (indexPath.row == arrDeatilRating.count+1) //Comment row
                {
                    cellHeight = 73
                }
                    
                else  if (indexPath.row == arrDeatilRating.count + 3) //Submit
                {
                    cellHeight = 62
                }
                else  if (indexPath.row == arrDeatilRating.count + 4) //Submit
                {
                    cellHeight = 62
                }
            }
            return cellHeight
            
            
        }
        else
        {
            
            if (indexPath.row == 0) //Restaurant name & current rating, reviews
            {
                cellHeight = 63
            }
            else if (indexPath.row == 1) //Message
            {
                cellHeight = 30
            }
            else if (indexPath.row == 2) //Rating header
            {
                
                cellHeight = 44
                
            }
            else if (indexPath.row == arrFinalMenu.count + 7) //Rating header
            {
                
                cellHeight = 100
                
            }
                
                
            else
            {
                cellHeight = 44
            }
            
            return cellHeight
            
        }
        return 7
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if(tableView ==  tblRateFoodItem)
        {
            
            if(Tag == 3)
            {
                if(indexPath.row < arrFinalMenu.count)
                {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReuestReceipe") as! Customcell
                
                
                cell.lblFoodItemName.text = (arrFinalMenu.value(forKey: "MenuName") as AnyObject).object(at: (indexPath as NSIndexPath).row ) as? String
                
                
                cell.lblFoodPrice.text = "$ " + String(describing:(arrFinalMenu.value(forKey: "TotalPrice")  as AnyObject).object(at: (indexPath as NSIndexPath).row) as! Int)
                    
                    
                    if (arrSelectedReceipe.contains(String(describing: (self.arrFinalMenu.object(at: (indexPath as NSIndexPath).row) as AnyObject) .value(forKey: "MenuId") as! Int)))
                    {
                        
                        cell.btnCheckBox.setImage(UIImage(named: "CheckBox-1.png"), for: UIControlState())
                        
                    }
                    else
                    {
                        cell.btnCheckBox.setImage(UIImage(named: "CheckBox-2.png"), for: UIControlState())
                    }
                   
                

                    
                cell.btnSelRow.addTarget(self, action: #selector(self.ValueSelectedtbl(_:)), for: UIControlEvents.touchUpInside)
                cell.btnSelRow.tag = indexPath.row

                    return cell

                }

                else if (indexPath.row == arrFinalMenu.count ) //Review rows
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "submitCell") as! Customcell
                    cell.btnSubmitReview.setTitle("Request Receipe", for: .normal)
                    cell.btnSubmitReview.addTarget(self, action: #selector(btnSubmitReview(sender:)), for: UIControlEvents.touchUpInside)
                    return cell

                }
                
            }
            else
            {
                
                
                if (indexPath.row == 0) //Review Header
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "rateHeader") as! Customcell
                    
                    
                    return cell
                }
                else if (indexPath.row < arrDeatilRating.count + 1) //Review rows
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell") as! Customcell
                    
                    let RateVal = ((arrDeatilRating.value(forKey: "Rating") as AnyObject).object(at: (indexPath as NSIndexPath).row - 1) as? Int)!
                    // print("strRateVal",RateVal)
                    
                    cell.lblReviewName.text = (arrDeatilRating.value(forKey: "ExperienceData") as AnyObject).object(at: (indexPath as NSIndexPath).row - 1) as? String
                    cell.vwContainerOfRateButtons.tag = indexPath.row - 1
                    
                    //add target to radio button
                    for i in 0..<5
                    {
                        let btnTemp = cell.viewWithTag(i+101) as! UIButton
                        btnTemp.addTarget(self, action: #selector(btnRateNowFoodItem(sender:)), for: UIControlEvents.touchUpInside)
                        selRow =  (indexPath as NSIndexPath).row
                        
                        if ((i+1) == RateVal)
                        {
                            btnTemp.isSelected = true
                        }
                    }
                    
                    return cell
                }
                else if (indexPath.row == arrDeatilRating.count + 1) //Comment Row
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell") as! Customcell
                    cell.txtReviewComments.delegate = self
                    
                    if (strUserComment == nil)
                    {
                        cell.txtReviewComments.text = ""
                    }
                    else
                    {
                        cell.txtReviewComments.text = strUserComment
                    }
                    
                    return cell
                }
                else if (indexPath.row == arrDeatilRating.count + 3) //Overall review Row
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "totalReviewCell") as! Customcell
                    cell.currReview.delegate = self
                    cell.currReview.halfRatings = true
                    
                    
                    if (overallRating == nil)
                    {
                        cell.currReview.rating = 0
                    }
                    else
                    {
                        cell.currReview.rating = overallRating
                    }
                    
                    return cell
                }
                else if (indexPath.row == arrDeatilRating.count + 4)
                    //Submit review Row
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "submitCell") as! Customcell
                    cell.btnSubmitReview.addTarget(self, action: #selector(btnSubmitReview(sender:)), for: UIControlEvents.touchUpInside)
                    return cell
                    
                }
            }
            
            
        }
        else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderD&TCell") as! Customcell
            
            if(indexPath.row % 2 == 0)
            {
                cell.backgroundColor = #colorLiteral(red: 0.9759238362, green: 0.9766622186, blue: 0.9760381579, alpha: 1)
            }
            else
                
            {
                cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                
            }
            
            if (indexPath.row == 0) // order Date and time
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OrderD&TCell") as! Customcell
                
                //sets restaurant name
                
                
                let formatter = DateFormatter()
                let date = NSDate()
                formatter.dateFormat = "yyyy-MM-dd HH:mm a"
                let dateString = formatter.string(from:(date as? Date)!)
                
                
                cell.lblDateandTime.text = String(describing:(self.arrData.value(forKey: "OrderDate")  as AnyObject) ) + "  " +  String(describing:(self.arrData.value(forKey: "OrderTime")  as AnyObject) )
                
                cell.lblOrderID.text = String(describing:(self.arrData.value(forKey: "Id")  as AnyObject) )
                
                cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
                cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
                
                
                
                return cell
            }
                
            else if (indexPath.row == 1) //Order Header
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "rateHeader") as! Customcell
                cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
                cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
                return cell
            }
                
            else if ( indexPath.row == arrFinalMenu.count  || indexPath.row == arrFinalMenu.count + 1 ) //Order Detail
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDeatilCell") as! Customcell
                
                
                
                cell.lblFoodItemName.text = (arrFinalMenu.value(forKey: "MenuName") as AnyObject).object(at: (indexPath as NSIndexPath).row - 2) as! String
                
                cell.lblFoodQty.text = String(describing:(arrFinalMenu.value(forKey: "Quantity")  as AnyObject).object(at: (indexPath as NSIndexPath).row - 2) as! Int)
                
                
                cell.btnRateFoodItem.addTarget(self, action: #selector(btnRateFoodItemPressed),  for: .touchUpInside)
                cell.btnRateFoodItem.tag = (indexPath as NSIndexPath).row
                
                
                /*if(String(describing:(arrFinalMenu.value(forKey: "OrderDiscount")  as AnyObject).object(at: (indexPath as NSIndexPath).row - 2)  as? Int).characters.count == 0)
                 
                 {*/
                cell.lblFoodPrice.text = "$ " + String(describing:(arrFinalMenu.value(forKey: "TotalPrice")  as AnyObject).object(at: (indexPath as NSIndexPath).row - 2) as! Int)
                
                /*}
                 else
                 {*/
                
                /*let attributeString: NSMutableAttributedString = NSMutableAttributedString(string:  "$" +  String(myIntValue * Qty) )
                 
                 attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length) )
                 attributeString.addAttribute(NSForegroundColorAttributeName, value:UIColor.init(colorLiteralRed: 92/255.0, green: 94/255.0, blue: 102/255.0, alpha: 1.0), range: NSMakeRange(0, attributeString.length))
                 // attributeString.addAttribute([NSFontAttributeName : UIFont (name: "OpenSans-Bold", size: 9)])
                 let attributeString1: NSMutableAttributedString = NSMutableAttributedString(string: FoodItemdetail.strTotalPrice + "  " )
                 
                 
                 let combination = NSMutableAttributedString()
                 
                 combination.append(attributeString1)
                 combination.append(attributeString)
                 
                 cell.lblFoodPrice.attributedText = combination
                 
                 
                 
                 cell.lblDiscount.text = FoodItemdetail.strFoodDiscount*/
                
                
                //}
                
                
                
                
                
                cell.contentView.addLeftBorderWithColor(color: borderColor, width: borderWidth)
                cell.contentView.addRightBorderWithColor(color: borderColor, width: borderWidth)
                
                
                
                
                
                
                return cell
            }
                
                
            else if (indexPath.row == arrFinalMenu.count + 2) //Review rows
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TotalCell") as! Customcell
                
                
                cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
                cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
                cell.addBottomBorderWithColor(color: borderColor, width: borderWidth)
                
                cell.lblGrandTotalAmount.text = "TOTAL AMOUNT"
                
                
                cell.lblTotalAmount.text = "$ " + String(describing:(self.arrData.value(forKey: "OrderSubTotal")  as AnyObject) as! Int)
                
                
                return cell
            }
            else if (indexPath.row == arrFinalMenu.count + 3) //Comment Row
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TaxCell") as! Customcell
                
                cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
                cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
                
                let strTax:Double = Double(((self.arrData.value(forKey: "OrderSubTotal")  as AnyObject) as! Int))
                
                cell.lblTax.text =  "$ " + String(  strTax * 7.00/100)
                
                
                strSubTotalIncTax = String( strTax + strTax * 7.00/100)
                
                
                
                return cell
            }
            else if (indexPath.row == arrFinalMenu.count + 4) //Overall review Row
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountCell") as! Customcell
                
                cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
                cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
                
                
                cell.lblDiscount.text =  "$ 0.0 "
                
                
                return cell
            }
            else  if (indexPath.row == arrFinalMenu.count + 5) //Submit review Row
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TotalCell") as! Customcell
                
                
                cell.addLeftBorderWithColor(color: borderColor, width: borderWidth)
                cell.addRightBorderWithColor(color: borderColor, width: borderWidth)
                cell.addBottomBorderWithColor(color: borderColor, width: borderWidth)
                
                cell.lblGrandTotalAmount.text = "GRAND TOTAL"
                
                
                
                
                
                if(ISDISOUNT ==  true)
                {
                    
                    /* let strTax:Double = Double(strprice)!
                     
                     
                     if(strDiscountPrice.contains(" %"))
                     {
                     
                     cell.lblTotalAmount.text =  "$ " + String( (strTax + strTax * 7.00/100) * DiscountPrice
                     )
                     
                     TotalPrice = strTax + strTax * 7.00/100
                     }
                     else
                     {
                     cell.lblTotalAmount.text =  "$ " + String( strTax + strTax * 7.00/100 - DiscountPrice
                     )
                     
                     TotalPrice = strTax + strTax * 7.00/100
                     }
                     
                     strSubTotalIncDiscount = String(TotalPrice)
                     
                     
                     strOrderTotal = strSubTotalIncDiscount
                     
                     */
                    
                }
                else
                {
                    cell.lblTotalAmount.text =  "$ " + String(describing:(self.arrData.value(forKey: "OrderTotal")  as AnyObject) as! Int)
                    
                    /*TotalPrice = strTax + strTax * 7.00/100
                     
                     strOrderTotal = String(TotalPrice)*/
                    
                }
                
                
                
                
                
                return cell
            }
                
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceOrderCell") as! Customcell
                cell.btnRequestReceipe.addTarget(self, action: #selector(self.btnRequestRecipePressed(_:)), for: UIControlEvents.touchUpInside)
                
                cell.btnReseverAgain.addTarget(self, action: #selector(self.btnReserveAgainPressed(_:)), for: UIControlEvents.touchUpInside)
                
                cell.btnWriteReview.addTarget(self, action: #selector(self.btnWriteReviewPressed(_:)), for: UIControlEvents.touchUpInside)

 
                return cell
            }
            
            
            
        }
        return cell
        
    }
    
    
    
    
    //MARK: Rating delegate
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float)
    {
        overallRating = rating
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //MARK:- ======BUTTON ACTIONS======
    
    func ValueSelectedtbl(_ sender:UIButton)
        
    {
        
        
        
        let selectedButtonCell = sender.superview?.superview as! Customcell
        //Incase your button is inside cell.contentview
        // var selectedButtonCell = sender.superview.superview as! UICollectionviewCell
        let indexPath = tblRateFoodItem.indexPath(for: selectedButtonCell)
        
        
        let row = indexPath?.row
       
        print((String( describing: (self.arrFinalMenu.object(at: sender.tag) as AnyObject) .value(forKey: "MenuId") as! Int)))
        
                if(arrSelectedReceipe.contains(String( describing: (self.arrFinalMenu.object(at: sender.tag) as AnyObject) .value(forKey: "MenuId") as! Int)))
                    
                {
                    arrSelectedReceipe.remove(String( describing: (self.arrFinalMenu.object(at: sender.tag) as AnyObject) .value(forKey: "MenuId") as! Int))
                }
                else
                    
                {
                    arrSelectedReceipe.add(String(describing: (self.arrFinalMenu.object(at: sender.tag) as AnyObject) .value(forKey: "MenuId") as! Int))
                    
                }
                
                
        
            
            /* let indexPath1 = IndexPath(row: row!, section: 0)
             MyRestcells = self.tblTimingDetails!.cellForRow(at: indexPath1) as! MyResListCell
             MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())*/
            
            // arr[row!]  = 1
        
            print(arrSelectedReceipe)

            self.tblRateFoodItem!.reloadData()
            
            
            
       
        
        
        
        
    }
    
    @IBAction func btnRateNowFoodItem(sender: UIButton)
    {
        let vwTemp = sender.superview
        unselectReviewbuttons(buttonContainer: vwTemp!)
        sender.isSelected = true
        let mutableArray = NSMutableArray(array:arrDeatilRating)
        let dicTemp:NSMutableDictionary =    mutableArray.object(at: (vwTemp?.tag)!) as! NSMutableDictionary
        dicTemp["Rating"] = Int(sender.tag - 100)
        mutableArray.replaceObject(at: (vwTemp?.tag)!, with: dicTemp)
        
        
        arrDeatilRating = NSMutableArray(array:mutableArray)
        
        //print("rate list after edititng arrDeatilRating: \(arrDeatilRating)")
        
        
        
        
        
    }
    @IBAction func btnSubmitReview(sender: UIButton)
    {
        
        view.endEditing(true)
        //Progress bar
        let size = CGSize(width: 80, height:80)
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        arrFinalRating = NSMutableArray()
        
        
        for i in 0..<arrDeatilRating.count
        {
            let dictTemp = arrDeatilRating[i] as! NSDictionary
            let dictRateToPass = NSMutableDictionary()
            
            dictRateToPass.setValue(dictTemp["RatingID"], forKey: "Id")
            dictRateToPass.setValue(dictTemp["Rating"], forKey: "Rating")
            
            arrFinalRating.add(dictRateToPass)
        }
        
        strWSURL=GETWSURL()
        
        let arrToPass = arrFinalRating as NSArray
        
        if(overallRating == nil)
            
        {
            overallRating = 0.0
        }
        
        let param1 : [ String : AnyObject] = [
            "ReviewId":strReviewId as AnyObject,
            "RestaurantId":strRestId as AnyObject,
            "Comment":strUserComment as AnyObject,
            "CustomerId":strUSERID,
            "MenuId":strMenuId as AnyObject,
            "Rating":overallRating as AnyObject,
            "ExperienceRating":arrToPass
        ]
        
        
        
        Alamofire.request(strURL as String + "ReviewMenu?", method: .post, parameters: param1, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            
            print ("submit response: \(response) ")
            
            if (response.result.isSuccess)
            {
                self.stopActivityAnimating()
                
                self.dialog = ZAlertView(title: "Foodies4u", message: "submitted successfully",closeButtonText: "Ok",
                                         closeButtonHandler: { (alertView) -> () in
                                            alertView.dismissAlertView()
                                            self.ViewRateFoodItem.isHidden = true
                                            self.arrRatingList.removeAllObjects()
                                            self.arrDeatilRating = nil
                                            
                })
                self.dialog.show()
            }
            else
            {
                self.stopActivityAnimating()
                
                /*self.dialog = ZAlertView(title: "Foodies4u", message: "Sorry you have not .",closeButtonText: "Ok",
                 closeButtonHandler: { (alertView) -> () in
                 alertView.dismissAlertView()})
                 self.dialog.show()*/
            }
        }
        
        
    }
    
    func unselectReviewbuttons(buttonContainer: UIView)
    {
        for i in 0..<5
        {
            let btnTemp = buttonContainer.viewWithTag(i+101) as! UIButton
            btnTemp.isSelected = false
        }
        
    }
    
    
    @IBAction func btnRateFoodItemPressed(_ sender: UIButton)
    {
        self.btnRateFoodItemName.setTitle("SUBMIT REVIEW", for: .normal)

        
        
        view.endEditing(true)
        
        let indexPath1 = IndexPath(row: sender.tag - 1 , section: 0)
        print(indexPath1)
        
        var cell:Customcell = (self.tblOrderDeatil.cellForRow(at: indexPath1) as? Customcell)!
        
        
        strRestId = String(describing:  (arrFinalMenu.value(forKey: "RestaurantId") as AnyObject).object(at: sender.tag - 2) as! NSNumber)
        
        strMenuId =  String(describing: (arrFinalMenu.value(forKey: "MenuId") as AnyObject).object(at: sender.tag - 2 ) as! NSNumber)
        
        
        strFoodItemName = String(describing: (arrFinalMenu.value(forKey: "MenuName") as AnyObject).object(at: sender.tag - 2 ) as! String)
        
        self.WSgetFoodItemReview()
        
        
        
    }
    
    @IBAction func btnCloseViewAllClicked(_ sender: UIButton)
    {
        
        ViewRateFoodItem.isHidden = true
        
        
    }
    @IBAction func btnRestInfoClicked(_ sender: UIButton){
        
        print("Foodies4U")
        
        let strTelPhoneNumner = (self.arrData.value(forKey: "RestaurantPhoneNumber")  as AnyObject) as! String
        
        if(sender.tag == 0)
        {
            
            if let url = NSURL(string: "tel://+19298004787"), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(url as URL)
            }
        }
        if(sender.tag == 1)
        {
            
            /* let customURL = "comgooglemaps://"
             
             if UIApplication.sharedApplication().canOpenURL(NSURL(string: customURL)!) {
             
             UIApplication.sharedApplication().openURL(NSURL(string:
             "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!)
             
             } else
             {
             let alert = UIAlertController(title: "Error", message: "Google maps not installed", preferredStyle: UIAlertControllerStyle.Alert)
             let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
             alert.addAction(ok)
             self.presentViewController(alert, animated:true, completion: nil)*/
            //}
            
            let coordinate = CLLocationCoordinate2DMake(CLLocationDegrees(Double((self.arrData.value(forKey: "RestaurantLatitude")  as AnyObject) as! String)!), CLLocationDegrees(Double((self.arrData.value(forKey: "RestaurantLongitude")  as AnyObject) as! String)!))
            
            let placemark:MKPlacemark = MKPlacemark(coordinate: coordinate, addressDictionary:nil)
            
            let mapItem:MKMapItem = MKMapItem(placemark: placemark)
            
            mapItem.name = "Target location"
            
            let launchOptions:NSDictionary = NSDictionary(object: MKLaunchOptionsDirectionsModeDriving, forKey: MKLaunchOptionsDirectionsModeKey as NSCopying)
            
            let currentLocationMapItem:MKMapItem = MKMapItem.forCurrentLocation()
            
            MKMapItem.openMaps(with: [currentLocationMapItem, mapItem], launchOptions: launchOptions as? [String : AnyObject])
        }
        
    }
    @IBAction func btnRequestRecipePressed(_ sender: UIButton)
    {
        
        Tag = sender.tag
        print("tag",Tag)
        tblRateFoodItem.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.btnRateFoodItemName.setTitle("REQUEST RECEIPE", for: .normal)
        self.ViewRateFoodItem.isHidden = false
        self.tblRateFoodItem.dataSource =  self
        self.tblRateFoodItem.delegate = self
        
        var height: CGFloat = 70
        
        height *= CGFloat(arrFinalMenu.count as Int)
        var tableFrame = self.tblRateFoodItem!.frame
        tableFrame.size.height = height
        self.tblRateFoodItem!.frame = tableFrame
        
        
        self.tblRateFoodItem.reloadData()
        
        
        
    }
    @IBAction func btnReserveAgainPressed(_ sender: UIButton)
    {
        
        let HomeViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        self.navigationController?.pushViewController(HomeViewControllerObj!, animated: false)
        
        
    }
    @IBAction func btnWriteReviewPressed(_ sender: UIButton)
    {
        
        let HomeViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ReviewRestaurantViewController") as? ReviewRestaurantViewController
        self.navigationController?.pushViewController(HomeViewControllerObj!, animated: false)
        
        
    }
    
    @IBAction func btnPlaceOrderPressed(_ sender: UIButton)
        
    {
        //webservice to add preferences
        
        if InternetConnection.isConnectedToNetwork()
        {
            
            
            
            //add food type
            if(arrFoodItemDetail.count > 0 )
            {
                
                
                let size = CGSize(width: 80, height:80)
                
                startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
                
                strWSURL=GETWSURL()
                
                arrFinalMenu = NSMutableArray()
                
                for i in 0...arrFoodItemDetail.count - 1
                {
                    
                    
                    
                    
                    FoodItemdetail = arrFoodItemDetail.object(at: i ) as! FoodItemInfo
                    
                    
                    self.dictFinalMenu =
                        [ "MenuId" : FoodItemdetail.strMenuId, "Quantity":FoodItemdetail.streQuantity ,"UnitPrice":FoodItemdetail.strUnitPrice ,"Comment":FoodItemdetail.strComment, "TotalPrice":FoodItemdetail.strTotalPrice]
                    
                    
                    //  arrFinalMenu.add(self.dictFinalMenu)
                    
                    
                }
                
                //  print(arrFinalMenu)
                
                let formatter = DateFormatter()
                let date = NSDate()
                formatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
                let dateString = formatter.string(from:(date as? Date)!)
                
                let param1 : [ String : AnyObject] = [
                    "RestaurantId":FoodItemdetail.strRestId as AnyObject,
                    "CustomerId":strUSERID,
                    "OrderDate":dateString as AnyObject,
                    "BillingAddressId":0 as AnyObject,
                    "DeliveryAddressId":0 as AnyObject,
                    "ShippingAddressId":0 as AnyObject,
                    "OrderTypeId":1 as AnyObject,
                    "OrderSubTotal":strprice as AnyObject,
                    "OrderTax":7.00 as AnyObject,
                    "OrderSubtotalInclTax":strSubTotalIncTax as AnyObject,
                    "OrderDiscount":strDiscountPrice as AnyObject,
                    "OrderSubTotalInclDiscount":strSubTotalIncDiscount as AnyObject,
                    "OrderTotal":strOrderTotal as AnyObject,
                    "MenuItems":arrFinalMenu as AnyObject
                ]
                
                // print(param1)
                
                Alamofire.request(strWSURL as String + "PlaceOrder?",method: .post, parameters: param1,  encoding: JSONEncoding.default, headers: nil).responseJSON
                    { response in switch response.result {
                        
                        
                    case .success(let JSON):
                        
                        print("Success with JSON: \(JSON)")
                        
                        let response = JSON as! NSDictionary
                        
                        self.stopActivityAnimating()
                        
                        if ((response.value(forKey: "Success") as? String) == "1")
                        {
                            
                            let arr:NSDictionary = response.value(forKey: "Data") as! NSDictionary
                            
                            
                            
                            
                            
                            self.dialog = ZAlertView(title: "Foodies4u", message: "Order placed successfully.",closeButtonText: "Ok",
                                                     closeButtonHandler: { (alertView) -> () in
                                                        alertView.dismissAlertView()
                                                        let HomeViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                                                        self.navigationController?.pushViewController(HomeViewControllerObj!, animated: false)
                                                        
                                                        UserDefaults.standard.set("", forKey: "ORDERID")
                                                        
                                                        
                            }
                                
                                
                                
                            )
                            self.dialog.show()
                            
                            
                            
                        }
                        else
                        {
                            
                            let arr:NSDictionary = (response.value(forKey: "Data") as? NSDictionary)!
                            self.dialog = ZAlertView(title: "Foodies4u", message: arr.value(forKey: "status") as? String,
                                                     closeButtonText: "Ok",
                                                     closeButtonHandler: { (alertView) -> () in
                                                        alertView.dismissAlertView()})
                            self.dialog.show()
                        }
                        
                        
                    case .failure(let error):
                        self.stopActivityAnimating()
                        
                        print("Request failed with error: \(error)")
                        }
                }
                
            }
            else
            {
                self.dialog = ZAlertView(title: "Foodies4u", message: "Your cart is empty. Please insert Food Item in your cart",closeButtonText: "Ok",
                                         closeButtonHandler: { (alertView) -> () in
                                            alertView.dismissAlertView()
                                            
                                            
                                            
                }
                    
                    
                    
                )
                self.dialog.show()
            }
        }
    }
    
    
    
    
    
    @IBAction func btngoBack(sender: UIButton)
    {
        
        
        _ = navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: Textview delegate
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        
        
        animateViewMoving(true, moveValue: 150)
        
    }
    internal func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text.characters.count != 0)
        {
            strUserComment = textView.text
        }
        else
        {
            if let strComment = self.dictResult["Comment"]
            {
                strUserComment = strComment as! String
            }
            tblRateFoodItem.reloadData()
        }
        
        animateViewMoving(false, moveValue: 150)
        
    }
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        strUserComment = textView.text + text
        
        if (text=="\n")
        {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    
    //MARK: - COLLECTIONVIEW DATASOURCE AND DELEGATES METHODS
    
    //for sorting at bottom of restaurant list view
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDefaultImage.count
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        Ccell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        
        Ccell.btnSelCell.addTarget(self, action: #selector(self.btnRestInfoClicked), for: UIControlEvents.touchUpInside)
        Ccell.btnSelCell.tag = (indexPath as NSIndexPath).row
        
        
        
        if (indexPath as NSIndexPath).row == 2
        {
            
            if(((self.arrData.value(forKey: "FavRestaurant")  as AnyObject) as! Bool) == false)
            {
                Ccell.itemImageView.image = UIImage(named:"Favourite-Icon-Red.png")
                
            }
            else
            {
                let strImg = arrDefaultImage[(indexPath as NSIndexPath).row]
                // sorting image
                Ccell.itemImageView.image = UIImage(named:strImg as! String)
            }
        }
        else
        {
            let strImg = arrDefaultImage[(indexPath as NSIndexPath).row]
            // sorting image
            Ccell.itemImageView.image = UIImage(named:strImg as! String)
            
        }
        // sorting name
        
        Ccell.addRightBorderWithColor(color: borderColor, width: borderWidth)
        
        return Ccell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        let numberOfCell: CGFloat = 4
        let cellWidth = UIScreen.main.bounds.size.width / numberOfCell
        let cellHeight = UIScreen.main.bounds.size.height / 9
        //print(cellHeight)
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        Ccell = collectionView.cellForItem(at: indexPath) as! CollectionViewCell
        
        
    }
    //MARK:- ======USER DEFINE METHODS======
    
    //MARK: Keyboard hide and unhide methods
    func keyboardWillShow (notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
            
            tblOrderDeatil.contentInset = contentInsets
            tblOrderDeatil.scrollIndicatorInsets = contentInsets
        }
    }
    
    
    
    func keyboardWillHide(notification: NSNotification)
    {
        tblOrderDeatil.contentInset = UIEdgeInsets.zero
        tblOrderDeatil.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    
}


