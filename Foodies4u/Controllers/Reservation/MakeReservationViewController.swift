 //
 //  MakeReservationViewController.swift
 //  Foodies4u
 //
 //  Created by Bhumika Patel on 25/08/16.
 //  Copyright © 2016 Bhavi Mistry. All rights reserved.
 //
 
 import Foundation
 import MapKit
 import Alamofire
 import NVActivityIndicatorView
 import SwiftyJSON
 
 class MakeReservationViewController: parentViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, NVActivityIndicatorViewable, UITextViewDelegate
 {
    // MARK: - VIEWLYFECYCLE METODS
    
    var RCell:MyResListCell!
    @IBOutlet var tblReservation:UITableView!
    
    //for dateandtimepicker
    
    let strDateSelected = ""
    var strDatePickerOpened = "no"
    var picker : UIDatePicker!
    var toolbar : UIToolbar!
    var doneButton : UIButton!
    var datePicker = MIDatePicker.getFromNib()
    var dateFormatter = DateFormatter()
    
    
    
    var strWSURL: NSString!
    var strUserID: NSString!
    
    
    //strings for stoeing the value
    
    var strName: NSString! = ""
    var strAdult: NSString! = ""
    var strChildren: NSString! = ""
    var strEmail: NSString! = ""
    var strTextrequest: NSString! = ""
    var strDate: NSString! = ""
    var strTime: NSString! = ""
    
    var strUSERID: String!
    var strMNumber: String!
    var strRestId: String!
    var strRestName: String!
    
    var dialog = ZAlertView()
    
    
    //for make reservation
    let arrList = ["", "Name", "","Adults","Email","Mobile","","Data & Time","","",""]
    let arrIconList = ["","ProfileIcon.png","" ,"AdultIcon", "EmailIcon", "MobileIcon","" ,"CalendarIcon",""]
    let arrTitle = ["On Broadway","","No Of Guests","","","", "Reservation Date & Time","", "Special Request",""]
    
    var strUserEmail: NSString! = ""
    var strUserName: NSString! = ""
    
    
    override func viewDidLoad()
    {
        self.navigationController?.navigationBar.isHidden=false
        simplenavigationBarUI()
        self.navigationItem.title = "Reservation"
        let img = UIImage(named: "BackArrow")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(BackTapped))
        
        //dismis keyboard any where you tap on the screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as! String
        strMNumber = UserDefaults.standard.value(forKey: "MNUMBER") as! String
        
        let dictUserData: NSDictionary = UserDefaults.standard.value(forKey: "FoodiesUserProfile") as! NSDictionary
        print(dictUserData)
        
        strUserEmail = dictUserData.value(forKey: "Email")! as? String as NSString!
        strUserName = dictUserData.value(forKey: "FirstName")! as? String as NSString! //)!// + " " + (dictUserData.valueForKey("LastName")! as! String)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden=true
    }
    // MARK: - BUTTONS METHODS
    
    func BackTapped()
    {
        self.navigationController!.popViewController(animated: false)
    }
    
    func handleTap() {
        
        self.view.endEditing(true)
        
        let lblToolbar : UILabel = UILabel(frame: CGRect.zero)
        picker = UIDatePicker()
        
        
        toolbar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.size.height-250, width: self.view.frame.size.width, height: 44))
        picker.frame = CGRect(x: 0, y: self.view.frame.size.height-210, width: self.view.frame.size.width, height: 230)
        
        toolbar.backgroundColor = UIColor.black
        toolbar.barTintColor = GlobalConstants.Theme_Color_Seperator
        lblToolbar.textColor = UIColor.white
        
        let itemDone: UIBarButtonItem!
        lblToolbar.textAlignment = NSTextAlignment.center
        
        lblToolbar.backgroundColor = UIColor.clear
        lblToolbar.font = UIFont.boldSystemFont(ofSize: 18.0)
        
        lblToolbar.text = "Select Date & Time"
        lblToolbar.sizeToFit()
        
        
        let btnTitle : UIBarButtonItem! = UIBarButtonItem(customView: lblToolbar)
        let flexibleSpace : UIBarButtonItem! = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        
        itemDone = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.done, target: self, action: #selector(self.doneButtonClicked(_:)))
        
        UIBarButtonItem.appearance().tintColor = UIColor.white
        
        var items = [UIBarButtonItem]()
        items = [flexibleSpace, btnTitle, flexibleSpace, itemDone]
        toolbar.setItems(items, animated: false)
        
        self.view.addSubview(toolbar)
        
        picker.autoresizingMask = UIViewAutoresizing.flexibleWidth;
        picker.datePickerMode = UIDatePickerMode.dateAndTime
        picker.minimumDate = Date()
        
        picker.addTarget(self, action: #selector(self.updateDatePickerValue(_:)), for: UIControlEvents.valueChanged)
        picker.backgroundColor = UIColor.white
        
        self.view.addSubview(picker)
        
    }
    
    @IBAction func doneButtonClicked (_ sender: UIButton){
        
        toolbar.removeFromSuperview()
        picker.removeFromSuperview()
        let dateFormatter : DateFormatter! = DateFormatter()
        //dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        
        //let strDate : String
        //var strDate: String = dateFormatter.stringFromDate(picker.date)
        
        print("date : \(dateFormatter.string(from: picker.date))")
        
        let indexPath = IndexPath(row: 7, section: 0)
        RCell = tblReservation.cellForRow(at: indexPath) as! MyResListCell
        RCell.txtfieldreservation.text = dateFormatter.string(from: picker.date)
        strDate = RCell.txtfieldreservation.text as NSString!
        print(strDate)
        
        strDatePickerOpened = "no"
        
    }
    
    @IBAction func updateDatePickerValue(_ sender: UIDatePicker) {
        
        let dateFormatter : DateFormatter! = DateFormatter()
        //dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        print("date = \(dateFormatter.string(from: picker.date))")
        
        
        let indexPath = IndexPath(row: 7, section: 0)
        RCell = tblReservation.cellForRow(at: indexPath) as! MyResListCell
        RCell.txtfieldreservation.text = dateFormatter.string(from: picker.date)
        strDate = RCell.txtfieldreservation.text as NSString!
        
    }
    // MARK: - TABLEVIEW DATASOURCE AND DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if ((indexPath as NSIndexPath).row == 0 || (indexPath as NSIndexPath).row == 2 || (indexPath as NSIndexPath).row == 6 || (indexPath as NSIndexPath).row == 8)
        {
            return 25.0
        }
        else if ((indexPath as NSIndexPath).row == 1 || (indexPath as NSIndexPath).row == 3 || (indexPath as NSIndexPath).row == 4 || (indexPath as NSIndexPath).row == 5 || (indexPath as NSIndexPath).row == 7)
        {
            return 60.0
            
        }
        else if ( (indexPath as NSIndexPath).row == 9)
        {
            return 100.0
            
        }
        return 50.0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if ((indexPath as NSIndexPath).row == 0 || (indexPath as NSIndexPath).row == 2 || (indexPath as NSIndexPath).row == 6 || (indexPath as NSIndexPath).row == 8)
        {
            RCell = tableView.dequeueReusableCell(withIdentifier: "RCellLabel", for: indexPath) as! MyResListCell
            
            RCell.selectionStyle = .none
            
            if ((indexPath as NSIndexPath).row == 0)
            {
                RCell.lblTitle.text = strRestName // as! String// + " On BoardWay"
            }
                
            else
            {
                RCell.lblTitle.text = arrTitle[((indexPath as NSIndexPath).row)]
                
            }
            
        }
        
        if ((indexPath as NSIndexPath).row == 1 || (indexPath as NSIndexPath).row == 3 || (indexPath as NSIndexPath).row == 4 || (indexPath as NSIndexPath).row == 5 || (indexPath as NSIndexPath).row == 7)
        {
            RCell = tableView.dequeueReusableCell(withIdentifier: "RCellfield", for: indexPath) as! MyResListCell
            
            if ((indexPath as NSIndexPath).row == 3)
            {
                
                RCell.txtfieldreservation.isHidden = true
                
                //for adult textfiled
                self.formatTextfieldWithoutBottomLine(RCell.txtfieldAdult)
                RCell.txtfieldAdult.keyboardType = .phonePad
                RCell.txtfieldAdult.attributedPlaceholder = NSAttributedString(string:"Adult", attributes:[NSForegroundColorAttributeName: UIColor.black])
                
                
                //RCell.txtfieldAdult.tag=indexPath.row
                RCell.txtfieldAdult.delegate=self
                RCell.selectionStyle = .none
                RCell.ImgViewicon.isHidden = false
                RCell.ImgMadentory.isHidden = false

                
                //for child textfield
                self.formatTextfieldWithoutBottomLine(RCell.txtfieldChildren)
                RCell.txtfieldChildren.keyboardType = .phonePad
                RCell.txtfieldChildren.attributedPlaceholder = NSAttributedString(string:"Children", attributes:[NSForegroundColorAttributeName: UIColor.black])
                
                //RCell.txtfieldChildren.tag=indexPath.row+1
                RCell.txtfieldChildren.delegate=self
                RCell.ImgChildren.isHidden = false
                
            }
            else
            {
                RCell.ImgChildren.isHidden = true
                RCell.txtfieldChildren.isHidden = true
                RCell.txtfieldAdult.isHidden = true
                RCell.ImgMadentory.isHidden = true

                
                self.formatTextfieldWithoutBottomLine(RCell.txtfieldreservation)
                RCell.ImgViewicon.image=UIImage(named:arrIconList[(indexPath as NSIndexPath).row])
                
                RCell.txtfieldreservation.tag=(indexPath as NSIndexPath).row
                RCell.txtfieldreservation.delegate=self
                RCell.selectionStyle = .none
                
                if arrList[(indexPath as NSIndexPath).row] == "Email" {
                    
                   // if strUserEmail.length > 0 {
                        
                        if (UserDefaults.standard.value(forKey: "USERNAME") != nil)
                        {
                            RCell.txtfieldreservation.text = UserDefaults.standard.value(forKey: "EMAILID") as? String
                            strEmail = UserDefaults.standard.value(forKey: "EMAILID") as? String as NSString!
                            
                            
                        }
                        else
                        {
                        RCell.txtfieldreservation.text = strUserEmail as String
                        }
                   // }
                    RCell.txtfieldreservation.keyboardType = .emailAddress
                }
                else if arrList[(indexPath as NSIndexPath).row] == "Name" {
                    
                   // if strUserName.length > 0 {
                        // display user name
                        
                        if (UserDefaults.standard.value(forKey: "USERNAME") != nil)
                        {
                            RCell.txtfieldreservation.text = UserDefaults.standard.value(forKey: "USERNAME") as? String
                            strName = UserDefaults.standard.value(forKey: "USERNAME") as? String as NSString!
                            
                            
                        }
                        else
                        {
                        
                              RCell.txtfieldreservation.text = strUserName as String
                       // }
                    }
                }
                RCell.txtfieldreservation.attributedPlaceholder = NSAttributedString(string:arrList[(indexPath as NSIndexPath).row],
                                                                                     attributes:[NSForegroundColorAttributeName: UIColor.black])
                if((indexPath as NSIndexPath).row == 5)
                {
                    RCell.txtfieldreservation.text = strMNumber as String
                    RCell.txtfieldreservation.isUserInteractionEnabled = false
                    RCell.selectionStyle = .none
                    RCell.txtfieldreservation.alpha=0.5
                    
                }
                
                if((indexPath as NSIndexPath).row == 7)
                {
                    let tap = UITapGestureRecognizer(target: self, action: #selector(MakeReservationViewController.handleTap))
                    tap.numberOfTapsRequired = 1
                    RCell.addGestureRecognizer(tap)
                    RCell.txtfieldreservation.isUserInteractionEnabled=false
                }
                
            }
        }
        
        
        if (indexPath as NSIndexPath).row == 9
        {
            RCell = tableView.dequeueReusableCell(withIdentifier: "RCellTextview", for: indexPath) as! MyResListCell
            
            RCell.txtRequest.layer.cornerRadius = 5
            RCell.txtRequest.layer.borderWidth = 1.0
            RCell.txtRequest.layer.borderColor = UIColor.lightGray.cgColor
            RCell.selectionStyle = .none
            
        }
        if (indexPath as NSIndexPath).row == 10
        {
            RCell = tableView.dequeueReusableCell(withIdentifier: "RCellbutton", for: indexPath) as! MyResListCell
            RCell.selectionStyle = .none
            
        }
        
        return RCell
    }
    
    //MARK: - TEXTVIEW DATASOURCE AND DELEGATES METHODS
    
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        
        toolbar.removeFromSuperview()
        picker.removeFromSuperview()
        animateViewMoving(true, moveValue: 220)
        print(textView.text);
        
    }
    func textViewDidEndEditing(_ textView: UITextView)
    {
        strTextrequest = textView.text as NSString!
        animateViewMoving(false, moveValue: 220)
        
    }
    
    //MARK: - TEXTFIELD DATASOURCE AND DELEGATES METHODS
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
        //toolbar.removeFromSuperview()
        //picker.removeFromSuperview()
        
        if textField.tag == 1 || textField.tag == 3 || textField.tag == 34
        {
            //            if(textField.tag == 1)
            //            {
            //                strName = textField.text
            //            }
            //            if(textField.tag == 3)
            //            {
            //                strAdult = textField.text
            //            }
        }
        else
        {
            animateViewMoving(true, moveValue: 180)
            
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if(textField.tag == 1)
        {
            strName = textField.text as NSString!
        }
        else if(textField.tag == 3)
        {
            strAdult = textField.text as NSString!
        }
        else if(textField.tag == 34)
        {
            strChildren = textField.text as NSString!
        }
        else if(textField.tag == 4)
        {
            strEmail = textField.text as NSString!
            animateViewMoving(false, moveValue: 180)
        }
        else
        {
            
            animateViewMoving(false, moveValue: 180)
        }
    }
    
    override func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        if(textField.tag == 1)
        {
            strName = textField.text as NSString!
        }
        else if(textField.tag == 3)
        {
            strAdult = textField.text as NSString!
        }
        else if(textField.tag == 34)
        {
            strChildren = textField.text as NSString!
        }
        else if(textField.tag == 4)
        {
            strEmail = textField.text as NSString!
        }
        else {
            textField.resignFirstResponder()
            return true
        }
        textField.resignFirstResponder()
        return true
        //        let nextTage=textField.tag+1;
        //        // Try to find next responder
        //        let nextResponder=textField.superview?.superview?.superview?.viewWithTag(nextTage) as UIResponder!
        //
        //        if (nextResponder != nil){
        //            // Found next responder, so set it.
        //            nextResponder?.becomeFirstResponder()
        //        }
        //        else
        //        {
        //            // Not found, so remove keyboard
        //            textField.resignFirstResponder()
        //        }
        //        if nextTage==4
        //        {
        //            textField.returnKeyType = .Done;
        //
        //        }
        //        return false // We do not want UITextField to insert line-breaks.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 3  || textField.tag == 34
        {
            //only enter the 3 digits
            
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount+1)
            {
                return false
            }
            
            print(currentCharacterCount)
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 2
            
            
        }
        
        return true
        
    }
    
    @IBAction func btnRequestPressed(_ sender: UIButton!)
    {
        if InternetConnection.isConnectedToNetwork()
        {
            
            requestReservation() { responseObject, error in
                
                
                if (responseObject != nil)
                {
                    
                let json = JSON(responseObject!)
                if let string = json.rawString()
                {
                    //Do something you want
                    print(string)
                    
                    
                    self.stopActivityAnimating()
                    
                    if let data = string.data(using: String.Encoding.utf8) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            print("json ",json)
                            
                            let successVal:String = json!["Success"] as! String
                            if (successVal == "1") {
                                
                                
                                
                                let dialog = ZAlertView(title: "Foodies4u",
                                                         message:GlobalConstants.Str_RequestReservationSuccess,
                                                         closeButtonText: "Ok",
                                                         closeButtonHandler: { (alertView) -> () in
                                                            alertView.dismissAlertView()
                                                            self.navigationController?.popViewController(animated: true)
                                    }
                                )
                                dialog.show()
                                dialog.allowTouchOutsideToDismiss = true
                                

                                
                               
                                
                                
                                
                            }else {
                                
                                let dialog = ZAlertView(title: "Foodies4u",
                                                         message:  json!["Data"]?["status"] as? String ,
                                                         closeButtonText: "Ok",
                                                         closeButtonHandler: { (alertView) -> () in
                                                            alertView.dismissAlertView()

                                    }
                                )
                                dialog.show()
                                dialog.allowTouchOutsideToDismiss = true


                                
                            }
                            
                            
                            
                        } catch {
                            print("Something went wrong")
                        }
                    }
//                    
//                    print("\(finalJSON)")
//                    print("\(finalJSON["Success"])")
//                    let name = finalJSON["Success"]
//                    
//                    if name == 1
//                    {
//                        self.dialog = ZAlertView(title: "Foodies4u",
//                            message:GlobalConstants.Str_Success,
//                            closeButtonText: "Ok",
//                            closeButtonHandler: { (alertView) -> () in
//                                alertView.dismissAlertView()
//                                
//                            }
//                        )
//                        self.dialog.show()
//                        self.dialog.allowTouchOutsideToDismiss = true
//                
//                    }
                }
                }
                else
                {
                    self.stopActivityAnimating()
                    self.AlertMethod()
                    
                }
                
            }
        }
    }
    
    //MARK:WEBSERVICES METHOD
    
    
    func requestReservation(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        
        self.view.endEditing(true)
        if strName.length <= 0 { // for name empty check
            dialog = ZAlertView(title: "Foodies4u",
                                message: GlobalConstants.Str_Name,
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
            
        else if (strAdult.length == 0 && strChildren.length == 0)
        {
            dialog = ZAlertView(title: "Foodies4u",
                                message: "Please enter the number of guests",
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
        else if strAdult == "0"  && strChildren ==  "0" { // for adults empty check
            dialog = ZAlertView(title: "Foodies4u",
                                message: "Please enter the number of guests",
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
        /*else if strChildren.length <= 0 { // for childrens empty check
            dialog = ZAlertView(title: "Foodies4u",
                                message: GlobalConstants.Str_Childrens,
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }*/
        else if strEmail.length <= 0 { // for email empty check
            dialog = ZAlertView(title: "Foodies4u",
                                message: GlobalConstants.Str_Valid_EmailId,
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
        else if strDate.length <= 0 { // for reservation date & time empty check
            dialog = ZAlertView(title: "Foodies4u",
                                message: GlobalConstants.Str_DateTime,
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
        else  if strEmail.length > 0
        {
            // validate email id if user entered
            
            if !isValidEmail(strEmail as String)
            {
                dialog = ZAlertView(title: "Foodies4u",
                                    message: GlobalConstants.Str_Valid_EmailId,
                                    closeButtonText: "Ok",
                                    closeButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                    }
                )
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
                
            }else {
                let size = CGSize(width: 80, height:80)
                strWSURL=GETWSURL()
                
                startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
                
                strMNumber = strMNumber.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                strMNumber = strMNumber.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                strMNumber = strMNumber.replacingOccurrences(of: "(", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                strMNumber = strMNumber.replacingOccurrences(of: ")", with: "", options: NSString.CompareOptions.regularExpression, range: nil)
                
                
                if(strChildren.length == 0)
                {
                    strChildren = "0"
                }
                if(strAdult.length == 0)
                {
                    strAdult = "0"
                }
                
                let param1 : [ String : AnyObject] = [
                    "RestaurantId":strRestId as AnyObject,
                    "ReservationTime": strDate,
                    "CustomerId": strUSERID as AnyObject,
                    "CustomerName": strName,
                    "CustomerEmail": strEmail,
                    "MobileNumber": strMNumber as AnyObject,
                    "NoOfAdult":strAdult,
                    "NoOfChilds": strChildren,
                    "Deleted": "false" as AnyObject,
                    "isApproved":"false" as AnyObject,
                    "StaffId": "0" as AnyObject,
                    "SpecialRequest":strTextrequest
                ]
               makeCall(section: strURL as String + "RequestReservation?",param:param1,completionHandler: completionHandler)
            }
        }
    }
    
 }
