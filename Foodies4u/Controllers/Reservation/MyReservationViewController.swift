//
//  MyOrderViewController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import GoogleMaps
import SwiftyJSON




class MyReservationViewController: parentViewController,UITableViewDataSource,UITableViewDelegate,NVActivityIndicatorViewable
{
    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet weak var tblRestData:UITableView!
    @IBOutlet weak var lblNoReservation:UILabel!

    //for tablecell and data
    var MyRestcells:MyResListCell!
    var arrData: NSMutableArray!
    var arrFoodItemData: NSArray!

    
    //for Calling webservices
    var strWSURL: NSString!
    var strUSERID: AnyObject!
    var strResId: String!

    
    
    
    // MARK: - LIFECYCLES METHODS
    
    
    override func viewDidLoad()
        
    {
        super.viewDidLoad()
        
        //navigationbar
        simplenavigationBarUI()
        self.navigationItem.title = "My Reservation"
        // get user-id
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!
       
        //side menu
        
        self.lblNoReservation.isHidden = true

        if self.revealViewController() != nil
        {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        }
        
        //get initial resturants list
        if InternetConnection.isConnectedToNetwork()
        {
          getMyReservation()
        }
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - WEBSERVICES METHODS
    
    
    func  getMyReservation()
    {
        
        
        
        getReservation() { responseObject, error in
            
            if (responseObject != nil)
            {
                let json = JSON(responseObject!)
            if let string = json.rawString()
            {
                
                self.stopActivityAnimating()
                
                if let data = string.data(using: String.Encoding.utf8) {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSMutableDictionary
                        print("json ",json )
                        
                        if(json!["Success"] as? String  == "1")
                        {
                            
                            self.arrData = json!["Data"] as! NSMutableArray
                            for i in 0...self.arrData.count-1
                            {
                               // print(i)
                                
                               // print("self.arrData.count",self.arrData.count)
                                self.tblRestData.dataSource=self
                                self.tblRestData.delegate=self
                                self.lblNoReservation.isHidden = true
                                
                                self.tblRestData.reloadData()
                                
                            }
                            
                        }
                        else
                        {
                            self.lblNoReservation.isHidden = false
                            self.tblRestData.isHidden = true

                            
                           /* let alertView = UNAlertView(title: "Foodies4u", message: "No resturants found.")
                            
                            alertView.addButton("Ok", action: {
                                alertView.dismissAlertView()
                                
                            })
                            // Show
                            alertView.show()*/
                            
                          
                        }
                        
                        
                        
                        
                        
                    } catch {
                        print("Something went wrong")
                    }
                }
            }
                else
            {
                self.stopActivityAnimating()
                self.AlertMethod()
                }
            
            
        }
        
        
    }
    }
    func getReservation(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        let size = CGSize(width: 80, height:80)
        
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)

        
        strWSURL=GETWSURL()
        let param1 : [ String : AnyObject] = [
            "CustomerId":strUSERID as AnyObject,
            
            ]
        makeCall(section: strURL as String + "MyReservation?",param:param1,completionHandler: completionHandler)
        
        
    }
    func DelReservation(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        
        strWSURL=GETWSURL()
        let param1 : [ String : AnyObject] = [
            "CustomerId":strUSERID as AnyObject,
            "ReservationId":strResId as AnyObject
            
            ]
        makeCall(section: strURL as String + "CancelReservation?",param:param1,completionHandler: completionHandler)
        
        
    }
    
    
    
    
   
    
    
    // MARK: - TABLEVIEW DATASOURCE AND DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return 151;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if arrData==nil
        {
            return 0
            
        }
        else
            
        {
            return arrData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        //display searching result
        
        MyRestcells = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyResListCell
        
        // Name
        
        MyRestcells.selectionStyle = .none
        
        
        MyRestcells.lblRestName.text=(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "RestaurantName") as? String
        
        // OrderID
    MyRestcells.lblOrderId.text = "Name: " + String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row)  as AnyObject).value(forKey: "CustomerName") as! String)
        
        
        // Date

        MyRestcells.lblDate.text = "Adult: " +  String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "NoOfAdult") as! NSNumber)
        
        // Time

        MyRestcells.lblTime.text = "Child: " +  String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "NoOfChild") as! NSNumber)
        
        // Price
        
        MyRestcells.btnPrice.setTitle(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "ReservationDateandTime") as! String), for: .normal)
        
        
        // Price
        
        if((self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "SpecialRequest") as? String == "")
        {
            
            
            MyRestcells.lblSpecialRequest.text =   "No Special Request"


        }
        else{
            MyRestcells.lblSpecialRequest.text =   String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "SpecialRequest") as! String)
        }
        
        
        
        // Status
        
        
        
        
        if(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "BookingStatusName") as! String) == "Pending")
        {
            MyRestcells.btnOrderStatus.backgroundColor = UIColor.gray
        }
        else if(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "BookingStatusName") as! String) == "Reserved")
        {
            MyRestcells.btnOrderStatus.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            
        }
        else if(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "BookingStatusName") as! String) == "Not Arrived")
        {
            MyRestcells.btnOrderStatus.backgroundColor = UIColor.orange
            
        }
        else
        {
        
            MyRestcells.btnOrderStatus.backgroundColor = UIColor.red
            
        }
    
    
        
        
        

        MyRestcells.btnOrderStatus.setTitle(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "BookingStatusName") as! String), for: .normal)
        
        return MyRestcells
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        if(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "BookingStatusName") as! String) == "Cancelled" ||  String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "BookingStatusName") as! String) == "Not Arrived")
        {
            return false

        }
        else
        {
            return true

        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        strResId = String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Id") as! NSNumber)
        
        if editingStyle == .delete
        {
            DelReservation() { responseObject, error in
                
                if (responseObject != nil)
                {
                    let json = JSON(responseObject!)
                    if let string = json.rawString()
                    {
                        
                        self.stopActivityAnimating()
                        
                        if let data = string.data(using: String.Encoding.utf8) {
                            do {
                                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSMutableDictionary
                                print("json ",json)
                                
                                if(json!["Success"] as? String  == "1")
                                {
                                    self.getMyReservation()

                                    
                                }
                                else
                                {
                                    let dialog = ZAlertView(title: "Foodies4u",
                                                            message:"Reservation cannot be cancelled." ,
                                                            closeButtonText: "Ok",
                                                            closeButtonHandler: { (alertView) -> () in
                                                                self.tblRestData.reloadData()
                                                                alertView.dismissAlertView()
                                                                
                                    }
                                    )
                                    dialog.show()
                                    dialog.allowTouchOutsideToDismiss = true
                                    
                                }
                                
                                
                                
                                
                                
                            } catch {
                                print("Something went wrong")
                            }
                        }
                    }
                    else
                    {
                        self.stopActivityAnimating()
                        self.AlertMethod()
                    }
                    
                    
                }
                
                
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        /*let strStatus:String = String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "OrderStatusName") as! String)
        
        if (strStatus == "In Progress"  || strStatus == "Placed")
        {
            let TOrderVC = self.storyboard?.instantiateViewController(withIdentifier: "TrackOrderViewController") as? TrackOrderViewController
            TOrderVC?.strStatus = strStatus
            self.navigationController?.pushViewController(TOrderVC!, animated: true)
        }
        else if (strStatus == "Cancelled")
        {
            
        }
        else
        {
            let BillSummaryVC = self.storyboard?.instantiateViewController(withIdentifier: "BillSummaryViewController") as? BillSummaryViewController
            
            BillSummaryVC?.arrData = self.arrData.object(at: (indexPath as NSIndexPath).row)  as! NSMutableDictionary

            print("self.arrFoodItemData" , BillSummaryVC?.arrData)

            self.navigationController?.pushViewController(BillSummaryVC!, animated: true)
        }*/
    }
    
}
