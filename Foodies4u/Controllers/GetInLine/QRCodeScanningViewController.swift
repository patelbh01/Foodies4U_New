//
//  QRCodeScanningViewController.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 23/11/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import Foundation
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON
import AVFoundation




class QRCodeScanningViewController: parentViewController,QRCodeReaderViewControllerDelegate,NVActivityIndicatorViewable
    
    
{
    
    var strWSURL: NSString!
    var strQrCode: String!
    var strRestId: String!
    var strResId: String!
    var strUSERID: AnyObject!
    var arrData: NSMutableArray!
    
    
    lazy var reader = QRCodeReaderViewController(builder: QRCodeReaderViewControllerBuilder {
        $0.reader          = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode])
        $0.showTorchButton = true
    })
    
    
    //MARK: ViewLife Cycle Methods

    
    override func viewDidLoad() {
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    
    
    
    //MARK: Button Actions Methods

    
    @IBAction func scanAction(_ sender: UIButton) {
        if QRCodeReader.supportsMetadataObjectTypes() {
            reader.modalPresentationStyle = .formSheet
            reader.delegate               = self
            
            reader.completionBlock = { (result: QRCodeReaderResult?) in
                if let result = result {
                    print("Completion with result: \(result.value) of type \(result.metadataType)")
                }
            }
            
            present(reader, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - QRCodeReader Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true) { [weak self] in
            
            
            self!.strQrCode = result.value
            
            print("QR CODe", self!.strQrCode)
            
            
                self!.CheckQRCodeScanning() { responseObject, error in
                    if (responseObject != nil)
                    {
                        let json = JSON(responseObject!)
                        if let string = json.rawString()
                        {
                            //Do something you want
                            //print(string)
                            self?.stopActivityAnimating()
                            
                            if let data = string.data(using: String.Encoding.utf8) {
                                do {
                                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                                    print("json ",json)
                                    
                                    let successVal:String = json!["Success"] as! String
                                    if (successVal == "1") {
                                        
                                        self?.arrData = json!["Data"] as! NSMutableArray

                                        
                                        let storyboard = self?.storyboard?.instantiateViewController(withIdentifier: "tableAssociationViewController") as! tableAssociationViewController
                                        storyboard.arrData = self?.arrData
                                        self?.navigationController?.pushViewController(storyboard, animated: true)

                                        
                                        
                                    }else {
                                        
                                        
                                        
                                        let dialog = ZAlertView(title: "Foodies4u",
                                                                message:  json!["Data"]?["status"] as? String,
                                                                closeButtonText: "Ok",
                                                                closeButtonHandler: { (alertView) -> () in
                                                                    
                                                                    alertView.dismissAlertView()
                                        }
                                        )
                                        dialog.show()
                                        dialog.allowTouchOutsideToDismiss = true
                                    }
                                    
                                    
                                    
                                } catch {
                                    print("Something went wrong")
                                }
                            }
                            
                        }
                    }
                    else
                    {
                        self?.stopActivityAnimating()
                        self?.AlertMethod()
                    }
                }
            
            
            /*let alert = UIAlertController(
             title: "QRCodeReader",
             message: String (format:"%@ (of type %@)", result.value, result.metadataType),*/
            
            /* preferredStyle: .alert
             )
             alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
             
             self?.present(alert, animated: true, completion: nil)*/
        }
    }
    
    
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        if let cameraName = newCaptureDevice.device.localizedName {
            print("Switching capturing to: \(cameraName)")
        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Check QR-Code scanning

    func CheckQRCodeScanning(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        //let size = CGSize(width: 80, height:80)
        
        //startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        strWSURL=GETWSURL()
        
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as! String as AnyObject!
        strRestId = UserDefaults.standard.value(forKey: "RESTID") as! String!
         strResId = UserDefaults.standard.value(forKey: "RESERVATIONID") as! String!


        let param1 : [ String : AnyObject] = [
            "QRCode":strQrCode as AnyObject,
            "CustomerId":strUSERID as AnyObject,
            "RestaurantId":strRestId as AnyObject,
            "ReservationId":strResId as AnyObject

        ]
        
        makeCall(section: strURL as String + "TableAssignByQRCode?",param:param1,completionHandler: completionHandler)
        
    }

}
