//
//  waitListViewController.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 17/11/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation

class waitListViewController: UIViewController {
    
    @IBOutlet weak var btnEstimateTime:UIButton!
    var strEstimateTime:String!
    var timer = Timer()
    var startTime = TimeInterval()
    var count = Int!.self
    var date:NSDate!
    private var timerValue:Int!


    //MARK: ViewLife Cycle Methods

    
    override func viewDidLoad() {
        
        self.navigationController?.navigationBar.isHidden = true
        
        let inFormatter = DateFormatter()
        inFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        inFormatter.dateFormat = "HH:mm:ss"
        
        let inStr = strEstimateTime
         date = inFormatter.date(from: inStr!)! as NSDate!
        let outStr = inFormatter.string(from: date as Date)
        print(outStr) // -> outputs 04:50
        
        
        btnEstimateTime.setTitle(outStr, for: .normal)
        
        startTime = self.parseDuration(timeString: outStr)

        print("startTime" , startTime)
        
        //convert time in second
        timerValue =  Int(startTime)

        // call timer for update waiting time
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        
        

        
    }
    
    //MARK: Convert time into timeinterval

    func parseDuration(timeString:String) -> TimeInterval {
        guard !timeString.isEmpty else {
            return 0
        }
        
        var interval:Double = 0
        
        let parts = timeString.components(separatedBy: ":")
        for (index, part) in parts.reversed().enumerated() {
            interval += (Double(part) ?? 0) * pow(Double(60), Double(index))
        }
        
        return interval
    }
    
    //MARK: Update waiting time

    func updateTime()
    
    {
        
        self.timerValue = self.timerValue - 1
        print(self.timerValue)
        
        if self.timerValue < 0
        {
            self.timer.invalidate()
               let RViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeScanningViewController") as? QRCodeScanningViewController
            self.navigationController?.pushViewController(RViewControllerObj!, animated: true)
        }
        else {
            
            btnEstimateTime.setTitle(self.timeFormatted(totalSeconds: self.timerValue), for: .normal)

        }
        
    }
    
    private func timeFormatted(totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    
   
    
}
