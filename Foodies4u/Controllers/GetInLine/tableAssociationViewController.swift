//
//  tableAssociationViewController.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 17/11/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation

class tableAssociationViewController: UIViewController {
    
    @IBOutlet var tblData : UITableView!

    let arrIconList = ["Table", "Time", "Waiter"]
    let arrInfo = ["Table Number : ", "Alloted Time : ", "Waiter at your service : "]

    var arrData:NSMutableArray!
    
    @IBOutlet weak var currReview: FloatRatingView!
    @IBOutlet weak var lblRestName: UILabel!
    @IBOutlet  var imgProfilePic: UIImageView!
    @IBOutlet var imgIcon : UIImageView!
    @IBOutlet  var arrowImageView: UIImageView!
    @IBOutlet weak var btnFav: UIButton!

    
    @IBOutlet var lblInfo : UILabel!
    @IBOutlet var lblName : UILabel!
    var cell2:Customcell!

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        currReview.rating = Float((self.arrData.value(forKey: "RestaurantRatings")  as AnyObject).object(at:0) as! NSNumber)
        
        lblRestName.text = String(describing: ((arrData.value(forKey: "RestaurantName") as AnyObject).object(at:0)) as! String)
        
        if (((arrData.value(forKey: "FavRestaurant") as AnyObject).object(at:0)) as! Bool == true)
        {
           
            btnFav.setImage(UIImage(named:"Favourite-Icon-Red.png"), for: .normal)
            
        }
        else
        {
            btnFav.setImage(UIImage(named:"Favourite-Icon-White.png"), for: .normal)

        }
        
        
        


    }
    
    // MARK: - TABLE VIEW DATA SOURCE AND DELEGATE
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat
    {
        
            return 50.0
            
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
                return 5
        
            
            
       
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell
    {
        
            cell2 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! Customcell
            tblData.separatorStyle = UITableViewCellSeparatorStyle.none
            cell2.selectionStyle = .none
        
        if((indexPath as NSIndexPath).row == 3)
        {
            cell2.btnPlaceOrder.isHidden = false
             cell2.lblInfo.isHidden = true
             cell2.ImgViewicon.isHidden = true
            cell2.btnBuzz.isHidden = true

        }
            else if((indexPath as NSIndexPath).row == 4)
        {
            cell2.btnPlaceOrder.isHidden = true
            cell2.lblInfo.isHidden = true
            cell2.ImgViewicon.isHidden = true
            cell2.btnBuzz.isHidden = false
 
        }
        else
        {
            if(((indexPath as NSIndexPath).row) == 0)
            {
                cell2.lblInfo.text =  "Table Number : " + String(describing: ((arrData.value(forKey: "TableNo") as AnyObject).object(at:0)) as! String)
                
                

            }
            else if(((indexPath as NSIndexPath).row) == 1)
            {
                
                cell2.lblInfo.text =  "Alloted Time : " + String(describing: ((arrData.value(forKey: "ReservationTime") as AnyObject).object(at:0)) as! String)

            }
            else
            {
                cell2.lblInfo.text =  "Waiter at your service : " + String(describing: ((arrData.value(forKey: "StaffName") as AnyObject).object(at:0)) as! String)

            }
            
            
            let imgLogo = UIImage(named:arrIconList[(indexPath as NSIndexPath).row])
            cell2.ImgViewicon.image = imgLogo
            cell2.btnPlaceOrder.isHidden = true
            cell2.btnBuzz.isHidden = true

        }
        
        
        
        return cell2
    
    
}
    
    @IBAction func btnMenuClicked(_ sender: UIButton){
        
        
        UserDefaults.standard.set("YES", forKey: "SHOWCART")
        
        let storyboard: UIStoryboard = UIStoryboard(name: "MainSW", bundle: nil)

        
        let RViewControllerObj = storyboard.instantiateViewController(withIdentifier: "menuListViewController") as? menuListViewController
        
        
        
        RViewControllerObj?.restName = String(describing: ((arrData.value(forKey: "RestaurantName") as AnyObject).object(at:0)) as! String)
        
        RViewControllerObj?.arrRestImages = ((arrData.value(forKey: "RestaurantImgs") as AnyObject).object(at:0) as! NSArray)
        
        print("RViewControllerObj?.restName" ,RViewControllerObj?.restName!)
        print("RViewControllerObj?.arrRestImages" ,RViewControllerObj?.arrRestImages as Any)
        
        self.navigationController?.navigationBar.isHidden  = false
        
        
        UserDefaults.standard.setValue(String(describing: ((arrData.value(forKey: "RestaurantId") as AnyObject).object(at:0)) as! NSNumber), forKey: "RESTID")

        
        self.navigationController?.pushViewController(RViewControllerObj!, animated: true)
        
        // self.performSegueWithIdentifier("menuListSegue", sender: self)
    }
}
