//
//  getInLineViewController.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 17/11/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation



class getInLineViewController: parentViewController {
    
    @IBOutlet weak var btnEstimateTime:UIButton!
    @IBOutlet weak var lblMsg:UILabel!
    
    @IBOutlet weak var timerLabel: UILabel!
    
    var remainingTime: TimeInterval = 0
    var endDate: NSDate!
    var timer = Timer()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnEstimateTime.isHidden = true
        lblMsg.text = "You have no reservation"
        
        endDate = NSDate().addingTimeInterval(remainingTime)   // set your future end date by adding the time for your timer
        
        
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden=false
    }
    
    
    
}
