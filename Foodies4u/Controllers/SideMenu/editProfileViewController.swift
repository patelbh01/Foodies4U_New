//
//  editProfileViewController.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 10/08/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON
import Alamofire
import GoogleMaps
import GooglePlacePicker


class editProfileViewController: parentViewController, UITextFieldDelegate, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NVActivityIndicatorViewable, KACircleCropViewControllerDelegate{
    
    @IBOutlet var arrUserProfileData: NSMutableArray!
    
    @IBOutlet var tblUserProfileData: UITableView!
    
    var dialog = ZAlertView()
    
    
    let arrIconList = ["ProfileIcon.png", "CalendarIcon", "EmailIcon", "MobileIcon", "Home-Address.png", "Work-Address.png","UPDATE"]
    
    var strFirstNameText: String?
    var strLastNameText: String?
    var strEmailIdText: String?
    var strHomeAddressText: String? = ""
    var HomeLat: String?
    var HomeLong: String?
    var WorkLat: String?
    var WorkLong: String?
    
    
    var strWorkddressText: String? = ""
    var strDOBText: String?
    
    var addHomedict:NSMutableDictionary!
    var addWorkdict:NSMutableDictionary!
    var addBothdict:NSMutableArray!
    
    
    //////////////// for date picker //////////////////
    
    let strDateSelected = ""
    var strDatePickerOpened = "no"
    var picker : UIDatePicker!
    var toolbar : UIToolbar!
    var doneButton : UIButton!
    var datePicker = MIDatePicker.getFromNib()
    var dateFormatter = DateFormatter()
    var Row : Int!
    var UserImage : UIImage! = nil
    var UserOLDImage : UIImage!
    var uploadTapped : Bool!
    
    
    
    ///////////////////////////////////////////////////
    
    //////////////////for google places //////////////
    var locationManager: CLLocationManager!
    var latitude: Double!
    var longitude: Double!
    var placePicker: GMSPlacePicker!
    
    
    ///////////////////////////////////////////////////
    
    //////////////////for IMAGE PICKER //////////////
    
    var actionSheetControllerIOS8: UIAlertController!
    var imagePicker = UIImagePickerController()
    var imgSelected: UIImage!
    var strBase64:NSString! = ""
    var arrUserAddress: NSArray!
    
    
    
    
    ///////////////////////////////////////////////////
    
    // MARK: - VIEW LIFE CYCLE
    
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        
        self.navigationBarUI()
        
        //dismis keyboard any where you tap on the screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        if(arrUserProfileData != nil)
        {
            
            
            if((arrUserAddress.object(at: 0) as AnyObject).count  == 0)
            {
                
            }
            else
            {
                
                if((arrUserAddress.object(at: 0) as AnyObject).count == 1)
                {
                    
                    if((arrUserAddress.value(forKey: "AddressId") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as?Int == 1
                    {
                        strHomeAddressText  = ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                        HomeLat = ((arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                        HomeLong = ((arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                    }
                    else{
                        
                        strWorkddressText = ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                        WorkLat = ((arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                        WorkLong = ((arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                    }
                }
                else
                {
                    strHomeAddressText  = ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                    strWorkddressText = ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String
                    HomeLat = ((arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                    HomeLong = ((arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                    WorkLat = ((arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String
                    WorkLong = ((arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String
                }
                
                
            }
            
            strFirstNameText = (arrUserProfileData.value(forKey: "FirstName") as AnyObject).object(at: 0) as? String
            strLastNameText = (arrUserProfileData.value(forKey: "LastName") as AnyObject).object(at: 0) as? String
            strDOBText = (arrUserProfileData.value(forKey: "DateOfBirth") as AnyObject).object(at: 0) as? String
            strEmailIdText = (arrUserProfileData.value(forKey: "Email") as AnyObject).object(at: 0) as? String
        }
        
        if let imageData = UserDefaults.standard.object(forKey: "USERIMAGE")
        {
            let image = UIImage(data: imageData as! Data)
            self.UserOLDImage = image
            
        }
        
        uploadTapped = false
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        let img = UIImage(named: "BackArrow")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(CancelTapped))
        
        
        
        
        imagePicker.delegate = self
    }
    func CancelTapped()
    {
        //as we save user image locally we have to check if image is uploaded to server or not
        
        
        if ((uploadTapped) ==  false)
        {
            if (self.UserOLDImage != nil)
            {
                
                UserDefaults.standard.set(UIImagePNGRepresentation(self.UserOLDImage), forKey: "USERIMAGE")
            }
            
        }
        else
        {
            if (self.UserImage != nil)
            {
                UserDefaults.standard.set(UIImagePNGRepresentation(self.UserImage), forKey: "USERIMAGE")
            }
            
        }
        self.navigationController!.popViewController(animated: false)
        
    }
    
    
    //MARK: TABLE VIEW DATA SOURCE AND DELEGATE METHODS
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat
    {
        if((indexPath as NSIndexPath).row == 0) {
            return 95
        }
        
        return 60.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrIconList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell: Customcell!
        if((indexPath as NSIndexPath).row == 0) {
            cell = tableView.dequeueReusableCell(withIdentifier: "nameTextfieldCell", for: indexPath) as! Customcell
        }
        else if((indexPath as NSIndexPath).row == (arrIconList.count-1)) {
            cell = tableView.dequeueReusableCell(withIdentifier: "submitBtnCell", for: indexPath) as! Customcell
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: "textfieldCell", for: indexPath) as! Customcell
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if((indexPath as NSIndexPath).row < (arrIconList.count-1)) {
            
            let imgIcon = UIImage(named:arrIconList[(indexPath as NSIndexPath).row])
            
            let btnLogo: UIButton = cell.viewWithTag(20) as! UIButton
            btnLogo.setImage(imgIcon, for: UIControlState())
            
        }
        
        if (indexPath as NSIndexPath).row == 0 { // for name and profile pic
            
            let txtFname: UITextField = cell.viewWithTag(30) as! UITextField
            let txtLname: UITextField = cell.viewWithTag(40) as! UITextField
            
            txtFname.placeholder = "First Name"
            txtLname.placeholder = "Last Name"
            
            
            
            let lblLabel: UIView = cell.viewWithTag(10)! as UIView
            lblLabel.layer.borderColor = UIColor.lightGray.cgColor
            lblLabel.layer.borderWidth = 1.0
            lblLabel.layer.cornerRadius = 5.0
            
            txtFname.backgroundColor = UIColor.clear
            txtLname.backgroundColor = UIColor.clear
            
            if (arrUserProfileData.value(forKey: "FirstName") as AnyObject).object(at: 0) as! String == "" &&  ((arrUserProfileData.value(forKey: "LastName") as AnyObject).object(at: 0) as! String) == ""{
                
                txtFname.placeholder = "First Name"
                txtLname.placeholder = "Last Name"
                
            } else {
                txtFname.text = ((arrUserProfileData.value(forKey: "FirstName") as AnyObject).object(at: 0) as! String)
                txtLname.text = ((arrUserProfileData.value(forKey: "LastName") as AnyObject).object(at: 0) as! String)
            }
            
            //for profilepic
            
            // let url = NSURL(string: "http://foodies4you.intransure.com/Media/Customer/"+(self.arrUserProfileData.valueForKey("ImageName").objectAtIndex(0) as? String)!)
            
            
            
            
            let btnProfilPic: UIButton = cell.viewWithTag(20) as! UIButton
            btnProfilPic.layer.borderWidth = 1.0
            btnProfilPic.layer.borderColor = UIColor.lightGray.cgColor
            btnProfilPic.layer.cornerRadius = btnProfilPic.frame.size.width/2
            btnProfilPic.imageView?.clipsToBounds = true
            btnProfilPic.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            
            if let imageData = UserDefaults.standard.object(forKey: "USERIMAGE"),
                let image = UIImage(data: imageData as! Data){
                
                btnProfilPic.setImage(image , for:UIControlState())
                btnProfilPic.layer.cornerRadius = btnProfilPic.frame.size.width/2
                
            }
            
        }
        else if (indexPath as NSIndexPath).row == 1 { //DOB
            
            cell.txtField.tag = (indexPath as NSIndexPath).row;
            cell.txtField.placeholder = "Your DOB"
            cell.txtField.isUserInteractionEnabled = false
            
            self.formatTextfieldWithoutBottomLine(cell.txtField)
            
            if (arrUserProfileData.value(forKey: "DateOfBirth") as AnyObject).object(at: 0) as! String == ""{
                cell.txtField.placeholder = "Your DOB"
            } else {
                cell.txtField.text = ((arrUserProfileData.value(forKey: "DateOfBirth") as AnyObject).object(at: 0) as! String)
            }
            
            let btnDOB: UIButton = UIButton(frame: cell.txtField.frame)
            btnDOB.addTarget(self, action: #selector(self.btnShowDatePickerClicked(_:)), for: UIControlEvents.touchUpInside);
            
            cell.addSubview(btnDOB)
        }
        else if (indexPath as NSIndexPath).row == 2 { // EMail ID
            
            cell.txtField.tag = (indexPath as NSIndexPath).row;
            cell.txtField.placeholder = "Your Email ID"
            cell.txtField.keyboardType = UIKeyboardType.emailAddress
            
            self.formatTextfieldWithoutBottomLine(cell.txtField)
            
            if (arrUserProfileData.value(forKey: "Email") as AnyObject).object(at: 0) as! String == ""{
                cell.txtField.placeholder = "Your Email ID"
            } else {
                cell.txtField.text = ((arrUserProfileData.value(forKey: "Email") as AnyObject).object(at: 0) as! String)
            }
        }
        else if (indexPath as NSIndexPath).row == 3 { // number
            
            cell.txtField.tag = (indexPath as NSIndexPath).row;
            cell.txtField.placeholder = "Contact Number"
            cell.txtField.isUserInteractionEnabled = false
            self.formatTextfieldWithoutBottomLine(cell.txtField)
            cell.txtField.text = ((arrUserProfileData.value(forKey: "PhoneNumber") as AnyObject).object(at: 0) as! String)
            cell.txtField.alpha=0.5
        }
        else  if (indexPath as NSIndexPath).row == 4 || (indexPath as NSIndexPath).row == 5 { //address
            
            cell.txtField.tag = (indexPath as NSIndexPath).row;
            
            self.formatTextfieldWithoutBottomLine(cell.txtField)
            let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 30, height: 30))
            cell.txtField.rightView=paddingView;
            cell.txtField.rightViewMode = UITextFieldViewMode.always
            
            if( (indexPath as NSIndexPath).row == 4)
            {
                
                if((arrUserAddress.object(at: 0) as AnyObject).count  == 0)
                {
                    cell.txtField.placeholder = "Your Home Address"
                }
                else
                {
                
                if((arrUserAddress.object(at: 0) as AnyObject).count == 1)
                {
                    
                    if((arrUserAddress.value(forKey: "AddressId") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as?Int == 2
                    {
                        cell.txtField.text = "Your Home Address"
                        
                    }
                    else
                    {
                        cell.txtField.text =  ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                    }
                    
                }
                else
                {
                    if (((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String == "")
                    {
                        
                        cell.txtField.placeholder = "Your Home Address"
                    }
                    else
                    {
                        cell.txtField.placeholder = "Your Home Address"
                        
                        cell.txtField.text = ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                        
                        
                    }
                }
                }
            } else {
                
                if((arrUserAddress.object(at: 0) as AnyObject).count  == 0)
                {
                    cell.txtField.placeholder = "Your Work Address"
                    
                }
                else
                {
                if((arrUserAddress.object(at: 0) as AnyObject).count == 1)
                {
                    
                    if((arrUserAddress.value(forKey: "AddressId") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as?Int == 1
                    {
                        cell.txtField.text = "Your Work Address"
                        
                    }
                    else
                    {
                        cell.txtField.text =  ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                    }
                    
                }
                else
                {
                    if( ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String == "")
                    {
                        
                        cell.txtField.placeholder = "Your Work Address"
                        
                    }
                    else
                    {
                        cell.txtField.placeholder = "Your Work Address"
                        
                        cell.txtField.text = ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String
                        
                        
                    }
                }
                }
            }
            
            if let btnGooglePlaces: UIButton = cell.viewWithTag(222) as? UIButton
            {
                
                btnGooglePlaces.isHidden = false
                btnGooglePlaces.setBackgroundImage(UIImage(named: "MapDirection.png"), for: UIControlState())
                btnGooglePlaces.tag = (indexPath as NSIndexPath).row
                
                
                //  btnGooglePlaces.setImage(imgIcon, forState: UIControlState.Normal)
                btnGooglePlaces.addTarget(self, action: #selector(self.btnGetGooglePlacesAddressClicked(_:)), for: UIControlEvents.touchUpInside)
            }
            
        } else {
            
            let btnUpdate: UIButton = cell.viewWithTag(10) as! UIButton
            btnUpdate.addTarget(self, action: #selector(self.btnUpdateProfileClicked(_:)), for: UIControlEvents.touchUpInside)
        }
        cell.selectionStyle = .none
        
        Row = (indexPath as NSIndexPath).row
        return cell
    }
    
    //MARK: BUTTON CLICKS
    
    
    @IBAction func btnUpdateProfileClicked(_ sender: UIButton){
        
        if InternetConnection.isConnectedToNetwork()
        {
        
        self.view.endEditing(true)
        
        let size = CGSize(width: 80, height:80)
        
        if(HomeLat == nil)
        {
            HomeLat = ""
        }
        if(HomeLong == nil)
        {
            HomeLong = ""
            
        }
        if(WorkLat == nil)
        {
            WorkLat = ""
        }
        if(WorkLong == nil)
        {
            WorkLong = ""
            
        }
        
        
        // update user data
        
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        if (strEmailIdText?.characters.count)! > 0
        {
            // validate email id if user entered
            
            self.stopActivityAnimating()
            
            if !isValidEmail(strEmailIdText! as String)
            {
                let dialog = ZAlertView(title: "Foodies4u",
                                        message: GlobalConstants.Str_Valid_EmailId,
                                        closeButtonText: "Ok",
                                        closeButtonHandler: { (alertView) -> () in
                    }
                )
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
                
                
            }
            else {
                
                
                
                
                let strWSURL=GETWSURL()
                
                addHomedict =
                    [ "AddressId" : 1, "Address":strHomeAddressText! ,"Latitude":String(describing: HomeLat!) ,"Longitude": String(describing: HomeLong!)]
                
                addWorkdict = [ "AddressId" : 2, "Address":strWorkddressText! ,"Latitude":String(describing: WorkLat!) ,"Longitude": String(describing: WorkLong!)]
                
                addBothdict = NSMutableArray()
                
                addBothdict.add(addHomedict)
                addBothdict.add(addWorkdict)
                UserDefaults.standard.set(addBothdict, forKey: "USERADDRESS")
                
                
                let param : [ String : AnyObject] =
                    [
                        "Id": (arrUserProfileData.value(forKey: "Id") as AnyObject).object(at: 0) as AnyObject,
                        "FirstName": strFirstNameText! as AnyObject,
                        "LastName": strLastNameText! as AnyObject,
                        "Email": strEmailIdText! as AnyObject,
                        "Address": addBothdict as AnyObject,
                        "Country": "1" as AnyObject,
                        "State": "2" as AnyObject,
                        "City": "3" as AnyObject,
                        "DateOfBirth": strDOBText! as AnyObject,
                        "PostalCode": "4" as AnyObject,
                        "ImageName": "temp" as AnyObject,
                        "pictureBinary": strBase64! as AnyObject,
                        "mimeType": ".png" as AnyObject
                ]
                
                print(param)
                
                Alamofire.request( strWSURL as String + "UpdateCustomerData?",method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON
                    { response in switch response.result {
                        
                        
                    case .success(let JSON):
                        
                        print("Success with JSON: \(JSON)")
                        
                        let response = JSON as! NSDictionary
                        
                        
                        if ((response.value(forKey: "Success") as? String) == "1")
                        {
                            self.uploadTapped = true
                            
                            self.stopActivityAnimating()
                            
                            self.dialog = ZAlertView(title: "Foodies4U",
                                                     message:GlobalConstants.Str_Success,
                                                     closeButtonText: "Ok",
                                                     closeButtonHandler: { (alertView) -> () in
                                                        alertView.dismissAlertView()
                            })
                            self.dialog.show()
                            self.dialog.allowTouchOutsideToDismiss = true
                            
                            
                            let arrData:NSDictionary = response["Data"] as! NSDictionary
                            
                            
                            self.arrUserProfileData.add(arrData)
                            
                            self.arrUserAddress = arrData.value(forKey: "CustomerAddress") as! NSArray
                            
                            
                            if((self.arrUserAddress.object(at: 0) as AnyObject).count == 1)
                            {
                                if((self.arrUserAddress.value(forKey: "AddressId") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as?Int == 1
                                {
                                    self.strHomeAddressText  = ((self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                                    self.HomeLat = ((self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                                    self.HomeLong = ((self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                                    
                                    self.addHomedict =
                                        [ "AddressId" : 1, "Address":self.strHomeAddressText ,"Latitude":self.HomeLat ,"Longitude":  self.HomeLong]
                                    
                                    self.addBothdict = NSMutableArray()
                                    self.addBothdict.add(self.addHomedict)
                                   UserDefaults.standard.set(self.addBothdict, forKey: "USERADDRESS")
                                    
                                }
                                else{
                                    self.strWorkddressText =  (self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as? String
                                    
                                    
                                    
                                    self.WorkLat = (self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as? String
                                    
                                    self.WorkLong = (self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as? String
                                    
                                    
                                    self.addWorkdict = [ "AddressId" : 2, "Address":self.strWorkddressText ,"Latitude":self.WorkLat ,"Longitude": self.WorkLong]
                                    
                                    self.addBothdict = NSMutableArray()
                                    self.addBothdict.add(self.addWorkdict)
                                    UserDefaults.standard.set(self.addBothdict, forKey: "USERADDRESS")

                                    
                                }
                                
                            }
                            
                                if((self.arrUserAddress.object(at: 0) as AnyObject).count == 2
                                    )
                                {
                                    
                                    self.strWorkddressText =  (self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 1) as? String
                                    
                                    
                                    
                                    self.WorkLat = (self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 1) as? String
                                    
                                    self.WorkLong = (self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 1) as? String
                                    self.strHomeAddressText =  (self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as? String
                                    self.HomeLat = (self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as? String
                                    
                                    self.HomeLong = (self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as? String
                                    
                                    
                                    
                                    self.addHomedict =
                                        [ "AddressId" : 1, "Address":self.strHomeAddressText ,"Latitude":self.HomeLat ,"Longitude":  self.HomeLong]
                                    
                                    self.addWorkdict = [ "AddressId" : 2, "Address":self.strWorkddressText ,"Latitude":self.WorkLat ,"Longitude": self.WorkLong]
                                    
                                    self.addBothdict = NSMutableArray()
                                    self.addBothdict.add(self.addHomedict)
                                    self.addBothdict.add(self.addWorkdict)
                                    
                                    //set userdefault to store home address and work address for map view
                                    UserDefaults.standard.set(self.addBothdict, forKey: "USERADDRESS")
                                    
                                    
                                }
                                    self.strEmailIdText = (self.arrUserProfileData.value(forKey: "Email") as AnyObject).object(at: 0) as? String
                                    self.strFirstNameText = (self.arrUserProfileData.value(forKey: "FirstName") as AnyObject).object(at: 0) as? String
                                    self.strLastNameText = (self.arrUserProfileData.value(forKey: "LastName") as AnyObject).object(at: 0) as? String
                                    self.strDOBText = (self.arrUserProfileData.value(forKey: "DateOfBirth") as AnyObject).object(at: 0) as? String
                            
                                    if(self.UserImage != nil)
                                     {
                                     UserDefaults.standard.set(UIImagePNGRepresentation(self.UserImage), forKey: "USERIMAGE")
                                    
                                      }
                                        
                                
                              
                        }
                            
                                
                            else
                            {
                                self.stopActivityAnimating()
                                
                            }
                            
                            
                            
                            //example if there is an id
                            
                            case .failure(let error):
                            self.stopActivityAnimating()
                            
                            print("Request failed with error: \(error)")
                        }
                        }
                        
                        
                        
                        
                }
                
            }
            else {
                
                let strWSURL=GETWSURL()
                
                
                addHomedict =
                    [ "AddressId" : 1, "Address":strHomeAddressText! ,"Latitude":String(describing: HomeLat!) ,"Longitude": String(describing: HomeLong!)]
                
                addWorkdict = [ "AddressId" : 2, "Address":strWorkddressText! ,"Latitude":String(describing: WorkLat!) ,"Longitude": String(describing: WorkLong!)]
                
                addBothdict = NSMutableArray()
                
                addBothdict.add(addHomedict)
                addBothdict.add(addWorkdict)
                
                //UserDefaults.standard.set(addBothdict, forKey: "USERADDRESS")
                
                
                let param : [ String : AnyObject] =
                    [
                        "Id": (arrUserProfileData.value(forKey: "Id") as AnyObject).object(at: 0) as AnyObject,
                        "FirstName": strFirstNameText! as AnyObject ,
                        "LastName": strLastNameText! as AnyObject ,
                        "Email": strEmailIdText!  as AnyObject ,
                        "Address": addBothdict as AnyObject,
                        "Country": "1" as AnyObject,
                        "State": "2" as AnyObject,
                        "City": "3" as AnyObject ,
                        "DateOfBirth": strDOBText! as AnyObject,
                        "PostalCode": "4" as AnyObject ,
                        "ImageName": "temp" as AnyObject,
                        "pictureBinary": strBase64 as AnyObject,
                        "mimeType": ".png" as AnyObject
                ]
                
                print(param)
                
                Alamofire.request( strWSURL as String + "UpdateCustomerData?",method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON
                    { response in switch response.result {
                        
                        
                    case .success(let JSON):
                        
                        print("Success with JSON: \(JSON)")
                        
                        let response = JSON as! NSDictionary
                        
                        
                        if ((response.value(forKey: "Success") as? String) == "1")
                        {
                            self.uploadTapped = true
                            
                            self.stopActivityAnimating()
                            self.dialog = ZAlertView(title: "Foodies4u",
                                                     message: "Updated successfully",
                                                     closeButtonText: "Ok",
                                                     closeButtonHandler: { (alertView) -> () in
                                                        alertView.dismissAlertView()
                                                        //self.navigationController!.popViewControllerAnimated(false)
                                }
                                
                                
                            )
                            
                            self.dialog.show()
                            self.dialog.allowTouchOutsideToDismiss = true
                            
                            let arrData:NSDictionary = response["Data"] as! NSDictionary
                            
                            self.arrUserProfileData.add(arrData)
                            
                            self.arrUserAddress = arrData.value(forKey: "CustomerAddress") as! NSArray
                            
                            if (self.arrUserAddress != nil)
                            {
                                
                                if(self.arrUserAddress.count  == 0)
                                {
                                    
                                }
                                else
                                {
                                    
                                    
                                    
                                    if((self.arrUserAddress.object(at: 0) as AnyObject).count == 1)
                                    {
                                        if((self.arrUserAddress.value(forKey: "AddressId") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as?Int == 1
                                        {
                                            self.strHomeAddressText  = ((self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                                            self.HomeLat = ((self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                                            self.HomeLong = ((self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                                            
                                            self.addHomedict =
                                                [ "AddressId" : 1, "Address":self.strHomeAddressText ,"Latitude":self.HomeLat ,"Longitude":  self.HomeLong]
                                            
                                            self.addBothdict = NSMutableArray()
                                            self.addBothdict.add(self.addHomedict)
                                            UserDefaults.standard.set(self.addBothdict, forKey: "USERADDRESS")
                                            
                                        }
                                        else{
                                            self.strWorkddressText =  (self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as? String
                                            
                                            
                                            
                                            self.WorkLat = (self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as? String
                                            
                                            self.WorkLong = (self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as? String
                                            
                                            
                                            self.addWorkdict = [ "AddressId" : 2, "Address":self.strWorkddressText ,"Latitude":self.WorkLat ,"Longitude": self.WorkLong]
                                            
                                            self.addBothdict = NSMutableArray()
                                            self.addBothdict.add(self.addWorkdict)
                                            UserDefaults.standard.set(self.addBothdict, forKey: "USERADDRESS")
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    if((self.arrUserAddress.object(at: 0) as AnyObject).count == 2
                                        )
                                    {
                                        
                                        self.strWorkddressText =  (self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 1) as? String
                                        
                                        
                                        
                                        self.WorkLat = (self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 1) as? String
                                        
                                        self.WorkLong = (self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 1) as? String
                                        self.strHomeAddressText =  (self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as? String
                                        self.HomeLat = (self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as? String
                                        
                                        self.HomeLong = (self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as? String
                                        
                                        
                                        
                                        self.addHomedict =
                                            [ "AddressId" : 1, "Address":self.strHomeAddressText ,"Latitude":self.HomeLat ,"Longitude":  self.HomeLong]
                                        
                                        self.addWorkdict = [ "AddressId" : 2, "Address":self.strWorkddressText ,"Latitude":self.WorkLat ,"Longitude": self.WorkLong]
                                        
                                        self.addBothdict = NSMutableArray()
                                        self.addBothdict.add(self.addHomedict)
                                        self.addBothdict.add(self.addWorkdict)
                                        
                                        //set userdefault to store home address and work address for map view
                                        UserDefaults.standard.set(self.addBothdict, forKey: "USERADDRESS")
                                        
                                        
                                    }
                                    
                                    //set userdefault to store home address and work address for map view
                                  //  UserDefaults.standard.set(self.addBothdict, forKey: "USERADDRESS")
                                }
                                
                            }
                            self.strEmailIdText = (self.arrUserProfileData.value(forKey: "Email") as! NSArray).object(at: 0) as! NSString as String?
                            self.strFirstNameText = (self.arrUserProfileData.value(forKey: "FirstName") as! NSArray).object(at: 0) as! NSString as String?
                            self.strLastNameText = (self.arrUserProfileData.value(forKey: "LastName") as! NSArray).object(at: 0) as! NSString as String?
                            self.strDOBText = (self.arrUserProfileData.value(forKey: "DateOfBirth") as! NSArray).object(at: 0) as! NSString as String?
                            
                            if(self.UserImage == nil)
                            {
                                
                            }
                            else
                            {
                                UserDefaults.standard.set(UIImagePNGRepresentation(self.UserImage), forKey: "USERIMAGE")
                            }
                            
                            
                            
                            // self.dialog.show()
                            //self.dialog.allowTouchOutsideToDismiss = true
                        }
                        
                        
                        
                        //example if there is an id
                        
                    case .failure(let error):
                        self.stopActivityAnimating()
                        
                        print("Request failed with error: \(error)")
                        }
                        
                        
                }
            }
        }
        
        }
        
        
        // MARK: DOB DATE PICKER
        @IBAction func btnShowDatePickerClicked(_ sender: UIButton){
            
            self.view.endEditing(true)
            
            if strDatePickerOpened == "no" {
                
                strDatePickerOpened = "yes"
                
                let lblToolbar : UILabel = UILabel(frame: CGRect.zero)
                picker = UIDatePicker()
                
                
                toolbar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.size.height-250, width: self.view.frame.size.width, height: 44))
                picker.frame = CGRect(x: 0, y: self.view.frame.size.height-210, width: self.view.frame.size.width, height: 230)
                
                toolbar.backgroundColor = UIColor.black
                toolbar.barTintColor = GlobalConstants.Theme_Color_Seperator
                lblToolbar.textColor = UIColor.white
                
                let itemDone: UIBarButtonItem!
                lblToolbar.textAlignment = NSTextAlignment.center
                
                lblToolbar.backgroundColor = UIColor.clear
                lblToolbar.font = UIFont.boldSystemFont(ofSize: 18.0)
                
                lblToolbar.text = "Select Date"
                lblToolbar.sizeToFit()
                
                
                let btnTitle : UIBarButtonItem! = UIBarButtonItem(customView: lblToolbar)
                let flexibleSpace : UIBarButtonItem! = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
                
                itemDone = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.done, target: self, action: #selector(self.doneButtonClicked(_:)))
                
                UIBarButtonItem.appearance().tintColor = UIColor.white
                
                var items = [UIBarButtonItem]()
                items = [flexibleSpace, btnTitle, flexibleSpace, itemDone]
                toolbar.setItems(items, animated: false)
                
                self.view.addSubview(toolbar)
                
                picker.autoresizingMask = UIViewAutoresizing.flexibleWidth;
                picker.datePickerMode = UIDatePickerMode.date
                picker.maximumDate = Date()
                
                picker.addTarget(self, action: #selector(self.updateDatePickerValue(_:)), for: UIControlEvents.valueChanged)
                picker.backgroundColor = UIColor.white
                
                self.view.addSubview(picker)
                
            }
            
        }
        @IBAction func updateDatePickerValue(_ sender: UIDatePicker) {
            
            let dateFormatter : DateFormatter! = DateFormatter()
            //dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
            dateFormatter.dateFormat = "MM/dd/yyyy"
            print("date = \(dateFormatter.string(from: picker.date))")
            
            
            let indexPath = IndexPath(row: 1, section: 0)
            let cell: Customcell = tblUserProfileData.cellForRow(at: indexPath) as! Customcell
            cell.txtField.text = dateFormatter.string(from: picker.date)
            
        }
        
        @IBAction func doneButtonClicked (_ sender: UIButton){
            
            let dateFormatter : DateFormatter! = DateFormatter()
            //dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
            dateFormatter.dateFormat = "MM/dd/yyyy"
            
            //let strDate : String
            //var strDate: String = dateFormatter.stringFromDate(picker.date)
            
            print("date : \(dateFormatter.string(from: picker.date))")
            
            let indexPath = IndexPath(row: 1, section: 0)
            let cell: Customcell = tblUserProfileData.cellForRow(at: indexPath) as! Customcell
            cell.txtField.text = dateFormatter.string(from: picker.date)
            strDOBText = cell.txtField.text
            
            toolbar.removeFromSuperview()
            picker.removeFromSuperview()
            
            strDatePickerOpened = "no"
        }
        
        //MARK: CHOOSE ADDRESS WORK AND HOME
        
        
        func btnGetGooglePlacesAddressClicked(_ sender:UIButton!)
        {
            
            
            
            let lat  = UserDefaults.standard.double(forKey: "LATITUDE")
            let long = UserDefaults.standard.double(forKey: "LONGITUDE")
            print(lat)
            print(long)
            
            let center = CLLocationCoordinate2DMake(40.7167411,-74.0486178)
            let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
            let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            let config = GMSPlacePickerConfig(viewport: viewport)
            
            self.placePicker = GMSPlacePicker(config: config)
            
            // 2
            placePicker.pickPlace { (place: GMSPlace?, error: Error?) -> Void in
                
                if let error = error {
                    print("Error occurred: \(error.localizedDescription)")
                    return
                }
                // 3
                if let place = place {
                    let coordinates = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
                    let marker = GMSMarker(position: coordinates)
                    marker.title = place.name
                    self.latitude =  place.coordinate.latitude
                    self.longitude = place.coordinate.longitude
                    
                    
                    if (sender.tag == 4)
                    {
                        
                        self.strHomeAddressText = place.formattedAddress!
                        self.HomeLat = String(self.latitude)
                        self.HomeLong = String(self.longitude)
                        /*  if let data0 = NSUserDefaults.standardUserDefaults().objectForKey("USERADDRESS") {
                         
                         self.WorkLat = data0.valueForKey("Latitude")!.objectAtIndex(1) as! String
                         self.WorkLong = data0.valueForKey("Longitude")!.objectAtIndex(1) as! String
                         }*/
                        let indexPath = IndexPath(row: 4, section: 0)
                        let cell: Customcell =  self.tblUserProfileData.cellForRow(at: indexPath) as! Customcell
                        cell.txtField.text = place.formattedAddress!
                        cell.txtField.placeholder = " "
                        
                        
                        
                    }
                    else
                    {
                        self.strWorkddressText =  place.formattedAddress!
                        self.WorkLat = String(self.latitude)
                        self.WorkLong =  String(self.longitude)
                        /* if let data0 = NSUserDefaults.standardUserDefaults().objectForKey("USERADDRESS") {
                         
                         self.HomeLat = data0.valueForKey("Latitude")!.objectAtIndex(0) as! String
                         self.HomeLong = data0.valueForKey("Longitude")!.objectAtIndex(0) as! String
                         }*/
                        let indexPath = IndexPath(row: 5, section: 0)
                        let cell: Customcell = self.tblUserProfileData.cellForRow(at: indexPath) as! Customcell
                        cell.txtField.text =  place.formattedAddress!
                        cell.txtField.placeholder = " "
                        
                        
                        
                        
                    }
                    
                }
            }
        }
        
        //MARK: TEXTFIELD DELEGATE METHODS
        func textFieldDidBeginEditing(_ textField: UITextField) {
            
            if textField.tag == 4
            {
                if (DeviceType.IS_IPHONE_4_OR_LESS)
                {
                    animateViewMoving(true, moveValue: 90)
                }
                    
                else
                {
                    animateViewMoving(true, moveValue: 215)
                    
                }
                
                
            }
            
            if textField.tag == 5
            {
                if (DeviceType.IS_IPHONE_4_OR_LESS)
                {
                    animateViewMoving(true, moveValue: 90)
                }
                    
                else
                {
                    animateViewMoving(true, moveValue: 215)
                    
                }
                
                
            }
            
           
            
            if (DeviceType.IS_IPHONE_4_OR_LESS)
            {
                
                if(textField.tag == 30 || textField.tag == 40)
                {
                    
                }
                else
                {
                    animateViewMoving(true, moveValue: 150)
                    
                }
                
                
            }
        }
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            
            if textField.tag == 4
            {
                if (DeviceType.IS_IPHONE_4_OR_LESS)
                {
                    animateViewMoving(false, moveValue: 90)
                }
                    
                else
                {
                    if(self.view.frame.origin.y != 0)
                    {
                        animateViewMoving(false, moveValue: 215)
                    }
                }
            }
            
            if textField.tag == 5
            {
                if (DeviceType.IS_IPHONE_4_OR_LESS)
                {
                    animateViewMoving(false, moveValue: 90)
                }
                    
                else
                {
                    animateViewMoving(false, moveValue: 215)
                    
                }
                
                
            }
            
            
            if textField.tag == 30 { // firstname
                strFirstNameText! = textField.text!
            }
            else if textField.tag == 40 { // lastname
                strLastNameText! = textField.text!
            }
            else if textField.tag == 1 { // DOB
                strDOBText! = textField.text!
            }
            else if textField.tag == 2 { // email id
                strEmailIdText! = textField.text!
            }
            else if textField.tag == 4 { // address
                strHomeAddressText! = textField.text!
            }
            else { // address
                strWorkddressText! = textField.text!
            }
            
            if (DeviceType.IS_IPHONE_4_OR_LESS)
            {
                if
                    (textField.tag == 30 || textField.tag == 40)
                {
                    
                }
                else
                {
                    animateViewMoving(false, moveValue: 150)
                }
                
            }
            
            
            
            
        }
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            
            if textField.tag == 4 {
                animateViewMoving(false, moveValue: 215)
            }
            textField.resignFirstResponder()
            return true
        }
        
        
        //MARK: LOCATION MANAGER METHODS
        
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
            
            let location:CLLocation = locations.last!
            self.latitude = location.coordinate.latitude
            self.longitude = location.coordinate.longitude
            
            print(location.coordinate.latitude)
            print(location.coordinate.longitude)
        }
        
        
        
        
        //MARK: PROFILE PIC
        @IBAction func btnProfilePicClicked(_ sender: UIButton)
        {
            
            self.perform(#selector(self.openactionsheet), with: nil, afterDelay: 0.1)
            
        }
        
        func openactionsheet()
        {
            self.actionSheetControllerIOS8 = UIAlertController(title: "Please select", message: "image from", preferredStyle: .actionSheet)
            
            
            let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                
                // self.parentViewController = registrationViewController
                //self.FlagImagepicker = false
                
            }
            
            self.actionSheetControllerIOS8.addAction(cancelActionButton)
            
            let CameraActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
            { action -> Void in
                self.openCamera()
            }
            self.actionSheetControllerIOS8.addAction(CameraActionButton)
            
            let GalleryActionButton: UIAlertAction = UIAlertAction(title: "Gallery", style: .default)
            {
                action -> Void in
                self.openGallary()
            }
            self.actionSheetControllerIOS8.addAction(GalleryActionButton)
            
            self.navigationController!.present(self.actionSheetControllerIOS8, animated: true, completion: nil)
        }
        
        func openCamera()
        {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imagePicker.allowsEditing = false
                
                self.view.endEditing(true)
                self .present(imagePicker, animated: true, completion: nil)
            }
            else
            {
                openGallary()
            }
        }
        func openGallary()
        {
            
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
            
        }
        
        //MARK:  IMAGEPICKER CONTROLLER METHODS
        
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
        {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                
                dismiss(animated: true, completion: nil)
                
                let circleCropController = KACircleCropViewController(withImage: pickedImage)
                circleCropController.delegate = self
                circleCropController.view.isOpaque = false
                present(circleCropController, animated: false, completion: nil)
            }
            
        }
        
        //MARK: KACIRCLEVIEWCONTROLLER METHODS
        
        func circleCropDidCancel() {
            //Basic dismiss
            dismiss(animated: false, completion: nil)
        }
        
        func circleCropDidCropImage(_ image: UIImage) {
            //Same as dismiss but we also return the image
            let indexPath = IndexPath(row: 0, section: 0)
            let cell: Customcell = tblUserProfileData.cellForRow(at: indexPath) as! Customcell
            let btnProfilePic: UIButton! = cell.viewWithTag(20) as! UIButton
            
            btnProfilePic.layer.borderColor = UIColor.lightGray.cgColor
            btnProfilePic.layer.borderWidth = 0.0;
            btnProfilePic.imageView?.clipsToBounds = true
            btnProfilePic.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            btnProfilePic.setImage(UIImage(named: " "),  for:UIControlState())
            btnProfilePic.setBackgroundImage(image, for: UIControlState())
            
            dismiss(animated: false, completion: nil)
            
            btnProfilePic.isOpaque = false
            btnProfilePic.backgroundColor = UIColor.clear
            UserImage = image
            UserDefaults.standard.set(UIImagePNGRepresentation(image), forKey: "USERIMAGE")
            
            let imageData = UIImagePNGRepresentation(image)
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let imageURL = documentsURL.appendingPathComponent("cached.png")
            
            if !((try? imageData!.write(to: imageURL, options: [])) != nil)
            {
                print("not saved")
            } else {
                print("saved")
            }
            
            //circleCropController.delegate = self
            strBase64 = imageData!.base64EncodedString(options: .lineLength64Characters) as NSString!
        }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        
        
}
