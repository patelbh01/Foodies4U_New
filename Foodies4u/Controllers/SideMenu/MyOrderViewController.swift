//
//  MyOrderViewController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import GoogleMaps
import SwiftyJSON




class MyOrderViewController: parentViewController,UITableViewDataSource,UITableViewDelegate,NVActivityIndicatorViewable
{
    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet weak var tblRestData:UITableView!
    
    //for tablecell and data
    var MyRestcells:MyResListCell!
    var arrData: NSMutableArray!
    var arrFoodItemData: NSArray!

    
    //for Calling webservices
    var strWSURL: NSString!
    var strUSERID: AnyObject!
    var strOrderId: String!
    @IBOutlet weak var lblNoReservation:UILabel!

    
    
    
    
    // MARK: - LIFECYCLES METHODS
    
    
    override func viewDidLoad()
        
    {
        super.viewDidLoad()
        
        //navigationbar
        simplenavigationBarUI()
        self.navigationItem.title = "Order History"
        // get user-id
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!
        
        
        let img = UIImage(named: "Menu")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(btnMenuClicked))
        self.navigationItem.setHidesBackButton(true, animated:true);
       
        //side menu
        if self.revealViewController() != nil
        {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        }
        
        //get initial resturants list
        if InternetConnection.isConnectedToNetwork()
        {
          getMyOrderDetails()
        }
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnMenuClicked(_ sender: UIButton){
        
        
        UserDefaults.standard.set("YES", forKey: "SHOWCART")
        
        let storyboard: UIStoryboard = UIStoryboard(name: "MainSW", bundle: nil)
        
        
        let RViewControllerObj = storyboard.instantiateViewController(withIdentifier: "menuListViewController") as? menuListViewController
        
        
        
        RViewControllerObj?.restName = String(describing: ((arrData.value(forKey: "RestaurantName") as AnyObject).object(at:0)) as! String)
        
        RViewControllerObj?.arrRestImages = ((arrData.value(forKey: "RestaurantImgs") as AnyObject).object(at:0) as! NSArray)
        
        print("RViewControllerObj?.restName" ,RViewControllerObj?.restName!)
        print("RViewControllerObj?.arrRestImages" ,RViewControllerObj?.arrRestImages as Any)
        
        self.navigationController?.navigationBar.isHidden  = false
        
        
        UserDefaults.standard.setValue(String(describing: ((arrData.value(forKey: "RestaurantId") as AnyObject).object(at:0)) as! NSNumber), forKey: "RESTID")
        
        
        self.navigationController?.pushViewController(RViewControllerObj!, animated: true)
        
        // self.performSegueWithIdentifier("menuListSegue", sender: self)
    }
    
    // MARK: - WEBSERVICES METHODS
    
    
    func  getMyOrderDetails()
    {
        
        
        
        getOrders() { responseObject, error in
            
            if (responseObject != nil)
            {
                let json = JSON(responseObject!)
            if let string = json.rawString()
            {
                
                self.stopActivityAnimating()
                
                if let data = string.data(using: String.Encoding.utf8) {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSMutableDictionary
                        print("json ",json)
                        
                        if(json!["Success"] as? String  == "1")
                        {
                            
                            self.arrData = json!["Data"] as! NSMutableArray
                            for i in 0...self.arrData.count-1
                            {
                               // print(i)
                                
                               // print("self.arrData.count",self.arrData.count)
                                self.tblRestData.dataSource=self
                                self.tblRestData.delegate=self
                                
                                self.tblRestData.reloadData()
                                self.lblNoReservation.isHidden = true
                                self.tblRestData.isHidden = false
                            }
                            
                        }
                        else
                        {
                            
                            
                            self.lblNoReservation.isHidden = false
                            self.tblRestData.isHidden = true
                           /* let alertView = UNAlertView(title: "Foodies4u", message: "No resturants found.")
                            
                            alertView.addButton("Ok", action: {
                                alertView.dismissAlertView()
                                
                            })
                            // Show
                            alertView.show()*/
                            
                          
                        }
                        
                        
                        
                        
                        
                    } catch {
                        print("Something went wrong")
                    }
                }
            }
                else
            {
                self.stopActivityAnimating()
                self.AlertMethod()
                }
            
            
        }
        
        
    }
    }
    
    func getOrders(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        let size = CGSize(width: 80, height:80)
        
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        strWSURL=GETWSURL()
        let param1 : [ String : AnyObject] = [
            "CustomerId":strUSERID as AnyObject,
            
            ]
        makeCall(section: strURL as String + "GetOrderHistoryByCustomerId?",param:param1,completionHandler: completionHandler)
        
        
    }
    func DelOrder(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        
        strWSURL=GETWSURL()
        let param1 : [ String : AnyObject] = [
            "CustomerId":strUSERID as AnyObject,
            "OrderId":strOrderId as AnyObject
            
        ]
        makeCall(section: strURL as String + "CancelOrder?",param:param1,completionHandler: completionHandler)
        
        
    }
    
    
    
    
   
    // MARK: - TABLEVIEW DATASOURCE AND DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        return 115;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if arrData==nil
        {
            return 0
            
        }
        else
            
        {
            return arrData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        //display searching result
        
        MyRestcells = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyResListCell
        
        // Name
        
        MyRestcells.selectionStyle = .none
        
        
        MyRestcells.lblRestName.text=(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "RestaurantName") as? String
        
        // OrderID
    MyRestcells.lblOrderId.text = "OrderId:" + String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row)  as AnyObject).value(forKey: "Id") as! Int)
        
        
        // Date

        MyRestcells.lblDate.text = "Date:" +  String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "OrderDate") as! String)
        
        // Time

        MyRestcells.lblTime.text = "Time:" +  String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "OrderTime") as! String)
        
        // Price
        
        MyRestcells.btnPrice.setTitle("$ " + String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "OrderTotal") as! NSNumber), for: .normal)
        
        
        // Status
        
        if(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "OrderStatusName") as! String) == "Placed")
        {
            MyRestcells.btnOrderStatus.backgroundColor = UIColor.gray
        }
        else if(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "OrderStatusName") as! String) == "Completed")
        {
            MyRestcells.btnOrderStatus.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)

        }
        else if(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "OrderStatusName") as! String) == "In Progress")
        {
            MyRestcells.btnOrderStatus.backgroundColor = #colorLiteral(red: 1, green: 0.8323456645, blue: 0.4732058644, alpha: 1)
            
        }
        else if(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "OrderStatusName") as! String) == "Cancelled")
        {
            MyRestcells.btnOrderStatus.backgroundColor = UIColor.red
            
        }
        else
        {
            MyRestcells.btnOrderStatus.backgroundColor = UIColor.orange

        }
        
        
        

        MyRestcells.btnOrderStatus.setTitle(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "OrderStatusName") as! String), for: .normal)
        
        return MyRestcells
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        if(String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "OrderStatusName") as! String) == "Placed" )
        {
            return true
            
        }
        else
        {
            return false
            
        }
    
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        
        strOrderId = String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Id") as! NSNumber)

        if editingStyle == .delete
        {
            DelOrder() { responseObject, error in
                
                if (responseObject != nil)
                {
                    let json = JSON(responseObject!)
                    if let string = json.rawString()
                    {
                        
                        self.stopActivityAnimating()
                        
                        if let data = string.data(using: String.Encoding.utf8) {
                            do {
                                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSMutableDictionary
                                print("json ",json)
                                
                                if(json!["Success"] as? String  == "1")
                                {
                                    self.getMyOrderDetails()
                                    
                                    
                                }
                                else
                                {
                                    let dialog = ZAlertView(title: "Foodies4u",
                                                            message:"Order cannot be cancelled." ,
                                                            closeButtonText: "Ok",
                                                            closeButtonHandler: { (alertView) -> () in
                                                                self.tblRestData.reloadData()
                                                                alertView.dismissAlertView()
                                                                
                                    }
                                    )
                                    dialog.show()
                                    dialog.allowTouchOutsideToDismiss = true
                                    
                                }
                                
                                
                                
                                
                                
                            } catch {
                                print("Something went wrong")
                            }
                        }
                    }
                    else
                    {
                        self.stopActivityAnimating()
                        self.AlertMethod()
                    }
                    
                    
                }
                
                
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let strStatus:String = String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "OrderStatusName") as! String)
        
        if (strStatus == "In Progress"  || strStatus == "Placed")
        {
            let TOrderVC = self.storyboard?.instantiateViewController(withIdentifier: "TrackOrderViewController") as? TrackOrderViewController
            TOrderVC?.strStatus = strStatus
            self.navigationController?.pushViewController(TOrderVC!, animated: true)
        }
        else if (strStatus == "Cancelled")
        {
            
        }
        else  if (strStatus == "Completed")
        {
            let BillSummaryVC = self.storyboard?.instantiateViewController(withIdentifier: "BillSummaryViewController") as? BillSummaryViewController
            
            BillSummaryVC?.arrData = self.arrData.object(at: (indexPath as NSIndexPath).row)  as! NSMutableDictionary

            print("self.arrFoodItemData" , BillSummaryVC?.arrData)

            self.navigationController?.pushViewController(BillSummaryVC!, animated: true)
        }
        else
        {
            let OHVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryViewController") as? OrderHistoryViewController
            OHVC?.arrData = self.arrData.object(at: (indexPath as NSIndexPath).row)  as! NSMutableDictionary
            
            print("self.arrFoodItemData" , OHVC?.arrData )
            
            self.navigationController?.pushViewController(OHVC!, animated: true)
        }
    }
    
}
