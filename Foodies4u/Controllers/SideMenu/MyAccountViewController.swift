//
//  MyAccountViewController.swift
//  SidebarMenu
// Bhumika
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
//import GoogleMaps
import SwiftyJSON

class MyAccountViewController: parentViewController, NVActivityIndicatorViewable
{
    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet weak var btnMyPrefernece:UIButton!
    @IBOutlet weak var btnAddCredtiCard:UIButton!
    @IBOutlet weak var lblStoredCards:UILabel!
    @IBOutlet weak var viewUnderline:UIView!
    @IBOutlet weak var viewData:UIView!


    @IBOutlet var btnLogin : UIButton!
    @IBOutlet var btnUserProfile : UIButton!
    @IBOutlet var btnMyOrders : UIButton!
    @IBOutlet var btnFav : UIButton!
    
    @IBOutlet  var imgProfilePic: UIImageView!
    @IBOutlet var imgIcon : UIImageView!
    @IBOutlet  var arrowImageView: UIImageView!
    
    @IBOutlet var lblInfo : UILabel!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var tblData : UITableView!
    
    let tapImage = UITapGestureRecognizer()

    
    var cell2:Customcell!
    
    var strWSURL: NSString!
    var strUSERID: AnyObject!
    var strRestID: String!
    var strCreditCardId: String!

    var arrData: NSMutableDictionary!
    var arrUserProfileData: NSMutableArray!
    var arrUserAddress: NSArray!

    var arrFavData = NSMutableArray()
    var arrCreditCardData = NSArray()
    
    var iscrditCardPressed:Bool! = false

    
    
    var btntag:Int! = 0

    let arrIconList = ["Icon-Name.png", "Icon-DOB.png", "Icon-Email.png", "Icon-Mobile.png", "HomeMyAccountAddress", "workMyAccountAddress"]
    
    // MARK: - VIEW LIFE CYCLE

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        arrowImageView.isHidden = true
        lblStoredCards.isHidden = true
        viewUnderline.isHidden = true
        btnAddCredtiCard.isHidden = true
        
        
        // get user-id
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!
        
        ///sidemenu
        btnLogin.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.navigationController?.revealViewController().rearViewRevealWidth = 150
        
        if self.revealViewController() != nil
        {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(revealViewController().tapGestureRecognizer())
        }
        self.navigationController?.isNavigationBarHidden = true
        
        
         UserDefaults.standard .setValue("NO", forKey: "HOME")
         UserDefaults.standard .setValue("NO", forKey: "SHOWRESTLIST")
        print(self.viewData.frame.origin.y)
        print(self.tblData.frame.origin.y)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        arrUserProfileData = NSMutableArray()
        
        
        
        // getting user details
        self.navigationController?.isNavigationBarHidden = true
        if InternetConnection.isConnectedToNetwork()
        {
           // self.WSgetFavrest()
            self.WSgetUserDetails()
           // self.WSgetCreditCard()


        }
        
        
        

    }
    
        
    
    
    // MARK: - TABLE VIEW DATA SOURCE AND DELEGATE
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat
    {
        if btntag == 3
        {
         return 80.0
        }
        if btntag == 2
        {
            return 80.0
        }
        else
        {
            return 50.0

        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if btntag == 3
        {
            if arrFavData.count > 0
            {
              return arrFavData.count
            }
            else
            
            {
                return 1
            }
        }
        else if btntag == 2
        {
           
            if arrCreditCardData.count > 0
            {
                return arrCreditCardData.count
            }
            else
                
            {
                return 1
            }
            

        }
        else
        {
            if arrUserProfileData.count > 0
            {
                return 6
            }
            
            
        }
        return arrUserProfileData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell
    {
        
        
        
        if btntag == 3
        {
            cell2 = tableView.dequeueReusableCell(withIdentifier: "FavCell", for: indexPath) as! Customcell
            cell2.selectionStyle = .none
            
            if (arrFavData.count < 1)
            {
                cell2.lblNoRecordFound.isHidden = false
                cell2.lblRestName.isHidden = true
                cell2.lblRestAdd.isHidden = true
                
                cell2.btnCancel.isHidden = true
                cell2.ImgViewicon.isHidden = true

                tblData.separatorStyle = UITableViewCellSeparatorStyle.none


            }
            else
            {
                tblData.separatorStyle = UITableViewCellSeparatorStyle.singleLine

                cell2.lblNoRecordFound.isHidden = true
                cell2.lblRestName.isHidden = false
                cell2.lblRestAdd.isHidden = false
                cell2.btnCancel.isHidden = false
                cell2.ImgViewicon.isHidden = false
                
                cell2.lblRestName.text = (arrFavData.value(forKey: "Name") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
                
                cell2.lblRestName.sizeToFit()
                cell2.lblRestName.numberOfLines = 0
                
                
               
                cell2.lblRestAdd.text = (arrFavData.value(forKey: "Address1") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
                cell2.lblRestAdd.sizeToFit()
                cell2.lblRestAdd.numberOfLines = 0
                
                cell2.btnCancel.addTarget(self, action: #selector(MyAccountViewController.btnCancelRest(_:)), for: UIControlEvents.touchUpInside)
                cell2.btnCancel.tag = (indexPath as NSIndexPath).row

                
            }
            

        }
       else if btntag == 2
        {
            
            
            
            cell2 = tableView.dequeueReusableCell(withIdentifier: "CreditCardCell", for: indexPath) as! Customcell
            
            tblData.separatorStyle = UITableViewCellSeparatorStyle.none

            if (arrCreditCardData.count == 0)
            {
                cell2.lblNoRecordFound.isHidden = false
                
                cell2.lblCardNumber.isHidden = true
                cell2.lblCardType.isHidden = true
                
                cell2.imgVIewCredtiCard.isHidden = true
                cell2.imgViewDefault.isHidden = true
                
            }
            else
            {
                cell2.lblNoRecordFound.isHidden = true
                cell2.lblCardNumber.isHidden = false
                cell2.lblCardType.isHidden = false
                
                cell2.imgVIewCredtiCard.isHidden = false
                cell2.imgViewDefault.isHidden = false
                
                cell2.imgVIewCredtiCard.layer.borderColor = UIColor.init(colorLiteralRed: 205/255.0, green: 205/255.0, blue: 205/255.0, alpha: 1.0).cgColor
                cell2.imgVIewCredtiCard.layer.borderWidth = 1.0
                
                cell2.lblCardNumber.text = (arrCreditCardData.value(forKey: "CardNumber") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
                
                cell2.lblCardType.text = (arrCreditCardData.value(forKey: "CardType") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
                
                if(((arrCreditCardData.value(forKey: "IsDefault") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? Int) == 0)
                {
                    cell2.imgViewDefault.isHidden = true
                }
                else
                {
                    cell2.imgViewDefault.isHidden = false
                    
                }
                
                if cell2.lblCardType.text == "MasterCard"
                {
                    cell2.imgCardImage.image=UIImage(named: "mastercard.png")
                    
                    
                }
                else if cell2.lblCardType.text == "Amex"
                {
                    cell2.imgCardImage.image=UIImage(named: "amex.png")
                    
                    
                }
                else if cell2.lblCardType.text == "Visa"
                {
                    cell2.imgCardImage.image=UIImage(named: "visa.png")
                    
                    
                }
                else if cell2.lblCardType.text == "Discover"
                {
                    cell2.imgCardImage.image=UIImage(named: "discover.png")
                    
                    
                }
                else if cell2.lblCardType.text == "Diners Club"
                {
                    cell2.imgCardImage.image=UIImage(named: "diners.png")
                    
                    
                }
                else if cell2.lblCardType.text == "JCB"
                {
                    cell2.imgCardImage.image=UIImage(named: "jcb.png")
                    
                    
                }
                
                cell2.btnDelete.addTarget(self, action: #selector(MyAccountViewController.btnDeleteCardPressed(_:)), for: UIControlEvents.touchUpInside)
                cell2.btnDelete.tag = (indexPath as NSIndexPath).row
                
                cell2.selectionStyle = .none
                
            }
            
            
        }
        else
            
        {
            cell2 = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! Customcell
            tblData.separatorStyle = UITableViewCellSeparatorStyle.none
            cell2.selectionStyle = .none

            let imgLogo = UIImage(named:arrIconList[(indexPath as NSIndexPath).row])
            cell2.ImgViewicon.image = imgLogo
            
            
            
            //Name
            
            if (indexPath as NSIndexPath).row == 0 {
                if (arrUserProfileData.value(forKey: "FirstName") as AnyObject).object(at: 0) as! String == "" &&  ((arrUserProfileData.value(forKey: "LastName") as AnyObject).object(at: 0) as! String) == ""{
                    cell2.lblInfo.text = "Your Name"
                } else {
                    cell2.lblInfo.text = ((arrUserProfileData.value(forKey: "FirstName") as AnyObject).object(at: 0) as! String) + " " + ((arrUserProfileData.value(forKey: "LastName") as AnyObject).object(at: 0) as! String)
                    
                  

                }
                
            }
                
            //DOB

            else if (indexPath as NSIndexPath).row == 1 {
                if (arrUserProfileData.value(forKey: "DateOfBirth") as AnyObject).object(at: 0) as! String == ""{
                    cell2.lblInfo.text = "Your DOB"
                } else {
                    cell2.lblInfo.text = ((arrUserProfileData.value(forKey: "DateOfBirth") as AnyObject).object(at: 0) as! String)
                }
            }
                
            //Email

            else if (indexPath as NSIndexPath).row == 2 {
                if (arrUserProfileData.value(forKey: "Email") as AnyObject).object(at: 0) as! String == ""{
                    cell2.lblInfo.text = "Your Email ID"
                } else {
                    cell2.lblInfo.text = ((arrUserProfileData.value(forKey: "Email") as AnyObject).object(at: 0) as! String)
                    
                   

                }
            }
                
            //PhoneNumber

            else if (indexPath as NSIndexPath).row == 3 {
                cell2.lblInfo.text = ((arrUserProfileData.value(forKey: "PhoneNumber") as AnyObject).object(at: 0) as! String)
            }
                
                
            //Home Address
                
                
                
            else  if (indexPath as NSIndexPath).row == 4 {
                
                if((arrUserAddress.object(at: 0) as AnyObject).count  > 0)
                {
                    
                    if((arrUserAddress.object(at: 0) as AnyObject).count == 1)
                    {
                        
                        if((arrUserAddress.value(forKey: "AddressId") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? Int == 2
                        {
                            cell2.lblInfo.text = "Your Home Address"

                        }
                        else
                        {
                             cell2.lblInfo.text =  ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                        }
                        
                    }
                    else
                        
                    {
                    if ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String == ""
                    {
                        cell2.lblInfo.text = "Your Home Address"
                    } else {
                        cell2.lblInfo.text =  ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                    }
                    }
                }
                else
                {
                                           cell2.lblInfo.text = "Your Home Address"

                }
               /* if  (NSUserDefaults.standardUserDefaults().objectForKey("USERADDRESS")  == nil)
                {                        cell2.lblInfo.text = "Your Home Address"

                }
                else
                {
                    if arrUserAddress.valueForKey("Address").objectAtIndex(0).objectAtIndex(0) as? String == ""{
                        cell2.lblInfo.text = "Your Home Address"
                    } else {
                        cell2.lblInfo.text =  arrUserAddress.valueForKey("Address").objectAtIndex(0).objectAtIndex(0) as? String
                    }
                }*/
                
            }
            //Work Address
                
            else  if (indexPath as NSIndexPath).row == 5 {
                
                
                if((arrUserAddress.object(at: 0) as AnyObject).count  > 0)
                {
                    
                    
                    if((arrUserAddress.object(at: 0) as AnyObject).count == 1)
                    {
                        
                        if((arrUserAddress.value(forKey: "AddressId") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as?Int == 1
                        {
                            cell2.lblInfo.text = "Your Work Address"
                            
                        }
                        else
                        {
                            cell2.lblInfo.text =  ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                        }
                        
                    }
                    else
                        
                    {
                    if  ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String == ""{
                        cell2.lblInfo.text = "Your Work Address"
                    } else {
                        cell2.lblInfo.text = ((arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String
                    }
                    }
                }
                else
                {
                   cell2.lblInfo.text = "Your Work Address"
                    
                }
                
            }
            let maxLabelWidth: CGFloat = 100
            let neededSize: CGSize = cell2.lblInfo.sizeThatFits(CGSize(width: maxLabelWidth, height: 1000))
            cell2.lblInfo.sizeThatFits(neededSize)
        }
        
       
        
        return cell2
    }
    @IBAction func btnDeleteCardPressed(_ sender: UIButton )
        
    {
        
        let btnsendtag: UIButton = sender
        strCreditCardId = String(describing: ((self.arrCreditCardData.value(forKey: "Id") as! NSArray).object(at: btnsendtag.tag)) as! NSNumber)

        
        if(self.arrCreditCardData.count > 1)
        {
            
            let dialog = ZAlertView(title: "Foodies4u",
                                    message:  "Are you sure you want to remove it from Stored Credit Cards? ",
                                    isOkButtonLeft: true,
                                    okButtonText: " Delete",
                                    cancelButtonText: "No",
                                    okButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                                        self.DelCreditCard()
                                        
                },
                                    cancelButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true

        }
        else
        {
            let dialog = ZAlertView(title: "Foodies4u",
                                    message:  "Atleast one credit card is required to place order",
                                    isOkButtonLeft: true,
                                    okButtonText: " Delete",
                                    cancelButtonText: "No",
                                    okButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                                        self.DelCreditCard()
                                        
                },
                                    cancelButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
        
        
        
        print("image tapped",strCreditCardId)
        
            }
    
    @IBAction func btnCancelRest(_ sender: UIButton )
    
    {
        
         let btnsendtag: UIButton = sender
        
        
        
        let dialog = ZAlertView(title: "Foodies4u",
                                message: " Are you sure you want to remove it from Favourites?",
                                isOkButtonLeft: true,
                                okButtonText: "Yes",
                                cancelButtonText: "No",
                                okButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                                    self.strRestID = String(describing: ((self.arrFavData.value(forKey: "RestaurantId") as! NSArray).object(at: btnsendtag.tag)) as! NSNumber)
                                    
                                    print("image tapped",self.strRestID)
                                    
                                    if InternetConnection.isConnectedToNetwork()
                                    {
                                        self.startIndicatorCircleAnimating()
                                        
                                        self.makeRestaurantFavUnFav() { responseObject, error in
                                            
                                            if (responseObject != nil)
                                            {
                                                let json = JSON(responseObject!)
                                                if let string = json.rawString()
                                                {
                                                    
                                                    
                                                    if let data = string.data(using: String.Encoding.utf8)
                                                    {
                                                        do {
                                                            
                                                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                                                            print("json ",json)
                                                            if(json!["Success"] as? String  == "1")
                                                            {
                                                                
                                                                self.getfavrest()
                                                                    { responseObject, error in
                                                                        if (responseObject != nil)
                                                                        {
                                                                            let json = JSON(responseObject!)
                                                                            
                                                                            if let string = json.rawString()
                                                                            {
                                                                                self.stopActivityAnimating()
                                                                                
                                                                                if let data = string.data(using: String.Encoding.utf8) {
                                                                                    do {
                                                                                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                                                                                        //print("json ",json)
                                                                                        
                                                                                        let successVal:String = json!["Success"] as! String
                                                                                        if (successVal == "1")
                                                                                        {
                                                                                            
                                                                                            self.arrFavData = json!["Data"] as! NSMutableArray
                                                                                            
                                                                                            print(self.arrFavData)
                                                                                            self.tblData.reloadData()
                                                                                            
                                                                                        }else
                                                                                        {
                                                                                            self.arrFavData = []
                                                                                            self.tblData.reloadData()
                                                                                        }
                                                                                        
                                                                                        
                                                                                        
                                                                                    } catch {
                                                                                        print("Something went wrong")
                                                                                    }
                                                                                }
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                self.stopActivityAnimating()
                                                                                self.AlertMethod()
                                                                            }
                                                                        }
                                                                        
                                                                        
                                                                }
                                                                
                                                            }
                                                        }
                                                            
                                                        catch {
                                                            print("Something went wrong")
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                                
                                            {
                                                self.stopActivityAnimating()
                                                self.AlertMethod()
                                            }
                                            
                                        }
                                    }
                                    
                                    
            },
                                cancelButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
            }
        )
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true

        
    }
    
   
    //MARK: DELETE FAVURITE REST FUNCITON

    func makeRestaurantFavUnFav(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        //let size = CGSize(width: 80, height:80)
        
        //startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        
        strWSURL=GETWSURL()
        let param1 : [ String : AnyObject] = [
            
            "CustomerId": strUSERID,
            "RestaurantId":strRestID as AnyObject
            
        ]
       makeCall(section: strURL as String + "Favourite?",param:param1,completionHandler: completionHandler)
    }
    
    func deleteCreditCard(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        let size = CGSize(width: 80, height:80)
        
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        
        strWSURL=GETWSURL()
        let param1 : [ String : AnyObject] = [
            
            "CustomerId": strUSERID,
            "Id":strCreditCardId as AnyObject
            
        ]
        makeCall(section: strURL as String + "DeleteCreditCard?",param:param1,completionHandler: completionHandler)
    }
    
    
    //MARK: GET USER DETAILS FUNCITON
    
    
    func WSgetCreditCard()
    {
        
        let size = CGSize(width: 80, height:80)
        
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        getStoredCreditCard()
            {
                responseObject, error in
                
                
                if (responseObject != nil)
                {
                    
                    
                    self.btnAddCredtiCard.isHidden = false
                    self.btnMyPrefernece.isHidden = true
                     self.lblStoredCards.isHidden = false
                     self.viewUnderline.isHidden = false
                    
                    
                let json = JSON(responseObject!)
                
                if let string = json.rawString()
                {
                    
                    self.stopActivityAnimating()
                    
                    if let data = string.data(using: String.Encoding.utf8) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            print("json ",json)
                            
                            let successVal:String = json!["Success"] as! String
                            if (successVal == "1")
                            {
                                
                                self.arrCreditCardData = json!["Data"] as! NSArray
                                print("credit card" , self.arrCreditCardData)
                                self.animateTable()

                                
                            }else {
                                self.animateTable()

                            }
                            
                            
                            
                            
                            
                        } catch {
                            print("Something went wrong")
                        }
                    }
                    
                }
                }
                else
                {
                    self.stopActivityAnimating()
                    self.AlertMethod()
                }
        }
    }
    
    func WSgetFavrest()
    {
        getfavrest()
            { responseObject, error in
                
                if (responseObject != nil)
                {
                let json = JSON(responseObject!)
                
                if let string = json.rawString()
                {
                    
                    self.stopActivityAnimating()
                    
                    if let data = string.data(using: String.Encoding.utf8) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            //print("json ",json)
                            
                            let successVal:String = json!["Success"] as! String
                            if (successVal == "1")
                            {
                                self.arrFavData = json!["Data"] as! NSMutableArray
                                print(self.arrFavData)
                                self.animateTable()


                                
                            }else {
                                self.animateTable()

                            }
                            
                            
                            
                        } catch {
                            print("Something went wrong")
                        }
                    }
                    
                }
                }
                    else
                {
                    self.stopActivityAnimating()
                    self.AlertMethod()
                  }
    }
    }
     func WSgetUserDetails()
     {
        getUserDetails() { responseObject, error in
            if (responseObject != nil)
            {
            let json = JSON(responseObject!)
            if let string = json.rawString()
            {
                //Do something you want
                //print(string)
                self.stopActivityAnimating()
                
                if let data = string.data(using: String.Encoding.utf8) {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                        //print("json ",json)
                        
                        let successVal:String = json!["Success"] as! String
                        if (successVal == "1") {
                            
                            self.arrData = json!["Data"] as! NSMutableDictionary
                            
                            self.arrUserProfileData.add(self.arrData)
                            self.lblName.text = ((self.arrUserProfileData.value(forKey: "FirstName") as AnyObject).object(at: 0) as! String) + " " + ((self.arrUserProfileData.value(forKey: "LastName") as AnyObject).object(at: 0) as! String)
                            
                            print("arrUserProfileData ",self.arrUserProfileData)
                            
                            
                            UserDefaults.standard.set(self.lblName.text, forKey: "USERNAME")
                            UserDefaults.standard.set((self.arrUserProfileData.value(forKey: "Email") as AnyObject).object(at: 0) as! String, forKey: "EMAILID")
                            
                            // display profile picture
                            
                            self.arrUserAddress = self.arrUserProfileData.value(forKey: "CustomerAddress") as! NSArray
                            print("CustomerAddress ",self.arrUserAddress)
                            
                            
                            //self.perform(#selector(self.animateTable), with: nil, afterDelay: 0.5)
                            // display profile picture
                            if let imageData = UserDefaults.standard.object(forKey: "USERIMAGE"),
                                let image = UIImage(data: imageData as! Data)
                            {
                                self.imgProfilePic.image = image
                                self.imgProfilePic.layer.borderWidth = 1.0
                                self.imgProfilePic.layer.borderColor = UIColor.lightGray.cgColor
                                self.imgProfilePic.layer.cornerRadius = image.size.width/2
                                
                                
                            }
                            
                            self.tblData.reloadData()
                            self.animateTable()

                            
                        }else {
                            
                            self.animateTable()

                            let dialog = ZAlertView(title: "Foodies4u",
                                                    message:  "No data found",
                                                    closeButtonText: "Ok",
                                                    closeButtonHandler: { (alertView) -> () in
                                                        
                                                        alertView.dismissAlertView()
                                                        self.navigationController!.popViewController(animated: false)
                                }
                            )
                            dialog.show()
                            dialog.allowTouchOutsideToDismiss = true
                        }
                        
                        
                        
                    } catch {
                        print("Something went wrong")
                    }
                }
                
            }
            }
            else
            {
                self.stopActivityAnimating()
                self.AlertMethod()
            }
        }
    }
    
    func getUserDetails(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        //let size = CGSize(width: 80, height:80)
        
        //startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        strWSURL=GETWSURL()
        
        let param1 : [ String : AnyObject] = [
            "customerId":strUSERID
            ]
        
       makeCall(section: strURL as String + "GetCustomerData?",param:param1,completionHandler: completionHandler)
        
    }
    
    //MARK: GET FAV RESTAURANT FUNCITON
    
    func getfavrest(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        
        
        strWSURL=GETWSURL()
        
        let lat  = UserDefaults.standard.double(forKey: "LATITUDE")
        let long = UserDefaults.standard.double(forKey: "LONGITUDE")
        
        
        let param2 : [ String : AnyObject] = [
            "customerid":strUSERID,
            "Latitude": lat as AnyObject,
            "Longitude":long as AnyObject ,
            "sortby":"ASC" as AnyObject
        ]
        

       makeCall(section: strURL as String + "GetFavouriteList?",param:param2,completionHandler: completionHandler)
        
        
    }
    
    //MARK: GET STORED CREDIT CARD DETAILS

    func getStoredCreditCard(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        
        strWSURL=GETWSURL()
        
        
        
        let param2 : [ String : AnyObject] = [
            "CustomerId":strUSERID,
          
        ]
        
        
        makeCall(section: strURL as String + "GetCustomerCreditDataByCustomerId?",param:param2,completionHandler: completionHandler)
        
        
    }
    
    
    //MARK: BUTTON CLICKS
    
    func DelCreditCard()
    {
        if InternetConnection.isConnectedToNetwork()
        {
            self.startIndicatorCircleAnimating()
            
            deleteCreditCard() { responseObject, error in
                
                if (responseObject != nil)
                {
                    
                    let json = JSON(responseObject!)
                    if let string = json.rawString()
                    {
                        
                        
                        if let data = string.data(using: String.Encoding.utf8)
                        {
                            do {
                                
                                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                                print("json ",json)
                                if(json!["Success"] as? String  == "1")
                                {
                                    
                                    self.getStoredCreditCard()
                                        { responseObject, error in
                                            if (responseObject != nil)
                                            {
                                                let json = JSON(responseObject!)
                                                
                                                if let string = json.rawString()
                                                {
                                                    self.stopActivityAnimating()
                                                    
                                                    if let data = string.data(using: String.Encoding.utf8) {
                                                        do {
                                                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                                                            //print("json ",json)
                                                            
                                                            let successVal:String = json!["Success"] as! String
                                                            if (successVal == "1")
                                                            {
                                                                
                                                                self.arrCreditCardData = json!["Data"] as! NSMutableArray
                                                                
                                                                print(self.arrCreditCardData)
                                                                self.tblData.reloadData()
                                                                
                                                            }else
                                                            {
                                                                self.arrCreditCardData = []
                                                                self.tblData.reloadData()
                                                            }
                                                            
                                                            
                                                            
                                                        } catch {
                                                            print("Something went wrong")
                                                        }
                                                    }
                                                    
                                                }
                                            }
                                            else
                                            {
                                                self.stopActivityAnimating()
                                                self.AlertMethod()
                                            }
                                    }
                                    
                                    
                                }
                                
                            }
                                
                            catch {
                                self.stopActivityAnimating()
                                
                                print("Something went wrong")
                            }
                        }
                    }
                }
                else
                {
                    self.stopActivityAnimating()
                    self.AlertMethod()
                    
                }
                
            }
        }

    }
    
    @IBAction func btnEditProfileClicked(_ sender: UIButton){
        
        let editProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "editProfileViewController") as? editProfileViewController
        editProfileVC?.arrUserProfileData = arrUserProfileData!
       editProfileVC?.arrUserAddress = arrUserAddress
        self.navigationController?.pushViewController(editProfileVC!, animated: true)
    }
    
    @IBAction func btnMyPreferencesClicked(_ sender: UIButton){
        
        let editProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "MyPreferencesViewController") as? MyPreferencesViewController
        
        self.navigationController?.navigationBar.isHidden=false
        self.navigationController?.pushViewController(editProfileVC!, animated: true)
    }
    @IBAction func btnAddCardPressed(_ sender: UIButton)
    {

        let editProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "AddCCDetailViewController") as? AddCCDetailViewController
        
        self.navigationController?.pushViewController(editProfileVC!, animated: true)
    }
    @IBAction func buttonHighlight(_ sender: UIButton)
    {
        
        let btnsendtag: UIButton = sender
        
        btntag = btnsendtag.tag
        
        
        print("btntag" ,btntag)
        
        
        arrowImageView.isHidden = true

        
        if btnsendtag.tag == 2
        {
            
            arrowImageView.frame=CGRect(x: btnMyOrders.frame.origin.x+btnMyOrders.frame.width/2.5, y: arrowImageView.frame.origin.y
                , width: arrowImageView.frame.size.width, height: arrowImageView.frame.size.height)
            btnMyOrders.backgroundColor = UIColor(red: 70.0/255, green: 22.0/255, blue: 20.0/255, alpha: 1.0)
            btnFav.backgroundColor = UIColor(red: 37.0/255, green:19.0/255, blue: 17.0/255, alpha: 1.0)
            btnUserProfile.backgroundColor = UIColor(red: 37.0/255, green:19.0/255, blue: 17.0/255, alpha: 1.0)
            tblData.frame=CGRect(x: tblData.frame.origin.x, y: tblData.frame.origin.y + 25, width: tblData.frame.size.width, height: tblData.frame.size.height)
            
            iscrditCardPressed = true
            print(self.viewData.frame.origin.y)
            print(self.tblData.frame.origin.y)
            
            if InternetConnection.isConnectedToNetwork()
            {
                self.WSgetCreditCard()
            }

            //self.animateTable()
            //fdsfddfdfdfs
        }
        else if btnsendtag.tag == 1
        {
            
            btnAddCredtiCard.isHidden = true
            btnMyPrefernece.isHidden = false
            lblStoredCards.isHidden = true
            viewUnderline.isHidden = true
            arrowImageView.frame=CGRect(x: btnUserProfile.frame.width/2.5, y: arrowImageView.frame.origin.y
                , width: arrowImageView.frame.size.width, height: arrowImageView.frame.size.height)
            
            btnUserProfile.backgroundColor = UIColor(red: 70.0/255, green: 22.0/255, blue: 20.0/255, alpha: 1.0)
            btnFav.backgroundColor = UIColor(red: 37.0/255, green:19.0/255, blue: 17.0/255, alpha: 1.0)
            btnMyOrders.backgroundColor = UIColor(red: 37.0/255, green:19.0/255, blue: 17.0/255, alpha: 1.0)
            print(self.viewData.frame.origin.y)
            print(self.tblData.frame.origin.y)
            
            if(iscrditCardPressed == true)
            {
               tblData.frame=CGRect(x: tblData.frame.origin.x, y: tblData.frame.origin.y - 20, width: tblData.frame.size.width, height: tblData.frame.size.height)
                iscrditCardPressed = false
            }
            else
            {
                tblData.frame=CGRect(x: tblData.frame.origin.x, y: tblData.frame.origin.y, width: tblData.frame.size.width, height: tblData.frame.size.height)

            }
            if InternetConnection.isConnectedToNetwork()
            {
                self.WSgetUserDetails()
                
            }

            //animateTable()

        }
        else
        {
            btnAddCredtiCard.isHidden = true
            btnMyPrefernece.isHidden = true
            lblStoredCards.isHidden = true
            viewUnderline.isHidden = true
            arrowImageView.frame=CGRect(x: btnUserProfile.frame.width+btnUserProfile.frame.width+btnUserProfile.frame.width/2.5, y: arrowImageView.frame.origin.y
                , width: arrowImageView.frame.size.width, height: arrowImageView.frame.size.height)
            btnFav.backgroundColor = UIColor(red: 70.0/255, green: 22.0/255, blue: 20.0/255, alpha: 1.0)
            btnMyOrders .backgroundColor = UIColor(red: 37.0/255, green:19.0/255, blue: 17.0/255, alpha: 1.0)
            btnUserProfile.backgroundColor = UIColor(red: 37.0/255, green:19.0/255, blue: 17.0/255, alpha: 1.0)
            
            if(iscrditCardPressed == true)
            {
                tblData.frame=CGRect(x: tblData.frame.origin.x, y: tblData.frame.origin.y - 20, width: tblData.frame.size.width, height: tblData.frame.size.height)
                iscrditCardPressed = false
            }
            else
            {
                tblData.frame=CGRect(x: tblData.frame.origin.x, y: tblData.frame.origin.y, width: tblData.frame.size.width, height: tblData.frame.size.height)
                
            }
            
            if InternetConnection.isConnectedToNetwork()
            {
                self.WSgetFavrest()
                
                
            }
            
           


            
        }
        


    }
    
    //MARK: ANIMATE TABLE FUNCTION
    func animateTable()
    {
        tblData.reloadData()
        
        let cells = tblData.visibleCells
        let tableHeight: CGFloat = tblData.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations:
                {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0);
                }, completion: nil)
            
            index += 1
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
