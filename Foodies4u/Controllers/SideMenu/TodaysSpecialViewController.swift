//
//  Today'sSpecialViewController.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 07/07/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import UIKit

class TodaysSpecialViewController: parentViewController {
    
    @IBOutlet weak var menuButton:UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarUI()
        
        self.revealViewController().rearViewRevealWidth = 150
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
}
}
