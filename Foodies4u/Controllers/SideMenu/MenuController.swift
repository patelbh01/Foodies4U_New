//
//  MenuController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit
import Foundation


class MenuController: UITableViewController{
    
    @IBOutlet  weak var btnBadge: UIButton!
    var arrFoodItemDetail:NSMutableArray!
    var badge:String!
    var FoodItemDetails = FoodItem()
    var arrQty:NSMutableArray! = []
    var FoodItemdetail:FoodItemInfo!
    var BadgeCount:Int!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        //update badges
        
              
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //model for fooditem to send data into database
       
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Table view data source & Delegates
    

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        //let deviceType = UIDevice.currentDevice().deviceType
        //print(deviceType)

        
      if DeviceType.IS_IPHONE_6P
      {
        return 105
      }
        else if DeviceType.IS_IPHONE_4_OR_LESS
      {
          return 90
        }
      else if DeviceType.IS_IPHONE_5
      {
          return 80
        }
      else if DeviceType.IS_IPHONE_6
      {
        return 95
        }
        /*switch deviceType
        {
        case .IPhone4:
            return 65

        case .IPhone5:
            return 80

            
        case .IPhone5S:
            return 80

        case .IPhone6:
            return 95
            
        case .IPhone6Plus:
            return 105

        case .IPhone6S:
            return 95

        case .IPhone6SPlus:
            return 105

        default:
           print("")
        }*/
       return 105
    }
    
      override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
            
            if ((indexPath as NSIndexPath).row ==  0)
            {
                UserDefaults.standard .setValue("NO", forKey: "SHOWRESTLIST")
                UserDefaults.standard .setValue("NO", forKey: "HOME")
                UserDefaults.standard.set("NO", forKey: "ZOOMED")



            }
            
            if ((indexPath as NSIndexPath).row ==  2)
            {
                UserDefaults.standard .setValue("YES", forKey: "MYCARTFROMSIDEMENU")
                
                
            }
            
            /*if((indexPath as NSIndexPath).row ==  3)
            {
                if(BadgeCount == 0)
                {
                let dialog:ZAlertView = ZAlertView(title: "Foodies4u",
                                                   message: "Your cart is empty"
                    ,
                                                   closeButtonText: "Ok",
                                                   closeButtonHandler: { (alertView) -> () in
                                                    alertView.dismissAlertView()
                                                    
                                                    
                    }
                    
                )
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
            }*/
            }

    }
    
    /* override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath)
    {
        let cellToDeSelect:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        print("cellToDeSelect",indexPath.row)
        
        
        cellToDeSelect.contentView.backgroundColor = UIColor.clearColor()
    }*/

    
   /*  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath as IndexPath) as UITableViewCell
        
        
        if(indexPath.row == 2)
        {
            
            FoodItemdetail  = FoodItemInfo()
            
            arrFoodItemDetail = NSMutableArray()
            
            arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
            
            
            if(arrFoodItemDetail.count > 0 )
            {
                
                self.view.addSubview(btnBadge)
                
                self.btnBadge.layer.backgroundColor = UIColor.red .cgColor
                self.btnBadge.layer.borderWidth = 1.0
                self.btnBadge.layer.cornerRadius = self.btnBadge.frame.width / 2
                // self.btnBadge.layer.borderColor = UIColor.white .cgColor
                
                arrQty.removeAllObjects()
                for i in 0...arrFoodItemDetail.count - 1
                {
                    FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                    
                    
                    arrQty.add(FoodItemdetail.streQuantity)
                    
                    
                    
                    
                    
                    
                }
                
                
                
                var sum : Int = 0
                
                for i in 0...arrQty.count - 1
                {
                    sum = sum + Int(arrQty[i] as! String)!
                }
                
                print("sum",sum)
                
                BadgeCount = sum
                
                self.btnBadge.setTitle(String(BadgeCount), for: .normal)
                
                
                
                
                
            }
            else
            {
                btnBadge.isHidden=true
                BadgeCount = 0
                
            }

        }
        else{
            btnBadge.isHidden=true

        }

        // Configure the cell...

        return cell
    }*/
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */


