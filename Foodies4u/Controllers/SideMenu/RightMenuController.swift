//
//  RightMenuController.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 22/08/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}




class RightMenuController: parentViewController,NVActivityIndicatorViewable,UITableViewDelegate,UITableViewDataSource,SWRevealViewControllerDelegate
{
    
    var MyRestcells:MyResListCell!
    var headerView:CollapsibleTableViewHeader!
    
    @IBOutlet weak var tblFilters: UITableView!
    
    //for location lat-long
    var latitude:Double!
    var longitude:Double!
    
    
    var strWSURL: NSString!
    var strUSERID: AnyObject!
    var arrData: NSMutableDictionary!
    
      //for array filters
    let arrName: NSMutableArray = ["  Open Now","  Food Type","  Category","  Distance","  Cuisines","  Offers","  Price",""]
    var arrOpenNow: NSArray!
    var arrFoodType: NSArray!
    var arrCategory: NSArray!
    var arrDistance: NSArray!
    var arrCuisines: NSArray!
    var arrOffers: NSArray!
    var arrPrice: NSArray!
    var arrFiltersData: [AnyObject] = []
    var arrItems: [Bool] = []
    var arrSelectedCuisines:NSMutableArray = []
    var arrSelectedOffers:NSMutableArray = []

    
    
    //for filters
    var FoodType: Int! = -1
    var strMinPrice: String! = ""
    var strMaxPrice: String! = ""
    var Category: Int! = -1
    var strCuisines: String! = ""
    var strSortBy: String! = ""
    var strType: String! = ""
    var strDistance: String! = "5"
    var strOffers: String! = ""
    var OpenNow: String! = "false"
    var strSearchType: String! = ""
    var strFoodType: String! = ""

    var strSearchCategory: String! = ""

    
    //To get section
    var SECTION: Int!
    var TapBtnRow:Int! = -1
    var TapBtn:Int! = 0
    
    
    var param1 = [String: AnyObject]()
    

    
    //MARK: LIFECYCLE METHODS

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.navigationBar.isHidden=true
        
        //location from nsuserdefaults
        latitude = UserDefaults.standard.double(forKey: "LATITUDE")
        longitude = UserDefaults.standard.double(forKey: "LONGITUDE")
        
        
        print(latitude)
        print(longitude)
        
        
        
        //array for expandabletableview
        for i in 0..<arrName.count
        {
            
            arrItems.append(false)
        }
        if InternetConnection.isConnectedToNetwork()
        {
            self.view.endEditing(true)
            
            getFiltersDetails() { responseObject, error in
                
                
                if (responseObject != nil)
                {
                let json = JSON(responseObject!)
                if let string = json.rawString()
                {
                    //Do something you want
                    //print(string)
                    self.stopActivityAnimating()
                    
                    if let data = string.data(using: String.Encoding.utf8) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            print("json ",json)
                            
                            if( (json!["Success"] as? String)!  == "1")
                            {
                                
                                
                                self.arrData = json!["Data"] as! NSMutableDictionary
                                print("bhumika",self.arrData)
                                
                                self.arrCategory = self.arrData.value(forKey: "category") as! NSArray
                                self.arrCuisines=self.arrData.value(forKey: "cuisines")  as! NSArray
                                self.arrDistance=self.arrData.value(forKey: "distance")  as! NSArray
                                self.arrFoodType=self.arrData.value(forKey: "foodtype")  as! NSArray
                                
                                self.arrOffers=self.arrData.value(forKey: "offers")  as! NSArray
                                
                                print("arrDistance",self.arrDistance)
                                print("arrCategory",self.arrCategory)
                                print("arrCuisines",self.arrCuisines)
                                print("arrFoodType",self.arrFoodType)
                                print("arrDistance",self.arrDistance)
                                self.tblFilters.separatorStyle = .none
                                
                                
                            }
                                
                            else
                            {
                                
                                
                              
                                let dialog = ZAlertView(title: "Foodies4u",
                                                        message:  "No data found",
                                                        closeButtonText: "Ok",
                                                        closeButtonHandler: { alertView
                                                            in
                                                            alertView.dismissAlertView()

                                                            
                                    }
                                    
                                )
                                dialog.show()
                                dialog.allowTouchOutsideToDismiss = true
                                
                            }
                            
                            
                            
                        } catch {
                            print("Something went wrong")
                        }
                    }
                    
                }
                }
                else
                {
                    self.stopActivityAnimating()
                    self.AlertMethod()
                }
            }
        }

        
        
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //dismis keyboard any where you tap on the screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        UserDefaults.standard .set(false, forKey: "SEARCHDONEANDAPPLYFILETR")

       /* self.revealViewController().frontViewController.view.userInteractionEnabled = false
        revealViewController().delegate = self*/
        //view.addGestureRecognizer(revealViewController().tapGestureRecognizer())
        
        //call webservice for get all filters data 
        
        
        
    }
    //MARK: GET FILTERS WEBSERVICES
    
    func getFiltersDetails(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        
        
        strWSURL=GETWSURL()
        
        let param1 : [ String : AnyObject] = [
            "":"" as AnyObject
        ]
        
       makeCall(section: strURL as String + "GetFilterList?",param:param1,completionHandler: completionHandler)
        // self.callWebservice("http://ble.intransure.biz:8080/oas/api/helpdeskLeaveStatusCounter/?", parameters: param)
        
    }
    
    
    //MARK: TABLEVIEW DATASOURCE & DELEGATES METHODS
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if CBool(arrItems[section] as NSNumber)
        {
            
            return  arrFiltersData.count
        }
        else {
            return 0
        }
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        
        return arrName.count;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        MyRestcells = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)  as! MyResListCell
        
        
        let manyCells = CBool(arrItems[(indexPath as NSIndexPath).section] as NSNumber)
        
        
        if (!manyCells)
        {
            
        }
        else
            
        {
            MyRestcells.viewLine.isHidden=true
            MyRestcells.txtFieldPrice.isHidden=true
            MyRestcells.btnRadioBtn.isHidden=false
            MyRestcells.btnFilterCatName.isHidden=false
            
            MyRestcells.btnFilterCatName.setTitle(arrFiltersData[(indexPath as NSIndexPath).row] as? String, for: UIControlState())
            MyRestcells.btnFilterCatName.addTarget(self, action: #selector(OpenMoreFilters(_:)), for: UIControlEvents.touchUpInside)
            MyRestcells.btnFilterCatName.tag=(indexPath as NSIndexPath).row
            if((indexPath as NSIndexPath).section==4)
            {
                MyRestcells.sliderDistance.isHidden = true
                MyRestcells.lblMinDistance.isHidden = true
                MyRestcells.lblMaxDistance.isHidden = true
                if (arrSelectedCuisines.contains(String(describing: (self.arrCuisines.object(at: (indexPath as NSIndexPath).row) as AnyObject) .value(forKey: "Id") as! NSNumber)))
                {
                    
                    MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
                    
                }
                else
                {
                    MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                }
            }
            else
                
            {
                if ((indexPath as NSIndexPath).section == 1 /*&& TapBtnRow == indexPath.row*/)
                {
                    if FoodType == -1
                    {
                        MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                    }
                    else
                    {
                        if FoodType == (self.arrFoodType.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Id") as! Int
                        {
                            MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active"), for: UIControlState())
                            
                        }
                        else
                            
                        {
                            MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                        }
                      //  MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active"), forState: .Normal)
                    }
                }
                
               else if ((indexPath as NSIndexPath).section == 2  /*&& TapBtnRow == indexPath.row*/)
                {
                    if Category == -1
                    {
                        MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                    }
                    else
                    {
                         if Category == (self.arrCategory.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Id") as! Int
                         {
                            MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active"), for: UIControlState())

                         }
                        else
                        
                         {
                            MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                         }
                    }
                    
                }
                    
                else if ((indexPath as NSIndexPath).section == 3  /*&& TapBtnRow == indexPath.row*/)
                {
                    /*if strDistance.characters.count == 0
                    {
                        MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), forState: .Normal)
                    }
                    else
                    {
                        if strDistance == self.arrDistance.objectAtIndex(indexPath.row).valueForKey("Name") as! String
                        {
                            MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active"), forState: .Normal)
                            
                        }
                        else
                            
                        {
                            MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), forState: .Normal)
                        }
                    
                    myrest
                     //   MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active"), forState: .Normal)
                    }*/
                }
                    
                else if ((indexPath as NSIndexPath).section == 5  /*&& TapBtnRow == indexPath.row*/)
                {
                    /*if strOffers.characters.count == 0
                    {
                        MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), forState: .Normal)
                    }
                    else
                    {
                        if strOffers == String(self.arrOffers.objectAtIndex(indexPath.row).valueForKey("Id") as! NSNumber)
                        {
                           MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active"), forState: .Normal)
                        }
                        else
                        
                        {
                            MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), forState: .Normal)
                            
                        }
                    }*/
                    
                    if (arrSelectedOffers.contains(String(describing: (self.arrOffers.object(at: (indexPath as NSIndexPath).row) as AnyObject) .value(forKey: "Id") as! NSNumber)))
                    {
                        MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
                        
                    }
                    else
                    {
                        MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                    }
                }
                
                else
                    
                {
                     MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                }
                
               if ((indexPath as NSIndexPath).section == 3)
               {
                MyRestcells.sliderDistance.isHidden = false
                  MyRestcells.btnFilterCatName.isHidden = true
                MyRestcells.sliderDistance.maximumValue = 50

                MyRestcells.sliderDistance!.addTarget(self, action: #selector(RightMenuController.sliderValueChanged(_:)), for: .valueChanged)
                
                MyRestcells.lblMinDistance.isHidden = false
                MyRestcells.lblMaxDistance.isHidden = false
               MyRestcells.btnRadioBtn.isHidden = true

                
                }

                else
                {
                    MyRestcells.sliderDistance.isHidden = true
                    MyRestcells.btnFilterCatName.isHidden = false
                    MyRestcells.lblMinDistance.isHidden = true
                    MyRestcells.lblMaxDistance.isHidden = true
                    MyRestcells.btnRadioBtn.isHidden = false

                }
                
            }
            
            
            
            MyRestcells.btnRadioBtn.addTarget(self, action: #selector(OpenMoreFilters(_:)), for: UIControlEvents.touchUpInside)
            MyRestcells.btnClickRow.addTarget(self, action: #selector(OpenMoreFilters(_:)), for: UIControlEvents.touchUpInside)
            MyRestcells.btnRadioBtn.tag=(indexPath as NSIndexPath).row
            MyRestcells.btnClickRow.tag=(indexPath as NSIndexPath).row

        }

      
        
            //.text = arrFiltersData[indexPath.row] as? String
        
        if (indexPath as NSIndexPath).section==6
        {
            MyRestcells.btnRadioBtn.isHidden=true
            MyRestcells.btnFilterCatName.isHidden=true
            MyRestcells.txtFieldPrice.isHidden=false
            MyRestcells.viewLine.isHidden=false
             MyRestcells.btnClickRow.isHidden = true
            
            let paddingView = UIView(frame:CGRect(x: 0, y: 0,width: 10, height: 30))
            MyRestcells.txtFieldPrice.leftView=paddingView;
            MyRestcells.txtFieldPrice.leftViewMode = UITextFieldViewMode.always
            
            if((indexPath as NSIndexPath).row == 0)
            {
                let placeHolder=NSAttributedString(string: "Min Price", attributes:    [NSForegroundColorAttributeName :UIColor.white])
                MyRestcells.txtFieldPrice .attributedPlaceholder=placeHolder
                MyRestcells.txtFieldPrice.textColor=UIColor.white
            }
            else
            {
                let placeHolder=NSAttributedString(string: "Max Price", attributes:    [NSForegroundColorAttributeName : UIColor.white])
                MyRestcells.txtFieldPrice .attributedPlaceholder=placeHolder
                MyRestcells.txtFieldPrice.textColor=UIColor.white
                
            }
            MyRestcells.txtFieldPrice.tag=(indexPath as NSIndexPath).row
        }
        
        
        
        
        return MyRestcells
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
        arrItems[(indexPath as NSIndexPath).section] = false
        
        
        tblFilters.reloadSections(IndexSet(integer:(indexPath as NSIndexPath).section), with: .automatic)
        
    }
    
    // tableview header for category of filters
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        headerView = tblFilters.dequeueReusableCell(withIdentifier: "header") as! CollapsibleTableViewHeader!
        let containerView = UIView(frame:headerView.frame)
        containerView.addSubview(headerView)

        
        if(section % 2 == 0)
        {
            headerView.backgroundColor = UIColor.init(red: 60.0/255.0, green: 21.0/255.0, blue: 19.0/255.0, alpha: 1.0)
        }
        else
            
        {
            headerView.backgroundColor = UIColor.init(red: 70.0/255.0, green:22.0/255.0, blue: 20.0/255.0, alpha: 1.0)
        }
        
        
        
        headerView.btnTitle.setTitle(arrName[section] as? String, for: UIControlState())
        
        if CBool(arrItems[section] as NSNumber)
        {
            headerView.btnTitle.setTitleColor(UIColor.init(red: 227.0/255.0, green: 123.0/255.0, blue: 4.0/255.0, alpha: 1.0), for: UIControlState())
        }
        else
            
        {
            headerView.btnTitle.setTitleColor(UIColor.white , for: UIControlState())
        }
        
        
        
        if section==0
        {
            headerView.btnApply.isHidden=true
            headerView.btnTitle.addTarget(self, action: #selector(ClickOpenNow(_:)), for: UIControlEvents.touchUpInside)
            headerView.btnCheckBox.addTarget(self, action: #selector(ClickOpenNow(_:)), for: UIControlEvents.touchUpInside)
            
            
            if(TapBtn % 2 == 0)
                
            {
                headerView.btnCheckBox.setImage(UIImage(named: "CheckBox-2.png"), for: UIControlState())
                
                OpenNow = "false"
            }
            else
                
            {
                headerView.btnCheckBox.setImage(UIImage(named: "CheckBox-1.png"), for:UIControlState())
                OpenNow = "true"
                
            }
        }
        else
            
        {
            if section == 7
                
            {
                headerView.btnApply.isHidden=false
                headerView.btnApply.addTarget(self, action: #selector(btnApplyPressed(_:)), for: UIControlEvents.touchUpInside)
            }
            else
            {
                if CBool(arrItems[section] as NSNumber)
                    
                {
                    headerView.btnCheckBox.setImage(UIImage(named: "Arrow-Up.png"), for: UIControlState())
                }
                else
                {
                    headerView.btnCheckBox.setImage(UIImage(named: "Arrow-Down.png"), for:UIControlState())
                }
                
                
                headerView.btnTitle.addTarget(self, action: #selector(handleTap(_:)), for: UIControlEvents.touchUpInside)
                headerView.btnCheckBox.addTarget(self, action: #selector(handleTap(_:)), for: UIControlEvents.touchUpInside)
                headerView.btnApply.isHidden=true
                
            }
            
            
            
        }
        
        headerView.btnTitle.tag=section
        headerView.btnCheckBox.tag=section
        return containerView
        
        
    }
    
    // MARK: - BUTTONS TAPE METHODS
    
    func sliderValueChanged(_ sender: UISlider)
    {
        let currentValue = Int(sender.value)
        print(currentValue)
        
        let indexPath1 = IndexPath(row: 0, section: 3)
        MyRestcells = tblFilters.cellForRow(at: indexPath1) as! MyResListCell!
        MyRestcells.lblMaxDistance.text = String(currentValue) + " miles"
        
        strDistance = String(currentValue)
        
        }

    func btnApplyPressed(_ sender:UIButton)
    {
        
        self.view.endEditing(true)
        strCuisines = (arrSelectedCuisines as NSArray).componentsJoined(by: ",")
        strOffers = (arrSelectedOffers as NSArray).componentsJoined(by: ",")

        if (Int(strMinPrice)>Int(strMaxPrice))
        {
            self.view.endEditing(true)
           let dialog = ZAlertView(title: "Foodies4u",
                                    message: "Maximum price should be greater than minimum price"
                ,
                                    closeButtonText: "Ok",
                                    closeButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                                        
                                        
                }
                
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true

        }
        else
            
        {
            if FoodType == -1
            {
                strFoodType = ""
            }
            if Category == -1
            {
                Category = 0
            }
            if strDistance.characters.count == 0
            {
                strDistance  = "5"
            }
           
        
        if InternetConnection.isConnectedToNetwork()
        {
            

            
            
            getOrders() { responseObject, error in
                
                if (responseObject != nil)
                {
                    let json = JSON(responseObject!)
                if let string = json.rawString()
                {
                    print("json ",json)
                    self.stopActivityAnimating()
                    
                    
                    if let data = string.data(using: String.Encoding.utf8) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            print("json ",json)
                            
                            
                          
                           
                          
                            if( json!["Success"] as? String  == "1")
                            {

                                

                                let storyboard: UIStoryboard = UIStoryboard(name: "MainSW", bundle: nil)
                                
                                // if this user deafult is yes than it means you came from search filters.
                                
                                if(UserDefaults.standard.bool(forKey: "SEARCHFORFILTERS"))
                                {
                                    // This will navigate you to the search screen from right menu

                                    
                                    if(self.strSearchType.characters.count != 0)
                                    {
                                        UserDefaults.standard .set(true, forKey: "SEARCHDONEANDAPPLYFILETR")

                                    }
                                    

                                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
                                   // secondViewController?.strSearchType = ""
                                   // secondViewController?.strSearchCategory = ""
                                    
                                    
                                    // pass the value of filters to seach view

                                    secondViewController?.strFoodType = self.strFoodType
                                    secondViewController?.OpenNow =  self.OpenNow
                                    secondViewController?.FoodType = self.FoodType
                                    secondViewController?.Category =  self.Category
                                    secondViewController?.strDistance =  self.strDistance
                                    secondViewController?.strCuisines =  self.strCuisines
                                    secondViewController?.strOffers =  self.strOffers
                                    secondViewController?.strMinPrice =  self.strMinPrice
                                    secondViewController?.strMaxPrice =  self.strMaxPrice
                                    secondViewController?.strSearchType =  self.strSearchType
                                    secondViewController?.strSearchCategory =  self.strSearchCategory
                                    secondViewController?.latitude = self.latitude
                                    secondViewController?.longitude = self.longitude
                                    
                                    let navController = UINavigationController(rootViewController: secondViewController!)
                                    navController.setViewControllers([secondViewController!], animated:true)
                                    self.revealViewController().setFront(navController, animated: true)
                                    self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)

                                    
                                    
                                }
                                else
                                {
                                    
                                    
                                    UserDefaults.standard .set(true, forKey: "GOTORESTLISTFROMFILTER")

                                    
                                    // This will navigate you to the home screen from right menu

                                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                                    
                                    // pass the value of filters to home view

                                    secondViewController?.OpenNow =  self.OpenNow
                                    secondViewController?.strFoodType = self.strFoodType
                                    secondViewController?.Category =  self.Category
                                    secondViewController?.strDistance =  self.strDistance
                                    secondViewController?.strCuisines =  self.strCuisines
                                    secondViewController?.strOffers =  self.strOffers
                                    secondViewController?.strMinPrice =  self.strMinPrice
                                    secondViewController?.strMaxPrice =  self.strMaxPrice
                                    secondViewController?.strSearchType =  ""
                                    secondViewController?.strSearchCategory =  ""
                                    secondViewController?.latitude = self.latitude
                                    secondViewController?.longitude = self.longitude

                                    let navController = UINavigationController(rootViewController: secondViewController!)
                                    navController.setViewControllers([secondViewController!], animated:true)
                                    self.revealViewController().setFront(navController, animated: true)
                                    self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)

                                    
                                    // secondViewController?.arrData = json!["Data"] as! NSMutableArray
                                    
                                    
                                }
                                
                                
                                
                            }
                            else
                            {
                               
                                
                                let storyboard: UIStoryboard = UIStoryboard(name: "MainSW", bundle: nil)
                                if(UserDefaults.standard.bool(forKey: "SEARCHFORFILTERS"))
                                {
                                    
                                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
                                    
                                    // pass the value of filters to search view

                                    secondViewController?.OpenNow =  self.OpenNow
                                    secondViewController?.strFoodType = self.strFoodType
                                    secondViewController?.Category =  self.Category
                                    secondViewController?.strDistance =  self.strDistance
                                    secondViewController?.strCuisines =  self.strCuisines
                                    secondViewController?.strOffers =  self.strOffers
                                    secondViewController?.strMinPrice =  self.strMinPrice
                                    secondViewController?.strMaxPrice =  self.strMaxPrice
                                    secondViewController?.strSearchType =  self.strSearchType
                                    secondViewController?.strSearchCategory =  self.strSearchCategory
                                    secondViewController?.latitude = self.latitude
                                    secondViewController?.longitude = self.longitude


                                    let navController = UINavigationController(rootViewController: secondViewController!)
                                    navController.setViewControllers([secondViewController!], animated:true)
                                    self.revealViewController().setFront(navController, animated: true)
                                    self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
                                    
                                    
                                    
                                }
                                else
                                {
                                    
                                    UserDefaults.standard .set(true, forKey: "GOTORESTLISTFROMFILTER")

                                    let secondViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                                    
                                    // pass the value of filters to home view

                                    secondViewController?.OpenNow =  self.OpenNow
                                     secondViewController?.strFoodType = self.strFoodType
                                    secondViewController?.Category =  self.Category
                                    secondViewController?.strDistance =  self.strDistance
                                    secondViewController?.strCuisines =  self.strCuisines
                                    secondViewController?.strOffers =  self.strOffers
                                    secondViewController?.strMinPrice =  self.strMinPrice
                                    secondViewController?.strMaxPrice =  self.strMaxPrice
                                    secondViewController?.strSearchType = ""
                                    secondViewController?.strSearchCategory =  ""
                                   

                                    secondViewController?.latitude = self.latitude
                                    secondViewController?.longitude = self.longitude


                                    let navController = UINavigationController(rootViewController: secondViewController!)
                                    navController.setViewControllers([secondViewController!], animated:true)
                                    self.revealViewController().setFront(navController, animated: true)
                                    self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
                                    
                                    
                                    // secondViewController?.arrData = json!["Data"] as! NSMutableArray
                                    
                                    
                                }
                                
                               /* let dialog = ZAlertView(title: "Foodies4u",
                                    message: json!["Data"]![0]["status"] as? String
 ,
                                    closeButtonText: "Ok",
                                    closeButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                                    
                                    
                                    }
                                    
                                )
                                dialog.show()
                                dialog.allowTouchOutsideToDismiss = true*/
                              
                                
                            }
                        }
                            
                        catch {
                            
                            print("Something went wrong")
                        }
                    }
                }
                }
                else{
                    self.stopActivityAnimating()
                    self.AlertMethod()
                }
                
                
            }
            
        }
        }
    }
    
    func ClickOpenNow(_ sender:UIButton)
    {
        
        TapBtn = TapBtn + 1
        
        self.tblFilters.reloadData()
        
        
    }
    func OpenMoreFilters(_ sender:UIButton)
    {
        TapBtnRow=sender.tag
        
        if SECTION==1
        {
            FoodType = (self.arrFoodType.object(at: sender.tag) as AnyObject).value(forKey: "Id") as! Int
            
            strFoodType = String(FoodType)
            
        }
        if SECTION == 2
        {
            Category = (self.arrCategory.object(at: sender.tag) as AnyObject).value(forKey: "Id") as! Int
        }
        if SECTION == 3
        {
            let distance: String = (self.arrDistance.object(at: sender.tag) as AnyObject).value(forKey: "Name") as! String
            
            strDistance = distance //String(self.arrDistance.objectAtIndex(sender.tag).valueForKey("Name") as! NSNumber)
        }
        
        if SECTION==4
        {
            if(arrSelectedCuisines.contains(String( describing: (self.arrCuisines.object(at: sender.tag) as AnyObject) .value(forKey: "Id") as! NSNumber)))
                
            {
                arrSelectedCuisines.remove(String( describing: (self.arrCuisines.object(at: sender.tag) as AnyObject) .value(forKey: "Id") as! NSNumber))
            }
            else
                
            {
                arrSelectedCuisines.add(String(describing: (self.arrCuisines.object(at: sender.tag) as AnyObject) .value(forKey: "Id") as! NSNumber))
                
            }
        }
        
        if SECTION == 5
        {
            if(arrSelectedOffers.contains(String(describing: (self.arrOffers.object(at: sender.tag) as AnyObject) .value(forKey: "Id") as! NSNumber)))
                
            {
                arrSelectedOffers.remove(String( describing: (self.arrOffers.object(at: sender.tag) as AnyObject) .value(forKey: "Id") as! NSNumber))
            }
            else
                
            {
                arrSelectedOffers.add(String( describing: (self.arrOffers.object(at: sender.tag) as AnyObject) .value(forKey: "Id") as! NSNumber))

              
                
            }
        }
        
        //TapBtnRow = TapBtnRow + 1
        
        self.tblFilters.reloadData()
  
    }
    
    
    func handleTap(_ sender:UIButton)
    {
        let indexPath = IndexPath(row: 0, section:sender.tag)
        if (indexPath as NSIndexPath).row == 0
        {
            
            let collapsed = CBool(arrItems[(indexPath as NSIndexPath).section] as NSNumber)
            for i in 0..<arrName.count
            {
                if (indexPath as NSIndexPath).section == i
                {
                    arrItems[i] = !collapsed
                    
                }
                
            }
            
            for i in 0..<arrItems.count
            {
                if arrItems[i]  == true
                {
                    if(i == sender.tag)
                    {
                        
                    }
                    else
                    {
                        arrItems[i] = false
                    }
                }
                
            }
        }
        
        if sender.tag == 1
        {
            
            self.arrFiltersData=self.arrFoodType.value(forKey: "Name")  as! NSArray as [AnyObject]
            
            
        }
        
        if sender.tag == 2
        {
            self.arrFiltersData=self.arrCategory.value(forKey: "Name")  as! NSArray
                as [AnyObject]
            
            
            
        }
        if sender.tag == 3
        {
            self.arrFiltersData = ["" as AnyObject]
            
            
            
        }
        if sender.tag == 4
        {
            self.arrFiltersData=self.arrCuisines.value(forKey: "Name")  as! NSArray
                as [AnyObject]
            
            
            
        }
        if sender.tag==5
        {
            self.arrFiltersData = self.arrOffers.value(forKey: "Name")  as! NSArray
                as [AnyObject]
        }
        if sender.tag==6
        {
            self.arrFiltersData = ["MinPrice" as AnyObject,"MaxPrice" as AnyObject]
            
        }
        
        SECTION=(indexPath as NSIndexPath).section
        
        print(arrFiltersData)
        self.tblFilters.reloadData()
        
        self.tblFilters.reloadSections(IndexSet(integer: sender.tag), with: UITableViewRowAnimation.automatic)
        
    }
   


    @IBAction func btnClearFiltersPressed(_ sender: UIButton!)
    {
        //for filters
         FoodType  = -1
        strFoodType = "0"
         strMinPrice = ""
         strMaxPrice = ""
         Category = -1
         strCuisines = ""
         strSortBy = ""
         strType = ""
         strDistance = ""
         strOffers = ""
         OpenNow = "false"
         TapBtn = 0
        strSearchCategory = ""
        strSearchType = ""
        
      /*  for i in 0..<2
        {*/
        
//        let paddingView = UIView(frame:CGRectMake(0, 0,10, 30))
//        MyRestcells.txtFieldPrice.leftView=paddingView;
//        MyRestcells.txtFieldPrice.leftViewMode = UITextFieldViewMode.Always
//        
//        let indexPath1 = NSIndexPath(forRow: 0, inSection: 6)
//        MyRestcells = tblFilters.cellForRowAtIndexPath(indexPath1) as! MyResListCell!
//        MyRestcells.txtFieldPrice.text=" "
//        let placeHolder=NSAttributedString(string: "Min Price", attributes:    [NSForegroundColorAttributeName : UIColor.whiteColor()])
//        MyRestcells.txtFieldPrice .attributedPlaceholder=placeHolder
//        MyRestcells.txtFieldPrice.textColor=UIColor.whiteColor()
//        
//        
//        let indexPath2 = NSIndexPath(forRow: 1, inSection: 6)
//        MyRestcells = tblFilters.cellForRowAtIndexPath(indexPath2) as! MyResListCell!
//        MyRestcells.txtFieldPrice.text=" "
//        let placeHolder1=NSAttributedString(string: "Max Price", attributes:    [NSForegroundColorAttributeName : UIColor.whiteColor()])
//        MyRestcells.txtFieldPrice .attributedPlaceholder=placeHolder1
//        MyRestcells.txtFieldPrice.textColor=UIColor.whiteColor()        //}
        
        
        

        
        let indexPath2 = IndexPath(row: 0, section: 3)
        self.MyRestcells = self.tblFilters.cellForRow(at: indexPath2) as! MyResListCell!
        if (self.MyRestcells != nil)
        {
        self.MyRestcells.sliderDistance.minimumValue = 5
        self.MyRestcells.sliderDistance.maximumValue = 5
        self.MyRestcells.lblMaxDistance.text = "5"
        }
        arrSelectedCuisines.removeAllObjects()
        arrSelectedOffers.removeAllObjects()
        tblFilters.reloadData()
        
        self.tblFilters.reloadSections(IndexSet(integer: 0), with: UITableViewRowAnimation.automatic)
        
        getOrders() { responseObject, error in
            
            if (responseObject != nil)
            {            let json = JSON(responseObject!)
            if let string = json.rawString()
            {
                print("json ",json)
                self.stopActivityAnimating()
                
                
                if let data = string.data(using: String.Encoding.utf8) {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                        print("json ",json)
                        
                       
                        
                        if( json!["Success"] as? String  == "1")
                        {
                            
                            
                            let storyboard: UIStoryboard = UIStoryboard(name: "MainSW", bundle: nil)
                            
                            
                                let secondViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                                secondViewController?.strSearchCategory =  ""
                                secondViewController?.strSearchType =  ""
                                secondViewController?.OpenNow =  self.OpenNow
                                secondViewController?.FoodType = self.FoodType
                                secondViewController?.Category =  self.Category
                                secondViewController?.strDistance =  self.strDistance
                                secondViewController?.strCuisines =  self.strCuisines
                                secondViewController?.strOffers =  self.strOffers
                                secondViewController?.strMinPrice =  self.strMinPrice
                                secondViewController?.strMaxPrice =  self.strMaxPrice
                            
                            
                            
                            
                            
                        }
                    }
                        
                    catch {
                        
                        print("Something went wrong")
                    }
                }
            }
            }
            else
            {
                self.stopActivityAnimating()
                self.AlertMethod()
            }
    }
    }
    
    //MARK:Textfield Delegates & Datasource Method
    
    func textFieldDidBeginEditing(_ tF: UITextField)
    {
        // print("keyboard %f",keyboardSize.height);
        
        animateViewMoving(true, moveValue: 215)
        
        
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        
        animateViewMoving(false, moveValue: 215)
        
        if(textField.tag==0)
        {
            strMinPrice = textField.text
            
        }
        else
            
        {
            strMaxPrice = textField.text
            
        }
        
        
        
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount+1)
        {
            return false
        }
        let txtAfterUpdate:NSString = textField.text! as NSString
        
        print(currentCharacterCount)
        // txtCountry.text=" "
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 10
    }
    
    override func animateViewMoving (_ up:Bool, moveValue :CGFloat)
    {
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        
        
        UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        
        textField.resignFirstResponder()
        
        return true
    }
    
    //MARK: CALL WEBSERVICES
    
    
    func getOrders(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        //   let size = CGSize(width: 80, height:80)
        
        // startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        strSearchType = UserDefaults.standard.string(forKey: "SEARCHTEXT")
        strSearchCategory = UserDefaults.standard.string(forKey: "SEARCHTEXTCATEGORY")

        
        strWSURL=GETWSURL()
        
        let size = CGSize(width: 50, height:50)
        
        startActivityAnimating(size, message: "", type: NVActivityIndicatorType(rawValue: 1)!)
        
        param1 = [
            "search":strSearchType as AnyObject,
            "type":strSearchCategory as AnyObject,
            "distance":strDistance as AnyObject,
            "foodtype":strFoodType as AnyObject,
            "category":Category as AnyObject,
            "minprice":strMinPrice as AnyObject,
            "maxprice":strMaxPrice as AnyObject,
                "offers":strOffers as AnyObject,
                "cuisines":strCuisines as AnyObject,
                "opennow": OpenNow as AnyObject,
                "Latitude": latitude as AnyObject,
                "Longitude": longitude as AnyObject,
                "sortby": "ASC" as AnyObject,
                "customerid":UserDefaults.standard.value(forKey: "USERID")! as AnyObject
        ]
        
        
        
            
        
        makeCall(section: strWSURL as String + "SearchContext?",param:param1 ,completionHandler: completionHandler)
        
        
        
    }
    
    
}







/*
 override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
 let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell
 
 // Configure the cell...
 
 return cell
 }
 */

/*
 // Override to support conditional editing of the table view.
 override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
 // Return NO if you do not want the specified item to be editable.
 return true
 }
 */

/*
 // Override to support editing the table view.
 override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
 if editingStyle == .Delete {
 // Delete the row from the data source
 tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
 } else if editingStyle == .Insert {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
 
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
 // Return NO if you do not want the item to be re-orderable.
 return true
 }
 */

/*
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


