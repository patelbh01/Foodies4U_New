//
//  MapDirectionCell.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 23/08/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class MapDirectionCell: UITableViewCell,MKMapViewDelegate
{
    @IBOutlet weak var ImgMapDirection: MKMapView!
    @IBOutlet weak var lblRestName: UILabel!
    @IBOutlet weak var imgFav:  UIImageView!
    @IBOutlet var btnFav:  UIButton!
    @IBOutlet weak var viewrestname:  UIView!


    @IBOutlet var floatRatingView: FloatRatingView!
    @IBOutlet weak var lblRating: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
       floatRatingView.emptyImage = UIImage(named: "StarEmpty")
        floatRatingView.fullImage = UIImage(named: "StarFull")
        floatRatingView.maxRating = 5
       floatRatingView.minRating = 1

    }
    
    
}