//
//  RestListDetails.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 23/08/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import MapKit
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON

class RestListDetailsViewController: parentViewController,CLLocationManagerDelegate,MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource
{
    var dictRestDetails:NSMutableDictionary!
    var totalCell = 0
    
    // related to weekly timingvar============================
    
    @IBOutlet var viewForWeeklyTiming:UIView!
    @IBOutlet weak var tblTimingDetails:UITableView!
    var arrWeeklyTiming:NSArray!
    //========================================================
    
    
    // related to review===============================
    
    var strTotalReviews = 0
    var arrRestaurantReviewList:NSArray!
    var reviewIndex=0
    //========================================================
    
    @IBOutlet weak var tblRestDetails:UITableView!
    
    var latitude:Double!
    var longitude:Double!
    var locationManager: CLLocationManager!
    var imagedData:Data!
    
    var sourceAnnotation = MKPointAnnotation()
    var destinationAnnotation = MKPointAnnotation()

    
    var cellRestDetail:MapDirectionCell!
    var arrData: NSMutableDictionary!
    
    
    var strWSURL: NSString!
    var strUSERID: AnyObject!
    
    var btn:UIButton!
    
    let arrImages = ["Map", "location", "BusinessHours", "CuisineType", "Highlights", "Reviews"]
    
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
        self.setNeedsStatusBarAppearanceUpdate()
        
        print("details",dictRestDetails)
        
        
        //initalize locationmanager
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
        
        latitude = (self.dictRestDetails.value(forKey: "Latitude")! as AnyObject)  .doubleValue
        longitude = (self.dictRestDetails.value(forKey: "Longitude")! as AnyObject).doubleValue
        
        
        
        
        tblRestDetails.register(UINib(nibName: "MapDirectionCell", bundle: nil), forCellReuseIdentifier: "MapDirectionCell")
        
        arrWeeklyTiming = self.dictRestDetails.value(forKey: "weektimedetails")! as! NSArray
        tblTimingDetails.layer.cornerRadius = 5.0
        tblTimingDetails.reloadData()
        
        let strTemp: String = self.dictRestDetails.value(forKey: "TotalReview") as! String
        strTotalReviews = Int(strTemp)!
        arrRestaurantReviewList = self.dictRestDetails.value(forKey: "RestaurantReviewdetails")! as! NSArray
        
        let strRestaurantId = String(describing: self.dictRestDetails.value(forKey: "RestaurantId")!)
        UserDefaults.standard.setValue(strRestaurantId, forKey: "RESTID")
        
        tblRestDetails.register(UINib(nibName: "MapDirectionCell", bundle: nil), forCellReuseIdentifier: "MapDirectionCell")
        
        
        self.navigationController?.navigationBar.isHidden = true
        
        tblRestDetails.estimatedRowHeight = 50
        tblRestDetails.rowHeight = UITableViewAutomaticDimension
        
        tblRestDetails.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true

        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden=false
    }
    // MARK: - TABLEVIEW DATASOURCE AND DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblTimingDetails {
            return 40
            
        }
        else {
            
            if ((indexPath as NSIndexPath).row == 0)
            {
                return 250;
            }
            else if (indexPath as NSIndexPath).row == 1 {
                
                tableView.estimatedRowHeight = 50
                tableView.rowHeight = UITableViewAutomaticDimension
                
                return UITableViewAutomaticDimension
            }
            else if (indexPath as NSIndexPath).row == 5 {
                return 38
            }
            else if (indexPath as NSIndexPath).row == 6 {
                
                tblRestDetails.estimatedRowHeight = 90
                
                tblRestDetails.rowHeight = UITableViewAutomaticDimension
                
                return UITableViewAutomaticDimension
            }
            else if (strTotalReviews > 3 && (indexPath as NSIndexPath).row == totalCell-1) { // view all review CELL
                return 35
            }
            else
            {
                
//                if DeviceType.IS_IPHONE_6P
//                {
//                    tblRestDetails.estimatedRowHeight = 90
//                }
//                else if DeviceType.IS_IPHONE_4_OR_LESS
//                {
//                    tblRestDetails.estimatedRowHeight = 50
//                }
//                else if DeviceType.IS_IPHONE_5
//                {
//                    tblRestDetails.estimatedRowHeight = 50
//                }
//                else //if DeviceType.IS_IPHONE_6
//                {
//                    tblRestDetails.estimatedRowHeight = 70
//                }
                
                tblRestDetails.estimatedRowHeight = 70
                tblRestDetails.rowHeight = UITableViewAutomaticDimension
                
                return UITableViewAutomaticDimension
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblTimingDetails {
            return arrWeeklyTiming.count
        }
        else {
            
            if strTotalReviews > 3 {
                totalCell = arrImages.count + arrRestaurantReviewList.count + 1
            }else {
                totalCell = arrImages.count + arrRestaurantReviewList.count
            }
            return totalCell
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if tableView == tblTimingDetails { //for weekly timing details
            
            let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "weeklyTimeCell", for: indexPath) as! Customcell
                //tableView.dequeueReusableCellWithIdentifier("weeklyTimeCell") as! Customcell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            let lblDayName: UILabel = cell.viewWithTag(20) as! UILabel
            //lblDayName.layer.masksToBounds = true
            //lblDayName.layer.cornerRadius = 5.0
            lblDayName.text = (arrWeeklyTiming.value(forKey: "Day") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
            
            let lblDayTime: UILabel = cell.viewWithTag(30) as! UILabel
            lblDayTime.text = (arrWeeklyTiming.value(forKey: "Time") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
            
            return cell
            
        } else {
            //display resturants deatils
            
            if strTotalReviews > 3 && (indexPath as NSIndexPath).row == totalCell-1 // view all review CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "moreReviewCell", for: indexPath) as! Customcell
                    //tableView.dequeueReusableCellWithIdentifier("moreReviewCell") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                cell.btnViewAllReview.tag = (indexPath as NSIndexPath).row
                //let btnViewAllReview: UIButton = cell.viewWithTag(20) as! UIButton
                
                
//                let string = NSMutableAttributedString(string: ("View All"))
//                
//                string.addAttribute(NSFontAttributeName, value: UIFont (name: "OpenSans-Bold", size: 14)!, range: NSMakeRange(0, string.length))
//                string.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: NSMakeRange(0, string.length))
//                //TextColor
//                string.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: NSMakeRange(0, string.length))
//                //Underline color
//                string.addAttribute(NSUnderlineColorAttributeName, value: UIColor.lightGrayColor(), range: NSMakeRange(0, string.length))
//                //TextColor
//                btnViewAllReview.titleLabel!.attributedText = string
//                
//                
                //btnViewAllReview.tag = indexPath.row
                //btnViewAllReview.addTarget(self, action: #selector(self.btnViewAllReviewClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                
                return cell
                
            }
            else  if (indexPath as NSIndexPath).row == 0 // MAP CELL
            {
                cellRestDetail = tableView.dequeueReusableCell(withIdentifier: "MapDirectionCell") as! MapDirectionCell
                
                let lat  = UserDefaults.standard.double(forKey: "LATITUDE")
                let long = UserDefaults.standard.double(forKey: "LONGITUDE")
                
                // 1.
                cellRestDetail.ImgMapDirection.delegate = self
                
                // 2.
                let sourceLocation = CLLocationCoordinate2D(latitude: lat, longitude:long)
                let destinationLocation = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                
                // 3.
                let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
                let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
                
                // 4.
                let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
                let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
                
                // 5.
                let sourceAnnotation = CustomPointAnnotation()
                sourceAnnotation.title = "Current Location"
                sourceAnnotation.imageName = "DestinationMarker"

                
                
                if let location = sourcePlacemark.location {
                    sourceAnnotation.coordinate = location.coordinate
                    
                }
                
                
                let destinationAnnotation = CustomPointAnnotation()
                destinationAnnotation.title = self.dictRestDetails.value(forKey: "Name") as? String
                destinationAnnotation.imageName = "SourceMarker"

                
                if let location = destinationPlacemark.location {
                    destinationAnnotation.coordinate = location.coordinate
                }
                
                cellRestDetail.ImgMapDirection.addAnnotation(sourceAnnotation)
                cellRestDetail.ImgMapDirection.addAnnotation(destinationAnnotation)

                // 7.
                let directionRequest = MKDirectionsRequest()
                directionRequest.source = sourceMapItem
                directionRequest.destination = destinationMapItem
                directionRequest.transportType = .automobile
                
                // Calculate the direction
                let directions = MKDirections(request: directionRequest)
                
                // 8.
                directions.calculate {
                    (response, error) -> Void in
                    
                    guard let response = response else {
                        if let error = error {
                            print("Error: \(error)")
                        }
                        
                        return
                    }
                    
                    let route = response.routes[0]
                    self.cellRestDetail.ImgMapDirection.add((route.polyline), level: MKOverlayLevel.aboveRoads)
                    
                     let rect = route.polyline.boundingMapRect
                    self.cellRestDetail.ImgMapDirection.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
                }
                
                cellRestDetail.ImgMapDirection.isZoomEnabled = false;
                cellRestDetail.ImgMapDirection.isScrollEnabled = false;
                cellRestDetail.ImgMapDirection.isUserInteractionEnabled = false;

                //   cellRestDetial.selectionStyle = .None
                
                cellRestDetail.selectionStyle = UITableViewCellSelectionStyle.none
                //cellRestDetail.contentView.userInteractionEnabled = false
                
                //name
                cellRestDetail.lblRestName.text = self.dictRestDetails.value(forKey: "Name") as? String
                
                
                //rating
                cellRestDetail.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
                cellRestDetail.floatRatingView.fullImage = UIImage(named: "StarFull")
                cellRestDetail.floatRatingView.maxRating = 5
                cellRestDetail.floatRatingView.minRating = 1
                cellRestDetail.floatRatingView.rating = ((self.dictRestDetails.value(forKey: "Ratings") as! NSString).floatValue)
                cellRestDetail.floatRatingView.floatRatings = false
                cellRestDetail.floatRatingView.editable = false

                
                //rating label
                cellRestDetail.lblRating.text = self.dictRestDetails.value(forKey: "Ratings") as? String
                
                //cellRestDetail.viewrestname.userInteractionEnabled = false
                
                
                //let btnFav: UIButton = cellRestDetail.viewWithTag(111) as! UIButton
                //set favourite restaturant
                let favStatus = String(describing: self.dictRestDetails.value(forKey: "FavRestaurant")!)
                //print(favStatus)
                
                if favStatus == "0" { // not fav yet
                    cellRestDetail.btnFav.setImage(UIImage.init(named: "Favourite-Icon-White"), for: UIControlState())
                }else { // fav
                    cellRestDetail.btnFav.setImage(UIImage.init(named: "Favourite-Icon-Red"), for: UIControlState())
                }
                cellRestDetail.btnFav.tag = (indexPath as NSIndexPath).row
                cellRestDetail.btnFav.addTarget(self, action: #selector(self.btnFavClicked(_:)), for: UIControlEvents.touchUpInside)
                
                
                return cellRestDetail
            }
            else if (indexPath as NSIndexPath).row == 1 // ADDRESS CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "textCell2") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                let imgIcon: UIImageView = cell.viewWithTag(10) as! UIImageView
                imgIcon.image = UIImage(named: arrImages[(indexPath as NSIndexPath).row])
                
                let strAddress = self.dictRestDetails.value(forKey: "Address1") as? String
                let lblText: UILabel = cell.viewWithTag(20) as! UILabel
                lblText.numberOfLines = 0
                lblText.text = strAddress
                
                return cell
            }
            else if (indexPath as NSIndexPath).row == 2 // OPEN TIME CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "openTimeCell") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                let imgIcon: UIImageView = cell.viewWithTag(10) as! UIImageView
                imgIcon.image = UIImage(named: arrImages[(indexPath as NSIndexPath).row])
                
                let lblToday: UILabel = cell.viewWithTag(20) as! UILabel
                lblToday.layer.masksToBounds = true
                lblToday.layer.cornerRadius = 5.0
                
                let lblTodayTime: UILabel = cell.viewWithTag(30) as! UILabel
                let strTodayTime = self.dictRestDetails.value(forKey: "todayTimeData") as? String
                lblTodayTime.text = strTodayTime
                
                let btnViewAllTiming: UIButton = cell.viewWithTag(40) as! UIButton
                
                if arrWeeklyTiming.count > 0 {
                    
                    btnViewAllTiming.tag = (indexPath as NSIndexPath).row
                    btnViewAllTiming.addTarget(self, action: #selector(self.btnViewAllTimingClicked(_:)), for: UIControlEvents.touchUpInside)
                    btnViewAllTiming.isHidden = false
                }else {
                    
                    
                    lblTodayTime.text = "No Timing Available"
                    btnViewAllTiming.isHidden = true
                    
                }
                
                return cell
            }
            else if (indexPath as NSIndexPath).row == 3 // CUSINE TYPE CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "textCell2") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                let imgIcon: UIImageView = cell.viewWithTag(10) as! UIImageView
                imgIcon.image = UIImage(named: arrImages[(indexPath as NSIndexPath).row])
                
                
                let arr = (self.dictRestDetails.value(forKey: "CuisineTypesdetails")! as AnyObject).value(forKey: "Name")! as! NSArray
                let strCuisineTypes = arr.componentsJoined(by: ", ")
                
                let lblText: UILabel = cell.viewWithTag(20) as! UILabel
                lblText.numberOfLines = 0
                
                if strCuisineTypes.characters.count > 0 {
                    lblText.text = strCuisineTypes
                }else {
                    lblText.text = "Not Available"
                }
                
                return cell
            }
            else if (indexPath as NSIndexPath).row == 4 // HIGHTLIGHTS CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "textCell2") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                let imgIcon: UIImageView = cell.viewWithTag(10) as! UIImageView
                imgIcon.image = UIImage(named: arrImages[(indexPath as NSIndexPath).row])
                
                let arr = (self.dictRestDetails.value(forKey: "Amenitiesdetails")! as AnyObject).value(forKey: "Name")! as! NSArray
                let strAmenitiesTypes = arr.componentsJoined(by: ", ")
                
                let lblText: UILabel = cell.viewWithTag(20) as! UILabel
                lblText.numberOfLines = 0
                //lblText.text = strAmenitiesTypes
                
                if strAmenitiesTypes.characters.count > 0 {
                    lblText.text = strAmenitiesTypes
                }else {
                    lblText.text = "Not Available"
                }
                
                return cell
            }
            else if (indexPath as NSIndexPath).row == 5 // ReviewHeader CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "reviewHeaderCell") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                let lblText: UILabel = cell.viewWithTag(10) as! UILabel
                lblText.text = "(\(strTotalReviews) Reviews)"
                
                let btnWriteReview: UIButton! = cell.viewWithTag(20) as? UIButton
                
                if  btnWriteReview == cell.viewWithTag(20) as? UIButton
                {
                    btnWriteReview.tag = (indexPath as NSIndexPath).row
                    btnWriteReview.addTarget(self, action: #selector(self.btnWriteReviewClicked(_:)), for: UIControlEvents.touchUpInside)
                    
                }
                if(self.dictRestDetails.value(forKey: "IsVisited") as? String  == "1")
                {
                    cell.lblReview.isHidden = false
                    cell.ImgViewicon.isHidden = false
                    btnWriteReview.isHidden = false
                    
                    
                }
                else
                {
                    cell.lblReview.isHidden = true
                    cell.ImgViewicon.isHidden = true
                    btnWriteReview.isHidden = true

                }
                
                
                
                
                return cell
            }
            else if (indexPath as NSIndexPath).row >= 6 // review CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "reviewContentCell") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                reviewIndex = (indexPath as NSIndexPath).row - arrImages.count
                
                let viewForBackground: UIView = cell.viewWithTag(10)! as UIView
                viewForBackground.layer.cornerRadius = 5.0
                viewForBackground.layer.borderColor = UIColor.lightGray.cgColor
                viewForBackground.layer.borderWidth = 0.8
                
                
                //for user profile image
                let url = URL(string: "http://foodies4you.intransure.com/Media/Customer/"  + String(describing: (arrRestaurantReviewList.value(forKey: "CustomerImage") as AnyObject).object(at: reviewIndex) as! String))
                //print("url",url)
                
                let imgIcon: UIImageView = viewForBackground.viewWithTag(20) as! UIImageView
                //make sure your image in this url does exist, otherwise unwrap in a if let check
                if(url ==  nil)
                {
                    
                    imgIcon.image = UIImage(named:"Food_PlaceHolder.png")
                    imgIcon.layer.cornerRadius = imgIcon.frame.width / 2
                    imgIcon.layer.borderColor = UIColor.lightGray.cgColor
                    imgIcon.layer.borderWidth = 0.8
                    imgIcon.clipsToBounds = true
                    imgIcon.contentMode = UIViewContentMode.scaleAspectFit
                    
                    
                }
                else
                {
                    
                    do {
                        let data = try Data(contentsOf: url!)
                        imgIcon.image = UIImage(data: data)
                        imgIcon.layer.cornerRadius = imgIcon.frame.width / 2
                        imgIcon.layer.borderColor = UIColor.lightGray.cgColor
                        imgIcon.layer.borderWidth = 0.8
                        imgIcon.clipsToBounds = true
                        imgIcon.contentMode = UIViewContentMode.scaleAspectFit

                        
                    } catch {
                        print("Dim background error \(error)")
                    }
                    
                }

                
                //for user profile image
                //            let imgIcon: UIImageView = viewForBackground.viewWithTag(20) as! UIImageView
                //            imgIcon.image = UIImage(named: arrImages[indexPath.row])
                
                //            //for user name
                let lblUserName: UILabel = viewForBackground.viewWithTag(30) as! UILabel
                lblUserName.text = (arrRestaurantReviewList.value(forKey: "Customer") as AnyObject).object(at: reviewIndex) as? String
                
                //for rating
                let lblRatingText: UILabel = viewForBackground.viewWithTag(40) as! UILabel
                let strRating = (arrRestaurantReviewList.value(forKey: "Rating") as AnyObject).object(at: reviewIndex) as? String
                lblRatingText.text = "Rated       \(strRating!)" // fix 7 space inbetween for UI formatting
                
                //for description
                let lblDescriptionText: UILabel = viewForBackground.viewWithTag(50) as! UILabel
                lblDescriptionText.text = (arrRestaurantReviewList.value(forKey: "Description") as AnyObject).object(at: reviewIndex) as? String
                
                return cell
            }
            
        }
        let tblcell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return tblcell
        
    }
    
    
    //MARK: -  BUTTON CLICKS
    
    @IBAction func btnFavClicked(_ sender: UIButton){
        
        //get inital resturants list
        
        if InternetConnection.isConnectedToNetwork()
        {
            self.startIndicatorCircleAnimating()
            
            makeRestaurantFavUnFav() { responseObject, error in
                
                if (responseObject != nil)
                {
                    let json = JSON(responseObject!)
                if let string = json.rawString()
                {
                    
                    
                    if let data = string.data(using: String.Encoding.utf8)
                    {
                        do {
                            
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            print("json ",json)
                            if(json!["Success"] as? String  == "1")
                            {
                                self.arrData = json!["Data"] as! NSMutableDictionary
                                
                                self.stopIndicatorCircleAnimating()
                                if(self.arrData.value(forKey: "Status") as? Bool == false)
                                {
                                    let indexPath1 = IndexPath(row: 0, section: 0)
                                    self.cellRestDetail = self.tblRestDetails.cellForRow(at: indexPath1) as! MapDirectionCell!
                                    
                                    
                                    let transition = CATransition()
                                    transition.duration = 0.2
                                    transition.type = kCATransitionPush
                                    transition.subtype = kCATransitionFromTop
                                    self.cellRestDetail.btnFav.layer.add(transition, forKey: kCATransition)
                                    
                                    self.cellRestDetail.btnFav.setImage(UIImage.init(named: "Favourite-Icon-White"), for: UIControlState())
                                }
                                else
                                {
                                    let indexPath1 = IndexPath(row: 0, section: 0)
                                    self.cellRestDetail = self.tblRestDetails.cellForRow(at: indexPath1) as! MapDirectionCell!
                                    //self.cellRestDetial.imgFav.image=UIImage(named:"Favourite-Icon-White.png")
                                    
                                    let transition = CATransition()
                                    transition.duration = 0.2
                                    transition.type = kCATransitionPush
                                    transition.subtype = kCATransitionFromBottom
                                    self.cellRestDetail.btnFav.layer.add(transition, forKey: kCATransition)
                                    
                                    self.cellRestDetail.btnFav.setImage(UIImage.init(named: "Favourite-Icon-Red"), for: UIControlState())
                                }
                                
                            }
                            
                        }
                            
                        catch {
                            print("Something went wrong")
                        }
                    }
                }
                }
                else
                {
                    self.stopIndicatorCircleAnimating()
                    self.AlertMethod()
                }
                
            }
        }
        
    }
    
    
    @IBAction func btnWriteReviewClicked(_ sender: UIButton)
    {
        self.performSegue(withIdentifier: "writeReview", sender: self)
    }
    
    @IBAction func btnViewAllReviewClicked(_ sender: UIButton){
        
        
        
        
        let allReviewsVC = self.storyboard?.instantiateViewController(withIdentifier: "allReviewListViewController") as? allReviewListViewController
        
        allReviewsVC!.strRestaurantID = String(describing: self.dictRestDetails.value(forKey: "RestaurantId")!)
            
        
        
        print("today is thursday" ,  allReviewsVC!.strRestaurantID!)

        self.navigationController?.pushViewController(allReviewsVC!, animated: true)
        
        //self.performSegueWithIdentifier("allReviewListSegue", sender: self)
    }
    @IBAction func btnViewAllTimingClicked(_ sender: UIButton){
        
        viewForWeeklyTiming.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewForWeeklyTiming.isHidden = false
    }
    @IBAction func btnCloseAllTimingClicked(_ sender: UIButton){
        
        viewForWeeklyTiming.isHidden = true
    }
    @IBAction func btnMenuClicked(_ sender: UIButton){
        
        
        let RViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "menuListViewController") as? menuListViewController
        RViewControllerObj!.arrRestImages = self.dictRestDetails.value(forKey: "restaurantimgs") as! NSArray
        
        RViewControllerObj?.restName = self.dictRestDetails.value(forKey: "Name") as? String
        
        UserDefaults.standard.setValue(self.dictRestDetails.value(forKey: "restaurantimgs") as! NSArray, forKey: "restaurantimgs")

        UserDefaults.standard.set("YES", forKey: "ZOOMED")
        
        self.navigationController?.pushViewController(RViewControllerObj!, animated: true)
        
       // self.performSegueWithIdentifier("menuListSegue", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "allReviewListSegue" {
            
            let allReviewListVC = segue.destination as? allReviewListViewController
            
            allReviewListVC?.strRestaurantID = self.dictRestDetails.value(forKey: "RestaurantId") as? String
                
        }
        else if segue.identifier == "menuListSegue" {
            
            let menuListSegue = segue.destination as? menuListViewController
            
            menuListSegue?.restName = self.dictRestDetails.value(forKey: "Name") as? String
            
           // menuListSegue!.arrRestaurantDetails = dictRestDetails
        }
        
        else if (segue.identifier == "writeReview")
        {
            let reviewController = segue.destination as! ReviewRestaurantViewController
            reviewController.dictRestaurantDetails = dictRestDetails
        }
    }
    //MARK: - BUTTONS ACTION METHODS
    func btngetdirection()
    {
        
        /* let customURL = "comgooglemaps://"
         
         if UIApplication.sharedApplication().canOpenURL(NSURL(string: customURL)!) {
         
         UIApplication.sharedApplication().openURL(NSURL(string:
         "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!)
         
         } else
         {
         let alert = UIAlertController(title: "Error", message: "Google maps not installed", preferredStyle: UIAlertControllerStyle.Alert)
         let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
         alert.addAction(ok)
         self.presentViewController(alert, animated:true, completion: nil)            }*/
        /*
         let coordinate = CLLocationCoordinate2DMake(CLLocationDegrees(latitude), CLLocationDegrees(longitude))
         
         let placemark:MKPlacemark = MKPlacemark(coordinate: coordinate, addressDictionary:nil)
         
         let mapItem:MKMapItem = MKMapItem(placemark: placemark)
         
         mapItem.name = "Target location"
         
         let launchOptions:NSDictionary = NSDictionary(object: MKLaunchOptionsDirectionsModeDriving, forKey: MKLaunchOptionsDirectionsModeKey)
         
         let currentLocationMapItem:MKMapItem = MKMapItem.mapItemForCurrentLocation()
         
         MKMapItem.openMapsWithItems([currentLocationMapItem, mapItem], launchOptions: launchOptions as? [String : AnyObject])*/
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton!)
    {
        
        let HomeViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        
        self.navigationController?.pushViewController(HomeViewControllerObj!, animated: false)
        
        //self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    @IBAction func btnReservationPressed(_ sender: UIButton!)
    {

        let MKRViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "MakeReservationViewController") as? MakeReservationViewController
        
        MKRViewControllerObj?.strRestName =  String(describing: self.dictRestDetails.value(forKey: "Name")!)
        MKRViewControllerObj?.strRestId =  String(describing: self.dictRestDetails.value(forKey: "RestaurantId")!)
        
        
        self.navigationController?.pushViewController(MKRViewControllerObj!, animated: true)
        
        //        let MKRViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("MakeReservationViewController") as? MakeReservationViewController
//        //MKRViewControllerObj.rest
//        self.navigationController?.pushViewController(MKRViewControllerObj!, animated: true)
    }
    
    
    
    
    // MARK: - SET FAVOURITE RESTARAUNT  WEB SERVICE
    func makeRestaurantFavUnFav(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        //let size = CGSize(width: 80, height:80)
        
        //startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        let strRestaurantId = String(describing: self.dictRestDetails.value(forKey: "RestaurantId")!)
        UserDefaults.standard.setValue(strRestaurantId, forKey: "RESTID")
        
        
        strWSURL=GETWSURL()
        let param1 : [ String : AnyObject] = [
            
            "CustomerId": UserDefaults.standard.value(forKey: "USERID")! as AnyObject,
            "RestaurantId":strRestaurantId as AnyObject
            
        ]
       makeCall(section: strURL as String + "Favourite?",param:param1,completionHandler: completionHandler)
    }
    
    // MARK: - MAPVIEW DELEGATES METHODS
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        let annotationIdentifier = "AnnotationIdentifier"
        
         var anView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        if anView == nil {

        
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            anView!.canShowCallout = true
        }
        else {
            anView!.annotation = annotation

        }
        
     /* if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
          //  annotationView.image = UIImage(named: "SourceMarker")

        }*/
        
        let cpa = annotation as! CustomPointAnnotation
        anView?.image = UIImage(named:cpa.imageName)
        
        
        
        return anView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.lineWidth = 2.0
         renderer.strokeColor = GlobalConstants.MAIN_BROWN_COLOR
        
        return renderer
    }
    // MARK: - LOCATIONS MANAGER METHODS
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let indexPath1 = IndexPath(row: 0, section: 0)
        cellRestDetail = tblRestDetails.cellForRow(at: indexPath1) as! MapDirectionCell!
        cellRestDetail.ImgMapDirection.delegate=self
        
        //show restauant
        
        let latDelta:CLLocationDegrees = 0.05
        let longDelta:CLLocationDegrees = 0.05
        
        let theSpan:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        let pointLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(pointLocation, theSpan)
        //cellRestDetail.ImgMapDirection.setRegion(region, animated: true)
        let pinLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        
        //add annotation
        
        let objectAnnotation = MKPointAnnotation()
        objectAnnotation.coordinate = pinLocation
        objectAnnotation.title = self.dictRestDetails.value(forKey: "Name") as? String
        cellRestDetail.ImgMapDirection.addAnnotation(objectAnnotation)
        //set favourite restaturant
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(btngetdirection))
        cellRestDetail.ImgMapDirection.addGestureRecognizer(tap)
        
        //print(latitude)
        //print(longitude)
        
        locationManager.stopUpdatingLocation()
        
    }
    
    //MARK: - STATUS BAR
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent;
    }
    
}
