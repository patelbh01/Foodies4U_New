//
//  ReviewRestaurantViewController.swift
//  Foodies4u
//
//  Created by Indrajeet Senger on 25/10/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import UIKit
import Alamofire

class ReviewRestaurantViewController: parentViewController, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, NVActivityIndicatorViewable, FloatRatingViewDelegate
{
    
    var dictRestaurantDetails: NSDictionary!
    var dialog = ZAlertView()
    
    var arrRatingList = NSMutableArray()
    var dictResult :NSDictionary!
    
    var strUserComment :String!
    var overallRating: Float!
    var strUSERID: AnyObject!
    
    
    @IBOutlet weak var tblContents: UITableView!
    
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Title and back button
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName : UIColor.white,
            NSFontAttributeName : UIFont.systemFont(ofSize: 25)]
        self.navigationItem.title = "Review"
        
        let img = UIImage(named: "BackArrow")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(btngoBack(sender:)))
        
        //keyboard appearence notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        print("dict value: \(dictRestaurantDetails)")
        
        
        //get userid value
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        getRatingData()
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- ======DELEGATE METHOD======
    //MARK: Table delegate and data source
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("arr count: \(arrRatingList.count)")
        return 6 + arrRatingList.count
    }
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var cellHeight: CGFloat = 0
        
        if (indexPath.row == 0) //Restaurant name & current rating, reviews
        {
            cellHeight = 150
        }
        else if (indexPath.row == 1) //Message
        {
            cellHeight = 63
        }
        else if (indexPath.row == 2) //Rating header
        {
            if (arrRatingList.count == 0)
            {
                cellHeight = 0
            }
            else
            {
                cellHeight = 30
            }
        }
        else if (indexPath.row <= arrRatingList.count+2) //Rating rows
        {
            cellHeight = 30
        }
        else if (indexPath.row == arrRatingList.count+3) //Comment row
        {
            cellHeight = 73
        }
        else if (indexPath.row == arrRatingList.count+4) //Overall review row
        {
            cellHeight = 62
        }
        else if (indexPath.row == arrRatingList.count+5) //Submit row
        {
            cellHeight = 62
        }
        
        return cellHeight
    }
    
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.row == 0) //Restaurant name & current rating, reviews
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "restDetailCell") as! Customcell
            
            //sets restaurant name
            if let restName = dictRestaurantDetails["Name"]
            {
                cell.lblRestauratnName.text = restName as? String
            }
            else
            {
                cell.lblRestauratnName.text = ""
            }
            
            //set restaurant rating
            if let rating = dictRestaurantDetails["Ratings"]
            {
                cell.currReview.rating = Float(rating as! String)!
                cell.lblRates.text = "\(rating)"
            }
            else
            {
                cell.currReview.rating = 0
                cell.lblRates.text = "0"
            }
            
            //set restaurant reviews
            if  let totReview = dictRestaurantDetails["TotalReview"]
            {
                cell.lblReviews.text = "\(totReview) Reviews"
            }
            else
            {
                cell.lblReviews.text = "0 Reviews"
            }
            
            return cell
        }
        else if (indexPath.row == 1) //Message
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell") as! Customcell
            
            return cell
        }
        else if (indexPath.row == 2) //Review Header
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "rateHeader") as! Customcell
            
            return cell
        }
        else if (indexPath.row <= arrRatingList.count+2) //Review rows
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell") as! Customcell
            
            let dictTemp = arrRatingList.object(at: (indexPath.row - 3)) as! NSDictionary
            let strRateVal = dictTemp["Rating"] as! String
            cell.lblReviewName.text = (dictTemp.object(forKey: "RatingName") as! String)
            cell.vwContainerOfRateButtons.tag = indexPath.row - 3
            
            //add target to radio button
            for i in 0..<5
            {
                let btnTemp = cell.viewWithTag(i+101) as! UIButton
                btnTemp.addTarget(self, action: #selector(btnRateRestaurant(sender:)), for: UIControlEvents.touchUpInside)
                
                if ((i+1) == Int(strRateVal))
                {
                    btnTemp.isSelected = true
                }
            }
            
            return cell
        }
        else if (indexPath.row == arrRatingList.count+3) //Comment Row
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell") as! Customcell
            cell.txtReviewComments.delegate = self
            
            if (strUserComment == nil)
            {
                cell.txtReviewComments.text = ""
            }
            else
            {
                cell.txtReviewComments.text = strUserComment
            }
            
            return cell
        }
        else if (indexPath.row == arrRatingList.count+4) //Overall review Row
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "totalReviewCell") as! Customcell
            cell.currReview.delegate = self
            cell.currReview.halfRatings = true

            
            if (overallRating == nil)
            {
                cell.currReview.rating = 0
            }
            else
            {
                cell.currReview.rating = overallRating
            }
            
            return cell
        }
        else //Submit review Row
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "submitCell") as! Customcell
            cell.btnSubmitReview.addTarget(self, action: #selector(self.btnSubmitReview(sender:)), for: UIControlEvents.touchUpInside)
            
            return cell
        }
    }
    
    
    //MARK: Textview delegate
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        
        
        animateViewMoving(true, moveValue: 220)
        
    }
    internal func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text.characters.count != 0)
        {
            strUserComment = textView.text
        }
        else
        {
            if let strComment = self.dictResult["Comment"]
            {
                strUserComment = strComment as! String
            }
            tblContents.reloadData()
        }
        
        animateViewMoving(false, moveValue: 220)
        
    }
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        strUserComment = textView.text + text
        
        if (text=="\n")
        {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    
    
    
    
    //MARK: Rating delegate
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float)
    {
        overallRating = rating
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //MARK:- ======BUTTON ACTIONS======
    @IBAction func btngoBack(sender: UIButton)
    {
        self.navigationController!.popViewController(animated: true)
    }
    
    
    @IBAction func btnRateRestaurant(sender: UIButton)
    {
        let vwTemp = sender.superview
        unselectReviewbuttons(buttonContainer: vwTemp!)
        sender.isSelected = true
        
        print("tag value: \(vwTemp?.tag)")
        
        let dicTemp =  NSMutableDictionary(dictionary:  arrRatingList[(vwTemp?.tag)!] as! NSDictionary)
        dicTemp["Rating"] = "\(sender.tag - 100)"
        arrRatingList.replaceObject(at: (vwTemp?.tag)!, with: dicTemp)
        
        print("rate list after edititng: \(arrRatingList)")
    }
    
    
    @IBAction func btnSubmitReview(sender: UIButton)
    {
        //Progress bar
        let size = CGSize(width: 80, height:80)
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        
        //Parameter to pass
        let arrPassing = NSMutableArray()
        let strWSURL=(GETWSURL() as String) + "AddRestaurantReview"
        
        for i in 0..<arrRatingList.count
        {
            let dictTemp = arrRatingList[i] as! NSDictionary
            let dictRateToPass = NSMutableDictionary()
            
            dictRateToPass.setValue(dictTemp["RatingID"], forKey: "Id")
            dictRateToPass.setValue(dictTemp["Rating"], forKey: "Rating")
            
            arrPassing.add(dictRateToPass)
        }
        
        let arrToPass = arrPassing as NSArray
        
        let dictParameter :[String: Any] = ["ReviewId":dictResult["Id"]!,
                                            "RestaurantId":dictResult["RestaurantId"]!,
                                            "Comment":strUserComment,
                                            "CustomerId":dictResult["CustomerId"]!,
                                            "Rating":overallRating,
                                            "ExperienceRating":arrToPass];
        
        print("parameter to pass: \(dictParameter)")
        
        
        Alamofire.request(strWSURL, method: .post, parameters: dictParameter, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            
            print ("submit response: \(response) ")
            
            if (response.result.isSuccess)
            {
                self.stopActivityAnimating()

                self.dialog = ZAlertView(title: "Foodies4u", message: "submitted successfully",closeButtonText: "Ok",
                                         closeButtonHandler: { (alertView) -> () in
                                            alertView.dismissAlertView()
                                            self.navigationController?.popViewController(animated: true)

                })
                self.dialog.show()
            }
            else
            {
                self.stopActivityAnimating()
                
                self.dialog = ZAlertView(title: "Foodies4u", message: "Sorry you have not visited the resturant yet.",closeButtonText: "Ok",
                                         closeButtonHandler: { (alertView) -> () in
                                            alertView.dismissAlertView()})
                self.dialog.show()
            }
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //MARK:- ======USER DEFINE METHODS======
    func unselectReviewbuttons(buttonContainer: UIView)
    {
        for i in 0..<5
        {
            let btnTemp = buttonContainer.viewWithTag(i+101) as! UIButton
            btnTemp.isSelected = false
        }
        
    }
    
    
    //MARK: Keyboard hide and unhide methods
    func keyboardWillShow (notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
            
            tblContents.contentInset = contentInsets
            tblContents.scrollIndicatorInsets = contentInsets
        }
    }
    
    
    func keyboardWillHide(notification: NSNotification)
    {
        tblContents.contentInset = UIEdgeInsets.zero
        tblContents.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    override func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
    
    //MARK: Get rating data
    func getRatingData()
    {
        let size = CGSize(width: 80, height:80)
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        let dictParameter = ["RestaurantId":dictRestaurantDetails["RestaurantId"]! , "CustomerId":strUSERID  as AnyObject ]
        
        print(dictParameter)
        let strWSURL=(GETWSURL() as String) + "GetRestaurantReviewByCustomerID"
        
        Alamofire.request(strWSURL, method: .post, parameters: dictParameter, encoding: URLEncoding.httpBody, headers: nil).responseJSON { response in
            
            print ("review response: \(response) ")
            
            if (response.result.isSuccess)
            {
                let dictTemp = response.result.value as! NSDictionary
                let arrResponse = dictTemp["Data"] as! NSArray
                self.dictResult = arrResponse.firstObject as! NSDictionary
                
                //Default values
                if let strComment = self.dictResult["Comment"]
                {
                    self.strUserComment = strComment as! String
                }
                
                if let tempRate = self.dictResult["Rating"]
                {
                    self.overallRating = Float(tempRate as! String)
                }
                
                guard let arrTemp = self.dictResult["ExperienceRatingData"]  else
                {
                    self.stopActivityAnimating()
                    self.dialog = ZAlertView(title: "Foodies4u", message: "Something went wrong. Please try after sometime.",closeButtonText: "Ok",
                                             closeButtonHandler: { (alertView) -> () in
                                                self.navigationController?.popViewController(animated: true)
                                                alertView.dismissAlertView()})
                    self.dialog.show()
                    
                    return
                }
                
                let arrTempRatingData = arrTemp as! NSArray
                self.arrRatingList.removeAllObjects()
                
                for i in 0..<arrTempRatingData.count
                {
                    self.arrRatingList.add(arrTempRatingData[i])
                }
                
                self.stopActivityAnimating()
                self.tblContents.reloadData()
                
                print ("array: \(self.arrRatingList)")
                
            }
            else
            {
                self.stopActivityAnimating()
                
                self.dialog = ZAlertView(title: "Foodies4u", message: "Sorry you have not visited the resturant yet .",closeButtonText: "Ok",
                                         closeButtonHandler: { (alertView) -> () in
                                            alertView.dismissAlertView()})
                self.dialog.show()
            }
        }
    }
    
    
    
    
    
    
    
    
}
