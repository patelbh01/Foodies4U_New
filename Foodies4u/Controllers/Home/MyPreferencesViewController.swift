//
//  FiltersViewController.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 11/08/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON






class MyPreferencesViewController: parentViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NVActivityIndicatorViewable,UITableViewDelegate,UITableViewDataSource
{
    
    // related to ViewAll
    @IBOutlet var viewForViewAll:UIView!
    @IBOutlet weak var tblTimingDetails:UITableView!
    @IBOutlet weak var btnMyPref:UIButton!
    var arrWeeklyTiming:NSArray!

    
    @IBOutlet weak var collectionView: UICollectionView!
    var headerView: CollectionViewHeader!


    //for tablecell and data
    var MyRestcells:MyResListCell!
    var arrDetails:NSArray!
    var arrvalueDetails:NSArray!

    
    var previouslySelectedHeaderIndex: Int?
    var selectedHeaderIndex: Int?
    var selectedItemIndex: Int?

    var strWSURL: NSString!
    var strUSERID: AnyObject!
    @IBOutlet weak var struUSERNAME :UILabel!
    @IBOutlet weak var btnUserImage :UIImageView!

    
    //array to get response from webservices
    
    var arrData: NSMutableArray!
    var arrPreferenceData: NSArray!
    
    //food type
    var arrFoodType: NSArray!
    var arrFoodTypeValue: NSArray!
    var arrFoodTypeId: NSArray!
    
    //food taste
    var arrFoodTaste: NSArray!
    var arrFoodTasteValue: NSArray!
    var arrFoodTasteId: NSArray!
    
    //cuisine
    var arrCuisine: NSArray!
    var arrCuisineValue: NSArray!
    var arrCuisineId: NSArray!
    var arrTip: NSArray!
    var strTip: String! = ""
    
    var arrName: NSMutableArray = ["  Food Taste:","  Food Type:","  Preferred Cuisine:","  Tip:"]
    
    var dictFinalCuisine:NSMutableDictionary!
    var dictFinalFoodTaste:NSMutableDictionary!
    var dictFinalType:NSMutableDictionary!
    var arrFinalCuisine:NSMutableArray!
    var arrFinalFoodTaste:NSMutableArray!
    var arrFinalType:NSMutableArray!
    
    var Ccell:CollectionViewCell!

    
    

    
    
    //MARK:LIFE CYCLE METHODS
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()

        
        
        
        //get userid value
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!
        
        
        //dismis keyboard any where you tap on the screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //webservices for getting preferences
        
        if InternetConnection.isConnectedToNetwork()
        {
            
            getUserPreferences() { responseObject, error in
                if (responseObject != nil)
                {
                let json = JSON(responseObject!)
                if let string = json.rawString()
                {
                    self.stopActivityAnimating()
                    
                    if let data = string.data(using: String.Encoding.utf8) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            
                            if (json!["Success"] as? String  == "1") {
                                
                                self.arrData = json!["Data"] as! NSMutableArray
                                
                                self.arrPreferenceData = self.arrData.value(forKey: "PreferenceData") as! NSArray
                                print(self.arrPreferenceData)
                                
                                
                                //cuisine
                                self.arrCuisine = (self.arrPreferenceData.object(at: 0) as! NSArray).value(forKey: "Cuisine") as! NSArray
                                
                                
                                self.arrCuisineValue = (self.arrCuisine.value(forKey: "Value") as! NSArray).object(at: 0) as! NSArray
                                
                                self.arrCuisineId = (self.arrCuisine.value(forKey: "Id") as! NSArray).object(at: 0) as! NSArray

                                
                                self.arrCuisine = (self.arrCuisine.value(forKey: "Cuisine") as! NSArray).object(at: 0) as! NSArray
                                
                                
                                
                                print("arrCuisine" ,self.arrCuisine)
                                
                                
                                //foodtaste
                                
                                self.arrFoodTaste = (self.arrPreferenceData.object(at: 0) as! NSArray).value(forKey: "FoodTaste") as! NSArray
                                
                                
                                self.arrFoodTasteValue = (self.arrFoodTaste.value(forKey: "Value") as! NSArray).object(at: 0) as! NSArray
                                
                                self.arrFoodTasteId = (self.arrFoodTaste.value(forKey: "Id") as! NSArray).object(at: 0) as! NSArray
                                
                                self.arrFoodTaste = (self.arrFoodTaste.value(forKey: "FoodTaste") as! NSArray).object(at: 0) as! NSArray
                                print("arrFoodTaste" ,self.arrFoodTaste)
                                
                                
                                
                                //foodype
                                self.arrFoodType = (self.arrPreferenceData.object(at: 0) as! NSArray).value(forKey: "FoodType") as! NSArray
                                
                                
                                self.arrFoodTypeValue = (self.arrFoodType.value(forKey: "Value") as! NSArray).object(at: 0) as! NSArray
                                
                                self.arrFoodTypeId = (self.arrFoodType.value(forKey: "Id") as! NSArray).object(at: 0) as! NSArray

                                
                               self.arrFoodType = (self.arrFoodType.value(forKey: "FoodType") as! NSArray).object(at: 0) as! NSArray
                                    print("arrFoodType" ,self.arrFoodType)
                                
                                
                                //tip
                                self.arrTip = (self.arrPreferenceData.object(at: 0) as! NSArray).value(forKey: "Tip") as! NSArray
                                
                                print("arrTip",self.arrTip)
                                
                                self.collectionView.dataSource = self
                                self.collectionView.delegate = self
                                self.collectionView.reloadData()

                                
                                
                            }else {
                               
                            }
                            
                            
                            
                        } catch {
                            print("Something went wrong")
                        }
                    }
                    
                }
                }
                else
                {
                    self.stopActivityAnimating()
                    self.AlertMethod()
                }
            }
        }
       
        
    }
    
     override func viewWillAppear(_ animated: Bool) {
        
        //navigation bar and back button
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationBarUI()
        self.navigationItem.title = "My Preferences"
        let img = UIImage(named: "BackArrow")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(BackTapped))

        
        // display user name

        if (UserDefaults.standard.value(forKey: "USERNAME") != nil)
        {
            struUSERNAME.text = UserDefaults.standard.value(forKey: "USERNAME") as? String
        }
        
        // display profile picture
        
        if let imageData = UserDefaults.standard.object(forKey: "USERIMAGE"),
            let image = UIImage(data: imageData as! Data)
        {
            self.btnUserImage.image = image
            self.btnUserImage.layer.borderWidth=1.0
            self.btnUserImage.layer.borderColor = UIColor.darkGray.cgColor
            self.btnUserImage.layer.cornerRadius = image.size.width/2
        }
        

    }
    
    
    //MARK: GET PREFERENCES
    func getUserPreferences(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        let size = CGSize(width: 80, height:80)
        
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        strWSURL=GETWSURL()
        
        let param1 : [ String : AnyObject] = [
            "CustomerId":strUSERID
        ]
        
        makeCall(section: strURL as String + "GetCustomerPreference?",param:param1,completionHandler: completionHandler)
        
    }
    
    
    //MARK: BUTTON'S METHODS
    
    
    @IBAction func btnClearPreferencesPressed(_ sender: UIButton!)
    {
        
        
       
        
        UIView.animate(withDuration: 0.5) { () -> Void in
            
            self.btnMyPref.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
        }
        
        UIView.animate(withDuration: 0.5, delay: 0.45, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            
            self.btnMyPref.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI * 2))
            }, completion: nil)
        
        
        for i in 0...arrFoodTaste.count - 1
        {
            let arr: NSMutableArray = self.arrFoodTasteValue.mutableCopy() as! NSMutableArray
            arr[i] = 0
            arrFoodTasteValue = arr
            
            
            for i in 0...3
            {
            let indexPath1 = IndexPath(row: i, section: 0)
            Ccell = self.collectionView!.cellForItem(at: indexPath1) as! CollectionViewCell
            Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
            }
            
            
        }
        for i in 0...arrFoodType.count - 1
        {
            
            let arr: NSMutableArray = self.arrFoodTypeValue.mutableCopy() as! NSMutableArray

            arr[i] = 0

            arrFoodTypeValue = arr
            
            let indexPath1 = IndexPath(row: i, section: 1)
            Ccell = self.collectionView!.cellForItem(at: indexPath1) as! CollectionViewCell
            Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
        }
        
        for i in 0...arrCuisine.count - 1
        {
            
            let arr: NSMutableArray = self.arrCuisineValue.mutableCopy() as! NSMutableArray
            
            arr[i] = 0
            
            arrCuisineValue = arr
            
            for i in 0...3
            {
            let indexPath1 = IndexPath(row: i, section: 2)
            Ccell = self.collectionView!.cellForItem(at: indexPath1) as! CollectionViewCell
            Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
            }
        }
        
        
        let indexPath2 = IndexPath(row: 0, section: 3)
        Ccell = self.collectionView.cellForItem(at:  indexPath2) as! CollectionViewCell!
        if (self.Ccell != nil)
        {
            Ccell.txtfieldTip.text = " "
        }
        
        /*UIView.performWithoutAnimation {
            collectionView.reloadData()
        }*/
        
        
        

        
        
    }
    
    @IBAction func btnApplyPressed(_ sender: UIButton)
    {
        
        self.view.endEditing(true)

        arrFinalType = NSMutableArray()
        arrFinalCuisine = NSMutableArray()
        arrFinalFoodTaste = NSMutableArray()

        //add food type
        
        for i in 0...arrFoodType.count - 1
        {

           if(dictFinalType != nil)
            {
                if(dictFinalType.count > 3)
                {
                self.dictFinalType.removeAllObjects()
                }

            }
            

            self.dictFinalType =
                [ "Id" : arrFoodTypeId.object(at: i), "FoodType":arrFoodType.object(at: i) as! String ,"Value":arrFoodTypeValue.object(at: i)]
            
            
             arrFinalType.add(self.dictFinalType)
            
            
        }
        
        print("arrFinalType" ,arrFinalType)

        
        //add food taste
        for i in 0...self.arrFoodTaste.count-1
        {
            
            print("bhumika")

            if(dictFinalFoodTaste != nil)
            {
                
                if(dictFinalFoodTaste.count > self.arrFoodTaste.count)
                {
                    self.dictFinalFoodTaste.removeAllObjects()
                }
                
            }
            self.dictFinalFoodTaste =
                [ "Id" : arrFoodTasteId.object(at: i), "FoodTaste":arrFoodTaste.object(at: i) as! String ,"Value":arrFoodTasteValue.object(at: i)]
            
            
            arrFinalFoodTaste.add(self.dictFinalFoodTaste)
            
            
        }

        
        //add food cuisine
        
        for i in 0...self.arrCuisine.count-1
        {
            
            if(dictFinalCuisine != nil)
            {
                if(dictFinalCuisine.count > arrCuisine.count)
                {
                    self.dictFinalCuisine.removeAllObjects()
                }
                
            }
            
            
            self.dictFinalCuisine =
                [ "Id" : arrCuisineId.object(at: i) , "Cuisine":arrCuisine.object(at: i) as! String , "Value":arrCuisineValue.object(at: i)]
            
            
            arrFinalCuisine.add(self.dictFinalCuisine)
            
            
        }
        
        print("arrFinalCuisine",arrFinalCuisine)
       
        
        //webservice to add preferences
        
        if InternetConnection.isConnectedToNetwork()
        {
            
            
            let size = CGSize(width: 80, height:80)
            
            startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
            
            strWSURL=GETWSURL()
            
            let param1 : [ String : AnyObject] = [
                "CustomerId":strUSERID  as AnyObject,
                "FoodTaste":arrFinalFoodTaste as AnyObject,
                "Cuisine":  arrFinalCuisine as AnyObject ,
                "FoodType": arrFinalType as AnyObject,
                "Tip":10.0 as AnyObject
            ]
            
            print(param1)
            
            Alamofire.request(strWSURL as String + "AddCustomerPreference?",method: .post, parameters: param1,  encoding: JSONEncoding.default, headers: nil).responseJSON
                { response in switch response.result {
                    
                    
                case .success(let JSON):
                    
                    print("Success with JSON: \(JSON)")
                    
                    let response = JSON as! NSDictionary
                    
                    self.stopActivityAnimating()
                    
                    if ((response.value(forKey: "Success") as? String) == "1")
                    {
                        let dialog = ZAlertView(title: "Foodies4u", message: "Preferences set successfully",closeButtonText: "Ok",
                                                 closeButtonHandler: { (alertView) -> () in
                                                    alertView.dismissAlertView()})
                        dialog.show()
                        
                    }
                    
                    
                case .failure(let error):
                    self.stopActivityAnimating()
                    
                    print("Request failed with error: \(error)")
                    }
            }

    }
    }
    
    @IBAction func btnViewAllClicked(_ sender: UIButton){
        
        tblTimingDetails.dataSource = self
        tblTimingDetails.delegate = self

        viewForViewAll.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        viewForViewAll.isHidden = false
        
        
        if(sender.tag == 0)
        {
            self.arrDetails = self.arrFoodTaste
            self.arrvalueDetails = self.arrFoodTasteValue
        }
        else
        {
            self.arrDetails = self.arrCuisine
            self.arrvalueDetails = self.arrCuisineValue


        }
        tblTimingDetails.reloadData()

    }
    @IBAction func btnCloseViewAllClicked(_ sender: UIButton){
        
        viewForViewAll.isHidden = true
      
        
    }
    
    func BackTapped()
    {
        self.navigationController!.popViewController(animated: false)
    }
    
    func ValueSelected(_ sender:UIButton)

    {
        
        let selectedButtonCell = sender.superview?.superview as! CollectionViewCell
        //Incase your button is inside cell.contentview
        // var selectedButtonCell = sender.superview.superview as! UICollectionviewCell
        let indexPath = collectionView.indexPath(for: selectedButtonCell)
        print(indexPath)
        let row = indexPath?.row
        let arr: NSMutableArray = self.arrFoodTasteValue.mutableCopy() as! NSMutableArray

        
        // for food taste
        if(indexPath?.section == 0)
        {
            
            
            for i in 0...3
            {
                let indexPath1 = IndexPath(row: i, section: 0)
                Ccell = self.collectionView!.cellForItem(at: indexPath1) as! CollectionViewCell
                Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                if( i == row)
                {
                    if(arr[row!] as! Int == 0)
                    {
                        Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
                        
                        arr[row!]  = 1
                        
                    }
                    else
                    {
                        arr[row!]  = 0
                        
                        Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                        
                    }
                }
                else
                {
                    arr[i]  = 0

                }

                
            }
            
           // Ccell = self.collectionView!.cellForItem(at: indexPath!) as! CollectionViewCell
            
            
            
            for i in 4...arrFoodTasteValue.count - 1
            {
                if( i == row)
                {
                    
                    if(arr[row!] as! Int == 0)
                    {
                        Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
                        
                        arr[row!]  = 1
                        
                    }
                    else
                    {
                        arr[row!]  = 0
                        
                        Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                        
                    }

                }
                else
                {
                    arr[i]  = 0

                }
            }
            
            
            arrFoodTasteValue = arr
            print(arr)
            print("gjsfjs" ,arrFoodTasteValue)
            

        }
        
        // for food type
        
        if(indexPath?.section == 1)
        {
            
            for i in 0...2
            {
            
                let indexPath1 = IndexPath(row: i, section: 1)
                print(indexPath1)
                Ccell = self.collectionView!.cellForItem(at: indexPath1) as! CollectionViewCell
                Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                arr[i]  = 0
                
                
            }
            
            Ccell = self.collectionView!.cellForItem(at: indexPath!) as! CollectionViewCell
            Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
            
            if(arr[row!] as! Int == 0)
            {
                Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
                
                arr[row!]  = 1
                
            }
            else
            {
                arr[row!]  = 0
                
                Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                
            }
            
            arrFoodTypeValue = arr
            print(arr)
            print(arrFoodTypeValue)
            
            
        }
            
        // for cuisine
            
        else
        {
            let arr: NSMutableArray = self.arrCuisineValue.mutableCopy() as! NSMutableArray

            if(arr[row!] as! Int == 1)
            {
                arr[row!]  = 0
                
            }
            else
                
            {
                arr[row!]  = 1
                
            }
            
            print(arr)
            
            arrCuisineValue = arr  

            print(arrCuisineValue)
            
            
            UIView.performWithoutAnimation {
                
                
                let indexPath = IndexPath(row: (indexPath?.row)!, section: (indexPath?.section)!)
                self.collectionView!.reloadItems(at: [indexPath])
                
            }


        }
        
        
        
        
    }
    
    func ValueSelectedtbl(_ sender:UIButton)
        
    {
        
        let selectedButtonCell = sender.superview?.superview as! MyResListCell
        //Incase your button is inside cell.contentview
        // var selectedButtonCell = sender.superview.superview as! UICollectionviewCell
        let indexPath = tblTimingDetails.indexPath(for: selectedButtonCell)
        
        print(indexPath)
        
        let row = indexPath?.row
        
        if(arrDetails .contains("Spicy"))
        {
            
            let arr: NSMutableArray = self.arrvalueDetails.mutableCopy() as! NSMutableArray

            for i in 0...self.arrDetails.count-1
            {
                let indexPath1 = IndexPath(row: i, section: 0)
                print(indexPath1)
                MyRestcells = self.tblTimingDetails!.cellForRow(at: indexPath1) as! MyResListCell
                MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                
                if( i == row)
                {
                    
                    if(arr[row!] as! Int == 0)
                    {
                        MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
                        
                        arr[row!]  = 1
                        
                    }
                    else
                    {
                        arr[row!]  = 0
                        
                        MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                        
                    }
                    
                }
                else
                {
                    arr[i]  = 0
                    
                }

                
            }
            
           /* let indexPath1 = IndexPath(row: row!, section: 0)
            MyRestcells = self.tblTimingDetails!.cellForRow(at: indexPath1) as! MyResListCell
            MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())*/
            
           // arr[row!]  = 1
            
            arrvalueDetails = arr
            print(arr)
            print(arrvalueDetails)
            self.arrFoodTasteValue = self.arrvalueDetails
            self.collectionView!.reloadData()
            
            
            
        }
        else
        {
            let arr: NSMutableArray = self.arrvalueDetails.mutableCopy() as! NSMutableArray
            
            if(arr[row!] as! Int == 1)
            {
                arr[row!]  = 0
                
            }
            else
                
            {
                arr[row!]  = 1
                
            }
            
            print(arr)
            
            arrvalueDetails = arr
            
            print(arrvalueDetails)
            
            self.arrCuisineValue = self.arrvalueDetails
            self.collectionView!.reloadData()
           
            
            UIView.performWithoutAnimation {
                self.tblTimingDetails!.reloadData()
                
            }
        }
        
       
        
        
    }
    
    //MARK: COLLECTIONVIEW DATASOURCE AND DELEGATES METHODS
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return arrName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        if(section == 1)
        {
            return 3
        }
        if(section == 3)
        {
            return 1
        }
            return 4
            
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if(indexPath.section < 3)
        {
            
        
        Ccell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
            
        //image 
        
        if (indexPath as NSIndexPath).section == 0
        {

            
            Ccell.backgroundColor = #colorLiteral(red: 0.9691255689, green: 0.9698591828, blue: 0.9692392945, alpha: 1)
            
            Ccell.btnPrefName.setTitle(self.arrFoodTaste.object(at: (indexPath as NSIndexPath).row) as? String , for: UIControlState())
            
            if(self.arrFoodTasteValue.object(at: (indexPath as NSIndexPath).row) as? Int == 1)
            {
                self.Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())

            }
            else
            {
                self.Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())

            }
            

            
            if(self.arrFoodTaste.count > 3)
            {
                if(indexPath.row == 3)
                {
                    Ccell.btnReview.isHidden = false
                    Ccell.btnReview.tag = (indexPath as NSIndexPath).section
                    Ccell.btnReview.addTarget(self, action: #selector(self.btnViewAllClicked(_:)), for: UIControlEvents.touchUpInside)

                }
                else{
                    
                    Ccell.btnReview.isHidden = true
                }
            }
            else
            {
                
                Ccell.btnReview.isHidden = true

            }
            
        }
        
        if (indexPath as NSIndexPath).section == 1
        {
            
            
            if(self.arrFoodTypeValue.object(at: (indexPath as NSIndexPath).row) as? Int == 1)
            {
                self.Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
                
            }
            else
            {
                self.Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                
            }
            
            Ccell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

            
            Ccell.btnPrefName.setTitle(self.arrFoodType.object(at: (indexPath as NSIndexPath).row) as? String , for: UIControlState())
            
            Ccell.btnReview.isHidden = true

            
        }
        
        if (indexPath as NSIndexPath).section == 2
        {
            
            Ccell.backgroundColor = #colorLiteral(red: 0.9691255689, green: 0.9698591828, blue: 0.9692392945, alpha: 1)

            if(self.arrCuisineValue.object(at: (indexPath as NSIndexPath).row) as? Int == 1)
            {
                self.Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
                
            }
            else
            {
                self.Ccell.btnRadio.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                
            }
            
            Ccell.btnPrefName.setTitle(self.arrCuisine.object(at: (indexPath as NSIndexPath).row) as? String , for: UIControlState())
            
            if(self.arrCuisine.count > 3)
            {
                if(indexPath.row == 3)
                {
                    Ccell.btnReview.isHidden = false
                    Ccell.btnReview.isHidden = false
                    Ccell.btnReview.tag = (indexPath as NSIndexPath).section
                    Ccell.btnReview.addTarget(self, action: #selector(self.btnViewAllClicked(_:)), for: UIControlEvents.touchUpInside)
                    
                }
                else{
                    
                    Ccell.btnReview.isHidden = true
                }
            }
            else
            {
                Ccell.btnReview.isHidden = true
                
            }
            
        }
            
        Ccell.btnSelCell.addTarget(self, action: #selector(ValueSelected(_:)), for: UIControlEvents.touchUpInside)

        }
        else
        {
            Ccell.backgroundColor = #colorLiteral(red: 0.9725260139, green: 0.9732618928, blue: 0.9726399779, alpha: 1)
            
            Ccell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellTip", for: indexPath) as! CollectionViewCell

           
            
            //for bottom border
            let border = CALayer()
            let width = CGFloat(2.0)
            border.borderColor = UIColor.black.cgColor
            border.frame = CGRect(x: 0, y: Ccell.txtfieldTip.frame.size.height - width, width:  Ccell.txtfieldTip.frame.size.width, height: Ccell.txtfieldTip.frame.size.height)
            
            border.borderWidth = width
            Ccell.txtfieldTip.layer.addSublayer(border)
            Ccell.txtfieldTip.layer.masksToBounds = true
            Ccell.txtfieldTip.isUserInteractionEnabled = true
            


            
        }
        
        return Ccell
    }
    
    //Header of collectionView
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        
        
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as! CollectionViewHeader

            if (indexPath.section == 0 || indexPath.section == 2)
            {
                headerView.backgroundColor = #colorLiteral(red: 0.9691255689, green: 0.9698591828, blue: 0.9692392945, alpha: 1)

            }
            else
            {
                headerView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

            }
            
            headerView.lblSection.text = arrName[(indexPath as NSIndexPath).section] as? String
            
            
            return headerView

        default:
            
            fatalError("Unexpected element kind")
        }
        

    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCell: CGFloat = 2
        let cellWidth = collectionView.frame.size.width / numberOfCell
        let cellHeight = collectionView.frame.size.height / 12
        print(cellHeight)
        
        if(indexPath.section == 3)
        {
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height / 3)

        }
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    // MARK: - TABLEVIEW DATASOURCE AND DELEGATES
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrDetails.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        MyRestcells = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)  as! MyResListCell
        
        MyRestcells.lblTitle.text = (self.arrDetails.object(at: (indexPath as NSIndexPath).row)) as? String
        
        
        if(self.arrvalueDetails.object(at: (indexPath as NSIndexPath).row) as? Int == 1)
        {
            
            MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
            
        }
        else
        {
            MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
        }
        
       MyRestcells.btnSelectCoupon.tag = (indexPath as NSIndexPath).row
       MyRestcells.btnSelectCoupon.addTarget(self, action: #selector(self.ValueSelectedtbl(_:)), for: UIControlEvents.touchUpInside)
        
        
        return MyRestcells
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    //MARK:Textfield Delegates & Datasource Method

    
    func textFieldDidBeginEditing(_ tF: UITextField)
    {
        
        animateViewMoving(true, moveValue: 215)
        
        
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        
        animateViewMoving(false, moveValue: 215)
        
            strTip = textField.text
        
        
    }
    
}
