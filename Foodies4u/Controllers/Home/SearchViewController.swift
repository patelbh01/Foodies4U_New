//
//  SearchViewController.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 10/08/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import NVActivityIndicatorView
import GoogleMaps
import SwiftyJSON

class SearchViewController: parentViewController, UITableViewDelegate, UITableViewDataSource,NVActivityIndicatorViewable,UITextFieldDelegate, CLLocationManagerDelegate
{
    @IBOutlet var tblSearch: UITableView!
    @IBOutlet var tblRestList: UITableView!
    
    
    
    //for Calling webservices
    var strWSURL: NSString!
    var strUSERID: AnyObject!
    
    //for Search
    var strSearchType: String!
    var strSearchCategory: String!
    
    //for filters
    
    var FoodType: Int! = 0
    var strMinPrice: String! = ""
    var strMaxPrice: String! = ""
    var Category: Int! = 0
    var strCuisines: String! = ""
    var strSortBy: String! = ""
    var strType: String! = ""
    var strDistance: String! = "5"
    var strOffers: String! = ""
    var strFoodType: String! = ""
    
    //  var strSearchType: String! = " "
    //  var strSearchCategory: String! = " "
    
    var OpenNow: String! = "false"
    
    var arrSearchName: NSArray! = NSArray()
    var arrSearchCategory: NSArray!=NSArray()
    var arrRestList: NSMutableArray!
    
    
    //for location lat-long
    var latitude:Double!
    var longitude:Double!
    var locationManager: CLLocationManager!
    
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var btncancel: UIButton!
    
    var flag: Bool!
    var MyRestcells:MyResListCell!
    
    @IBOutlet weak var txtfieldsearch:UITextField!
    
    var param1 = [String: AnyObject]()


      //MARK: LIFECYCLE METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //this userdeafualts is used when you come to search from home menu.
        UserDefaults.standard .set(true, forKey: "SEARCHFORFILTERS")
        
        btncancel.isHidden = true
        
        //customize textfield
        
        txtfieldsearch.leftViewMode = .always
        let emailImgContainer = UIView(frame: CGRect(x: txtfieldsearch.frame.origin.x, y: txtfieldsearch.frame.origin.y, width: 30.0, height: 30.0))
        txtfieldsearch.leftView = emailImgContainer
        let emailImView = UIImageView(frame: CGRect(x: 10,y: 7, width: 20.0, height: 20.0))
        emailImView.image = UIImage(named:"search")
        emailImgContainer.addSubview(emailImView)
        let strPlaceholder = "Restaurant Or Cuisine"
        txtfieldsearch.attributedPlaceholder = NSAttributedString(string:strPlaceholder, attributes:[NSForegroundColorAttributeName: UIColor.gray])
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        //hide search table anf label
        tblSearch.isHidden=true
        lblResult.isHidden=true
        
        //and get current location
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        if !(UserDefaults.standard.bool(forKey: "SEARCHDONEANDAPPLYFILETR"))
        {
            self.txtfieldsearch.becomeFirstResponder()
            
        }
        
        

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        self.navigationController?.navigationBar.isHidden=false
        customBarButtons()
        self.navigationBarUI()
        
        
        // this webservice will call beacuse if you have filter value than it will show you response with filter
        
        if InternetConnection.isConnectedToNetwork()
        {
        
        getOrders() { responseObject, error in
            
            if (responseObject != nil)
            {            let json = JSON(responseObject!)
            if let string = json.rawString()
            {
                //print("json ",json)
                self.stopActivityAnimating()
                
                
                if let data = string.data(using: String.Encoding.utf8) {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                        print("json ",json)
                        
                        if( json!["Success"] as? String  == "1")
                        {
                            
                            
                            //this userdeafualts is used when you have done filter and after that you aredoing search
                            
                            if(UserDefaults.standard.bool(forKey: "SEARCHDONEANDAPPLYFILETR"))
                            {
                                self.txtfieldsearch.resignFirstResponder()
                                
                                self.arrRestList = json!["Data"] as! NSMutableArray
                                
                                self.tblRestList.isHidden=false
                                self.tblRestList.delegate=self
                                self.tblRestList.dataSource=self
                                
                                self.tblRestList.reloadData()
                                self.lblResult.isHidden=true
                            }
                            
                        }
                        else
                        {
                            
                            self.txtfieldsearch.becomeFirstResponder()
                        }
                    }
                        
                    catch {
                        
                        print("Something went wrong")
                    }
                }
            }
            }
            else
            {
                self.stopActivityAnimating()
                self.AlertMethod()
            }
            
            
        }
        }
    }
    
    //MARK: CUSTOMIZE NAVIGATIONBAR AND IT'S METHODS
    
    func customBarButtons()
    {
        //side menu button
        
        let img = UIImage(named: "BackArrow")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(backPressed))
        
        //filters button
        
        //filters button
        let btnFilters = UIButton(frame: CGRect(x: self.view.frame.width-75, y: 0, width: 20, height: 20))
        btnFilters.setBackgroundImage(UIImage(named: "FilterIcon.png"), for: UIControlState())
        btnFilters.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)),  for: .touchUpInside)
        let rightBarButtonItemDelete: UIBarButtonItem = UIBarButtonItem(customView: btnFilters)
        self.navigationItem.setRightBarButton(rightBarButtonItemDelete, animated: true)
        
        
        if self.revealViewController() != nil
        {
            view.addGestureRecognizer(revealViewController().tapGestureRecognizer())
        }
        
    }
    
    
    func CancelTapped()
    {
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromTop
        self.view.layer.add(transition, forKey: kCATransition)
        self.navigationController!.popViewController(animated: false)
        
    }
    
    //MARK: BUTTONS METHODS
    
    @IBAction func btnCancelSearchPressed(_ sender: UIButton!)
    {
        strSearchType =  ""
        strSearchCategory =  ""
        self.tblSearch.isHidden=true
        self.txtfieldsearch.text = " "
        btncancel.isHidden = true
    }
    func backPressed(_ sender: AnyObject)
    {
        // this will hold your filters and nil you searchtext and search category
        
        UserDefaults.standard.set(false, forKey:"SEARCHFORFILTERS")
        
        
        let HViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        self.navigationController?.pushViewController(HViewControllerObj!, animated: false)
        
        // pass the value of filters to Home view
        
        HViewControllerObj?.OpenNow =  self.OpenNow as String!
        HViewControllerObj?.strFoodType = self.strFoodType as String! as String!
        HViewControllerObj?.Category =  self.Category
        HViewControllerObj?.strDistance =  self.strDistance as String! as String!
        HViewControllerObj?.strCuisines =  self.strCuisines as String! as String!
        HViewControllerObj?.strOffers =  self.strOffers as String!
        HViewControllerObj?.strMinPrice =  self.strMinPrice as String!
        HViewControllerObj?.strMaxPrice =  self.strMaxPrice as String!
        HViewControllerObj?.strSearchType =  ""
        HViewControllerObj?.strSearchCategory =  ""
        HViewControllerObj?.latitude = self.latitude
        HViewControllerObj?.longitude = self.longitude
        
        
    }
    
    
    //MARK: TEXTFIELD DATASOURCE & DELEGATES METHODS
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        // this usedeafult is set for  to make your search text and search category nil only filters will remain .
        UserDefaults.standard .set(false, forKey: "SEARCHDONEANDAPPLYFILETR")
        flag=true
        print("textField.text", textField.text)
        print("string", string)
        if InternetConnection.isConnectedToNetwork()
        {
            if string.characters.count != 0
            {
                btncancel.isHidden = false
                lblResult.isHidden=true
                
                strSearchType=textField.text! + string
                strSearchCategory=""
                
                
                
                
                getOrders() { responseObject, error in
                    
                    if (responseObject != nil)
                    {                    let json = JSON(responseObject!)
                    if let string = json.rawString()
                    {
                        print("json ",json)
                        self.stopActivityAnimating()
                        
                        
                        if let data = string.data(using: String.Encoding.utf8) {
                            do {
                                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                                print("json ",json)
                                
                                if( json!["Success"] as? String  == "1")
                                {
                                    
                                    let arrData:NSMutableArray = json!["Data"] as! NSMutableArray
                                    self.arrSearchName = arrData.value(forKey: "Name") as! NSArray
                                    self.arrSearchCategory = arrData.value(forKey: "Category") as! NSArray
                                    
                                    self.tblSearch.isHidden=false
                                    self.tblSearch.delegate=self
                                    self.tblSearch.dataSource=self
                                    self.tblSearch.frame=CGRect(x: self.tblSearch.frame.origin.x, y: self.tblSearch.frame.origin.y, width: self.view.frame.width, height: CGFloat(arrData.count*44))
                                    self.tblSearch.reloadData()
                                    
                                }
                                else
                                {
                                    print("item 1")
                                    self.lblResult.isHidden=false
                                    self.tblSearch.isHidden=true
                                    self.tblRestList.isHidden=true
                                    //self.arrSearchName = nil
                                    //self.arrSearchCategory = nil
                                    
                                }
                            }
                                
                            catch {
                                
                                print("Something went wrong")
                            }
                        }
                    }
                    }
                    else
                    {
                        self.stopActivityAnimating()
                        self.AlertMethod()
                    }
                    
                    
                }
                
                
            }
                
            else
            {
                if (txtfieldsearch.text?.characters.count != 0)
                {
                    btncancel.isHidden = false
                    
                    lblResult.isHidden=true
                    
                    strSearchType = String(txtfieldsearch.text!.characters.dropLast())
                    if strSearchType?.characters.count != 0
                    {
                        
                        strSearchCategory=""
                        strMinPrice=""
                        strMaxPrice=""
                        
                        getOrders() { responseObject, error in
                            
                            if (responseObject != nil)
                            {
                                let json = JSON(responseObject!)
                            if let string = json.rawString()
                            {
                                print("json ",json)
                                self.stopActivityAnimating()
                                
                                
                                if let data = string.data(using: String.Encoding.utf8) {
                                    do {
                                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                                        print("json ",json)
                                        
                                        if( json!["Success"] as? String  == "1")
                                        {
                                            let arrData:NSMutableArray = json!["Data"] as! NSMutableArray
                                            self.arrSearchName = arrData.value(forKey: "Name") as! NSArray
                                            self.arrSearchCategory = arrData.value(forKey: "Category") as! NSArray
                                            
                                            self.tblSearch.isHidden=false
                                            self.tblSearch.delegate=self
                                            self.tblSearch.dataSource=self
                                            self.tblSearch.frame=CGRect(x: self.tblSearch.frame.origin.x, y: self.tblSearch.frame.origin.y, width: self.view.frame.width, height: CGFloat(arrData.count*44))
                                            self.tblSearch.reloadData()
                                            
                                        }
                                        else
                                        {
                                            print("item 2")
                                            
                                            if (self.strSearchType?.characters.count != 0)
                                            {
                                                self.lblResult.isHidden=false
                                                
                                            }
                                            
                                            self.tblSearch.isHidden=true
                                            self.arrSearchName = nil
                                            self.arrSearchCategory = nil
                                            
                                        }
                                    }
                                        
                                    catch {
                                        
                                        print("Something went wrong")
                                    }
                                }
                            }
                            }
                            else
                            {
                                self.stopActivityAnimating()
                                self.AlertMethod()
                            }
                            
                            
                        }
                    }
                    else
                    {
                        btncancel.isHidden = true
                        
                        self.tblSearch.isHidden=true
                        self.arrSearchName = nil
                        self.arrSearchCategory = nil
                    }
                }
                else
                {
                    print("item 3")
                    
                    self.tblSearch.isHidden=true
                    self.arrSearchName = nil
                    self.arrSearchCategory = nil
                }
                
            }
        }
        return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //MARK: TABLEVIEW DATASOURCE & DELEGATES METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView==self.tblSearch
        {
            return arrSearchName.count
            
        }
        return arrRestList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView==self.tblSearch
        {
            
            //display searching result
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell!
            
            let lblCountryName = cell?.viewWithTag(10) as! UILabel
            
            lblCountryName.text = (arrSearchName.object(at: (indexPath as NSIndexPath).row) as! String)  + " in " + (arrSearchCategory.object(at: (indexPath as NSIndexPath).row) as! String)
            lblCountryName.numberOfLines = 0;
            return cell!
            
        }
        else
        {
            //display resturants deatils
            
            MyRestcells = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyResListCell
            
            // Name
            MyRestcells.lblRestName.text=(self.arrRestList.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Name") as? String
            
            // address
            MyRestcells.lblRestAddress.text=(self.arrRestList.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Address1") as? String
            
            // distance
            
            let distanceVal: String = (self.arrRestList.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Distance") as! String
            MyRestcells.lblDistance.text = distanceVal + " miles away"
            
            MyRestcells.lblDistance.numberOfLines = 0
            MyRestcells.lblDistance.sizeToFit()
            
            // ratings
            MyRestcells.lblRating.text=(self.arrRestList.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Ratings") as? String
            MyRestcells.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
            MyRestcells.floatRatingView.fullImage = UIImage(named: "StarFull")
            // Optional params
            MyRestcells.floatRatingView.maxRating = 5
            MyRestcells.floatRatingView.minRating = 1
            MyRestcells.floatRatingView.rating = (((self.arrRestList.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Ratings") as! NSString).floatValue)
            MyRestcells.floatRatingView.floatRatings = true
            
            //veg  & nonveg
            if((self.arrRestList.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "FoodType") as? String  == "2")
            {
                MyRestcells.imgVeg.isHidden = false
                MyRestcells.imgVeg.image = UIImage(named: "NonVegIcon")
                MyRestcells.imgNonVeg.isHidden = true
                
                
            }
                
            else if((self.arrRestList.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "FoodType") as? String  == "1")
            {
                MyRestcells.imgVeg.isHidden = false
                MyRestcells.imgVeg.image = UIImage(named: "VegIcon")
                MyRestcells.imgNonVeg.isHidden = true
                
            }
            else if((self.arrRestList.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "FoodType") as? String  == "")
            {
                
                MyRestcells.imgNonVeg.isHidden = true
                MyRestcells.imgVeg.isHidden = true
                
            }
            else
                
            {
                MyRestcells.imgVeg.image = UIImage(named: "VegIcon")
                MyRestcells.imgNonVeg.image = UIImage(named: "NonVegIcon")
                MyRestcells.imgNonVeg.isHidden = false
                MyRestcells.imgVeg.isHidden = false
                
            }
            // Images
            
            MyRestcells.ImgViewRest.image = UIImage(named:"Placeholder")
            
            /* let url = NSURL(string: "http://foodies4you.intransure.com/Media/Restaurant/Thumbs/0000/"+(self.arrRestList.objectAtIndex(indexPath.row).valueForKey("RestaurantImage") as? String)!)
             let data = NSData(contentsOfURL: url!)
             if(data ==  nil)
             {
             MyRestcells.ImgViewRest.image = UIImage(named:"Placeholder")
             }
             else
             {
             MyRestcells.ImgViewRest.image = UIImage(data: data!)
             }*/
            return MyRestcells
        }
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (tableView == tblSearch)
        {
            txtfieldsearch.resignFirstResponder()
            txtfieldsearch.text = ""
            btncancel.isHidden = true
            tblSearch.isHidden=true
            flag=false
            
            //animation
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.5)
            let transition = CATransition()
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromTop
            CATransaction.commit()
            self.tblSearch.layer.add(transition, forKey: kCATransition)
            
            
            strSearchType = arrSearchName.object(at: (indexPath as NSIndexPath).row) as? String
            strSearchCategory = (arrSearchCategory.object(at: (indexPath as NSIndexPath).row) as! String)
            UserDefaults.standard.setValue(strSearchType, forKey: "SEARCHTEXT")
            UserDefaults.standard.setValue(strSearchCategory, forKey: "SEARCHTEXTCATEGORY")
            
            if InternetConnection.isConnectedToNetwork()
            {
                self.perform(#selector(callwebservice), with: nil, afterDelay: 0.1)
            }
            
        } else {
            
            let arrSel:NSMutableDictionary = self.arrRestList.object(at: (indexPath as NSIndexPath).row) as! NSMutableDictionary
            
            let restaurantDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "RestListDetailsViewController") as? RestListDetailsViewController
            
            restaurantDetailsVC!.dictRestDetails = arrSel
            
            self.navigationController?.pushViewController(restaurantDetailsVC!, animated: true)
        }
        
    }
    
    //MARK: CALL WEBSERVICES
    func callwebservice()
    {
        getOrders() { responseObject, error in
            
            if (responseObject != nil)
            {
            print("responseObject = \(responseObject); error = \(error)")
            let json = JSON(responseObject!)
            if let string = json.rawString()
            {
                //print("json ",json)
                // self.stopActivityAnimating()
                
                
                if let data = string.data(using: String.Encoding.utf8) {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                        print("json ",json)
                        
                        if( json!["Success"] as? String  == "1")
                        {
                            
                            let imgFilter = UIImage(named: "FilterIcon.png")
                            let btnFilters = UIButton(frame: CGRect(x: self.view.frame.width-35, y: 0, width: 20, height: 20))
                            btnFilters.setBackgroundImage(imgFilter, for: UIControlState())
                            btnFilters.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
                            let rightBarButtonItemEdit: UIBarButtonItem = UIBarButtonItem(customView: btnFilters)
                            
                            self.navigationItem.setRightBarButton(rightBarButtonItemEdit, animated: true)
                            
                            self.arrRestList = json!["Data"] as! NSMutableArray
                            
                            self.tblRestList.isHidden=false
                            self.tblRestList.delegate=self
                            self.tblRestList.dataSource=self
                            
                            self.tblRestList.reloadData()
                            self.lblResult.isHidden=true
                            
                        }
                        else
                        {
                            if (self.strSearchType?.characters.count != 0)
                            {
                                self.lblResult.isHidden=false
                                
                            }
                            
                            self.tblRestList.isHidden=true
                            self.arrSearchName = nil
                            self.arrSearchCategory = nil
                            
                        }
                    }
                        
                    catch {
                        
                        print("Something went wrong")
                    }
                }
            }
            }
            else
            {
                self.stopActivityAnimating()
                self.AlertMethod()
            }
            
            
        }
    }
    
    
    
    func getOrders(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        
        // this usedeafult is set for  to hold your search text and search category when you came from filter screen
        if(UserDefaults.standard.bool(forKey: "SEARCHDONEANDAPPLYFILETR"))
            
        {
            
            strSearchType = UserDefaults.standard.string(forKey: "SEARCHTEXT")
            strSearchCategory = UserDefaults.standard.string(forKey: "SEARCHTEXTCATEGORY")
        }
        //get userid value
        
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!

        
        strWSURL=GETWSURL()
        
      param1 = [
            "search":strSearchType as AnyObject,
            "type":strSearchCategory as AnyObject,
            "distance":strDistance as AnyObject,
            "foodtype":strFoodType as AnyObject,
            "category":Category as AnyObject ,
            "minprice":strMinPrice as AnyObject,
            "maxprice":strMaxPrice as AnyObject,
            "offers":strOffers as AnyObject ,
            "cuisines":strCuisines as AnyObject,
            "opennow": OpenNow as AnyObject ,
            "Latitude": latitude  as AnyObject,
            "Longitude": longitude  as AnyObject,
            "sortby": "ASC" as AnyObject,
            "customerid":strUSERID as AnyObject
        ]
        
      
        
        makeCall(section: strWSURL as String + "SearchContext?",param: param1 ,completionHandler: completionHandler)
        
        
    }
    
    //MARK: CLLOCATION MANAGER DELEGATE METHODS
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
//        let location:CLLocation = locations.last!
//        self.latitude = location.coordinate.latitude
//        self.longitude = location.coordinate.longitude
        
           UserDefaults.standard.set(self.latitude, forKey: "LATITUDE")
           UserDefaults.standard.set(self.longitude, forKey: "LONGITUDE")
          UserDefaults.standard.synchronize()
        manager.stopUpdatingLocation()
        print(latitude)
        print(longitude)
        
    }
    
}
