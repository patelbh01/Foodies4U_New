




//
//  HomeViewController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Alamofire
import NVActivityIndicatorView
import GoogleMaps
import GoogleMapsBase
import GoogleMapsCore
import GooglePlacePicker


import SwiftyJSON
import AddressBookUI
import Contacts
import ContactsUI





class HomeViewController: parentViewController, CLLocationManagerDelegate,NVActivityIndicatorViewable,MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,SWRevealViewControllerDelegate
{
    
    
    var MyRestcells:MyResListCell!
    @IBOutlet var ibSwitch: SevenSwitch!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnShowCurrentLocation: UIButton!

    var Ccell:CollectionViewCell!
    var dialog = ZAlertView()

    
    var btnMenu=UIButton()
    
    
    
    var firstView: UIView!
    var secondView: UIView!
    var DynamicView:UIView! = nil
    
    var strWSURL: NSString!
    var strUSERID: AnyObject!
    var imageName:NSString!
    var arrvalueDetails:NSArray!

    
    var arrData:  NSMutableArray = []
    var arrLatitude: NSMutableArray!
    var arrLongitude: NSMutableArray!
    var arrName: NSMutableArray!
    var arrNumber: NSMutableArray!
    var arrDistance: NSMutableArray!
    
    let arrDefaultImage: NSMutableArray = ["Distance-1.png","Rating1.png","Cost-1.png","A-to-Z-1.png"]
    
    let arrImage: NSMutableArray = ["Distance-2.png","Rating-2.png","Cost-2.png","A-to-Z-2.png"]
    
    let arrSelImage: NSMutableArray = ["Distance-3.png","Rating-3.png","Cost-3.png","A-to-Z-3.png"]
    
    let arrSort:NSMutableArray = ["Distance","Rating","Cost","Name"]
    
    let arrLocationImg = ["Home-Address.png", "Work-Address.png", "AddressIcon.png"]

    
    var arrSelIndex: NSMutableArray = []
    
    var arrSelSaveIndex: NSMutableArray = []
    
    
    fileprivate var mapChangedFromUserInteraction = false
    var a: Bool = false
    
    var locationManager: CLLocationManager!
    var latitude:Double!
    var longitude:Double!
    var milesWide:Double!
    
    var arr: NSDictionary!
    var ann: MapDataModel!
    
    var i:Int=0
    var TapCell:Int=0
    var sidebarMenuOpen:Bool = false
    
    
    @IBOutlet weak var tblLocation:UITableView!
    @IBOutlet weak var tblRestData:UITableView!
    @IBOutlet weak var viewRestList:UIView!
    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var btnGetinLine:UIButton!
    @IBOutlet weak var btnCurrentLocation:UIButton!
    @IBOutlet weak var viewLocation:UIView!
    @IBOutlet weak var lblNoData:UILabel!


    //for filters
    var strSearchType: String! = ""
    var strSearchCategory: String! = ""
    var FoodType: Int! = 0
    var strMinPrice: String! = ""
    var strMaxPrice: String! = ""
    var Category: Int! = 0
    var strCuisines: String! = ""
    var strSortBy: String! = ""
    var strType: String! = ""
    var strDistance: String! = "5"
    var strOffers: String! = ""
    var OpenNow: String! = "false"
    var strFoodType: String! = ""
    
    var blurEffect = UIBlurEffect()
    var placePicker: GMSPlacePicker!

    //for address(home and work)
    var strHomeAddressText: String! = ""
    var HomeLat: Double! = 0.0
    var HomeLong: Double! = 0.0
    var WorkLat: Double! = 0.0
    var WorkLong: Double! = 0.0
    var rowAddress: Int!
    var arrUserProfileData: NSMutableArray! = []
    var arrUserAddress: NSArray!
    var tap:UITapGestureRecognizer!
    
    var strWorkddressText: String! = ""
    var contentOffset:CGPoint!
    
    var param1 = [String: AnyObject]()
    var param2 = [String: AnyObject]()
    var param  = [String: AnyObject]()

    
    // MARK: - VIEW LYFECYCLE METHOD
    
    override func viewDidLoad()
    {
        

        super.viewDidLoad()
        
        
        //google analytics for individual screen
        
        /*  let tracker = GAI.sharedInstance().trackerWithTrackingId("UA-82850469-1")
         tracker.set(kGAIScreenName, value: "mainView")
         
         let builder = GAIDictionaryBuilder.createScreenView()
         tracker.send(builder.build() as [NSObject : AnyObject])*/
        
        //customize navigationbar and buttons
        
        navigationBarUI()
        customBarButtons()
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationController?.navigationBar.isHidden=false
        
        //customize switch
        
        customSwitch()

        //get userid value
        
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!

        
        //hide table and map
        
         let strStatus = UserDefaults.standard.value(forKey: "SHOWRESTLIST")
            
        if (strStatus as? String == "YES")
        {
         
            self.map.isHidden=true
            self.viewRestList.isHidden=false
            self.btnShowCurrentLocation.isHidden = true
            
        }
        else
        {

            self.map.isHidden=false
            self.viewRestList.isHidden=true
            self.btnShowCurrentLocation.isHidden = false

            

        }
        
        
        //initalize locationmanager
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
        
        //this userdefault is for to get current location when I came to home view from side menu.
        
         if (UserDefaults.standard.value(forKey: "HOME") as? String == "NO")
          {
            
            latitude = locationManager.location?.coordinate.latitude
            longitude = locationManager.location?.coordinate.longitude
            
           
            
            
            UserDefaults.standard .setValue("YES", forKey: "HOME")
          }
        else
         {
            let lat  = UserDefaults.standard.double(forKey: "LATITUDE")
            let long = UserDefaults.standard.double(forKey: "LONGITUDE")
            
            latitude = lat
            longitude = long
            
            
            
        }
        
        
        // show current address in home screen
        
        let geocoder = CLGeocoder()
        let location = CLLocation(latitude: self.latitude, longitude: self.longitude)
        geocoder.reverseGeocodeLocation(location) {
            (placemarks, error) -> Void in
            if let placemarks = placemarks , placemarks.count > 0 {
                let placemark = placemarks[0]
                print("placemark.addressDictionary", placemark.addressDictionary)
                
                
                let myAddressStrings: NSArray = placemark.addressDictionary?["FormattedAddressLines"] as! NSArray
                let strCL: String = (myAddressStrings.componentsJoined(by: (("") as NSString) as String) ) as String
                self.btnCurrentLocation.setTitle(strCL, for:UIControlState())
            }
        }
      
        
        map.showsUserLocation = true
        tblLocation.isHidden = true;
        lblNoData.isHidden = true
        
        //sidemenu initalization
        
        revealViewController().delegate = self
        view.addGestureRecognizer(revealViewController().tapGestureRecognizer())
        sidebarMenuOpen = false
        
        //get inital resturants list
        
        
            if InternetConnection.isConnectedToNetwork()
            {
                
                getOrders() { responseObject, error in
                    
                    // print("responseObject = \(responseObject); error = \(error)")
                    
                    if (responseObject != nil)
                    {
                    let json = JSON(responseObject!)
                    if let string = json.rawString()
                    {
                        
                        self.stopActivityAnimating()
                        
                        if let data = string.data(using: String.Encoding.utf8) {
                            do {
                                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                                print("json ",json)
                                
                                if( json!["Success"] as? String  == "1")
                                {
                                    
                                    self.arrData = json!["Data"] as! NSMutableArray
                                    
                                    
                                    print("Arrdata",self.arrData)
                                    
                                    if (strStatus as? String == "YES")
                                    {
                                        UserDefaults.standard .setValue("YES", forKey: "SHOWRESTLIST")
                                        self.map.isHidden=true
                                        self.viewRestList.isHidden=false
                                        self.tblRestData.isHidden=false
                                        self.collectionView.isHidden=false
                                        self.tblRestData.reloadData()
                                        self.ibSwitch.thumbImageView.image=UIImage(named: "Restaurant.png")
                                        self.ibSwitch.setOn(true, animated: true)
                                        self.collectionView.allowsMultipleSelection=false
                                    }
                                    else
                                    
                                    {
                                    
                                    for i in 0...self.arrData.count-1
                                    {
                                      //  self.locationManager.startUpdatingLocation()

                                        
                                        let strlat: Double =
                                            ((self.arrData[i] as! NSMutableDictionary).value(forKey: "Latitude") as! NSString).doubleValue
                                        let strlong: Double = ((self.arrData[i] as! NSMutableDictionary).value(forKey: "Longitude") as! NSString).doubleValue
                                        self.ibSwitch.thumbImageView.image=UIImage(named: "LocationPin.png")
                                        self.ibSwitch.setOn(false, animated: true)

                                            self.viewRestList.isHidden=true
                                            self.map.isHidden=false
                                            self.tblRestData.isHidden=true
                                            self.map.delegate=self
                                            
                                            let annotation = MKPointAnnotation()
                                            annotation.title = (self.arrData[i] as AnyObject).value(forKey: "Name") as? String
                                            annotation.coordinate = CLLocationCoordinate2D (latitude: strlat, longitude:strlong)
                                            
                                            
                                            self.map.addAnnotation(annotation)
                                        
             
                                        
                                        }
                                    
                                     
                                        
                                    }
                                    
                                    
                                }
                                else
                                    
                                {
                                    
                                    if (strStatus as? String == "YES")
                                    {
                                        self.ibSwitch.thumbImageView.image=UIImage(named: "Restaurant.png")
                                        self.ibSwitch.setOn(true, animated: true)

                                    }
                                    else
                                    {
                                        self.ibSwitch.thumbImageView.image=UIImage(named: "LocationPin.png")
                                        self.ibSwitch.setOn(false, animated: true)

                                    }
                                    self.stopActivityAnimating()
                                    self.dialog = ZAlertView(title: "Foodies4u",
                                        message:  "No record found",
                                        closeButtonText: "Ok",
                                        closeButtonHandler: { (alertView) -> () in
                                            alertView.dismissAlertView()
                                            //self.navigationController!.popViewControllerAnimated(false)
                                        }
                                        
                                        
                                    )
                                    
                                    self.collectionView.isHidden = true
                                    self.view.makeToast("Oops! No Restaurant Found", duration: 2.0, position: CSToastPositionBottom, title: "Foodies4u")
                                    let allAnnotations = self.map.annotations
                                    self.map.removeAnnotations(allAnnotations)
                                }
                            }
                                
                            catch {
                                print("Something went wrong")
                            }
                        }
                    }
                    
                    
                }
                    else
                        {
                            self.stopActivityAnimating()
                            self.AlertMethod()

                    }
                }
            }
      
        
        
        
        // flip animation is swift.
        
        /*  firstView = UIView(frame: CGRect(x: 32, y: 32, width: 128, height: 128))
         secondView = UIView(frame: CGRect(x: 32, y: 32, width: 128, height: 128))
         
         firstView.backgroundColor = UIColor.redColor()
         secondView.backgroundColor = UIColor.blueColor()
         
         secondView.hidden = true
         
         view.addSubview(firstView)
         view.addSubview(secondView)
         
         performSelector(#selector(HomeViewController.flip), withObject: nil, afterDelay: 2)*/
        
        // Example of color customization
        // Example of a bigger switch with images
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        //to get home and work address call user data API
        
        arrUserProfileData = NSMutableArray()
        
       

        
       
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        self.getUserOfDetail()

        
        navigationBarUI()
        customBarButtons()
        self.navigationController?.navigationBar.isHidden=false
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        
        
        
        
    }
    
    func getUserOfDetail()
    {
        getUserDetails() { responseObject, error in
            
            
            if (responseObject != nil)
            {
                
                let json = JSON(responseObject!)
                if let string = json.rawString()
                {
                    //Do something you want
                    //print(string)
                    self.stopActivityAnimating()
                    
                    if let data = string.data(using: String.Encoding.utf8) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            print("json ",json)
                            
                            let successVal:String = json!["Success"] as! String
                            if (successVal == "1") {
                                
                                let arruserdata:NSMutableDictionary = json!["Data"] as! NSMutableDictionary
                                
                                self.arrUserProfileData.add(arruserdata)
                                
                                
                                print("arrUserProfileData ",self.arrUserProfileData)
                                
                                
                                self.arrUserAddress = self.arrUserProfileData.value(forKey: "CustomerAddress") as! NSArray
                                print("CustomerAddress ",self.arrUserAddress)
                                
                                
                                if((self.arrUserAddress.object(at: 0) as AnyObject).count == 1)
                                {
                                    if((self.arrUserAddress.value(forKey: "AddressId") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as?Int == 1
                                    {
                                        self.strHomeAddressText =  ((self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                                        self.HomeLat = Double((((self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
                                        
                                        self.HomeLong = Double((((self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
                                        
                                        let indexPath1 = IndexPath(row: 0, section: 0)
                                        self.MyRestcells = self.tblLocation.cellForRow(at: indexPath1) as! MyResListCell!
                                        self.MyRestcells.txtfieldLocationAddress.text = self.strHomeAddressText
                                        
                                    }
                                    else{
                                        self.strWorkddressText =  ((self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                                        self.WorkLat =  Double((((self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
                                        
                                        self.WorkLong =  Double((((self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
                                        let indexPath1 = IndexPath(row: 1, section: 0)
                                        self.MyRestcells = self.tblLocation.cellForRow(at: indexPath1) as! MyResListCell!
                                        self.MyRestcells.txtfieldLocationAddress.text = self.strWorkddressText
                                        
                                        
                                        
                                    }
                                    
                                }
                                
                                if((self.arrUserAddress.object(at: 0) as AnyObject).count == 2
                                    )
                                {
                                    
                                    self.strHomeAddressText =  ((self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                                    
                                    self.strWorkddressText =  ((self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String
                                    
                                    self.HomeLat = Double((((self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
                                    
                                    self.HomeLong = Double((((self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
                                    
                                    self.WorkLat =  Double((((self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String)!)
                                    
                                    self.WorkLong =  Double((((self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String)!)
                                    
                                    
                                    let indexPath1 = IndexPath(row: 0, section: 0)
                                    self.MyRestcells = self.tblLocation.cellForRow(at: indexPath1) as! MyResListCell!
                                    self.MyRestcells.txtfieldLocationAddress.text = self.strHomeAddressText
                                    
                                    let indexPath2 = IndexPath(row: 1, section: 0)
                                    self.MyRestcells = self.tblLocation.cellForRow(at: indexPath2) as! MyResListCell!
                                    self.MyRestcells.txtfieldLocationAddress.text = self.strWorkddressText
                                    
                                    
                                }
                                
                                
                                
                                
                                
                            }else {
                                
                            }
                            
                            
                            
                        } catch {
                            print("Something went wrong")
                        }
                    }
                    
                }
            }
            else
            {
                self.stopActivityAnimating()
                self.AlertMethod()
                
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
       // self.navigationController?.navigationBar.hidden=true
    }
    
    // MARK: - MAPVIEW DELEGATES METHODS
    
    fileprivate func mapViewRegionDidChangeFromUserInteraction() -> Bool {
        let view = self.map.subviews[0]
        //  Look through gesture recognizers to determine whether this region change is from user interaction
        if let gestureRecognizers = view.gestureRecognizers {
            for recognizer in gestureRecognizers {
                if(recognizer.state == UIGestureRecognizerState.began || recognizer.state == UIGestureRecognizerState.ended )
                {
                    let point = recognizer.location(in: map)
                    let tapPoint = map.convert(point, toCoordinateFrom: view)
                    latitude=tapPoint.latitude
                    longitude=tapPoint.longitude
                    
                                       UserDefaults.standard.set("YES", forKey: "ZOOMED")
                    
                    
                    
                    self.perform(#selector(getlocation), with: nil, afterDelay: 5.0)

                    return true
                }
            }
        }
        return false
    }
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        mapChangedFromUserInteraction = mapViewRegionDidChangeFromUserInteraction()
        if (mapChangedFromUserInteraction)
        {
            
            
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if (mapChangedFromUserInteraction)
        {
            
            /*let centre = self.map!.centerCoordinate as CLLocationCoordinate2D
            
            let getLat: CLLocationDegrees = centre.latitude
            let getLon: CLLocationDegrees = centre.longitude
            
            latitude = centre.latitude
            longitude = centre.longitude*/
            
            
           /* print("getLat",getLat)
            
            print("getLon",getLon)*/

            
        }
        
    }
    
    
    internal func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error)
    {
        print(error)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        //print("bhumika")
        
        switch (newState) {
        case .ending, .canceling:
            view.dragState = .none
            print("dragState")
            
        default: break
        }
        // pa.coordinate = placemark.location.coordinate
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !annotation.isKind(of: MKUserLocation.self) else {
            return nil
        }
        
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
             let av = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
             av.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
             annotationView = av
             av.canShowCallout = false
            
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "Location-Marker.png")
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {

        
        print(self.arrData)
        var strName = view.annotation?.title!
        let strLat = (view.annotation?.coordinate.latitude)!
        let strLong = (view.annotation?.coordinate.longitude)!
        
        strName = strName!.replacingOccurrences(of: "'", with: "\\'")
        //strName = strName!.containsString("'") ? strName!.stringByReplacingOccurrencesOfString("'", withString: "'\\") : strName!
        
        let predicate = NSPredicate(format:"Name == '\(strName!)'")
        let arrPassData = self.arrData.filtered(using: predicate)
        
        print(arrPassData)
        
        if arrPassData.count > 0 {
        // navigate to restaurant details screen
        
            let arrSel:NSMutableDictionary = arrPassData[0] as! NSMutableDictionary
        
            let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "RestListDetailsViewController") as? RestListDetailsViewController
        
            mapViewControllerObj!.dictRestDetails = arrSel
        
            self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        //RealEstate* estate = ann.object;
    print("bhumika" , view.annotation?.title!)
    }

    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        
        //print("user",userLocation.location)
        // Not getting called
    }
    
    // MARK: - LOCATIONS MANAGER METHODS
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        //set latitiude and longitude values
        
        
        /*UserDefaults.standard.set(latitude, forKey: "LATITUDE")
        UserDefaults.standard.set(longitude, forKey: "LONGITUDE")
        UserDefaults.standard.synchronize()*/
        
        
        //print("lat1", latitude)
        //print("long1" , longitude)
        
        let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
        
        self.map.setRegion(region, animated: true)
        
        UserDefaults.standard.set((locationManager.location?.coordinate.latitude)!, forKey: "CLATITUDE")
        UserDefaults.standard.set((locationManager.location?.coordinate.longitude)!, forKey: "CLONGITUDE")
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            manager.startUpdatingLocation()
        //  self.map.showsUserLocation = true
        default: break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
       // print("didFailWithError: \(error.description)")
       
    }
    
    // MARK: - WEBSERVICES METHODS
    
    func getUserDetails(_ completionHandler: @escaping  (AnyObject?, NSError?) -> ())
    {
        let size = CGSize(width: 80, height:80)
        
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        strWSURL=GETWSURL()
        
        let param1 : [ String : AnyObject] = [
            "customerId":strUSERID
        ]
        
        makeCall(section:strURL as String + "GetCustomerData?",param:param1,completionHandler: completionHandler)
      
        
    }
    
    func  getlocation()
    {
        
        if InternetConnection.isConnectedToNetwork()
        {
            
            getOrders() { responseObject, error in
                
                if (responseObject != nil)
                {
                // print("responseObject = \(responseObject); error = \(error)")
                let json = JSON(responseObject!)
                if let string = json.rawString()
                {
                    
                    // self.stopActivityAnimating()
                    
                    if let data = string.data(using: String.Encoding.utf8) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSMutableDictionary
                            print("json ",json)
                            
                            if(json!["Success"] as? String  == "1")
                            {
                                
                               // self.locationManager.startUpdatingLocation()

                                
                                self.arrData = json!["Data"] as! NSMutableArray
                                for i in 0...self.arrData.count-1
                                {
                                   
                                    
                                    let strlat: Double =
                                        ((self.arrData[i] as! NSMutableDictionary).value(forKey: "Latitude") as! NSString).doubleValue
                                    let strlong: Double = ((self.arrData[i] as! NSMutableDictionary).value(forKey: "Longitude") as! NSString).doubleValue
                                    let strStatus = UserDefaults.standard.value(forKey: "ZOOMED")
                                    
                                    if (strStatus as? String == "NO")
                                    {
                                        let center = CLLocationCoordinate2D(latitude:strlat, longitude: strlong)
                                        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
                                        
                                        self.map.setRegion(region, animated: true)
                                    }
                                    else
                                    {
                                        self.locationManager.stopUpdatingLocation()

                                    }

                                    

                                    
                                    self.ibSwitch.thumbImageView.image=UIImage(named: "LocationPin.png")
                                    self.ibSwitch.setOn(false, animated: true)
                                    self.viewRestList.isHidden=true
                                    self.map.isHidden=false
                                    self.tblRestData.isHidden=true
                                    self.map.delegate=self
                                    
                                    print("strlat",strlat)
                                    print("strlat",strlong)

                                    
                                    
                                   // UserDefaults.standard.set(strlat, forKey: "LATITUDE")
                                   // UserDefaults.standard.set(strlong, forKey: "LONGITUDE")
                                    UserDefaults.standard.synchronize()
                                    let annotation = MKPointAnnotation()
                                    annotation.title = (self.arrData[i] as AnyObject).value(forKey: "Name") as? String
                                    annotation.coordinate = CLLocationCoordinate2D (latitude: strlat, longitude:strlong)
                                    self.map.addAnnotation(annotation)
                            
                                 

                                }
                                
                            }
                            else
                            {
                                
                                let strStatus = UserDefaults.standard.value(forKey: "ZOOMED")
                                
                                if (strStatus as? String == "NO")
                                {
                                    let center = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                                    let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.09, longitudeDelta: 0.09))
                                    
                                    
                                    
                                    self.map.setRegion(region, animated: true)
                                }
                                else
                                {
                                    self.locationManager.stopUpdatingLocation()
                                    
                                }
                                
                                
                                let allAnnotations = self.map.annotations
                                self.map.removeAnnotations(allAnnotations)
                                
                                if(self.arrData  != nil)
                                {
                                    self.arrData.removeAllObjects()
                                }
                                   self.view.makeToast("Oops! No Restaurant Found", duration: 2.0, position: CSToastPositionBottom, title: "Foodies4u")
                            }
                            
                        } catch {
                            print("Something went wrong")
                        }
                    }
                }
                }
                    
                    else
                {
                    self.stopActivityAnimating()
                    self.AlertMethod()
                }
                
                
            }
        }
        
        
    }
    
    func getOrders(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        strWSURL=GETWSURL()
        
     //   let size = CGSize(width: 50, height:50)
        
       // startActivityAnimating(size, message: "", type: NVActivityIndicatorType(rawValue: 1)!)
        
        param1 = [ "search":strSearchType as AnyObject,
                   "type":strSearchCategory as AnyObject,
                   "distance":strDistance as AnyObject,
                   "foodtype":strFoodType as AnyObject,
                   "category":Category as AnyObject,
                   "minprice":strMinPrice as AnyObject,
                   "maxprice":strMaxPrice as AnyObject,
            "offers":strOffers as AnyObject,
            "cuisines":strCuisines as AnyObject,
            "opennow": OpenNow as AnyObject,
            "Latitude": latitude as AnyObject,
            "Longitude": longitude as AnyObject,
            "sortby": "ASC" as AnyObject,
            "customerid":strUSERID as AnyObject
        ]
        
      

      
        makeCall(section:strWSURL as String + "SearchContext?",param:param1 ,completionHandler: completionHandler)
        
        
        
    }

    
        

    
    // MARK: - CUSTOM BARBUTTONS
    
    func customBarButtons()
    {
        
        //side menu button
        
        let img = UIImage(named: "MenuBar.png")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        // let img1 = UIImage(named: "FilterIcon.png")
        // navigationItem.rightBarButtonItem = UIBarButtonItem(image:img1, style: .Plain, target: self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)))
        
        
        //filters button
        
        let imgFilter = UIImage(named: "FilterIcon.png")
        let btnFilters = UIButton(frame: CGRect(x: self.view.frame.width-35, y: 0, width: 20, height: 20))
        btnFilters.setBackgroundImage(imgFilter, for: UIControlState())
        btnFilters.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
        let rightBarButtonItemEdit: UIBarButtonItem = UIBarButtonItem(customView: btnFilters)
        
        
        //search button
        
        let btnSearch = UIButton(frame: CGRect(x: self.view.frame.width-75, y: 0, width:20, height: 20))
        btnSearch.setBackgroundImage(UIImage(named: "SearchIcon.png"), for: UIControlState())
        btnSearch.addTarget(self, action: #selector(HomeViewController.btnSearchPressed(_:)), for: .touchUpInside)
        let rightBarButtonItemDelete: UIBarButtonItem = UIBarButtonItem(customView: btnSearch)
        
        self.navigationItem.setRightBarButtonItems([rightBarButtonItemEdit, rightBarButtonItemDelete], animated: true)
        
        
    }
    
    @IBAction func btnShowCurrentLocationPressed(_ sender: UIButton)
    {
        
        // show current address in home screen
        
        latitude  = UserDefaults.standard.double(forKey: "CLATITUDE")
        longitude = UserDefaults.standard.double(forKey: "CLONGITUDE")
        
        UserDefaults.standard.set("NO", forKey: "ZOOMED")

        
        if InternetConnection.isConnectedToNetwork()
        {
        
        let geocoder = CLGeocoder()
        let location = CLLocation(latitude: self.latitude, longitude: self.longitude)
        geocoder.reverseGeocodeLocation(location) {
            (placemarks, error) -> Void in
            if let placemarks = placemarks , placemarks.count > 0 {
                let placemark = placemarks[0]
                print("placemark.addressDictionary", placemark.addressDictionary)
                
                
                let myAddressStrings: NSArray = placemark.addressDictionary?["FormattedAddressLines"] as! NSArray
                let strCL: String = (myAddressStrings.componentsJoined(by: (("") as NSString) as String) ) as String
                self.btnCurrentLocation.setTitle(strCL, for:UIControlState())
            }
        }
        
        

        self.getlocation()
        }
    }
    
    
    @IBAction func btnChooseLocationPressed(_ sender: UIButton)
    {
        
        tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.ChooseAddress))
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)
        
        //add blur effect in  view
        blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        map.addSubview(blurEffectView)

        
        ibSwitch.isHidden = true
        tblLocation.isHidden = false;
        tblLocation.dataSource = self;
        tblLocation.delegate = self;
        
        
        
        
        
        
        /*if((self.arrUserAddress.object(at: 0) as AnyObject).count == 1)
        {
            if((self.arrUserAddress.value(forKey: "AddressId") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as?Int == 1
            {
                self.strHomeAddressText =  ((self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                self.HomeLat = Double((((self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
                
                self.HomeLong = Double((((self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
                
            }
            else{
                self.strWorkddressText =  ((self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
                self.WorkLat =  Double((((self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
                
                self.WorkLong =  Double((((self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
                
                
            }
            
        }
        
        if((self.arrUserAddress.object(at: 0) as AnyObject).count == 2
            )
        {
            
            self.strHomeAddressText =  ((self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String
            
            self.strWorkddressText =  ((self.arrUserAddress.value(forKey: "Address") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String
            
            self.HomeLat = Double((((self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
            
            self.HomeLong = Double((((self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 0) as? String)!)
            
            self.WorkLat =  Double((((self.arrUserAddress.value(forKey: "Latitude") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String)!)
            
            self.WorkLong =  Double((((self.arrUserAddress.value(forKey: "Longitude") as AnyObject).object(at: 0) as AnyObject).object(at: 1) as? String)!)
            
            
        }*/
        
       
        tblLocation .reloadData()
        
        

    }
    
    
    func openplacepicker()
        {
           
            
            latitude = locationManager.location?.coordinate.latitude
            longitude = locationManager.location?.coordinate.longitude
            
            let center = CLLocationCoordinate2DMake(latitude, longitude)
                print(center)
            let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
            let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            let config = GMSPlacePickerConfig(viewport: viewport)
            
            self.placePicker = GMSPlacePicker(config: config)
            
            // 2
            placePicker.pickPlace { (place: GMSPlace?, error: Error?) -> Void in
                
                if let error = error {
                    print("Error occurred: \(error.localizedDescription)")
                    return
                }
                // 3
                if let place = place {
                    let coordinates = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
                    let marker = GMSMarker(position: coordinates)
                    marker.title = place.name
                    self.latitude =  place.coordinate.latitude
                    self.longitude = place.coordinate.longitude
                    
                    print("getLat",self.latitude)
                    print("getLon",self.longitude)
                    
                    UserDefaults.standard.set(self.latitude, forKey: "LATITUDE")
                    UserDefaults.standard.set(self.longitude, forKey: "LONGITUDE")
                    self.getlocation()
                    self.btnCurrentLocation.setTitle(place.formattedAddress!, for:UIControlState())

                    
                }
                
                
                
            } 
            
        
    }
    
    func CallGooglePlacesAPI(_ sender: UIButton)
    {
        
        
        UserDefaults.standard.set("NO", forKey: "ZOOMED")

        self.ChooseAddress()
        
        let btnsendtag: UIButton = sender
        
        rowAddress = btnsendtag.tag
        if (btnsendtag.tag == 0)
            
        {
            
            if(strHomeAddressText.characters.count == 0)
            {
                
                let editProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "editProfileViewController") as? editProfileViewController
                
                editProfileVC?.navigationController?.navigationBar.isHidden = false
                
                editProfileVC?.arrUserProfileData = self.arrUserProfileData!
                editProfileVC?.arrUserAddress = self.arrUserAddress
                
                
                 let dialog = ZAlertView(title: "Foodies4u",
                                         message: "Please insert home address first",
                                         closeButtonText: "Ok",
                                         closeButtonHandler: { (alertView) -> () in
                                            
                                            
                                            alertView.dismissAlertView()

                                            
                                            self.navigationController?.pushViewController(editProfileVC!, animated: true)
                })
                
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
            }
            else
            {
                latitude = HomeLat
                longitude = HomeLong
                self.btnCurrentLocation.setTitle(strHomeAddressText, for:UIControlState())
                UserDefaults.standard.set(self.latitude, forKey: "LATITUDE")
                UserDefaults.standard.set(self.longitude, forKey: "LONGITUDE")
                self.getlocation()

            }
           

           
            
        }
        else if(btnsendtag.tag == 1)
        {
            if(strWorkddressText.characters.count == 0)
            {
                
                let editProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "editProfileViewController") as? editProfileViewController
                
                editProfileVC?.navigationController?.navigationBar.isHidden = false
                editProfileVC?.arrUserProfileData = self.arrUserProfileData!
                editProfileVC?.arrUserAddress = self.arrUserAddress
                
                
             
                
                let dialog = ZAlertView(title: "Foodies4u",
                                         message: "Please insert work address first",
                                         closeButtonText: "Ok",
                                         closeButtonHandler: { (alertView) -> () in
                                            
                                            
                                            alertView.dismissAlertView()

                                            
                                            self.navigationController?.pushViewController(editProfileVC!, animated: true)
                })
                
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
            }
            else
            {
            latitude = WorkLat
            longitude = WorkLong
            self.btnCurrentLocation.setTitle(strWorkddressText, for:UIControlState())
            UserDefaults.standard.set(self.latitude, forKey: "LATITUDE")
            UserDefaults.standard.set(self.longitude, forKey: "LONGITUDE")
            self.getlocation()

            }

        }
        else
        {
            self.openplacepicker()
        }
        
        UserDefaults.standard.set(self.latitude, forKey: "LATITUDE")
        UserDefaults.standard.set(self.longitude, forKey: "LONGITUDE")
        UserDefaults.standard.synchronize()
    }
    
    // remove viusal effect(Blur) from view.
    func ChooseAddress()
    {
        tblLocation.isHidden = true
        ibSwitch.isHidden = false
        view.removeGestureRecognizer(tap)
        for subview in map.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }

     // MARK: - BUTTONS TAPE METHODS
    
    func btnSearchPressed(_ sender: AnyObject)
    {
        
        
        // this will navigate you to the search screen
         if (sidebarMenuOpen == true)
          {
            
          }
         else
         {
            UserDefaults.standard .set(false, forKey: "SEARCHDONEANDAPPLYFILETR")
            let SVControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
            
            // pass the value of filters to seach view
            SVControllerObj?.strSearchType = strSearchType
            SVControllerObj?.strSearchCategory = strSearchCategory
            SVControllerObj?.OpenNow =  self.OpenNow
            SVControllerObj?.strFoodType = self.strFoodType
            SVControllerObj?.Category =  self.Category
            SVControllerObj?.strDistance =  self.strDistance
            SVControllerObj?.strCuisines =  self.strCuisines
            SVControllerObj?.strOffers =  self.strOffers
            SVControllerObj?.strMinPrice =  self.strMinPrice
            SVControllerObj?.latitude = self.latitude
            SVControllerObj?.longitude = self.longitude
            
            SVControllerObj?.strMaxPrice =  self.strMaxPrice
            self.navigationController?.pushViewController(SVControllerObj!, animated: true)

        }
    }
    
    // MARK: - CUSTOM SWITCH(SevenSwitch)
    
    func customSwitch()
    {
       
        
        // turn the switch on with animation
        
        let strStatus = UserDefaults.standard.value(forKey: "SHOWRESTLIST")
        
        if (strStatus as? String == "YES")
        {
             ibSwitch = SevenSwitch(frame: CGRect(x: self.view.frame.width-90, y: self.view.frame.origin.y+85, width: 80, height: 35))
           ibSwitch.setOn(true, animated: true)
            viewLocation.isHidden=true

        }
        else
        
        {
             ibSwitch = SevenSwitch(frame: CGRect(x: self.view.frame.width-90, y: self.view.frame.origin.y+140, width: 80, height: 35))
            ibSwitch.setOn(false, animated: true)
            viewLocation.isHidden=false

        }
        
        ibSwitch.addTarget(self, action: #selector(HomeViewController.switchChanged(_:)), for: UIControlEvents.valueChanged)
        ibSwitch.thumbImageView.image=UIImage(named: "LocationPin.png")
        
        ibSwitch.offImage = UIImage(named: "Restaurant-Gray.png")
        ibSwitch.onImage = UIImage(named: "LocationPin-Gray.png")
        ibSwitch.onTintColor = UIColor.white
        ibSwitch.isRounded = true
        self.view.addSubview(ibSwitch)

    }
    
   
    
    func switchChanged(_ sender: SevenSwitch)
    {
        
        if sender.on
        {
            ibSwitch.frame = CGRect(x: self.view.frame.width-90, y: self.view.frame.origin.y+85
                , width: 80, height: 35)

            self.map.isHidden=true
            self.btnShowCurrentLocation.isHidden=true

            
            
             //contentOffset = self.tblRestData.contentOffset;
            

            self.viewRestList.isHidden=false

            if (arrData.count > 0)
            {
                self.tblRestData.isHidden=false
                self.collectionView.isHidden=false
                self.tblRestData.reloadData()
                tblRestData.dataSource = self
                tblRestData.delegate = self
                lblNoData.isHidden = true
            }
            else
            {
                lblNoData.isHidden = false
            }

        
        

            self.ibSwitch.thumbImageView.image=UIImage(named: "Restaurant.png")
            self.ibSwitch.setOn(true, animated: true)
            self.collectionView.allowsMultipleSelection=false
            viewLocation.isHidden=true
            
            UserDefaults.standard .setValue("YES", forKey: "SHOWRESTLIST")
            
        }
        else
        {
            
            
            
            
            ibSwitch.frame = CGRect(x: self.view.frame.width-90, y: self.view.frame.origin.y+140, width: 80, height: 35)

            UserDefaults.standard .setValue("NO", forKey: "SHOWRESTLIST")
            self.btnShowCurrentLocation.isHidden=false

            map.isHidden=false
            collectionView.isHidden=true
            viewRestList.isHidden=true
           // getlocation()
            ibSwitch.thumbImageView.image=UIImage(named: "LocationPin.png")

            UserDefaults.standard.synchronize()
            viewLocation.isHidden=false

            
        }
        
        
        print("Changed value to: \(sender.on)")
    }
    
    /*  func flip()
     {
     let transitionOptions: UIViewAnimationOptions = [.TransitionCurlUp, .ShowHideTransitionViews]
     
     UIView.transitionWithView(firstView, duration: 1.0, options: transitionOptions, animations: {
     self.firstView.hidden = true
     }, completion: nil)
     
     UIView.transitionWithView(secondView, duration: 1.0, options: transitionOptions, animations: {
     self.secondView.hidden = false
     }, completion: nil)
     }*/
    
    // MARK: - TABLEVIEW DATASOURCE AND DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if (tableView == tblLocation)
        {
            return 50;

        }
        else
        {
            return 115;

        }
        
        
        return 115;

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if (tableView == tblLocation)
        {
           return arrLocationImg.count
        }
        else
        
        if arrData.count  == 0
        {
            return 0 
            
        }
        else
            
        {
            return arrData.count
        }
       
        return 0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if (tableView == tblLocation)
        {
            

           MyRestcells = tableView.dequeueReusableCell(withIdentifier: "LCell", for: indexPath) as! MyResListCell
            
            //display home  address

            if ((indexPath as NSIndexPath).row == 0)
                
            {
                if (strHomeAddressText.characters.count > 0)
                
                {
                    MyRestcells.txtfieldLocationAddress.text = strHomeAddressText
                    MyRestcells.txtfieldLocationAddress.placeholder = " "


                }
                else
                {
                    
                    MyRestcells.txtfieldLocationAddress.placeholder = "Home Adress"

                }
            }
                
            //display work  address
                
                
            else if ((indexPath as NSIndexPath).row == 1)
            {
                if (strWorkddressText.characters.count > 0)
                    
                {
                    MyRestcells.txtfieldLocationAddress.text = strWorkddressText
                    MyRestcells.txtfieldLocationAddress.placeholder = " "


                    
                }
                else
                {
                    MyRestcells.txtfieldLocationAddress.placeholder = "Work Address"
                    
                }

            }
            else
            {
                
                MyRestcells.txtfieldLocationAddress.placeholder = "Other"
             

            }
          
            MyRestcells.txtfieldLocationAddress.tag = (indexPath as NSIndexPath).row
            
            //display Other  address

            MyRestcells.btnChooseLocation.tag = (indexPath as NSIndexPath).row
            MyRestcells.btnChooseLocation.addTarget(self, action: #selector(HomeViewController.CallGooglePlacesAPI), for: .touchUpInside)
            MyRestcells.selectionStyle = UITableViewCellSelectionStyle.none
            let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 40, height: 30))
            MyRestcells.txtfieldLocationAddress.leftView=paddingView;
            MyRestcells.txtfieldLocationAddress.leftViewMode = UITextFieldViewMode.always
            let strImg = arrLocationImg[(indexPath as NSIndexPath).row]
            MyRestcells.imgLocation.image = UIImage(named:strImg)
        }
        else
        
        {
            
            //display resturants deatils

        
        MyRestcells = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyResListCell
        
        MyRestcells.selectionStyle = UITableViewCellSelectionStyle.none
        
        //name
        
        MyRestcells.lblRestName.text=(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Name") as? String
        
        //address
        
        MyRestcells.lblRestAddress.text=(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Address1") as? String
        
        // favourite
       /* let favStatus = ((self.arrData.value(forKey: "FavRestaurant") ) as AnyObject).object(at: (indexPath as NSIndexPath).row) as! Int
     
        let viewForFav: UIView = MyRestcells.viewWithTag(111)! as UIView
        
        let imgFav: UIImageView = viewForFav.viewWithTag(1) as! UIImageView
        
        if favStatus == 0 { // not fav yet
            imgFav.image = UIImage.init(named: "Favourite-Icon-White")
        }else { // fav
            imgFav.image = UIImage.init(named: "Favourite-Icon-Red")
        }*/
        
        //distanceƒ
        
        let y = Double(round(100*(((self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Distance")! as AnyObject).floatValue)/100))
        //print(y)
        MyRestcells.lblDistance.text = String(y) + " miles away"
       
        
        //ratings
         MyRestcells.lblRating.text=(self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Ratings") as? String
        MyRestcells.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
        MyRestcells.floatRatingView.fullImage = UIImage(named: "StarFull")
        MyRestcells.floatRatingView.maxRating = 5
        MyRestcells.floatRatingView.minRating = 1
        MyRestcells.floatRatingView.rating = (((self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "Ratings") as! NSString).floatValue)
        MyRestcells.floatRatingView.floatRatings = true
        
        if((self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "FoodType") as? String  == "2")
        {
            MyRestcells.imgVeg.isHidden = false
            MyRestcells.imgVeg.image = UIImage(named: "NonVeg-Icon.png")
            MyRestcells.imgNonVeg.isHidden = true
            
            
        }
        else if((self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "FoodType") as? String  == "1")
        {
            MyRestcells.imgVeg.isHidden = false
            MyRestcells.imgVeg.image = UIImage(named: "VegIcon")
            MyRestcells.imgNonVeg.isHidden = true
            
        }
        else if((self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey: "FoodType") as? String  == "")
        {
            

            MyRestcells.imgNonVeg.isHidden = true
             MyRestcells.imgVeg.isHidden = true
            
        }
        
        else
            
        {
            
                MyRestcells.imgVeg.image = UIImage(named: "VegIcon")
                MyRestcells.imgNonVeg.image = UIImage(named: "NonVeg-Icon.png")
                MyRestcells.imgNonVeg.isHidden = false
                MyRestcells.imgVeg.isHidden = false
            
            
            
            
        }
        
        
         MyRestcells.ImgViewRest.image = UIImage(named:"Placeholder")
        
        //image
        
            
           /* if((self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey:"RestaurantImage")  as? String != nil)
            {
                let url = URL(string: "http://foodies4you.intransure.com/Media/Restaurant/" + (String(describing:(self.arrData.object(at: (indexPath as NSIndexPath).row)  as AnyObject).value(forKey: "RestaurantId") as! NSNumber)) + "/" + ((self.arrData.object(at: (indexPath as NSIndexPath).row) as AnyObject).value(forKey:"RestaurantImage")  as! String))
                
                print("url",url)
                
                
                let data = try? Data(contentsOf: url!)
                
                //make sure your image in this url does exist, otherwise unwrap in a if let check
                
                if(url ==  nil)
                {
                    
                    MyRestcells.ImgViewRest.image = UIImage(named:"Placeholder")
                    
                    
                }
                else
                {
                    MyRestcells.ImgViewRest.image = UIImage(data: data!)
                }
            }
            else
            {*/
                MyRestcells.ImgViewRest.image = UIImage(named:"Placeholder")

            //}
            
            

            
       // let url = NSURL(string: "http://foodies4you.intransure.com/Media/Restaurant/Thumbs/0000/"+(self.arrData.objectAtIndex(indexPath.row).valueForKey("RestaurantImage") as? String)!)
       /* print("url",url)
        let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
        if(data ==  nil)
        {
            MyRestcells.ImgViewRest.image = UIImage(named:"Placeholder")
        }
        else
        {
            print("data",data)
            //MyRestcells.ImgViewRest.image = UIImage(named:"Placeholder")

            MyRestcells.ImgViewRest.image = UIImage(data: data!)
        }*/
        }
        return MyRestcells

    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
         let arrSel:NSMutableDictionary = self.arrData.object(at: (indexPath as NSIndexPath).row) as! NSMutableDictionary
        
         let restaurantDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "RestListDetailsViewController") as? RestListDetailsViewController
         
         restaurantDetailsVC!.dictRestDetails = arrSel
         
         self.navigationController?.pushViewController(restaurantDetailsVC!, animated: true)
        
    }
    
    
    
    //MARK: - COLLECTIONVIEW DATASOURCE AND DELEGATES METHODS
    
    //for sorting at bottom of restaurant list view
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImage.count
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        Ccell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        
        
        
        let strImg = arrDefaultImage[(indexPath as NSIndexPath).row]
        
        // sorting image
        Ccell.itemImageView.image = UIImage(named:strImg as! String)
        
        // sorting name

        Ccell.lblName.textColor =  UIColor.white
        Ccell.lblName.text = arrSort[(indexPath as NSIndexPath).row] as? String
        
        
        Ccell.layer.borderColor = UIColor.init(red: 201.0/255, green: 107.0/255, blue: 30.0/255.0, alpha: 0.5).cgColor
        Ccell.layer.borderWidth = 0.5
        
        /* if indexPath.row == 3
         {
         Ccell.ViewSepratorLine.hidden=true
         }*/
        
        return Ccell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        Ccell = collectionView.cellForItem(at: indexPath) as! CollectionViewCell
        
        
        if arrSelSaveIndex.contains((indexPath as NSIndexPath).row)
        {
            arrSelIndex [0] = 6
            arrSelSaveIndex.remove((indexPath as NSIndexPath).row)
            //print("arrSelSaveIndex",arrSelSaveIndex)
            let strImg = arrImage[(indexPath as NSIndexPath).row]
            Ccell.itemImageView.image = UIImage(named:strImg as! String)
            Ccell.lblName.textColor =  UIColor.init(red: 227.0/255.0, green: 123.0/255.0, blue: 4.0/255.0, alpha: 1.0)
            Ccell.lblName.text = arrSort[(indexPath as NSIndexPath).row] as? String
            
            //sorting in ascending manner
            
            if ((indexPath as NSIndexPath).row == 0)
                
            {
                
               let descriptor: NSSortDescriptor = NSSortDescriptor(key: "Distance", ascending: false, selector: #selector(NSString.localizedStandardCompare(_:)))
                self.arrData = (self.arrData.sortedArray(using: [descriptor]) ) as! NSMutableArray


                tblRestData.contentOffset = contentOffset;

                
                
                
                
            }
            if ((indexPath as NSIndexPath).row == 1)
                
            {
                
                let descriptor = NSSortDescriptor(key: "Ratings", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
                self.arrData = (self.arrData.sortedArray(using: [descriptor]) ) as! NSMutableArray



                print(arrSelSaveIndex)
                
                
                
            }
            if ((indexPath as NSIndexPath).row == 2)
                
            {
                
                let descriptor = NSSortDescriptor(key: "MaxMenuPrice", ascending: false, selector: #selector(NSString.localizedStandardCompare(_:)))
                self.arrData = (self.arrData.sortedArray(using: [descriptor]) ) as! NSMutableArray


                
                
                
                
            }
            if ((indexPath as NSIndexPath).row == 3)
                
            {
                
                let descriptor = NSSortDescriptor(key: "Name", ascending: false, selector: #selector(NSString.caseInsensitiveCompare(_:)))
                self.arrData = (self.arrData.sortedArray(using: [descriptor]) ) as! NSMutableArray


                
                
                
                
            }
            
        }
        else
        {
            arrSelIndex [0] = (indexPath as NSIndexPath).row
            
            arrSelSaveIndex.add((indexPath as NSIndexPath).row)
            
            if (arrSelSaveIndex.count>1)
            {
              arrSelSaveIndex.removeObject(at: 0)
            }
            
            let strImg = arrSelImage[(indexPath as NSIndexPath).row]
            print("arrSelSaveIndex",arrSelSaveIndex)
            Ccell.itemImageView.image = UIImage(named:strImg as! String)
            Ccell.lblName.textColor=UIColor.init(red: 227.0/255.0, green: 123.0/255.0, blue: 4.0/255.0, alpha: 1.0)
            Ccell.lblName.text = arrSort[(indexPath as NSIndexPath).row] as? String
            
            //sorting in decending manner

            if ((indexPath as NSIndexPath).row == 0)
                
            {
                
                let descriptor: NSSortDescriptor = NSSortDescriptor(key: "Distance", ascending: true, selector: #selector(NSString.localizedStandardCompare(_:)))
                self.arrData = (self.arrData.sortedArray(using: [descriptor]) ) as! NSMutableArray


                
                print( "Distance", self.arrData)
                
                
                
            }
            if ((indexPath as NSIndexPath).row == 1)
                
            {
                
                let descriptor = NSSortDescriptor(key: "Ratings", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
                self.arrData = (self.arrData.sortedArray(using: [descriptor]) ) as! NSMutableArray


                
                print( "Ratings", self.arrData)
                
                
                
            }
            if ((indexPath as NSIndexPath).row == 2)
                
            {
                
                let descriptor = NSSortDescriptor(key: "MaxMenuPrice", ascending: true, selector: #selector(NSString.localizedStandardCompare(_:)))
                self.arrData = (self.arrData.sortedArray(using: [descriptor])) as! NSMutableArray

                
                print( "Cost", self.arrData)
                
                
                
            }
            if ((indexPath as NSIndexPath).row == 3)
                
            {
                
                let descriptor = NSSortDescriptor(key: "Name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
                self.arrData = (self.arrData.sortedArray(using: [descriptor])) as! NSMutableArray

                
                print( "Name", self.arrData)
                
                
                
            }
            
            
        }
        
        
        self.tblRestData.reloadData()
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        
        let cell = collectionView.cellForItem(at: indexPath)
        let indexPath1 = IndexPath(row: (indexPath as NSIndexPath).row, section: 0)
        Ccell = collectionView.cellForItem(at: indexPath1) as! CollectionViewCell
        let strImg = arrDefaultImage[(indexPath as NSIndexPath).row]
        Ccell.itemImageView.image = UIImage(named:strImg as! String)
        
        
        Ccell.lblName.textColor =  UIColor.white
        cell?.backgroundColor = UIColor.clear
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
      
        let numberOfCell: CGFloat = 4
        let cellWidth = UIScreen.main.bounds.size.width / numberOfCell
        let cellHeight = UIScreen.main.bounds.size.height / 9
        //print(cellHeight)
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    //MARK: SWREVEALVIEWCONTROLLER DATASOURCE AND DELEGATES METHODS
    
    //disable user interaction in frontmenu when sidemenu open

    func revealController(_ revealController: SWRevealViewController!,  willMoveTo position: FrontViewPosition){
        if(position == FrontViewPosition.left || position == FrontViewPosition.right) {
            // self.view.userInteractionEnabled = true
            sidebarMenuOpen = false
        } else {
            // self.view.userInteractionEnabled = false
            sidebarMenuOpen = true
        }
    }
    
    func revealController(_ revealController: SWRevealViewController!,  didMoveTo position: FrontViewPosition){
        if(position == FrontViewPosition.left || position == FrontViewPosition.right ) {
            // self.view.userInteractionEnabled = true
            sidebarMenuOpen = false
        } else {
       // self.view.userInteractionEnabled = false
            sidebarMenuOpen = true
        }
    }
    
    
    
    
    
}
