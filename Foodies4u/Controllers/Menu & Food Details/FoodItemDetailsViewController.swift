//
//  RestListDetails.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 23/08/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import MapKit
import Alamofire
import NVActivityIndicatorView
//import GoogleMaps
import SwiftyJSON



class FoodItemDetailsViewController: parentViewController,CLLocationManagerDelegate,MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource,ViewPagerDataSource
{
    

    
    var dictRestDetails:NSMutableDictionary!
    var totalCell = 0
    
    // related to weekly timingvar============================
    
    @IBOutlet var viewForWeeklyTiming:UIView!
    @IBOutlet weak var tblTimingDetails:UITableView!
    var arrWeeklyTiming:NSArray!
    //========================================================
    
    
    // related to review===============================
    
    var strTotalReviews = 0
    var arrRestaurantReviewList:NSArray!
    var reviewIndex=0
    //========================================================
    
    @IBOutlet weak var tblRestDetails:UITableView!
    
    var latitude:Double!
    var longitude:Double!
    var imagedData:Data!
    
    
    var arrData: NSMutableDictionary!
    var arrMenuImages: NSArray!
    
    var BadgeCount:Int! = 0

    var strWSURL: NSString!
    var strUSERID: AnyObject!
    var strComment: AnyObject!
    var strRestId:String!
    var strRestName:String!
    var strMenuId:String!
    var strDiscuont:String!

    
    
    var FoodItemdetail:FoodItemInfo!
    var cellRestDetail:MapDirectionCell!

    
    var arrFoodItemDetail:NSMutableArray!
    var arrRestId:NSMutableArray! = []
    var arrMenuId:NSMutableArray! = []
    var arrQty:NSMutableArray! = []

    
    
    var btn:UIButton!
    
    var DetailinDatabase: Bool = false
    
    let arrImages = ["foodimages", "details", "comments", "review"]
    
    var foodQty : Int = 1
    
    
    //MARK: VIEWLIFE CYCLE METHODS


    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        print(arrData)
        let strTemp: NSNumber = self.arrData.value(forKey: "TotalReview") as! NSNumber
        strTotalReviews = Int(strTemp)
        arrRestaurantReviewList = self.arrData.value(forKey: "MenuReview")! as! NSArray
   
        UserDefaults.standard .setValue("NO", forKey: "MYCARTFROMSIDEMENU")

        self.arrMenuImages = (self.arrData.value(forKey: "MenuImages")! as AnyObject).value(forKey: "Name") as! NSArray
        tblRestDetails.estimatedRowHeight = 50
        tblRestDetails.rowHeight = UITableViewAutomaticDimension
        
        
        strRestId = UserDefaults.standard.value(forKey: "RESTID") as! String

        strMenuId = String(describing:self.arrData.value(forKey: "Id") as! NSNumber)
        
        
        FoodItemdetail  = FoodItemInfo()
        arrFoodItemDetail = NSMutableArray()
        
        
        
        

    }
    override func viewWillAppear(_ animated: Bool) {
        
        

        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated:true);

        customBarButtons()
        self.navigationItem.title =  arrData.value(forKey: "MenuName") as? String
        simplenavigationBarUI()
        
        //check wheater detail is in database or not
        
        arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
        
        if(arrFoodItemDetail.count > 0 )
        {
            for i in 0...arrFoodItemDetail.count - 1
            {
                FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                
                
                arrMenuId.add(FoodItemdetail.strMenuId)
                
                
            }
            
        }
        
        if( arrMenuId .contains(strMenuId))
        {
            DetailinDatabase = true
        }
        
        strDiscuont = FoodItemdetail.strFoodDiscount
        
        tblRestDetails.reloadData()
        
        UserDefaults.standard.set("NO", forKey: "SHOWCART")

        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden=false
    }
    
    func customBarButtons()
    {
        //side menu button
        
        
        let str = UserDefaults.standard.value(forKey: "SHOWCART") as! String
        
        if(str == "YES")
        {
            //filters button
            let btnFilters = UIButton(frame: CGRect(x: self.view.frame.width-70, y: 5, width: 25, height: 25))
            btnFilters.setBackgroundImage(UIImage(named: "ic_menu.png"), for: UIControlState())
            btnFilters.addTarget(self, action: #selector(btnAddToCartpressed),  for: .touchUpInside)
            let rightBarButtonItemDelete: UIBarButtonItem = UIBarButtonItem(customView: btnFilters)
            self.navigationItem.setRightBarButton(rightBarButtonItemDelete, animated: true)
            
            self.navigationItem.rightBarButtonItem!.badgeValue = String(BadgeCount); //your value
            
            UserDefaults.standard.set("NO", forKey: "SHOWCART")

        }
        else
        {
            let img = UIImage(named: "BackArrow")
            navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(backPressed))
            self.navigationItem.setHidesBackButton(true, animated:true);


        }
        
        
       
        
        

        
    }
    
    func backPressed(_ sender: UIButton)
    {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: - TABLEVIEW DATASOURCE AND DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
            if ((indexPath as NSIndexPath).row == 0)
            {
                if DeviceType.IS_IPHONE_6P
                {
                    return 200
                }
                else if DeviceType.IS_IPHONE_4_OR_LESS
                {
                    return 175
                }
                else if DeviceType.IS_IPHONE_5
                {
                    return 175
                }
                else //if DeviceType.IS_IPHONE_6
                {
                    return 200
                    
                }
                return 175
            }
            else if (indexPath as NSIndexPath).row == 1 {
                
                //tableView.estimatedRowHeight = 150
                //tableView.rowHeight = UITableViewAutomaticDimension
                
                return UITableViewAutomaticDimension
            }
           else if (indexPath as NSIndexPath).row == 2 {
                
                //tableView.estimatedRowHeight = 150
                //tableView.rowHeight = UITableViewAutomaticDimension
                
                return 185

            }
           
            else if (strTotalReviews > 3 && (indexPath as NSIndexPath).row == totalCell - 1) { // view all review CELL
                return 35
            }
            else
            {
                   
                tblRestDetails.estimatedRowHeight = 70
                tblRestDetails.rowHeight = UITableViewAutomaticDimension
                
                return UITableViewAutomaticDimension
            }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
            
            if strTotalReviews > 3 {
                totalCell = arrImages.count + arrRestaurantReviewList.count + 1
            }else {
                totalCell = arrImages.count + arrRestaurantReviewList.count
        }
            return totalCell
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
          //display resturants deatils
            
            if strTotalReviews > 3 && (indexPath as NSIndexPath).row == totalCell - 1 // view all review CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "moreReviewCell", for: indexPath) as! Customcell
                    //tableView.dequeueReusableCellWithIdentifier("moreReviewCell") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                cell.btnViewAllReview.tag = (indexPath as NSIndexPath).row
                //let btnViewAllReview: UIButton = cell.viewWithTag(20) as! UIButton
                
                return cell
                
            }
            else  if (indexPath as NSIndexPath).row == 0 // MAP CELL
            {
                
                
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "ImagesCell") as! Customcell
                
                cell.viewPager.dataSource = self;
                cell.viewPager.animationNext()

                
                if(arrData.value(forKey: "FoodType") as? String ==  "Veg")
                {
                    cell.imgVegNonveg.image = UIImage(named: "Veg-Icon.png")
                    
                }
                else
                {
                    cell.imgVegNonveg.image = UIImage(named: "NonVeg-Icon.png")
                    
                }
                
                return cell

                
            }
        
           
            else if (indexPath as NSIndexPath).row == 1 // ADDRESS CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "textCell") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                cell.lblFoodItem.text = arrData.value(forKey: "MenuName") as? String
                
                cell.lblFoodPrice.text = "$ " + (arrData.value(forKey: "Price") as! String)
                
                if(arrData.value(forKey: "FoodTaste") as? String ==  "Spicy")
                {
                    cell.imgSpicy.isHidden = false
                }
                else
                {
                    cell.imgSpicy.isHidden = true

                }
                
                if (DetailinDatabase)
                {
                    FoodItemdetail = arrFoodItemDetail.object(at: (indexPath as NSIndexPath).row - 1 ) as! FoodItemInfo
                    
                    cell.lblQty.text = FoodItemdetail.streQuantity
                    
                    foodQty = Int(FoodItemdetail.streQuantity)!

                }
                else
                {
                    
                }
                
                
                
                cell.lblFoodDesc.text = arrData.value(forKey: "MenuShortDescription") as? String
                
                cell.btnPlus.addTarget(self, action: #selector(self.btnIncreaseQtyPressed(_:)), for: UIControlEvents.touchUpInside);
                cell.btnMinus.addTarget(self, action: #selector(self.btnDecreaseQtyPressed(_:)), for: UIControlEvents.touchUpInside);

                return cell
            }
          
            else if (indexPath as NSIndexPath).row == 2 // CUSINE TYPE CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "FoodCellTextview") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.txtComments.layer.borderWidth = 1.0
                cell.txtComments.layer.borderColor = UIColor.lightGray.cgColor
                
                let str = UserDefaults.standard.value(forKey: "SHOWCART") as! String

                
                if(str == "YES")
                {
                    cell.btnAddToCart.isUserInteractionEnabled = true
                    
                }
                else
                    
                {
                    cell.btnAddToCart.isUserInteractionEnabled = false
                }

                
                if (DetailinDatabase)
                {
                    FoodItemdetail = arrFoodItemDetail.object(at: (indexPath as NSIndexPath).row - 2 ) as! FoodItemInfo
                    
                    cell.txtComments.text = FoodItemdetail.strComment
                    
                    cell.btnAddToCart.setTitle("UPDATE", for: .normal)
                    
                }
                else
                {
                    cell.btnAddToCart.setTitle("ADD TO CART", for: .normal)

                }
                
                
                
                
                cell.btnAddToCart.addTarget(self, action: #selector(self.btnAddToCartPressed(_:)), for: UIControlEvents.touchUpInside);
                cell.btnCancelCart.addTarget(self, action: #selector(self.backPressed(_:)), for: UIControlEvents.touchUpInside);
                
                return cell
            }
            
            else if (indexPath as NSIndexPath).row == 3 // ReviewHeader CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "reviewHeaderCell") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                let lblText: UILabel = cell.viewWithTag(10) as! UILabel
                lblText.text = "(\(strTotalReviews) Reviews)"
                
                
                 if let btnWriteReview: UIButton = cell.viewWithTag(50) as? UIButton
                 {
                    
                btnWriteReview.tag = (indexPath as NSIndexPath).row

                btnWriteReview.addTarget(self, action: #selector(self.btnWriteReviewClicked(_:)), for: UIControlEvents.touchUpInside)
                }
                
                
                return cell
            }
            else if (indexPath as NSIndexPath).row >= 4 // review CELL
            {
                let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "reviewContentCell") as! Customcell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                reviewIndex = (indexPath as NSIndexPath).row - arrImages.count 
                
                
                let viewForBackground: UIView = cell.viewWithTag(10)! as UIView
                viewForBackground.layer.cornerRadius = 5.0
                viewForBackground.layer.borderColor = UIColor.lightGray.cgColor
                viewForBackground.layer.borderWidth = 0.8
                
                //for user profile image
                let url = URL(string: "http://foodies4you.intransure.com/Media/Customer/"  + String(describing: (arrRestaurantReviewList.value(forKey: "CustomerImage") as AnyObject).object(at: reviewIndex) as! String))
                //print("url",url)
                
                let imgIcon: UIImageView = viewForBackground.viewWithTag(20) as! UIImageView
                //make sure your image in this url does exist, otherwise unwrap in a if let check
                if(url ==  nil)
                {
                    
                    //imgIcon.image = UIImage(named:"Food_PlaceHolder.png")
                    
                    
                }
                else
                {
                    
                    do {
                        imgIcon.contentMode = .scaleAspectFit
                        let data = try Data(contentsOf: url!)
                        imgIcon.image = UIImage(data: data)
                        imgIcon.layer.borderColor = UIColor.lightGray.cgColor
                        imgIcon.layer.borderWidth = 0.8
                        imgIcon.layer.cornerRadius = imgIcon.frame.size.width/2

                    } catch {
                        print("Dim background error \(error)")
                    }

                }
                
                
                
                
                //for user name
                let lblUserName: UILabel = viewForBackground.viewWithTag(30) as! UILabel
                lblUserName.text = (arrRestaurantReviewList.value(forKey: "Customer") as AnyObject).object(at: reviewIndex) as? String
                
                //for rating
                let lblRatingText: UILabel = viewForBackground.viewWithTag(40) as! UILabel
                let strRating = (arrRestaurantReviewList.value(forKey: "Rating") as AnyObject).object(at: reviewIndex) as? String
                lblRatingText.text = "Rated       \(strRating!)" // fix 7 space inbetween for UI formatting
                
                //for description
                let lblDescriptionText: UILabel = viewForBackground.viewWithTag(50) as! UILabel
                lblDescriptionText.text = (arrRestaurantReviewList.value(forKey: "Description") as AnyObject).object(at: reviewIndex) as? String
                
                return cell
            }
            
        
        let tblcell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        return tblcell
        
    }
    
    
    // MARK: -DATABASE TO STORE VALUE INTO CART
    
    func dbInsertFoorItemIntoCart()   {
        self.view.endEditing(true)
        
        //model for fooditem to send data into database
        FoodItemdetail  = FoodItemInfo()
        
        
        arrFoodItemDetail = NSMutableArray()
        //menuid
        
        arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
        
        print("bhumika", arrFoodItemDetail)
        
         if (arrFoodItemDetail.count > 0)
         {
        
        for i in 0...arrFoodItemDetail.count - 1
        {
            FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
            
            
            
            arrMenuId.add(FoodItemdetail.strMenuId)
            
            print(arrMenuId)
            
            
            
        }
        }
        
        
        if(arrMenuId.contains(strMenuId))
        {
            
            
            let indexPath1 = IndexPath(row: 1, section: 0)
            let cell:Customcell = (self.tblRestDetails.cellForRow(at: indexPath1) as? Customcell)!
            
            let qnt:Int = Int(FoodItemdetail.streQuantity)!
            
            let qnt1:Int = Int(cell.lblQty.text!)!
            
            

            
            FoodItemdetail.streQuantity =  String(qnt1)
            
            let price:Double = Double(arrData.value(forKey: "Price") as! String)!
            let qty:Double = Double(cell.lblQty.text!)!
            
            // totalprice
            FoodItemdetail.strTotalPrice = String(qty * price)
            
            let TotalPrice:Double = qty * price
            
            //comment
            if(strComment != nil)
            {
                FoodItemdetail.strComment = strComment as! String
            }
            else
            {
                FoodItemdetail.strComment = ""
                
            }
            
            //menuid
            FoodItemdetail.strMenuId = strMenuId
            
            
            //Badge
            
            FoodItemdetail.strBadge = String(qnt1)
            
            
            if(strDiscuont.characters.count > 0)
            {
                FoodItemdetail.strFoodDiscount = strDiscuont
                
                if(strDiscuont.contains("%"))
                {
                    strDiscuont = strDiscuont.replacingOccurrences(of: "%", with: "")
                    
                    let disc:Double = Double(strDiscuont)!
                    
                    FoodItemdetail.strTotalPrice = String( (TotalPrice  * 1) - (TotalPrice * disc)/100)
                    
                }
                else
                {
                    
                    let disc:Double = Double(strDiscuont)!
                    FoodItemdetail.strTotalPrice = String( (TotalPrice  * 1) - disc)
                    
                }
            }
                
            else
            {
                FoodItemdetail.strFoodDiscount = ""
                
            }
            
            
            let isUpdated = ModelManager.getInstance().updateStudentData(FoodItemdetail)
            
            if isUpdated {
                
                self.view.makeToast("Successfully added to the cart", duration: 1.0, position: CSToastPositionBottom, title: "Foodies4u")
                
                let HomeViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "menuListViewController") as? menuListViewController
                
                UserDefaults.standard .set(self.BadgeCount
                    + self.foodQty , forKey: "Badge")
                _ = self.navigationController?.popViewController(animated: true)

                
                
            }
            

        }
            
            
            
            
            
        else {
            
            //model for fooditem to send data into database
            FoodItemdetail  = FoodItemInfo()
            
            
            let indexPath1 = IndexPath(row: 1, section: 0)
            let cell:Customcell = (self.tblRestDetails.cellForRow(at: indexPath1) as? Customcell)!
            
            let qnt1:Int = Int(cell.lblQty.text!)!
            
            
            FoodItemdetail.streQuantity = String(qnt1)
            
            let price:Double = Double(arrData.value(forKey: "Price") as! String)!
            let qty:Double = Double(cell.lblQty.text!)!
            
            // totalprice
            FoodItemdetail.strTotalPrice = String(qty * price)
            
            //comment
            if(strComment != nil)
            {
                FoodItemdetail.strComment = strComment as! String
            }
            else
            {
                FoodItemdetail.strComment = ""
                
            }
            
            //menuid
            FoodItemdetail.strMenuId = strMenuId
            
            //menuname
            FoodItemdetail.strMenuName = (arrData.value(forKey: "MenuName") as? String)!
            
            
            //menuunitprice
            FoodItemdetail.strUnitPrice = arrData.value(forKey: "Price") as! String
            
            //restName
            
            FoodItemdetail.strRestName = strRestName
            
            //Badge
            
            FoodItemdetail.strBadge = String(qnt1)
            
            //restId
            FoodItemdetail.strRestId = strRestId
            
            
            let isInserted = ModelManager.getInstance().addStudentData(FoodItemdetail)
            if isInserted {
                
                self.view.makeToast("Successfully added to the cart", duration: 1.0, position: CSToastPositionBottom, title: "Foodies4u")
                let HomeViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "menuListViewController") as? menuListViewController
                //  HomeViewControllerObj?.BadgeCount = self.foodQty + self.BadgeCount
                
                UserDefaults.standard .set(self.BadgeCount
                    + self.foodQty , forKey: "Badge")
                _ = self.navigationController?.popViewController(animated: true)


            } else {
            }
            

        }
        
        
        //update badges
        
        if(arrFoodItemDetail.count > 0 )
        {
            for i in 0...arrFoodItemDetail.count - 1
            {
                FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                
                
                arrQty.add(FoodItemdetail.streQuantity)
            }
            
            
            
            var sum : Int = 0
            
            for i in 0...arrQty.count - 1
            {
                sum = sum + Int(arrQty[i] as! String)!
            }
            
            print("sum",sum)
            
            self.navigationItem.rightBarButtonItem!.badgeValue = String(sum);

            
        }
        
        
        
    }
    
    func dbDeletefromItemIntoCart()
    {
        
        
        let isDeleted = ModelManager.getInstance().deleteAllStudentData(FoodItemdetail)
        
        if isDeleted {
            
            BadgeCount = 0
            self.navigationItem.rightBarButtonItem!.badgeValue = String(BadgeCount);
            //Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
            //Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
    }
    
    //MARK: -  BUTTON CLICKS
    
    func btnAddToCartpressed(_ sender: AnyObject)
    {
        
        if(BadgeCount > 0)
        {
            
            UserDefaults.standard .setValue("NO", forKey: "MYCARTFROMFOODITEM")

            let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "PlaceOrderViewController") as? PlaceOrderViewController
            self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
        }
        
    }
    @IBAction func btnAddToCartPressed(_ sender: UIButton)
    {
        
        
        
        //model for fooditem to send data into database
        FoodItemdetail  = FoodItemInfo()
        
        
        arrFoodItemDetail = NSMutableArray()
        arrRestId = NSMutableArray()
        
        
        arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
        
        
        //check wether from other resturant or from same resturant
        if(arrFoodItemDetail.count > 0)
        {
        
        for i in 0...arrFoodItemDetail.count - 1
        {
            FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
            arrRestId.add(FoodItemdetail.strRestId)
            print("multiples",arrRestId)
            
        }
        
        if !(arrRestId.contains(strRestId))
        {
            let dialog = ZAlertView(title: "Foodies4u",
                                    message: "You have some items from different restaurant in your cart, Adding this item will clear up your earlier items.            Are you sure you want to clear your cart?",
                                    isOkButtonLeft: true,
                                    okButtonText: "Yes",
                                    cancelButtonText: "No",
                                    okButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                                        self.dbDeletefromItemIntoCart()

                },
                                    cancelButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
        else if (foodQty == 0)
        {
           
            
            let dialog = ZAlertView(title: "Foodies4u", message: "Please increse the Quantity",closeButtonText: "Ok",
                                    closeButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()})
            dialog.show()
        }
            
        else
        {
            
            self.dbInsertFoorItemIntoCart()
            
        }
            
        }
        else
        {
            
             if (foodQty == 0)
            {
                let dialog = ZAlertView(title: "Foodies4u", message: "Please increse the Quantity",closeButtonText: "Ok",
                                        closeButtonHandler: { (alertView) -> () in
                                            alertView.dismissAlertView()})
                dialog.show()
            }
            else
             {
            
            self.dbInsertFoorItemIntoCart()
            }

        }

    }

    

    
    @IBAction func btnIncreaseQtyPressed(_ sender: UIButton)
    {
        foodQty = foodQty + 1
    
        let indexPath1 = IndexPath(row: 1, section: 0)
        let cell:Customcell = (self.tblRestDetails.cellForRow(at: indexPath1) as? Customcell)!
        cell.lblQty.text = String(foodQty)
    }
    @IBAction func btnDecreaseQtyPressed(_ sender: UIButton)
    {

        
        if (foodQty == 1)
        {
            foodQty = 1
            
            // get menu id
            
            if (DetailinDatabase)
            {
            
            strMenuId = FoodItemdetail.strMenuId
            
            let isDeleted = ModelManager.getInstance().deleteStudentData(FoodItemdetail)
            
            if isDeleted
            {
                //Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
                //get data from database
                
                arrFoodItemDetail.removeAllObjects()
                FoodItemdetail  = FoodItemInfo()
                arrFoodItemDetail = NSMutableArray()
                arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
                //self.dbInsertFoorItemIntoCart()
                
                
                
                
            }
            else
            {
                
            }
            }

        }
        else
        {
            foodQty = foodQty - 1

        }
        let indexPath1 = IndexPath(row: 1, section: 0)
        let cell:Customcell = (self.tblRestDetails.cellForRow(at: indexPath1) as? Customcell)!
        cell.lblQty.text = String(foodQty)
        
    }
    
    
    
    @IBAction func btnWriteReviewClicked(_ sender: UIButton){
        
    }
    
    @IBAction func btnViewAllReviewClicked(_ sender: UIButton){
        
        let allReviewsVC = self.storyboard?.instantiateViewController(withIdentifier: "allReviewListViewController") as? allReviewListViewController
        
        allReviewsVC?.strMenuID = (String(describing: arrData.value(forKey: "Id") as! String!))
        
        self.navigationController?.pushViewController(allReviewsVC!, animated: true)
        
        //self.performSegueWithIdentifier("allReviewListSegue", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "allReviewListSegue" {
            
            let allReviewListVC = segue.destination as? allReviewListViewController
            
            let RestId:String = (self.dictRestDetails.value(forKey: "RestaurantId") as! String)
            
            print("RestId" , RestId)
            
            allReviewListVC?.strRestaurantID = "\(RestId)"
        
        }
        else if segue.identifier == "menuListSegue" {
            
            let menuListSegue = segue.destination as? menuListViewController
            
            menuListSegue?.restName = self.dictRestDetails.value(forKey: "Name") as? String
            
           // menuListSegue!.arrRestaurantDetails = dictRestDetails
        }
    }
    
    //MARK: - TEXTVIEW DATASOURCE AND DELEGATES METHODS
    
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (DeviceType.IS_IPHONE_4_OR_LESS)
        {
              animateViewMoving(true, moveValue: 90)
        }

        else
        {
            animateViewMoving(true, moveValue: 220)

        }
        
        
        print(textView.text);
        
    }
    func textViewDidEndEditing(_ textView: UITextView)
    {
        
        strComment = textView.text as AnyObject!

        
        if (DeviceType.IS_IPHONE_4_OR_LESS)
        {
            animateViewMoving(false, moveValue: 90)
        }
            
        else
        {
            animateViewMoving(false, moveValue: 220)
            
        }
        
    }
    
    
    
    //MARK: - STATUS BAR
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent;
    }
    
    // MARK: VIEWPAGE DATASOURCE
    
    func numberOfItems(_ viewPager:ViewPager) -> Int {
        
        if(arrMenuImages.count == 0)
        {
            
            return 1;
            
        }
        else
        {
            return arrMenuImages.count;
        }
        
        return 0
        
    }
    
    func viewAtIndex(_ viewPager:ViewPager, index:Int, view:UIView?) -> UIView {
        var newView = UIImageView();
        if(newView.image == nil){
            newView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height:  self.view.frame.height))
            
            
            
            if(arrMenuImages.count == 0)
            {
                newView.image = UIImage(named:"Food_PlaceHolder.png")
                
            }
            else
            {
                let restId:String = UserDefaults.standard.value(forKey: "RESTID") as! String
                print(restId)
                
                let url = URL(string: "http://foodies4you.intransure.com/Media/Menu/" + String(describing: (self.arrData.value(forKey: "Id")) as! NSNumber) + "/" + (arrMenuImages [index] as! String))
                //print("url",url)
                
                
                 //make sure your image in this url does exist, otherwise unwrap in a if let check
                if(url ==  nil)
                {
                    
                    newView.image = UIImage(named:"Food_PlaceHolder.png")
                    
                    
                }
                else
                {
                    let data = try? Data(contentsOf: url!)
                    newView.image  = UIImage(data: data!)
                }
                
                
            }
            
        }
        
        return newView
    }
    
    func didSelectedItem(_ index: Int) {
        print("select index \(index)")
    }

    
}
