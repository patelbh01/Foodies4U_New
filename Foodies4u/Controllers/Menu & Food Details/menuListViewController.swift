//
//  menuListViewController.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 13/09/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import Alamofire
import NVActivityIndicatorView
//import GoogleMaps
import SwiftyJSON


extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}


extension UIColor {
    static func randomColor() -> UIColor {
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}

class menuListViewController: parentViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate,ViewPagerDataSource,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {
    
    // farray to get data
    var arrRestaurantDetails: NSDictionary!
    
    //for horizontal tab
    
    @IBOutlet var tabCollectionView: UICollectionView?
    @IBOutlet var foodItemCollectionView: UICollectionView?

    var Ccell:CollectionViewCell!
    var MyRestcells:MyResListCell!

    var btnTaped:Int! = 0
    var row:Int! = 0
    var tblrow:Int! = -1

    var foodItemTapped:Int! = 0
    var BadgeCount:Int!
    
    
    let arrSelectedItem : NSMutableArray=[]
    var arrData: NSMutableArray!
    var arrMenuData: NSMutableArray!

    var arrRestImages: NSArray!
    
    
    var strRestId:String!
    var strMenuId:String!

    
    var restName:String!
    var strWSURL: NSString!
    var strUSERID: AnyObject!
   
    
    //
    var arrTabData: NSArray!
    var arrFoodName:NSArray!
    var arrFoodPrice:NSArray!
    var arrFoodMenuId:NSArray!
    var arrfavFoodItem:NSArray!
    var arrFoodItemImages:NSArray!
    var arrAddToCartItems:NSMutableArray=[]
    var arrQty:NSMutableArray! = []


    
    @IBOutlet var viewForDiscount:UIView!
    @IBOutlet var tblDiscount: UITableView?
    @IBOutlet var lblNoData: UILabel!
    
    var arrDiscounts:NSArray!
    var arrIsSpcial:NSArray!

    var arrDiscountsDetails:NSArray!
    var strDiscuont:String! = ""
    var strDiscuontForFood:String! = ""

    var arrSelDiscounts:NSMutableArray!=[]


    var FoodItemDetails = FoodItem()
    
    @IBOutlet weak var viewPager: ViewPager!
    
    var arrFoodItemDetail:NSMutableArray!
    var FoodItemdetail:FoodItemInfo!
    var arrBadge:NSMutableArray! = []
    var arrRestId:NSMutableArray! = []
    var badge:String!
    var arrMenuId:NSMutableArray! = []

    //for database addto cart
    

    
    //MARK: - VIEW DELEGATE METHODS
    
    
    override func viewDidLoad()
    {
        
        self.setNeedsStatusBarAppearanceUpdate()
        UserDefaults.standard .set(0 , forKey: "Badge")

        viewPager.dataSource = self;
        

        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.title =  restName
        strRestId = UserDefaults.standard.value(forKey: "RESTID") as! String
        UserDefaults.standard .setValue("NO", forKey: "MYCARTFROMSIDEMENU")


        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        

        
        simplenavigationBarUI()
        customBarButtons()
        

        self.navigationController?.isNavigationBarHidden = false

        self.navigationItem.setHidesBackButton(true, animated:true);

        
        
        if InternetConnection.isConnectedToNetwork()
        {
            row = 0
            getMenuList() {
                responseObject, error in
                
                if (responseObject != nil)
                {
                    
                
                let json = JSON(responseObject!)
                if let string = json.rawString()
                {
                    
                    self.stopActivityAnimating()
                    
                    
                    if let data = string.data(using: String.Encoding.utf8)
                    {
                        do {
                            
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            print("json ",json)
                            if(json!["Success"] as? String  == "1")
                            {
                                
                                self.arrData = json!["Data"] as! NSMutableArray
                                self.arrTabData =  self.arrData.value(forKey: "CategoryName") as! NSArray
                                self.arrFoodName = ((self.arrData[0] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "MenuName") as! NSArray
                                self.arrFoodPrice = ((self.arrData[0] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "Price") as! NSArray
                                self.arrFoodMenuId = ((self.arrData[0] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "Id") as! NSArray
                                self.arrFoodItemImages = ((self.arrData[0] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "DefaultMenuImage") as! NSArray
                                self.arrfavFoodItem = ((self.arrData[0] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "Favorite") as! NSArray
                                self.arrDiscounts = ((self.arrData[0] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "DiscountData") as! NSArray
                                self.arrIsSpcial = ((self.arrData[0] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "IsSpecial") as! NSArray
                                self.arrMenuData =  (self.arrData[0] as AnyObject).value(forKey: "MenuData") as! NSMutableArray
                                
                                self.tabCollectionView?.isHidden = false
                                self.foodItemCollectionView?.isHidden = false
                                self.lblNoData.isHidden = true
                                
                                self.tabCollectionView?.dataSource = self
                                self.tabCollectionView?.delegate = self
                                self.tabCollectionView?.reloadData()
                                
                                self.foodItemCollectionView?.dataSource = self
                                self.foodItemCollectionView?.delegate = self
                                self.foodItemCollectionView?.reloadData()
                                
                            }
                            else
                            {
                                self.tabCollectionView?.isHidden = true
                                self.foodItemCollectionView?.isHidden = true
                                self.lblNoData.isHidden = false
                                
                            }
                            
                        }
                            
                        catch {
                            print("Something went wrong")
                        }
                    }
                }
                }
                else
                {
                    self.stopActivityAnimating()
                    self.AlertMethod()
                    
                }
                
            }
        }
        
        UserDefaults.standard.set("NO", forKey: "SHOWCART")

        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewPager.scrollToPage(0)
    }
    
    //MARK: CUSTOMIZE NAVIGATIONBAR AND IT'S METHODS
    
    
    
    func customBarButtons()
    {
        //side menu button
        
        
        
        //add tocart button
        
        
        
        
        
        
        
        //model for fooditem to send data into database
        FoodItemdetail  = FoodItemInfo()
        
        
        arrFoodItemDetail = NSMutableArray()
        arrBadge = NSMutableArray()
        arrAddToCartItems.removeAllObjects()

        //menuid
        
        arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
        
        
            
            //update badges
            
            if(arrFoodItemDetail.count > 0 )
            {
                
                arrQty.removeAllObjects()
                for i in 0...arrFoodItemDetail.count - 1
                {
                    FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                    
                    
                    arrQty.add(FoodItemdetail.streQuantity)
                    
                    
                    let someString = FoodItemdetail.strMenuId
                    let number = Int(someString)
                    let myNumber = NSNumber(value:number!)
                    
                    
                    if(FoodItemdetail.strRestId == strRestId)
                    {
                        arrAddToCartItems.add(myNumber)
                    }
                }
                
                
                
                var sum : Int = 0
                
                for i in 0...arrQty.count - 1
                {
                    sum = sum + Int(arrQty[i] as! String)!
                }
                
                print("sum",sum)
                
                BadgeCount = sum
                
                
            }
            else
            {
                BadgeCount = 0

            }
        
        
        let str = UserDefaults.standard.value(forKey: "SHOWCART") as! String
        
        if(str == "YES")
        {
            let btnFilters = UIButton(frame: CGRect(x: self.view.frame.width-70, y: 5, width: 25, height: 25))
            btnFilters.setBackgroundImage(UIImage(named: "ic_menu.png"), for: UIControlState())
            btnFilters.addTarget(self, action: #selector(btnAddToCartPressed),  for: .touchUpInside)
            let rightBarButtonItemDelete: UIBarButtonItem = UIBarButtonItem(customView: btnFilters)
            self.navigationItem.setRightBarButton(rightBarButtonItemDelete, animated: true)
            self.navigationItem.rightBarButtonItem!.badgeValue = String(BadgeCount) //your value

            
        }
        else
        {
            let img = UIImage(named: "BackArrow")
            navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(backPressed))

        }
        
        
        

       


        
    //}
    }
    
    
    
    func btnAddToCartPressed(_ sender: AnyObject)
    {
        
        if(BadgeCount > 0)
        {
            UserDefaults.standard .setValue("NO", forKey: "MYCARTFROMFOODITEM")
            let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "PlaceOrderViewController") as? PlaceOrderViewController
            self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
        }
        
    }
    
    @IBAction func btnCloseDiscountClicked(_ sender: UIButton){
        
        viewForDiscount.isHidden = true
    }
    func backPressed(_ sender: AnyObject)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == foodItemCollectionView {
            return self.arrFoodPrice.count
        }
        return self.arrTabData.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        
        if collectionView == tabCollectionView
        {
            Ccell = collectionView.dequeueReusableCell(withReuseIdentifier: "tabCell", for: indexPath) as! CollectionViewCell
            
            
            Ccell?.btnMenu .setTitle((arrTabData[(indexPath as NSIndexPath).row] as? String)!.uppercased(), for: UIControlState())
            
            
            
            if(row == (indexPath as NSIndexPath).row)
            {
                Ccell.viewUnerline.isHidden = false
                Ccell.btnMenu.setTitleColor(UIColor.red, for: UIControlState())
                Ccell.btnMenu.tag =  (indexPath as NSIndexPath).row
            }
            else
            {
                Ccell.viewUnerline.isHidden = true
                Ccell.btnMenu.setTitleColor(UIColor.black, for: UIControlState())
                Ccell.btnMenu.tag =  (indexPath as NSIndexPath).row
            }
            
            
            
            
        }
        else
        {
            Ccell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodItemCell", for: indexPath) as! CollectionViewCell
            
            let str = UserDefaults.standard.value(forKey: "SHOWCART") as! String
            
            if(str == "YES")
            {
                Ccell.btnAddtoCart.isHidden = false

               Ccell.btnAddtoCart.addTarget(self, action: #selector(self.btnAddtoCartSelected(_:)), for: UIControlEvents.touchUpInside)
               Ccell.btnAddtoCart.tag = (indexPath as NSIndexPath).row
            }
            else
            
            {
                 Ccell.btnAddtoCart.isHidden = true
            }
            
            //food item name
            Ccell.lblName.text =  self.arrFoodName.object(at: (indexPath as NSIndexPath).row) as? String
            
            //food item price
            Ccell.lblprice.text = "$ " + String(describing: self.arrFoodPrice.object(at: (indexPath as NSIndexPath).row))
            
            //food item image
            
            if(arrFoodItemImages [(indexPath as NSIndexPath).row] as! String == "")
            {
                Ccell.itemImageView.image = UIImage(named:"Food_PlaceHolder.png")
                
            }
            else
            {
                
                
                let url = URL(string: "http://foodies4you.intransure.com/Media/Menu/" + String(describing: self.arrFoodMenuId.object(at: (indexPath as NSIndexPath).row)  as! NSNumber) + "/" + (arrFoodItemImages [(indexPath as NSIndexPath).row] as! String))
               // print("url",url)
                
                if(url == nil)
                {
                    Ccell.itemImageView.image = UIImage(named:"Food_PlaceHolder.png")

                }
                else
                {
                    let data = try? Data(contentsOf: url!)

                    Ccell.itemImageView.image  = UIImage(data: data!)

                }
                
  
                
            }
            
            // add to cart food items
            
            
            let mutableArr = NSMutableArray(array: self.arrFoodMenuId)
 
            
        if(arrAddToCartItems.contains(mutableArr.object(at: (indexPath as NSIndexPath).row)  as! NSNumber))
            {
              self.Ccell.viewSelected.isHidden = false
            }
            else
        
           {
            self.Ccell.viewSelected.isHidden = true

            
            }
        
            //fav food item image
            
            
            if (self.arrfavFoodItem.object(at: (indexPath as NSIndexPath).row) as! Bool == false)
            {
                
                self.Ccell.btnFavItem.setImage(UIImage.init(named: "Heart"), for: UIControlState())

            }
            else
            {
                
                self.Ccell.btnFavItem.setImage(UIImage.init(named: "ActiveHeart"), for: UIControlState())

            }

            
            
            Ccell.btnFavItem.addTarget(self, action: #selector(self.btnFavItemSelected(_:)), for: UIControlEvents.touchUpInside)
            
            Ccell.btnFavItem.tag = (indexPath as NSIndexPath).row
            
            //for discount
            
            if((self.arrDiscounts.object(at: (indexPath as NSIndexPath).row) as AnyObject).count > 0)
            {
                Ccell.btnDiscount.isHidden = false

            Ccell.btnDiscount.addTarget(self, action: #selector(self.btnDiscountSelected(_:)), for: UIControlEvents.touchUpInside)
            
            Ccell.btnDiscount.tag = (indexPath as NSIndexPath).row
            }
            else
            {
                Ccell.btnDiscount.isHidden = true
            }
            
            if(self.arrIsSpcial.object(at: (indexPath as NSIndexPath).row) as! Int == 0)
            {
                
              
                
                Ccell.imgisspecial.isHidden = true
            }
            else
            {
                Ccell.imgisspecial.isHidden = false
            }
            
            
            return Ccell
        }
        return Ccell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        if collectionView == foodItemCollectionView
        {
            let numberOfCell: CGFloat = 2
            let cellWidth = foodItemCollectionView!.frame.size.width / numberOfCell
            //let cellHeight = foodItemCollectionView!.frame.size.height / numberOfCell
            return CGSize(width: cellWidth, height: cellWidth)
            
        }
        else
        {
            let numberOfCell: CGFloat = 3
            let cellWidth = foodItemCollectionView!.frame.size.width / numberOfCell
            //   let cellHeight = UIScreen.mainScreen().bounds.size.height / 9
            return CGSize(width: cellWidth, height: cellWidth)
        }
        
        return CGSize(width: 0, height: 0)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        row = (indexPath as NSIndexPath).row
        
        if collectionView == tabCollectionView
        {
            
          
            
            let indexPath1 = IndexPath(row: row, section: 0)
            Ccell = self.tabCollectionView!.cellForItem(at: indexPath1) as! CollectionViewCell
            Ccell.viewUnerline.isHidden = false
            Ccell.btnMenu.setTitleColor(UIColor.red, for: UIControlState())
           // Ccell.viewSelected.hidden = true

            
            self.arrMenuData =  (self.arrData[(indexPath as NSIndexPath).row] as AnyObject).value(forKey: "MenuData") as! NSMutableArray
            print(self.arrMenuData)

            self.arrFoodName = ((self.arrData[(indexPath as NSIndexPath).row] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "MenuName") as! NSArray
            self.arrFoodPrice = ((self.arrData[(indexPath as NSIndexPath).row] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "Price") as! NSArray
            self.arrFoodMenuId = ((self.arrData[(indexPath as NSIndexPath).row] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "Id") as! NSArray
            self.arrFoodItemImages = ((self.arrData[(indexPath as NSIndexPath).row] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "DefaultMenuImage") as! NSArray
            self.arrfavFoodItem = ((self.arrData[(indexPath as NSIndexPath).row] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "Favorite") as! NSArray
            self.arrDiscounts = ((self.arrData[(indexPath as NSIndexPath).row] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "DiscountData") as! NSArray
            self.arrIsSpcial = ((self.arrData[(indexPath as NSIndexPath).row] as AnyObject).value(forKey: "MenuData")! as AnyObject).value(forKey: "IsSpecial") as! NSArray

            tabCollectionView?.reloadData()

            foodItemCollectionView?.reloadData()
            
        }
        else
        {
            
          print("home " , self.arrMenuData[(indexPath as NSIndexPath).row] )
            
            
            let FoodDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "FoodItemDetailsViewController") as? FoodItemDetailsViewController
            
            
            FoodDetailsVC?.strRestName = restName
            
            let arr:NSMutableDictionary =  (self.arrMenuData[(indexPath as NSIndexPath).row] as? NSMutableDictionary)!
            
            //update badge and pass araay according to that
          UserDefaults.standard .set(BadgeCount , forKey: "Badge")
         FoodDetailsVC?.BadgeCount = BadgeCount
         FoodDetailsVC?.arrData = arr
        FoodDetailsVC?.strDiscuont = strDiscuontForFood

            self.navigationController?.pushViewController(FoodDetailsVC!, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        
        let cell = collectionView.cellForItem(at: indexPath)
        let indexPath1 = IndexPath(row: (indexPath as NSIndexPath).row, section: 0)
        Ccell = collectionView.cellForItem(at: indexPath1) as! CollectionViewCell
        if( Ccell != nil)

        {
        Ccell.viewUnerline.isHidden = true
        Ccell.btnMenu.setTitleColor(UIColor.black, for: UIControlState())
        }
        cell
    }
    
    //MARK: - BUTTON CLICKS

    
    func btnAddtoCartSelected(_ sender:UIButton!)
    {
        
        
        
        //model for fooditem to send data into database
        
        
        
        //check wether from other resturant or from same resturant
        
        FoodItemdetail  = FoodItemInfo()
        
        
        arrFoodItemDetail = NSMutableArray()
        arrRestId = NSMutableArray()
        
        
        arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
        
        
        if(arrFoodItemDetail.count > 0 )
        {
            for i in 0...arrFoodItemDetail.count - 1
            {
                FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                arrRestId.add(FoodItemdetail.strRestId)
                print("multiples",arrRestId)
                
            }
        
        
        if !(arrRestId.contains(strRestId))
        {
            let dialog = ZAlertView(title: "Foodies4u",
                                    message: "You have some items from different restaurant in your cart, Adding this item will clear up your earlier items.            Are you sure you want to clear your cart?",
                                    isOkButtonLeft: true,
                                    okButtonText: "Yes",
                                    cancelButtonText: "No",
                                    okButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                                        self.dbDeletefromItemIntoCart()

                                        
                },
                                    cancelButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
    else
        {
            
            foodItemTapped = foodItemTapped + 1
            let indexPath1 = IndexPath(row:sender.tag, section: 0)
            Ccell = self.foodItemCollectionView!.cellForItem(at: indexPath1) as! CollectionViewCell
            
            
            let mutableArr = NSMutableArray(array: self.arrFoodMenuId)

            
                
                if(arrAddToCartItems.contains( mutableArr.object(at: (indexPath1 as NSIndexPath).row) as! NSNumber))
                {
                    
                    arrSelectedItem.remove((indexPath1 as NSIndexPath).row)
                    arrAddToCartItems.remove(self.arrFoodMenuId.object(at: (indexPath1 as NSIndexPath).row))
                    
                    strDiscuont = ""
                    Ccell.viewSelected.isHidden = true
                    
                    
                    
                    //model for fooditem to send data into database
                    var FoodItemdetail:FoodItemInfo  = FoodItemInfo()
                    
                    //menuid
                    FoodItemdetail.strMenuId = String(describing: self.arrFoodMenuId.object(at: (indexPath1 as NSIndexPath).row) as! NSNumber)
                    
                    
                    //cehck if discount is applied than deslect it 
                    
                    if(self.arrDiscountsDetails != nil)
                    {
                    
                    for i in 0...self.arrDiscountsDetails.count - 1
                    {
                        let indexPath1 = IndexPath(row: i, section: 0)
                        MyRestcells = self.tblDiscount!.cellForRow(at: indexPath1) as! MyResListCell
                        MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                        arrSelDiscounts.removeAllObjects()
                        
                    }
                    }
                    
                    
                    let isDeleted = ModelManager.getInstance().deleteStudentData(FoodItemdetail)
                    
                    if isDeleted {
                        //Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
                        
                        //make badges 0 when it willl delete from the cart
                        
                        arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
                        
                        print("arrFoodItemDetail", arrFoodItemDetail)
                        
                            //update badges
                            
                            if(arrFoodItemDetail.count > 0 )
                            {
                                arrQty.removeAllObjects()

                                
                                for i in 0...arrFoodItemDetail.count - 1
                                {
                                    
                                    FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                                    
                                    
                                    arrQty.add(FoodItemdetail.streQuantity)
                                }
                                
                                
                                
                                var sum : Int = 0
                                
                                for i in 0...arrQty.count - 1
                                {
                                    sum = sum + Int(arrQty[i] as! String)!
                                }
                                
                                print("sum",sum)
                                
                                BadgeCount = sum
                                
                                
                            }
                            else
                            {
                                BadgeCount = 0
                                arrAddToCartItems.removeAllObjects()
                                
                                
                            }
                            
                            
                        self.navigationItem.rightBarButtonItem!.badgeValue = String(BadgeCount);

                        
                        
                    }
                    
                   
                    
                }
                    
                else
                {
                    
                    
                    arrSelectedItem.add((indexPath1 as NSIndexPath).row)
                    arrAddToCartItems.add(self.arrFoodMenuId.object(at: (indexPath1 as NSIndexPath).row) as! NSNumber)
                    print("hello " ,arrAddToCartItems)
                    Ccell.viewSelected.isHidden = false
                    
                    BadgeCount = BadgeCount + 1
                    
                    self.navigationItem.rightBarButtonItem!.badgeValue = String( BadgeCount); //your value
                    
                    
                    for i in 0...arrFoodItemDetail.count - 1
                    {
                        FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                        
                        
                        
                        arrMenuId.add(FoodItemdetail.strMenuId)
                        
                        print(arrMenuId)
                        
                        
                        
                    }
                    
                    //model for fooditem to send data into database
                    FoodItemdetail  = FoodItemInfo()
                    
                    //menuid
                    FoodItemdetail.strMenuId = String(describing: self.arrFoodMenuId.object(at: (indexPath1 as NSIndexPath).row) as! NSNumber)
                    
                    //menuname
                    FoodItemdetail.strMenuName = self.arrFoodName.object(at: (indexPath1 as NSIndexPath).row) as! String
                    
                    //menucomment
                    FoodItemdetail.strComment = ""
                    
                    //menuunitprice
                    FoodItemdetail.strUnitPrice = self.arrFoodPrice.object(at: (indexPath1 as NSIndexPath).row) as! String
                    
                    //menuquanitty
                    
                    FoodItemdetail.streQuantity = "1"
                    
                    
                    // totalprice
                    
                    let price:Double = Double(self.arrFoodPrice.object(at: (indexPath1 as NSIndexPath).row) as! String)!

                    FoodItemdetail.strTotalPrice = self.arrFoodPrice.object(at: (indexPath1 as NSIndexPath).row) as! String
                    
                    //restName
                    
                    FoodItemdetail.strRestName = restName
                    
                    //Badge
                    
                    FoodItemdetail.strBadge = String( BadgeCount)
                    
                    //restId
                    FoodItemdetail.strRestId = strRestId
                    
                    //Dicount
                    
                    //
                    
                    if(strDiscuont.characters.count > 0)
                    {
                        FoodItemdetail.strFoodDiscount = strDiscuont
                        
                        strDiscuontForFood = strDiscuont

                        if(strDiscuont.contains("%"))
                        {
                            strDiscuont = strDiscuont.replacingOccurrences(of: "%", with: "")
                            
                            let disc:Double = Double(strDiscuont)!
                            
                            FoodItemdetail.strTotalPrice = String( (price  * 1) - (price * disc)/100)
                            
                        }
                        else
                        {
                            
                            let disc:Double = Double(strDiscuont)!
                            FoodItemdetail.strTotalPrice = String( (price  * 1) - disc)
                            
                        }
                    }
                    
                    else
                    {
                        FoodItemdetail.strFoodDiscount = ""
                        
                    }
                    
                    
                    
                    let isInserted = ModelManager.getInstance().addStudentData(FoodItemdetail)
                    if isInserted {
                        //Util.invokeAlertMethod("", strBody: "Record Inserted successfully.", delegate: nil)
                        
                            arrQty.removeAllObjects()
                        
                            strDiscuont = ""
                            strDiscuontForFood = ""
                            FoodItemdetail.strFoodDiscount = ""
                           FoodItemdetail  = FoodItemInfo()
                           arrFoodItemDetail = NSMutableArray()
                            arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
                        
                            for i in 0...arrFoodItemDetail.count - 1
                            {
                                FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                                
                                
                                arrQty.add(FoodItemdetail.streQuantity)
                            }
                            
                            
                            
                            var sum : Int = 0
                            
                            for i in 0...arrQty.count - 1
                            {
                                sum = sum + Int(arrQty[i] as! String)!
                            }
                            
                            print("sum",sum)
                            
                            BadgeCount = sum
                       
                        
                        
                        self.navigationItem.rightBarButtonItem!.badgeValue = String(BadgeCount);

                    } else {
                        //Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
                    }
                    
                    // print(arrAddToCartItems)
                    
                    
                }
                
                
            }
            
            
            
        }
            
       
        else
        {
            
            
            
            foodItemTapped = foodItemTapped + 1
            let indexPath1 = IndexPath(row:sender.tag, section: 0)
            Ccell = self.foodItemCollectionView!.cellForItem(at: indexPath1) as! CollectionViewCell
            
            let mutableArr = NSMutableArray(array: self.arrFoodMenuId)

            
                if(arrAddToCartItems.contains(String(describing: mutableArr.object(at: (indexPath1 as NSIndexPath).row) as! NSNumber)))
                {
                    //model for fooditem to send data into database
                    
                    arrSelectedItem.remove((indexPath1 as NSIndexPath).row)
                    arrAddToCartItems.remove(self.arrFoodMenuId.object(at: (indexPath1 as NSIndexPath).row))
                    
                    Ccell.viewSelected.isHidden = true
                    
                    
                    
                    //model for fooditem to send data into database
                    var FoodItemdetail:FoodItemInfo  = FoodItemInfo()
                    
                    //menuid
                    FoodItemdetail.strMenuId = String(describing: self.arrFoodMenuId.object(at: (indexPath1 as NSIndexPath).row) as! NSNumber)
                    
                    //cehck if discount is applied than deslect it
                    
                    if(arrDiscountsDetails != nil)
                    {
                    
                    for i in 0...self.arrDiscountsDetails.count - 1
                    {
                        let indexPath1 = IndexPath(row: i, section: 0)
                        MyRestcells = self.tblDiscount!.cellForRow(at: indexPath1) as! MyResListCell
                        MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                        arrSelDiscounts.removeAllObjects()

                    }
                    }
                    
                    let isDeleted = ModelManager.getInstance().deleteStudentData(FoodItemdetail)
                    
                    if isDeleted {
                        //Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
                        
                        //make badges 0 when it willl delete from the cart
                        
                        arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
                        
                        
                        //update badges
                        
                        if(arrFoodItemDetail.count > 0 )
                        {
                            arrQty.removeAllObjects()
                            
                            
                            for i in 0...arrFoodItemDetail.count - 1
                            {
                                
                                FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                                
                                
                                arrQty.add(FoodItemdetail.streQuantity)
                            }
                            
                            
                            
                            var sum : Int = 0
                            
                            for i in 0...arrQty.count - 1
                            {
                                sum = sum + Int(arrQty[i] as! String)!
                            }
                            
                            print("sum",sum)
                            
                            BadgeCount = sum
                            
                            
                        }
                        else
                        {
                            BadgeCount = 0
                            arrAddToCartItems.removeAllObjects()
                            
                            
                        }
                        
                        

                        
                        
                    } else {
                        //Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
                    }
                    
                    
                   
                        self.navigationItem.rightBarButtonItem!.badgeValue = String(BadgeCount); //your value
                    

                }
                    
                    
                    
                else {
                
                    arrSelectedItem.add((indexPath1 as NSIndexPath).row)
                    arrAddToCartItems.add(self.arrFoodMenuId.object(at: (indexPath1 as NSIndexPath).row) as! NSNumber)
                    print("hello 12 " ,arrAddToCartItems)

                    Ccell.viewSelected.isHidden = false
                    
                    BadgeCount = BadgeCount + 1
                    
                    self.navigationItem.rightBarButtonItem!.badgeValue = String( BadgeCount); //your value
                    
                    
                    if(arrFoodItemDetail.count > 0)
                    {
                        
                        for i in 0...arrFoodItemDetail.count - 1
                        {
                            FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                            
                            
                            
                            arrMenuId.add(FoodItemdetail.strMenuId)
                            
                            print(arrMenuId)
                            
                            
                            
                        }
                    }
                    
                //model for fooditem to send data into database
                 FoodItemdetail  = FoodItemInfo()
                
                //menuid
                FoodItemdetail.strMenuId = String(describing: self.arrFoodMenuId.object(at: (indexPath1 as NSIndexPath).row) as! NSNumber)
                
                //menuname
                FoodItemdetail.strMenuName = self.arrFoodName.object(at: (indexPath1 as NSIndexPath).row) as! String
                
                //menucomment
                FoodItemdetail.strComment = ""
                
                //menuunitprice
                FoodItemdetail.strUnitPrice = self.arrFoodPrice.object(at: (indexPath1 as NSIndexPath).row) as! String
                
                //menuquanitty
                
                FoodItemdetail.streQuantity = "1"
                
                
                // totalprice
                 
                let price:Double = Double(self.arrFoodPrice.object(at: (indexPath1 as NSIndexPath).row) as! String)!

                    
                FoodItemdetail.strTotalPrice = self.arrFoodPrice.object(at: (indexPath1 as NSIndexPath).row) as! String
                
                //restName
                
                FoodItemdetail.strRestName = restName
                
                //Badge
                
                FoodItemdetail.strBadge = String( BadgeCount)
                
                //restId
                FoodItemdetail.strRestId = strRestId
                
               //dicount
                    
  
                    
                if(strDiscuont.characters.count > 0)
                {
                    FoodItemdetail.strFoodDiscount = strDiscuont

                    
                    if(strDiscuont.contains("%"))
                    {
                            strDiscuont = strDiscuont.replacingOccurrences(of: "%", with: "")
                        
                            let disc:Double = Double(strDiscuont)!
                            
                            FoodItemdetail.strTotalPrice = String( (price  * 1) - (price * disc)/100)
                            
                        }
                        else
                        {
                            
                            let disc:Double = Double(strDiscuont)!
                            FoodItemdetail.strTotalPrice = String( (price  * 1) - disc)
                            
                        }
                    }
                    else
                {
                    FoodItemdetail.strFoodDiscount = ""

                }
                
                let isInserted = ModelManager.getInstance().addStudentData(FoodItemdetail)
                if isInserted {
                    
                    
                    strDiscuont = ""
                    strDiscuontForFood = ""
                    FoodItemdetail.strFoodDiscount = ""
                    FoodItemdetail  = FoodItemInfo()
                    arrFoodItemDetail = NSMutableArray()
                    arrFoodItemDetail = ModelManager.getInstance().getAllStudentData()
                    
                    if(arrFoodItemDetail.count > 0 )
                    {
                        arrQty.removeAllObjects()
                        
                        
                        for i in 0...arrFoodItemDetail.count - 1
                        {
                            
                            FoodItemdetail = arrFoodItemDetail.object(at: i) as! FoodItemInfo
                            
                            
                            arrQty.add(FoodItemdetail.streQuantity)
                        }
                        
                        
                        
                        var sum : Int = 0
                        
                        for i in 0...arrQty.count - 1
                        {
                            sum = sum + Int(arrQty[i] as! String)!
                        }
                        
                        print("sum",sum)
                        
                        BadgeCount = sum
                        
                        
                    }
                    else
                    {
                        BadgeCount = 0
                        arrAddToCartItems.removeAllObjects()
                        
                        
                    }
                    
                    
                    self.navigationItem.rightBarButtonItem!.badgeValue = String(BadgeCount); //your value


                } else {
                    //Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
                }
                
                // print(arrAddToCartItems)
                
                
                }
                
                
            }
            
        
        
        
        
        
        
    }
    
    func btnDiscountSelected(_ sender:UIButton!)
    {
        
        
        arrDiscountsDetails =  self.arrDiscounts.object(at: sender.tag) as! NSArray
        viewForDiscount.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        viewForDiscount.isHidden = false
        self.tblDiscount?.dataSource = self
        self.tblDiscount?.delegate = self
        self.tblDiscount?.reloadData()

        var height: CGFloat = 70
        height *= CGFloat(arrDiscountsDetails.count as Int)
        var tableFrame = self.tblDiscount!.frame
        tableFrame.size.height = height
        self.tblDiscount!.frame = tableFrame
        
        
    }
    func btnSelectOffers(_ sender:UIButton!)
    {
        
        tblrow =  sender.tag
        
        
            //this is for one radio button to select and dselect discount
            
            if(arrSelDiscounts.contains((arrDiscountsDetails.value(forKey: "DiscountId") as AnyObject).object(at:tblrow)))
            {
                arrSelDiscounts.remove((arrDiscountsDetails.value(forKey: "DiscountId") as AnyObject).object(at:tblrow))
                
                for i in 0...self.arrDiscountsDetails.count-1
                {
                 let indexPath1 = IndexPath(row: i, section: 0)
                 print(indexPath1)
                 MyRestcells = self.tblDiscount!.cellForRow(at: indexPath1) as! MyResListCell
                 MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                    strDiscuont = ""
                }
                
                
                
                
                
                


            }
            else
            {
                //this is to check wether it is there or not
                
                for i in 0...self.arrDiscountsDetails.count-1
                {
                    let indexPath1 = IndexPath(row: i, section: 0)
                    print(indexPath1)
                    MyRestcells = self.tblDiscount!.cellForRow(at: indexPath1) as! MyResListCell
                    MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
                    
                    arrSelDiscounts.removeAllObjects()

                }
                
                self.arrSelDiscounts.add ((self.arrDiscountsDetails.value(forKey: "DiscountId") as AnyObject).object(at:tblrow))
                
                let indexPath1 = IndexPath(row: tblrow, section: 0)
                MyRestcells = self.tblDiscount!.cellForRow(at: indexPath1) as! MyResListCell
                MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
                
                
                if(((self.arrDiscountsDetails.value(forKey: "IsPercentage") as! NSArray).object(at:tblrow)as! Int) ==  1)
                {
                    strDiscuont =  ((self.arrDiscountsDetails.value(forKey: "DiscountPercentage") as! NSArray).object(at:tblrow) as! String) + "%"
                }
                else
                {
                    strDiscuont =  ((arrDiscountsDetails.value(forKey: "DiscountAmount") as! NSArray).object(at: tblrow ) as! String)
                }



            }
            
        print(strDiscuont)

           /* let indexPath1 = IndexPath(row: i, section: 0)
            print(indexPath1)
            MyRestcells = self.tblDiscount!.cellForRow(at: indexPath1) as! MyResListCell
            
            if (MyRestcells.btnRadioBtn.currentImage?.isEqual(UIImage(named: "Radio-Button-Disable.png")))!
            {
                MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())

            }
            else
            {
                MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())

            }*/
            
        
        
        
    }
    
    func btnFavItemSelected(_ sender:UIButton!)
    {
        
        strMenuId =  String(describing: self.arrFoodMenuId.object(at: sender.tag)  as! NSNumber)
        
        if InternetConnection.isConnectedToNetwork()
        {
            self.startIndicatorCircleAnimating()
            
            makeRestaurantFavUnFav() { responseObject, error in
                
                if (responseObject != nil)
                {
                    
                let json = JSON(responseObject!)
                if let string = json.rawString()
                {
                    
                    
                    if let data = string.data(using: String.Encoding.utf8)
                    {
                        do {
                            
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            print("json ",json)
                            if(json!["Success"] as? String  == "1")
                            {
                                let arr:NSMutableDictionary = json!["Data"] as! NSMutableDictionary
                                let indexPath1 = IndexPath(row: sender.tag, section: 0)
                                self.Ccell = self.foodItemCollectionView?.cellForItem(at: indexPath1) as! CollectionViewCell!
                                if(arr.value(forKey: "Status") as? Bool == false)
                                {
                                    let transition = CATransition()
                                    transition.duration = 0.2
                                    transition.type = kCATransitionPush
                                    transition.subtype = kCATransitionFromTop
                                    self.Ccell.btnFavItem.layer.add(transition, forKey: kCATransition)
                                    self.Ccell.btnFavItem.setImage(UIImage.init(named: "Heart"), for: UIControlState())
                                }
                                else
                                {
                                    let transition = CATransition()
                                    transition.duration = 0.2
                                    transition.type = kCATransitionPush
                                    transition.subtype = kCATransitionFromBottom
                                    self.Ccell.btnFavItem.layer.add(transition, forKey: kCATransition)
                                    self.Ccell.btnFavItem.setImage(UIImage.init(named: "ActiveHeart"), for: UIControlState())
                                }
                                
                                
                            }
                            
                        }
                            
                        catch {
                            print("Something went wrong")
                        }
                    }
                }
                }
                else
                {
                    self.stopActivityAnimating()
                    self.AlertMethod()
                    
                }
                
            }
        }
        
        
    }
    
    // MARK: - SET FAVOURITE FOOD ITEM  WEB SERVICE
    
    func makeRestaurantFavUnFav(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        //let size = CGSize(width: 80, height:80)
        
        //startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        // let strRestaurantId = String(self.dictRestDetails.valueForKey("RestaurantId")!)
        
        strWSURL=GETWSURL()
        let param1 : [ String : AnyObject] = [
            
            "CustomerId": UserDefaults.standard.value(forKey: "USERID")! as AnyObject,
             "RestaurantId":strRestId as AnyObject,
            "MenuId": strMenuId as AnyObject

            
        ]
       makeCall(section: strURL as String + "FavouriteMenu?",param:param1,completionHandler: completionHandler)
    }
    func getMenuList(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        let size = CGSize(width: 80, height:80)
        
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        
        strWSURL=GETWSURL()
        let param1 : [ String : AnyObject] = [
            
            "CustomerId":9139 /* UserDefaults.standard.value(forKey: "USERID")!*/ as AnyObject,
            "restaurantid":strRestId as AnyObject
        ]
       makeCall(section: strURL as String + "GetMenuListByRestaurant?",param:param1,completionHandler: completionHandler)
    }
    
    // MARK: VIEWPAGE DATASOURCE
    
    func numberOfItems(_ viewPager:ViewPager) -> Int {
        
        if(arrRestImages.count == 0)
        {
            
            return 1;
            
        }
        else
        {
            return arrRestImages.count;
        }
        
        return 0
        
    }
    
    func viewAtIndex(_ viewPager:ViewPager, index:Int, view:UIView?) -> UIView {
        var newView = UIImageView();
        if(newView.image == nil){
            newView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height:  self.view.frame.height))
            
            
            
            if(arrRestImages.count == 0)
            {
                newView.image = UIImage(named:"DefaultRestaurantImage-3.jpg")
                
            }
            else
            {
                let restId:String = UserDefaults.standard.value(forKey: "RESTID") as! String
                print(restId)
                
                let url = URL(string: "http://foodies4you.intransure.com/Media/Restaurant/" + restId + "/" + ((arrRestImages [index] as AnyObject).value(forKey: "Name") as! String))
                print("url",url)
                
                
                if(url ==  nil)
                {
                    
                    newView.image = UIImage(named:"Food_PlaceHolder.png")
                    
                    
                }
                else
                {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check

                    newView.image  = UIImage(data: data!)
                }
              
                
            }
            
        }
        
        return newView
    }
    
    func didSelectedItem(_ index: Int) {
        print("select index \(index)")
    }
    
    // MARK: - TABLEVIEW DATASOURCE AND DELEGATES

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return arrDiscountsDetails.count
       
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        MyRestcells = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)  as! MyResListCell
        
        MyRestcells.lblTitle.text = (arrDiscountsDetails.value(forKey: "Name") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
        
        
         if(((arrDiscountsDetails.value(forKey: "IsPercentage") as AnyObject).object(at: (indexPath as NSIndexPath).row) as! Int) ==  1)
         {
           MyRestcells.lblCouponCode.text =  ((arrDiscountsDetails.value(forKey: "DiscountPercentage") as AnyObject).object(at: (indexPath as NSIndexPath).row) as! String) + " % OFF "
         }
        else
         {
            MyRestcells.lblCouponCode.text =  ((arrDiscountsDetails.value(forKey: "DiscountAmount") as AnyObject).object(at: (indexPath as NSIndexPath).row) as! String) + " $ OFF "
          }
        
          MyRestcells.lblValidDate.text = " Valid up to " + ((arrDiscountsDetails.value(forKey: "EndDate") as AnyObject).object(at: (indexPath as NSIndexPath).row) as! String)

        
        if(arrSelDiscounts.contains((arrDiscountsDetails.value(forKey: "DiscountId") as AnyObject).object(at:(indexPath as NSIndexPath).row) as! NSNumber))
        {
            
                MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())
                
        }
        else
        {
                MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())
        }

        MyRestcells.btnSelectCoupon.tag = (indexPath as NSIndexPath).row
        MyRestcells.btnSelectCoupon.addTarget(self, action: #selector(self.btnSelectOffers(_:)), for: UIControlEvents.touchUpInside)

        
        return MyRestcells
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 70
    }
    
    @objc(tableView:didSelectRowAtIndexPath:) func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        tblrow =  (indexPath as NSIndexPath).row
        
        for i in 0...self.arrDiscountsDetails.count-1
        {
            let indexPath1 = IndexPath(row: i, section: 0)
            print(indexPath1)
            MyRestcells = self.tblDiscount!.cellForRow(at: indexPath1) as! MyResListCell
            MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Disable.png"), for: UIControlState())

        }
        
        let indexPath1 = IndexPath(row: tblrow, section: 0)
        MyRestcells = self.tblDiscount!.cellForRow(at: indexPath1) as! MyResListCell
        MyRestcells.btnRadioBtn.setImage(UIImage(named: "Radio-Button-Active.png"), for: UIControlState())

        
        
    }
    
    // MARK: -DATABASE TO STORE VALUE INTO CART

    func dbDeletefromItemIntoCart()   {
        
        if(self.arrDiscountsDetails != nil)
        {
        
            arrSelDiscounts.removeAllObjects()

        
        }
        
        let isDeleted = ModelManager.getInstance().deleteAllStudentData(FoodItemdetail)
        
        if isDeleted {
            
            BadgeCount = 0
            self.navigationItem.rightBarButtonItem!.badgeValue = String(BadgeCount);
            //Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
            //Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
    }
    
    
    
    
}


