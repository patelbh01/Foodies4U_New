

//
//  mobileNumViewController.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 01/07/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import UIKit
import PhoneNumberKit
import SpriteKit
import SwiftyJSON
import NVActivityIndicatorView
import Alamofire
import GoogleMaps


fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}




let kStringConstant:String = "monitoredRegions"


struct Constants
{
    static let someNotification = kStringConstant
}

public enum AlertType: Int {
    case alert
    case confirmation
    case multipleChoice
}
extension SKNode {
}

class mobileNumViewController: parentViewController, UITextFieldDelegate, countrySelectedDataDelegate,NVActivityIndicatorViewable,CLLocationManagerDelegate {
    
    weak var delegate: countrySelectedDataDelegate? = nil
    
    let phoneNumberKit = PhoneNumberKit()
    var strCountrycode : String!
    var strWSURL: NSString!
    var strUserID: NSString!
    var strMNumber: NSString!
    var strOTP: NSString!
    var strmnumber:String!
    
    var locManager = CLLocationManager()

    
    @IBOutlet var txtCountry : UITextField!
    @IBOutlet weak var txtMblNumber: PhoneNumberTextField!
    @IBOutlet weak var imgviewLogo: UIImageView!
    @IBOutlet weak var imgMobile: UIImageView!
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet var btnContinue : UIButton!
    let button = UIButton(type: UIButtonType.custom)
    
    
    
    
    
    
    //MARK: ViewLife Cycle Methods
    
    override func viewDidLoad()
    {
        
        
        //OR next possibility
        
        super.viewDidLoad()
        
        // moving backgound image
        
        /* let skView = self.view as! SKView
         let myScene = GameScene(size: skView.frame.size)
         skView.presentScene(myScene)*/
        
        
        
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        //dismis keyboard any where you tap on the screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        lblInfo.isHidden=true
        txtMblNumber.isHidden=true
        txtCountry.isHidden=true
        btnContinue.isHidden=true
        imgMobile.isHidden=true
        imgCountry.isHidden=true
        self.customanimation()
        
        
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locManager = CLLocationManager()
            locManager.delegate = self
            locManager.desiredAccuracy = kCLLocationAccuracyBest
            locManager.requestAlwaysAuthorization()
            locManager.startUpdatingLocation()
        }
            
        
        //get country code from device
        let currentLocale = Locale.current
        let countryCode = (currentLocale as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String
        
        var arrTemp: NSArray!
        var arrFilter: NSArray!
        
        arrTemp = DBManager.getSharedInstance().getAllCountryDialCodeList()
        
        // fetch country dial code according to country code
        let predicate = NSPredicate(format:"countryCode ==[c] '\(countryCode!)'")
        arrFilter = arrTemp.filtered(using: predicate) as NSArray!
        
        if arrFilter.count > 0
        {
            strCountrycode = "+" + ((arrFilter.object(at: 0) as AnyObject).value(forKey: "countryDialCode") as!  String)
            txtCountry.text = (arrFilter.object(at: 0) as AnyObject).value(forKey: "countryName") as?  String
            txtMblNumber.text = "+" + ((arrFilter.object(at: 0) as AnyObject).value(forKey: "countryDialCode") as!  String)
        }
        
        // for number pad
        
        let numberToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.blackTranslucent
        numberToolbar.barTintColor = GlobalConstants.Theme_Color_Seperator
        
        
        numberToolbar.items = [UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(mobileNumViewController.cancelNumberPad)),
                               UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                               UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(mobileNumViewController.doneWithNumberPad))]
        UIBarButtonItem.appearance().tintColor = UIColor.white
        numberToolbar.sizeToFit()
        txtMblNumber.inputAccessoryView = numberToolbar
    }
    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        self.navigationController?.isNavigationBarHidden = true;
        
        btnContinue.layer.cornerRadius = 5.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: GET USER LOCATION

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        let location = locations.last! as CLLocation
        
        UserDefaults.standard.set(location.coordinate.latitude, forKey: "LATITUDE")
        UserDefaults.standard.set(location.coordinate.longitude, forKey: "LONGITUDE")
        
      //  print("LOCARION",UserDefaults.standard.double(forKey: "LATITUDE"))

    }
    
    //MARK: ANIMATIONS METHOD
    
    
    func customanimation()
    {
        CATransaction.begin()
        
        CATransaction.setAnimationDuration(1.5)
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        
        imgviewLogo.layer.add(transition, forKey: kCATransition)
        
        self.perform(#selector(self.formatTextfield(_:)), with: txtCountry, afterDelay: 0.1)
        self.perform(#selector(self.formatTextfield(_:)), with: txtMblNumber, afterDelay: 0.1)
        
        CATransaction.commit()
        
        CATransaction.setCompletionBlock
            {
                CATransaction.begin()
                
                CATransaction.setAnimationDuration(1.5)
                let transition1 = CATransition()
                transition1.type = kCATransitionPush
                transition1.subtype = kCATransitionFromLeft
                
                self.lblInfo.layer.add(transition1, forKey: kCATransition)
                self.lblInfo.isHidden=false
                
                let transition2 = CATransition()
                transition2.type = kCATransitionMoveIn
                transition2.subtype = kCATransitionFromBottom
                
                self.txtMblNumber.layer.add(transition2, forKey: kCATransition)
                self.txtCountry.layer.add(transition2, forKey: kCATransition)
                self.btnContinue.layer.add(transition2, forKey: kCATransition)
                
                self.txtMblNumber.isHidden=false
                self.txtCountry.isHidden=false
                self.btnContinue.isHidden=false
                self.imgCountry.isHidden=false
                self.imgMobile.isHidden=false
                
                
                
                CATransaction.commit()
                
                
        }
        
        
    }
    //MARK: WS-For Mobile Verification
    
    
    func getOrders(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        let size = CGSize(width: 80, height:80)
        
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        strWSURL=GETWSURL()
        let str = UserDefaults.standard.object(forKey: "DEVICETOEKN")
        
        /* var trimmed = txtMblNumber.text!.stringByReplacingOccurrencesOfString(" ",
         withString: "", options: .RegularExpressionSearch)
         trimmed = trimmed.stringByReplacingOccurrencesOfString("-",
         withString: "", options: .RegularExpressionSearch)
         trimmed = trimmed.stringByReplacingOccurrencesOfString("(",withString: "", options: .RegularExpressionSearch)
         trimmed = trimmed.stringByReplacingOccurrencesOfString(")", withString: "", options: .RegularExpressionSearch)
         print(trimmed)*/
        let param1 : [ String : AnyObject] =
            [
                "MobileNumber":(strmnumber as? NSString)!,
                "DeviceToken":("strmnumber" as? NSString)!//(str as? NSString)!
        ]
       makeCall(section: strURL as String + "Registration",param:param1,completionHandler: completionHandler)
        
        
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    
    func userSelectedCountry(_ arrData: NSDictionary)
    {
        print("\(arrData)")
        
        strCountrycode = "+" + (arrData.value(forKey: "countryDialCode") as!  String)
        txtCountry.text = arrData.value(forKey: "countryName") as?  String
        txtMblNumber.text = "+" + (arrData.value(forKey: "countryDialCode") as!  String)
        txtMblNumber.becomeFirstResponder()
        
        
        if DeviceType.IS_IPHONE_4_OR_LESS
        {
            txtMblNumber.resignFirstResponder()
            
        }
        
        //self.performSelector(#selector(textfieldBeginRespond), withObject: nil, afterDelay: 0.5)
    }
    
    func textfieldBeginRespond()
    {
        txtMblNumber.becomeFirstResponder()
    }
    
    func removeSpecialCharsFromString(_ text: String) -> String {
        let okayChars : Set<Character> =
            Set("1234567890+".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }

    //MARK: COUTNRY BUTTON CLICK
    @IBAction func btnCountrySelectionClicked(_ sender: UIButton){
        
        txtMblNumber.resignFirstResponder()
        self.performSegue(withIdentifier: "countrySegue", sender: self)
    }
    
    @IBAction func btnContinueClicked(_ sender: UIButton)
    {
        view.endEditing(true)
        
        if InternetConnection.isConnectedToNetwork()
        {
            
            if txtMblNumber.text?.characters.count > strCountrycode.characters.count
            {
                strmnumber = removeSpecialCharsFromString((txtMblNumber.text)!)
                let size = CGSize(width: 80, height:80)
                
                startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
                strWSURL=GETWSURL()
                
                
                
                
                
                getOrders() { responseObject, error in
                    
                    if (responseObject != nil)
                    {
                                           let json = JSON(responseObject!)
                    if let string = json.rawString()
                    {
                        //Do something you want
                        print(string)
                        
                        self.stopActivityAnimating()
                        
                        let encodedString : Data = (string as NSString).data(using: String.Encoding.utf8.rawValue)!
                        let finalJSON = JSON(data: encodedString)
                        
                        //print("\(finalJSON)")
                        // print("\(finalJSON["Id"])")
                        let name = finalJSON["Success"]
                        
                        if name == "1"
                        {
                            self.strUserID = finalJSON["Data"]["Id"].stringValue as NSString!
                            self.strMNumber = finalJSON["Data"]["PhoneNumber"].stringValue as NSString!
                            self.strOTP = finalJSON["OTP"].stringValue as NSString!
                            
                            
                            UserDefaults.standard.setValue(self.strUserID, forKey: "USERID")
                            UserDefaults.standard.setValue(self.txtMblNumber.text, forKey: "MNUMBER")
                            UserDefaults.standard.setValue(self.strOTP, forKey: "OTP")
                            UserDefaults.standard.synchronize()
                            self.performSegue(withIdentifier: "verificationSegue", sender: self)
                        }
                        else if name == "2"
                        {
                            self.strUserID = finalJSON["Data"]["Id"].stringValue as NSString!
                            self.strMNumber = finalJSON["Data"]["PhoneNumber"].stringValue as NSString!
                            self.strOTP = finalJSON["OTP"].stringValue as NSString!
                            
                            UserDefaults.standard.setValue(self.strUserID, forKey: "USERID")
                            UserDefaults.standard.setValue(self.txtMblNumber.text, forKey: "MNUMBER")
                            UserDefaults.standard.setValue(self.strOTP, forKey: "OTP")
                            UserDefaults.standard.synchronize()
                            if finalJSON["Data"]["status"].stringValue == "Mobile number already registered"
                            {
                               
                                UserDefaults.standard.setValue("yes", forKey: "ALREADYREGISTRED")

                              self.performSegue(withIdentifier: "verificationSegue", sender: self)
                              
                            }
                        }
                        else  if name == "-1"
                        {
                            
                           

                                
                            let dialog = ZAlertView(title: "Foodies4u",
                                                    message: GlobalConstants.Str_Valid_MNumber,
                                                    closeButtonText: "Ok",
                                                    closeButtonHandler: {  (alertView) -> () in
                                                        
                                                        alertView.dismissAlertView()

                                }
                            )
                            dialog.show()
                            dialog.allowTouchOutsideToDismiss = true
                                
                                
                           
                        }
                        
                    }
                    }
                    else
                    {
                        self.stopActivityAnimating()
                        self.AlertMethod()
                        
                    }
                    
                    
                }
                
                
            }
            else
            {
                
                let dialog = ZAlertView(title: "Foodies4u",
                                        message:GlobalConstants.Str_Valid_MNumber,
                                        closeButtonText: "Ok",
                                        closeButtonHandler: { (alertView) -> () in
                                            alertView.dismissAlertView()

                    }
                )
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
                
            }
        }
        //self.performSegueWithIdentifier("verificationSegue", sender: self)
    }
    
    //MARK: PREPARE FOR SEGUE
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "countrySegue" {
            let countryDialCodeVC = segue.destination as! countryDialCodeViewController
            countryDialCodeVC.delegate = self
        }
    }
    
    //MARK: numberpad methods
    
    
    func cancelNumberPad() {
        
        //  animateViewMoving(false, moveValue: 200)
        
        txtMblNumber.resignFirstResponder()
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        //txtMblNumber.text = ""
    }
    
    func doneWithNumberPad() {
        
        txtMblNumber.resignFirstResponder()
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
    }
    override func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    //MARK: textfield delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
        if DeviceType.IS_IPHONE_4_OR_LESS
        {
         self.view.frame = CGRect(x: 0, y: -120, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        
        //animateViewMoving(true, moveValue: 200)
        
        
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let txtCount : Int = (textField.text?.characters.count)!
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            if txtCount <= strCountrycode.characters.count {
                return false
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        
        if DeviceType.IS_IPHONE_4_OR_LESS
        {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        
        
        //animateViewMoving(false, moveValue: 200)
        /*if textField == txtMblNumber {
         self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
         }*/
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /*func keyboardWillShow(note : NSNotification) -> Void{
     
     dispatch_async(dispatch_get_main_queue()) { () -> Void in
     
     self.button.hidden = false
     let keyBoardWindow = UIApplication.sharedApplication().windows.last
     self.button.frame = CGRectMake(0, (keyBoardWindow?.frame.size.height)!-53, 106, 53)
     keyBoardWindow?.addSubview(self.button)
     keyBoardWindow?.bringSubviewToFront(self.button)
     
     UIView.animateWithDuration(((note.userInfo! as NSDictionary).objectForKey(UIKeyboardAnimationCurveUserInfoKey)?.doubleValue)!, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
     
     self.view.frame = CGRectOffset(self.view.frame, 0, 0)
     }, completion: { (complete) -> Void in
     print("Complete")
     })
     }
     
     }
     
     func Done(sender : UIButton){
     
     dispatch_async(dispatch_get_main_queue()) { () -> Void in
     
     self.txtMblNumber.resignFirstResponder()
     
     }
     }*/
    
    
    
    
    
    
}
