//
//  ViewController.swift
//  Foodie
//
//  Created by Bhumika Patel on 29/06/16.
//  Copyright © 2016 Bhumika Patel. All rights reserved.
//

import UIKit
import Foundation
import SpriteKit
import SwiftyJSON
import NVActivityIndicatorView
import BraintreeCard
import BraintreeDataCollector
//import SwiftLuhn






class CCDetailViewController:parentViewController, UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,SWRevealViewControllerDelegate,NVActivityIndicatorViewable,UIPickerViewDelegate,UIPickerViewDataSource
    
{
    
    
    //MARK:Variables Declrations
    
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var BD: UIView!
    @IBOutlet var ViewAnimation: UIView!
    @IBOutlet weak var datePickerview :UIView!
    @IBOutlet weak var btnConform :UIButton!
    @IBOutlet weak var btnCancel :UIButton!
    
    let months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
    var years: [Int]!

    var keyboardSize : CGRect!
    var Status:String!
    var myInt = 0
    var myInt1 = 0
    var year=0
    var month=0
    
    var window:UIWindow!
    var cellcustom:Customcell!
    
    
    var strWSURL: NSString!
    var strUserID: NSString!
    var strCardType: NSString!
    var strUSERID: AnyObject!
    var creditCardValidator: CreditCardValidator!
    
    var flag:Bool!
    var isValid:Bool!
    
    var  strCreditcardNumber:NSString!
    var  strCreditcardName:NSString!
    var  strCreditcardExpirydate:NSString!
    var  strCreditcardCvv:NSString!
    
   @IBOutlet weak var expiryDatePicker:UIPickerView!
    var selectedmonth:String!
    var selectedyear:String!
    var cmonth:Int=0
    
    var array:NSArray!
    
    var dialog = ZAlertView()
    var strToken: NSString!
    var strNounce:String!



    
    var someInts:[String] =  ["Credit Card Number", "Name on Card", "Expiry Date(MM/YYYY)","CVV"]
    var arrIcon:[String] =  ["CardIcon-1.png", "ProfileIcon.png", "CalendarIcon","CardIcon-2.png"]
    
    //MARK:LIFECYCLES METHOS
    
    override func viewDidLoad()
    {
        
        // moving backgound image
        
        /* let skView = ViewAnimation as! SKView
         let myScene = GameScene(size: skView.frame.size)
         skView.presentScene(myScene)*/

        datePickerview.isHidden=true
        BD.layer.borderWidth=2.0;
        BD.layer.borderColor=UIColor.init(colorLiteralRed: 48.0/255.0, green: 163.0/255.0, blue: 39.0/255.0, alpha: 1.0).cgColor;
        
        
        getCreditCardToken()


        flag=true
        strCardType=""
        navigationBarUI()
        commonSetup()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title:"Skip", style: .plain, target: self, action: #selector(addTapped))
        navigationItem.rightBarButtonItem!.setTitleTextAttributes([ NSFontAttributeName: UIFont (name: "OpenSans-Bold", size: 14)!], for: UIControlState())
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!
        
        
        //dismis keyboard any where you tap on the screen
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //keyboard appearence notification
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        
        
        let defaults = UserDefaults.standard
        let str = defaults.object(forKey: "SavedCCDetails")
        
        if str ==  nil
        {
            strCreditcardCvv = "";
            strCreditcardName = "";
            strCreditcardExpirydate = "";
            strCreditcardNumber = "";
        }
        else
            
        {
            
            array = defaults.object(forKey: "SavedCCDetails") as? NSArray
            flag=false

            if array.count>0
            {
                tableView .reloadData()
                strCreditcardNumber = array[0] as! NSString
                let string1 = array[1]
                let intValue = array[2]
                let appendString = "\(string1)/\(intValue)"
                strCreditcardExpirydate = appendString as NSString!
                strCreditcardCvv = array[3] as! NSString
                
                strCreditcardName = "";
                
                
                
            }
        }
        
        
        // creditcard validate intializtion
        
        
        creditCardValidator = CreditCardValidator()
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func commonSetup() {
        var years: [Int] = []
        if years.count == 0 {
            var year = (Calendar(identifier: Calendar.Identifier.gregorian) as NSCalendar).component(.year, from: Date())
            for _ in 1...15 {
                years.append(year)
                year += 1
            }
        }
        self.years = years
        
        expiryDatePicker.delegate = self
       expiryDatePicker.dataSource = self
        
        let month = (Calendar(identifier: Calendar.Identifier.gregorian) as NSCalendar).component(.month, from: Date())
        self.expiryDatePicker.selectRow(month-1, inComponent: 0, animated: false)
    }
    
    //MARK:PICKERS DATASOURCES AND DELEGATES METHODS

    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return months[row]
        case 1:
            return "\(years[row])"
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return months.count
        case 1:
            return years.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        selectedmonth = String (pickerView.selectedRow(inComponent: 0)+1)
        selectedyear = String(years[pickerView.selectedRow(inComponent: 1)])
        
        
        
        
        
        
    }
    //MARK:WEBSERVICES METHOD
    
    
    func getOrders(_ completionHandler:  @escaping   (AnyObject?, NSError?) -> ())
    {
        let size = CGSize(width: 80, height:80)
        
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        let session = UserDefaults.standard.value(forKey: "USERID")
        
        strWSURL=GETWSURL()
        
        if  flag==true
        {
            
            let param1 : [ String : AnyObject] = [
                "CustomerId":session! as AnyObject,
                "IsDefault":"true" as AnyObject ,
                "PaymentMethodToken":strNounce as AnyObject
            ]
            makeCall(section: strURL as String + "AddCreditCardDetails?",param:param1,completionHandler: completionHandler)
        }
        else
        {
            
            let param1 : [ String : AnyObject] = [
                "CustomerId":session! as AnyObject,
                "IsDefault":"true" as AnyObject ,
                "PaymentMethodToken":strNounce as AnyObject
            ]
           makeCall(section: strURL as String + "AddCreditCardDetails?",param:param1,completionHandler: completionHandler)
        }
        
        
        
     
        
        
        
        
        
        
        
        
    }
    
    //MARK: TABLEVIEW DATASOURCE AND DELEGATES METHODS
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        
        return 5;
    }
    
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell
    {
        
        cellcustom = tableView.dequeueReusableCell(withIdentifier: "CustomCellOne", for:indexPath) as! Customcell
        //print("array %@",indexPath.row)
        
        cellcustom.lblInfo.isHidden=true;

        if ((indexPath as NSIndexPath).row==4)
        {
            cellcustom.btnRegister.isHidden=false;
            cellcustom.btnBackToScan.isHidden=false;
            cellcustom.txtField.isHidden=true;
            cellcustom.ImgViewicon.isHidden=true;

        }
        else
        {
            cellcustom.btnRegister.isHidden=true;
            cellcustom.txtField.isHidden=false;
            cellcustom.ImgViewicon.isHidden=false;
            cellcustom.btnBackToScan.isHidden=true;
            
            
            
            
            let paddingView = UIView(frame:CGRect(x: 150, y: 0, width: 30, height: 30))
            
            cellcustom.txtField.leftView=paddingView;
            cellcustom.txtField.leftViewMode = UITextFieldViewMode.always
            cellcustom.txtField.tag=(indexPath as NSIndexPath).row
            cellcustom.txtField.delegate=self
            cellcustom.selectionStyle = .none
            
            cellcustom.txtField.attributedPlaceholder = NSAttributedString(string:someInts[(indexPath as NSIndexPath).row],
                                                                           attributes:[NSForegroundColorAttributeName: UIColor.black])
            
            
            cellcustom.txtField.tag=(indexPath as NSIndexPath).row
            cellcustom.ImgViewicon.image=UIImage(named:arrIcon[(indexPath as NSIndexPath).row])
            
            if (indexPath as NSIndexPath).row==1
            {
                cellcustom.txtField.keyboardType = .default
                
            }
            
            let str = UserDefaults.standard.object(forKey: "SavedCCDetails")
            
            if str !=  nil
            {
                if array.count>0
                {
                    if (indexPath as NSIndexPath).row==0
                    {
                        
                        cellcustom.txtField.isUserInteractionEnabled = false
                        //CCcell.selectionStyle = UITableViewCellSelectionStyle.Gray
                        cellcustom.selectionStyle = .none
                        cellcustom.txtField.alpha=0.5
                    }
                    if (indexPath as NSIndexPath).row==2
                    {
                        
                        cellcustom.txtField.isUserInteractionEnabled = false
                        //CCcell.selectionStyle = UITableViewCellSelectionStyle.Gray
                        cellcustom.selectionStyle = .none
                        cellcustom.txtField.alpha=0.5
                    }
                    
                    if cellcustom.txtField.placeholder == "Name on Card"
                    {
                        
                    }
                    else
                        
                    {
                        
                        if (indexPath as NSIndexPath).row == 2
                        {
                            let string1 = array[1]
                            let intValue = array[2]
                            let appendString = "\(string1)/\(intValue)"
                            cellcustom.txtField.text = appendString
                            
                            
                        }
                        else
                            
                        {
                            cellcustom.txtField.text = (array[(indexPath as NSIndexPath).row]) as? String
                            
                        }
                    }
                    
                }
                
            }
            else
                
            {
                
                if (indexPath as NSIndexPath).row==0
                {
                    flag=true
                    
                }
                if( (indexPath as NSIndexPath).row==2 )
                {
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(registrationViewController.handleTap))
                    tap.numberOfTapsRequired = 1
                    cellcustom.addGestureRecognizer(tap)
                    cellcustom.txtField.isUserInteractionEnabled=false
                    
                    
                }
            }
            
            
            if (indexPath as NSIndexPath).row==0
            {
                
                
                cellcustom.lblInfo.isHidden=false
                
                if strCardType == "MasterCard"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "mastercard.png")
                    
                    
                }
                else if strCardType == "Amex"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "amex.png")
                    
                    
                }
                else if strCardType == "Visa"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "visa.png")
                    
                    
                }
                else if strCardType == "Discover"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "discover.png")
                    
                    
                }
                else if strCardType == "Diners Club"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "diners.png")
                    
                    
                }
                else if strCardType == "JCB"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "jcb.png")
                    
                    
                }
                
            }
            else
                
            {
                cellcustom.lblInfo.isHidden=true
                cellcustom.imgCardImage.image=UIImage(named: "")
                
            }
            
            
        }
        
        
        return cellcustom
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func handleTap()
    {

        /*if isValid == true
        {*/
            view.endEditing(true)
            
            datePickerview.isHidden=false
        //}
        

    }
    //MARK:TEXTFIELD DATASOURCE AND DELEGATES METHODS
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        animateViewMoving(true, moveValue: 215)
        
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField.tag==1
        {
            strCreditcardName = textField.text! as NSString!
            print(strCreditcardName)
            
            
        }
        if textField.tag==0
        {
            
            let indexPath1 = IndexPath(row: 0, section: 0)
            cellcustom = tableView.cellForRow(at: indexPath1)! as! Customcell
            
            strCreditcardNumber = textField.text! as NSString!
            var text = strCreditcardNumber as String
            print(strCreditcardNumber)
            
            text = text.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)
            
            
             isValid = text.isValidCardNumber()
            
            
            /*if (isValid == false )
            {*/
                
                //view.frame=CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: view.frame.size.width,  height: view.frame.size.height)
                //view.endEditing(true)
                
                /*flag = false
                
                
               
                            let dialog = ZAlertView(title: "Foodies4u",
                                    message: "Please enter valid card number",
                                    closeButtonText: "Ok",
                                    closeButtonHandler: { (alertView) -> () in
                                        textField.becomeFirstResponder()
                                    alertView.dismissAlertView()

                    }
                )
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
                
               
            }*/
            
        }
        if textField.tag==3
        {
            strCreditcardCvv = textField.text! as NSString!
            print(strCreditcardCvv)
        }
        
        print("bhumika")
        animateViewMoving(false, moveValue: 215)
        print("bhumika1")

    }
    
    
    //MARK: Keyboard hide and unhide methods
    func keyboardWillShow (notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        {
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
            
            tableView.contentInset = contentInsets
            tableView.scrollIndicatorInsets = contentInsets
        }
    }
    
    
    func keyboardWillHide(notification: NSNotification)
    {
        tableView.contentInset = UIEdgeInsets.zero
        tableView.scrollIndicatorInsets = UIEdgeInsets.zero
    }

    
    override  func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        
        
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
   
        let nextTage=textField.tag+1;
        // Try to find next responder
        let nextResponder=textField.superview?.viewWithTag(nextTage) as UIResponder!
        
        if (nextResponder != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        if nextTage==4
        {
            textField.returnKeyType = .done;
            
        }
        return false // We do not want UITextField to insert line-breaks.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
  
        if textField.tag == 0
        {
            
            if range.location==4 || range.location==9 ||
                range.location==14
                
            {
                //add -  after every four characters
                
                let nsString = textField.text! as NSString
                let newString = nsString.replacingCharacters(in: range,
                                                                            with: "-")
                textField.text=newString as String
                
                
                
            }
            //validate the card number
            let indexPath1 = IndexPath(row: 0, section: 0)
            cellcustom = tableView.cellForRow(at: indexPath1)! as! Customcell
            
            if let type = creditCardValidator.type(from: textField.text!)
            {
                strCardType = type.name as NSString!
                
                cellcustom.lblInfo.isHidden=true
                cellcustom.lblInfo.text = ""

                if type.name == "MasterCard"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "mastercard.png")
                    
                    
                }
                else if type.name == "Amex"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "amex.png")
                    
                }
                else if type.name == "Visa"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "visa.png")
                    
                    
                }
                else if type.name == "Discover"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "discover.png")
                    
                    
                }
                else if type.name == "Diners Club"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "diners.png")
                    
                    
                }
                else if type.name == "JCB"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "jcb.png")
                    
                    
                }
                else if type.name == "Maestro"
                {
                    cellcustom.imgCardImage.image=UIImage(named: "Maestro.png")
                    
                    
                }
                
            }
            else
            {
                cellcustom.lblInfo.isHidden=false
                cellcustom.imgCardImage.image=UIImage(named: "")
                cellcustom.lblInfo.text = "invalid card"
                cellcustom.lblInfo.textColor = UIColor.red
            }
            
            //only enter the 16 digits
            
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount+1)
            {
                return false
            }
            
            print(currentCharacterCount)
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 19
        }
        else if textField.tag==1
        {
            
            for chr in string.characters
            {
                if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") && !(chr == " " ))
                {
                    return false
                }
            }
            return true
        }
        else if textField.tag==3
        {
            //only enter the 3 digits
            
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount+1)
            {
                return false
            }
            
            print(currentCharacterCount)
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 3
        }
        
        
        
        return true
    }
    
    
    // Hide Keyboard AnyWhere you Tap
    
    override func dismissKeyboard() {
        view.frame=CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: view.frame.size.width,  height: view.frame.size.height)
        view.endEditing(true)
    }
    
    
    //MARK:BUTTONS METHODS
    
    
    func RegisterCreditCard()
    {
        getOrders() { responseObject, error in
            
            
            if (responseObject != nil)
            {
            let json = JSON(responseObject!)
            if let string = json.rawString()
            {
                //Do something you want
                print(string)
                
                
                self.stopActivityAnimating()
                
                let encodedString : Data = (string as NSString).data(using: String.Encoding.utf8.rawValue)!
                let finalJSON = JSON(data: encodedString)
                
                print("\(finalJSON["Id"])")
                let name = finalJSON["Success"]
                UserDefaults.standard.synchronize()
                if name == "1"
                {
                    self.dialog = ZAlertView(title: "Your Account",
                                             message:GlobalConstants.Str_Success,
                                             closeButtonText: "Ok",
                                             closeButtonHandler: { (alertView) -> () in
                                                alertView.dismissAlertView()
                                                UserDefaults.standard.setValue("YES", forKey: "REGISTERED")
                                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                                
                                                let storyboard: UIStoryboard = UIStoryboard(name: "MainSW", bundle: nil)
                                                
                                                let frontViewController =  storyboard.instantiateViewController(withIdentifier: "HomeViewController")
                                                let rearViewController=storyboard.instantiateViewController(withIdentifier: "MenuController")
                                                
                                                let rearrightViewController=storyboard.instantiateViewController(withIdentifier: "RightMenuController")
                                                let frontNavigationController: UINavigationController = UINavigationController(rootViewController: frontViewController)
                                                let rearNavigationController: UINavigationController = UINavigationController(rootViewController: rearViewController)
                                                
                                                let rearrightNavigationController: UINavigationController = UINavigationController(rootViewController: rearrightViewController)
                                                
                                                let mainRevealController: SWRevealViewController = SWRevealViewController( rearViewController: rearNavigationController, frontViewController: frontNavigationController)
                                                
                                                mainRevealController.rightViewController = rearrightNavigationController
                                                
                                                mainRevealController.delegate = self
                                                rearNavigationController.isNavigationBarHidden = true
                                                appDelegate.window!.rootViewController = mainRevealController
                                                appDelegate.window!.makeKeyAndVisible()
                        }
                    )
                    self.dialog.show()
                    self.dialog.allowTouchOutsideToDismiss = true
                    
                    
                }
                
                
            }
            }
            else
            {
                self.stopActivityAnimating()

                self.AlertMethod()
            
        }
    }
    }
    
    
    func getCreditCardToken()   {
        
        getCreditCardTokenFromServer() { responseObject, error in
            
            if (responseObject != nil)
            {
                let json = JSON(responseObject!)
            if let string = json.rawString()
            {
                let encodedString : Data = (string as NSString).data(using: String.Encoding.utf8.rawValue)!
                let finalJSON = JSON(data: encodedString)
                
                
                let name = finalJSON["Success"]
                
                if name == "1"
                {
                    self.strToken = finalJSON["Data"].stringValue as NSString!
                    print(self.strToken)
                    
                }
                else
                {
                    
                }
            }
            }
                
        else
            {
                self.stopActivityAnimating()

                self.AlertMethod()
                }
        
    }
    }
    
    func getCreditCardTokenFromServer(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        
        strWSURL=GETWSURL()
        
        
        let param1 : [ String : AnyObject] =
            [
                
                :]
        makeCall(section: strWSURL as String + "Client_Token",param:param1,completionHandler: completionHandler)
        
        
    }
    
    func addTapped()
    {
        UserDefaults.standard.setValue("YES", forKey: "REGISTERED")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let storyboard: UIStoryboard = UIStoryboard(name: "MainSW", bundle: nil)
        
        let frontViewController =  storyboard.instantiateViewController(withIdentifier: "HomeViewController")
        let rearViewController=storyboard.instantiateViewController(withIdentifier: "MenuController")
        
        let rearrightViewController=storyboard.instantiateViewController(withIdentifier: "RightMenuController")
        let frontNavigationController: UINavigationController = UINavigationController(rootViewController: frontViewController)
        let rearNavigationController: UINavigationController = UINavigationController(rootViewController: rearViewController)
        
        let rearrightNavigationController: UINavigationController = UINavigationController(rootViewController: rearrightViewController)
        
        let mainRevealController: SWRevealViewController = SWRevealViewController( rearViewController: rearNavigationController, frontViewController: frontNavigationController)
        
        mainRevealController.rightViewController = rearrightNavigationController
        
        mainRevealController.delegate = self
        rearNavigationController.isNavigationBarHidden = true
        appDelegate.window!.rootViewController = mainRevealController
        appDelegate.window!.makeKeyAndVisible()
        
    }
    
    @IBAction func btnConfromPressed(_ sender: UIButton!)
    {
        datePickerview.isHidden=true
      
        let indexPath1 = IndexPath(row: 2, section: 0)
        cellcustom = tableView.cellForRow(at: indexPath1)! as! Customcell
        cellcustom.txtField.text = selectedmonth+"/"+selectedyear
        strCreditcardExpirydate=cellcustom.txtField.text as NSString!
        
        let date1 = Date()
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.day , .month , .year], from: date1)
        
        year =  components.year!
        month = components.month!
        cmonth=Int(month)

         if (Int(selectedyear) == Int(year) )
        {
            if !(cmonth...12  ~= Int(selectedmonth)!)
            {
                 let dialog = ZAlertView(title: "Foodies4u",
                                    message:GlobalConstants.Str_Valid_ExpiryDate,
                                    closeButtonText: "Ok",
                                    closeButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                    }
                )
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
                
                datePickerview.isHidden=false
            }
        }
    }
   
    func playSpecificBeepNoiseWithDelayOf(_ aNumber:  Int, _ filename: Int)
    {
        
    }
    
    @IBAction func btncancelPressed(_ sender: UIButton!)
    {
        datePickerview.isHidden=true

    }
    
    @IBAction func btnRegisterPressed(_ sender: UIButton!)
    {
    
        view.frame=CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: view.frame.size.width,  height: view.frame.size.height)
        view.endEditing(true)
        var text = strCreditcardNumber as String
        text = text.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)
        let isValid = text.isValidCardNumber()
        let creditcardexpiredate=strCreditcardExpirydate as String
        
        if(strCreditcardExpirydate.length>0)
        
        {
            
            let date1 = Date()
            let calendar = Calendar.current
            let components = (calendar as NSCalendar).components([.day , .month , .year], from: date1)
            
            year =  components.year!
            month = components.month!
            cmonth=Int(month)
        }
        
        if  (strCreditcardNumber.length==0)
        {
            dialog = ZAlertView(title: "Foodies4u",
                                message:GlobalConstants.Str_CardNumber,
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
            
        else if  (strCreditcardName.length==0)
        {
            dialog = ZAlertView(title: "Foodies4u",
                                message: GlobalConstants.Str_Valid_CardNumber,
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
        else if  (strCreditcardExpirydate.length==0)
        {
            dialog = ZAlertView(title: "Foodies4u",
                                message: GlobalConstants.Str_Valid_ExpiryDate,
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
        else if  (strCreditcardCvv.length==0)
        {
            dialog = ZAlertView(title: "Foodies4u",
                                message: GlobalConstants.Str_Valid_CVV,
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
        }
        else if(isValid == false)
            
        {
            
            dialog = ZAlertView(title: "Foodies4u",
                                message: "Please enter valid card number",
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                }
            )
            dialog.show()
            dialog.allowTouchOutsideToDismiss = true
            
        }
        else if  flag==true
        {
            
         if (Int(selectedyear) == Int(year) )
        {
            if !(cmonth...12  ~= Int(selectedmonth)!)
            {
                dialog = ZAlertView(title: "Foodies4u",
                                    message: GlobalConstants.Str_Valid_ExpiryDate,
                                    closeButtonText: "Ok",
                                    closeButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                    }
                )
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
            }
         
            else
            {
                if InternetConnection.isConnectedToNetwork()
                {
                    if(strToken != nil)
                    {
                    let braintreeClient = BTAPIClient(authorization: strToken as String )!
                    
                     let cardClient  = BTCardClient(apiClient: braintreeClient)
                    
                        let card = BTCard(number: strCreditcardNumber as String, expirationMonth: selectedmonth, expirationYear: selectedyear, cvv: nil)
                        
                        cardClient.tokenizeCard(card) { (tokenizedCard, error) in
                            // Communicate the tokenizedCard.nonce to your server, or handle error
                        print("nonce" ,tokenizedCard?.nonce)
                            
                            
                        self.strNounce = tokenizedCard!.nonce
                            print(error)
                            self.RegisterCreditCard()
                        }
                    
                    
                    
                    
                }
                }
                
            }
            
            
        }
         else
         {
            if InternetConnection.isConnectedToNetwork()
            {
                
                if(strToken != nil)
                {
                
                let braintreeClient = BTAPIClient(authorization: strToken as String )!
                
                let cardClient  = BTCardClient(apiClient: braintreeClient)
                
                let card = BTCard(number: strCreditcardNumber as String, expirationMonth: selectedmonth, expirationYear: selectedyear, cvv: nil)
                
                cardClient.tokenizeCard(card) { (tokenizedCard, error) in
                    // Communicate the tokenizedCard.nonce to your server, or handle error
                    print("nonce" ,tokenizedCard?.nonce)
                    self.strNounce = tokenizedCard!.nonce

                    print(error)
                    self.RegisterCreditCard()
                }
                
            }
            
            
            }
        }
        }

            
        else
            
        {
            
            
            if !isValid
            {
                flag = false
                
                dialog = ZAlertView(title: "Foodies4u",
                                    message: "Please enter valid card number",
                                    closeButtonText: "Ok",
                                    closeButtonHandler: { (alertView) -> () in
                                        alertView.dismissAlertView()
                    }
                )
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
                
            }
            else
            {
            if InternetConnection.isConnectedToNetwork()
            {
                if(strToken != nil)
                {
                let braintreeClient = BTAPIClient(authorization: strToken as String )!
                
                let cardClient  = BTCardClient(apiClient: braintreeClient)
                
                let card = BTCard(number: strCreditcardNumber as String, expirationMonth: selectedmonth, expirationYear: selectedyear, cvv: nil)
                
                cardClient.tokenizeCard(card) { (tokenizedCard, error) in
                    // Communicate the tokenizedCard.nonce to your server, or handle error
                    print("nonce" ,tokenizedCard?.nonce)
                    
                    
                    self.strNounce = tokenizedCard!.nonce

                    print(error)
                    self.RegisterCreditCard()
                }
                }
                
            }
                
            }
            
            
            
            
            
            print("Button tapped")
        }
    
    }
    @IBAction func btnBackToScanPressed(_ sender: UIButton!) {
        print("Button tapped")
        self.navigationController!.popViewController(animated: true)
        
    }
    
    
    
    
    
    
    
}

