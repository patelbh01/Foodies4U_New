//
//  CreditCardViewController.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 15/07/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit
import SwiftyJSON
import NVActivityIndicatorView
import Alamofire





class CreditCardViewController: parentViewController, UITextFieldDelegate,CardIOPaymentViewControllerDelegate,SWRevealViewControllerDelegate
{
    @IBOutlet weak var ViewBD : UIView!
    
    @IBOutlet weak var img : UIImageView!
    
    @IBOutlet weak var lbl : UILabel!
    
    @IBOutlet weak var btnForIphone4 : UIButton!


    var strWSURL: NSString!
    var strToken: NSString!

    @IBOutlet weak var btnManuallyCardEnter: UIButton!
     var FlagCardIo=true

    
    var attrs = [
        NSFontAttributeName : UIFont.systemFont(ofSize: 14.0),
        NSForegroundColorAttributeName : UIColor.blue,
        NSUnderlineStyleAttributeName : 1] as [String : Any]
    
    
    
    //MARK: ViewLife Cycle Methods

    override func viewDidLoad()
    {
        ViewBD.layer.borderWidth=2.0;
        ViewBD.layer.borderColor=UIColor.init(colorLiteralRed: 48.0/255.0, green: 163.0/255.0, blue: 39.0/255.0, alpha: 1.0).cgColor;
        
        self.navigationBarUI()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title:"Skip", style: .plain, target: self, action: #selector(addTapped))
        navigationItem.rightBarButtonItem!.setTitleTextAttributes([ NSFontAttributeName: UIFont (name: "OpenSans-Bold", size: 14)!], for: UIControlState())
        FlagCardIo=true
        
        //get credit card token
        
        
        
         
        // moving backgound image
      /*  let skView = self.view as! SKView
        let myScene = GameScene(size: skView.frame.size)
        skView.presentScene(myScene)*/
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        let buttonTitleStr = NSMutableAttributedString(string:"Click here to enter card manually", attributes:attrs)
        if DeviceType.IS_IPHONE_4_OR_LESS
        {
            lbl.isHidden=true
            btnForIphone4.isHidden=false
            btnForIphone4.setAttributedTitle(buttonTitleStr, for: UIControlState())

        }
        else
        {
            btnManuallyCardEnter.setAttributedTitle(buttonTitleStr, for: UIControlState())

            btnForIphone4.isHidden=true

        }
    }
    
    // MARK: Button Clicks Methods

    
   
    func addTapped()
    {
        UserDefaults.standard.setValue("YES", forKey: "REGISTERED")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let storyboard: UIStoryboard = UIStoryboard(name: "MainSW", bundle: nil)
        
        let frontViewController =  storyboard.instantiateViewController(withIdentifier: "HomeViewController")
        let rearViewController=storyboard.instantiateViewController(withIdentifier: "MenuController")
        
        let rearrightViewController=storyboard.instantiateViewController(withIdentifier: "RightMenuController")
        let frontNavigationController: UINavigationController = UINavigationController(rootViewController: frontViewController)
        let rearNavigationController: UINavigationController = UINavigationController(rootViewController: rearViewController)
        
        let rearrightNavigationController: UINavigationController = UINavigationController(rootViewController: rearrightViewController)
        
        let mainRevealController: SWRevealViewController = SWRevealViewController( rearViewController: rearNavigationController, frontViewController: frontNavigationController)
        
        mainRevealController.rightViewController = rearrightNavigationController
        
        mainRevealController.delegate = self
        rearNavigationController.isNavigationBarHidden = true
        appDelegate.window!.rootViewController = mainRevealController
        appDelegate.window!.makeKeyAndVisible()
        
    }
    
    @IBAction func btnScanCardPressed(_ sender: UIButton)
    {
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC?.modalPresentationStyle = .formSheet
        cardIOVC?.disableManualEntryButtons=true
        cardIOVC?.navigationBarTintColor = GlobalConstants.Theme_Color_Seperator
        cardIOVC?.title="Card11"
        cardIOVC?.hideCardIOLogo=true
        UIBarButtonItem.appearance().tintColor = UIColor.white
        
            self.present(cardIOVC!, animated: true, completion: nil)
       
    }
    
    @IBAction func btnNextStepClicked(_ sender: UIButton)
    {
        
        
        let CCDControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "CCDetailViewController") as? CCDetailViewController
        self.navigationController?.pushViewController(CCDControllerObj!, animated: true)
        
        
        
    }
    
    // MARK: CARD-IO Methods(Payment)

    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        FlagCardIo=false
        // resultLabel.text = "user canceled"
        paymentViewController?.dismiss(animated: true, completion: nil)
    }
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo
        {
            let str = NSString(format: "Received card info.\n Number: %@\n expiry: %02lu/%lu\n cvv: %@.", info.redactedCardNumber, info.expiryMonth, info.expiryYear, info.cvv)
            print(str)
            
            
            
            let array = [info.redactedCardNumber, info.expiryMonth,  info.expiryYear, info.cvv] as [Any]
            paymentViewController?.dismiss(animated: true, completion: nil)
            
            let defaults = UserDefaults.standard
            defaults.set(array, forKey: "SavedCCDetails")
            print(defaults .object(forKey: "SavedCCDetails"))
            let CCDControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "CCDetailViewController") as? CCDetailViewController
            self.navigationController?.pushViewController(CCDControllerObj!, animated: true)
            // resultLabel.text = str as String
        }
    }
    
    
    
}
