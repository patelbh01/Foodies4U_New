//
//  vertificationViewController.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 20/06/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import SpriteKit
import NVActivityIndicatorView
import PhoneNumberKit
import SpriteKit
import SwiftyJSON
import Alamofire


class verificationViewController: parentViewController, UITextFieldDelegate,NVActivityIndicatorViewable,SWRevealViewControllerDelegate {
    
    
    @IBOutlet var txtCountry : UITextField!
    @IBOutlet var txtPhnNumber : UITextField!
    var strOTP: AnyObject!
    var strEnteredOTP: AnyObject!
    
    var strWSURL: NSString!
    var strUSERID: AnyObject!
    let button = UIButton(type: UIButtonType.custom)
    
    
    
    
    
    override func viewDidLoad()
        
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        /* let skView = self.view as! SKView
         let myScene = GameScene(size: skView.frame.size)
         skView.presentScene(myScene)*/
        
        
        strOTP = UserDefaults.standard.value(forKey: "OTP") as AnyObject!
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!
        
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationBarUI()
        
        //create return key in number pad
        
        button.setTitle("Return", for: UIControlState())
        button.setTitleColor(UIColor.black, for: UIControlState())
        button.frame = CGRect(x: 0, y: 163, width: 106, height: 53)
        button.adjustsImageWhenHighlighted = false
        //button.addTarget(self, action: Selector("Done:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        
        UIApplication.shared.statusBarStyle = .lightContent
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        let numberToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.blackTranslucent
        numberToolbar.barTintColor = GlobalConstants.Theme_Color_Seperator
        
        numberToolbar.items = [UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(mobileNumViewController.cancelNumberPad)),
                               UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                               UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(mobileNumViewController.doneWithNumberPad))]
        UIBarButtonItem.appearance().tintColor = UIColor.white
        numberToolbar.sizeToFit()
        txtCountry.inputAccessoryView = numberToolbar
        
    }
    
    
    //MARK: numberpad methods
    func cancelNumberPad() {
        
        //  animateViewMoving(false, moveValue: 200)
        
        txtCountry.resignFirstResponder()
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        //txtMblNumber.text = ""
    }
    
    func doneWithNumberPad() {
        
        txtCountry.resignFirstResponder()
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //   self.performSelector(#selector(self.formatTextfield(_:)), withObject: txtCountry, afterDelay: 0.1)
        
        self.perform(#selector(designTextFields), with: nil, afterDelay: 0.1)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    func designTextFields()
    {
        
        //txtCountry.attributedPlaceholder = NSAttributedString(string:txtCountry.placeholder!, attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.black.cgColor
        border.frame = CGRect(x: 0, y: txtCountry.frame.size.height - width, width:  txtCountry.frame.size.width, height: txtCountry.frame.size.height)
        
        border.borderWidth = width
        txtCountry.layer.addSublayer(border)
        txtCountry.layer.masksToBounds = true
        //txtCountry.becomeFirstResponder()
        
        
    }
    
    //MARK:Textfield Delegates & Datasource Method
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        // NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(mobileNumViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        animateViewMoving(true, moveValue: 200)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        animateViewMoving(false, moveValue: 200)
    }
    
    override func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount+1)
        {
            return false
        }
        let txtAfterUpdate:NSString = textField.text! as NSString
        
        print(currentCharacterCount)
        // txtCountry.text=" "
        
        
        if (currentCharacterCount % 2 == 0)
        {
            
            let text: String = txtAfterUpdate.replacingCharacters(in: range, with: " ")
            textField.text = "\(text)"
        }
        
        
        
        
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 8
    }
    //MARK: WS-For Mobile Verification
    
    
    func getOrders(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        
        strWSURL=GETWSURL()
        
        let param1 : [ String : AnyObject] =
            [
                "customerId":strUSERID,
                "OTPNumber":strOTP!
        ]
        makeCall(section : strWSURL as String + "ValidateCustomer?",param:param1,completionHandler: completionHandler)
        
        
    }
    
    @IBAction func btnsubmitPressed(_ sender: UIButton)
    {
        view.endEditing(true)
        strEnteredOTP = txtCountry.text!.replacingOccurrences(of: "\\s", with: "", options: NSString.CompareOptions.regularExpression, range: nil) as AnyObject!
        let size = CGSize(width: 80, height:80)
        
        startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        if InternetConnection.isConnectedToNetwork()
        {
            if(txtCountry.text!.characters.count != 0)
            {
                
                if (strOTP as? String)! == strEnteredOTP as! String
                {
                    
                    getOrders() { responseObject, error in
                        
                        if (responseObject != nil)
                        {
                            let json = JSON(responseObject!)
                        
                        if let string = json.rawString()
                        {
                            //Do something you want
                            print(string)
                            
                            self.stopActivityAnimating()
                            
                            let encodedString : Data = (string as NSString).data(using: String.Encoding.utf8.rawValue)!
                            let finalJSON = JSON(data: encodedString)
                            
                            
                            if let data = string.data(using: String.Encoding.utf8) {
                                do {
                                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                                    print("json ",json)
                                    
                                    let successVal:String = json!["Success"] as! String
                                    
                                    if (successVal == "1") {
                                        
                                        let arrUserData: NSDictionary =  json!["Data"] as! NSDictionary
                                        
                                        UserDefaults.standard.set(arrUserData, forKey: "FoodiesUserProfile")
                                        
                                        print(UserDefaults.standard.value(forKey: "FoodiesUserProfile") )
                                        
                                        let str  = UserDefaults.standard.value(forKey: "ALREADYREGISTRED") as! String
                                        
                                        if( str == "yes")
                                        {
                                            UserDefaults.standard.setValue("YES", forKey: "REGISTERED")
                                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                            
                                            let storyboard: UIStoryboard = UIStoryboard(name: "MainSW", bundle: nil)
                                            
                                            let frontViewController =  storyboard.instantiateViewController(withIdentifier: "HomeViewController")
                                            let rearViewController=storyboard.instantiateViewController(withIdentifier: "MenuController")
                                            
                                            let rearrightViewController=storyboard.instantiateViewController(withIdentifier: "RightMenuController")
                                            let frontNavigationController: UINavigationController = UINavigationController(rootViewController: frontViewController)
                                            let rearNavigationController: UINavigationController = UINavigationController(rootViewController: rearViewController)
                                            
                                            let rearrightNavigationController: UINavigationController = UINavigationController(rootViewController: rearrightViewController)
                                            
                                            let mainRevealController: SWRevealViewController = SWRevealViewController( rearViewController: rearNavigationController, frontViewController: frontNavigationController)
                                            
                                            mainRevealController.rightViewController = rearrightNavigationController
                                            
                                            mainRevealController.delegate = self
                                            rearNavigationController.isNavigationBarHidden = true
                                            appDelegate.window!.rootViewController = mainRevealController
                                            appDelegate.window!.makeKeyAndVisible()
                                            
                                        }
                                        else
                                        {
                                            let RViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "registrationViewController") as? registrationViewController
                                            self.navigationController?.pushViewController(RViewControllerObj!, animated: true)
                                        }
                                        
                                    }
                                    
                                    
                                } catch {
                                    print("Something went wrong")
                                }
                            }
                            //
                            //
                            //
                            //
                            //                            let name = finalJSON["Success"]
                            //                            if name == "1"
                            //                            {
                            //
                            //                                let arrUserData: NSDictionary = (finalJSON["Data"] as? NSDictionary)!
                            //
                            //                                NSUserDefaults.standardUserDefaults().setObject(arrUserData, forKey: "FoodiesUserProfile")
                            //
                            //                                print(NSUserDefaults.standardUserDefaults().valueForKey("FoodiesUserProfile"))
                            //
                            //                                let RViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("registrationViewController") as? registrationViewController
                            //                                self.navigationController?.pushViewController(RViewControllerObj!, animated: true)
                            //                            }
                            //                            else if name == "0"
                            //                            {
                            //                                if finalJSON["status"].stringValue == "Mobile number already registered"
                            //                                {
                            //
                            //                                    let dialog = ZAlertView(title: "Foodies4u",
                            //                                        message: finalJSON["Data"][0]["status"].stringValue,
                            //                                        closeButtonText: "Ok",
                            //                                        closeButtonHandler: { (alertView) -> () in
                            //                                            alertView.dismissAlertView()
                            //                                            self.performSegueWithIdentifier("verificationSegue", sender: self)
                            //
                            //                                        }
                            //
                            //                                    )
                            //                                    dialog.show()
                            //                                    dialog.allowTouchOutsideToDismiss = true
                            //                                }
                            //
                            //                            }
                            
                        }
                        }
                        else
                        {
                            self.stopActivityAnimating()
                            self.AlertMethod()
                        }
                        
                        
                    }
                    
                    
                }
                else
                {
                    
                    self.stopActivityAnimating()
                    
                   let dialog = ZAlertView(title: "Foodies4u",
                                            message: GlobalConstants.Str_Valid_OTP,
                                            closeButtonText: "Ok",
                                            closeButtonHandler: { (alertView) -> () in
                                                alertView.dismissAlertView()
                        }
                    )
                    dialog.show()
                    dialog.allowTouchOutsideToDismiss = true
                    //txtCountry.becomeFirstResponder()
                    
                    
                }
            }else
                
            {
                self.stopActivityAnimating()
                
                let dialog = ZAlertView(title: "Foodies4u",
                                        message: GlobalConstants.Str_OTP,
                                        closeButtonText: "Ok",
                                        closeButtonHandler: { (alertView) -> () in
                                            alertView.dismissAlertView()
                    }
                )
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
            }
        }
        
        
        /* let mapViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("registrationViewController") as? registrationViewController
         self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)*/
        //  self.performSegueWithIdentifier("ABC", sender: self)
        
    }
    
    /*func keyboardWillShow(note : NSNotification) -> Void{
     
     dispatch_async(dispatch_get_main_queue()) { () -> Void in
     
     self.button.hidden = false
     let keyBoardWindow = UIApplication.sharedApplication().windows.last
     self.button.frame = CGRectMake(0, (keyBoardWindow?.frame.size.height)!-53, 106, 53)
     keyBoardWindow?.addSubview(self.button)
     keyBoardWindow?.bringSubviewToFront(self.button)
     
     UIView.animateWithDuration(((note.userInfo! as NSDictionary).objectForKey(UIKeyboardAnimationCurveUserInfoKey)?.doubleValue)!, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
     
     self.view.frame = CGRectOffset(self.view.frame, 0, 0)
     }, completion: { (complete) -> Void in
     print("Complete")
     })
     }
     
     }
     
     func Done(sender : UIButton){
     
     dispatch_async(dispatch_get_main_queue()) { () -> Void in
     
     self.txtCountry.resignFirstResponder()
     
     }
     }*/
    
    
}
