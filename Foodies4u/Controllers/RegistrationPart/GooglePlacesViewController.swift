//
//  GooglePlacesViewController.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 05/08/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
protocol GooglePlacesDelegates: class
{
    func userAddress(_ arrData: String , Latitude: Double ,Longitude: Double)
}

class GooglePlacesViewController:UIViewController,CLLocationManagerDelegate
{
    var locationManager: CLLocationManager!
    weak var delegate: GooglePlacesDelegates? = nil


    var googleMapView: GMSMapView!
    var placePicker: GMSPlacePicker!
    var lat: Double!
    var long: Double!
    @IBOutlet var mapViewContainer: UIView!

    override func viewDidLoad()
    {
        self.googleMapView = GMSMapView(frame: self.mapViewContainer.frame)
        self.googleMapView.animate(toZoom: 18.0)
        self.view.addSubview(googleMapView)
        
        let center = CLLocationCoordinate2DMake(lat, long)
        let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
        let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        self.placePicker = GMSPlacePicker(config: config)
        
        // 2
        placePicker.pickPlace { (place: GMSPlace?, error: Error?) -> Void in
            
            if let error = error {
                print("Error occurred: \(error.localizedDescription)")
                return
            }
            // 3
            if let place = place {
                let coordinates = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
                let marker = GMSMarker(position: coordinates)
                marker.title = place.name
                marker.map = self.googleMapView
                self.googleMapView.animate(toLocation: coordinates)
                
                
//                let geocoder = CLGeocoder()
//                let location = CLLocation(latitude: place.coordinate.latitude, longitude:place.coordinate.longitude)
//                geocoder.reverseGeocodeLocation(location)
//                {
//                    (placemarks, error) -> Void in
//                    let placeArray = placemarks as [CLPlacemark]!
//                    
//                    // Place details
//                    var placeMark: CLPlacemark!
//                    placeMark = placeArray?[0]
//                    
//                    // Address dictionary
//                    print(placeMark.addressDictionary)
//                    
//                    // Location name
//                    if let locationName = placeMark.addressDictionary?["Name"] as? NSString
//                    {
//                        print(locationName)
//                    }
//                    
//                    // Street address
//                    if let street = placeMark.addressDictionary?["Thoroughfare"] as? NSString
//                    {
//                        print(street)
//                    }
//                    
//                    // City
//                    if let city = placeMark.addressDictionary?["City"] as? NSString
//                    {
//                        print(city)
//                    }
//                    
//                    // Zip code
//                    if let zip = placeMark.addressDictionary?["ZIP"] as? NSString
//                    {
//                        print(zip)
//                    }
//                    
//                    // Country
//                    if let country = placeMark.addressDictionary?["Country"] as? NSString
//                    {
//                        print(country)
//                    }
//                   
//            }
                
                self.delegate?.userAddress(place.formattedAddress! , Latitude: place.coordinate.latitude , Longitude: place.coordinate.longitude)
                self.navigationController!.popViewController(animated: true)
            }
            else {
                print("No place was selected")
            }
        } 

    }
    
  
    
}
