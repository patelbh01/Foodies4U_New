//
//  countryDialCodeViewController.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 06/07/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
//MARK: DELEGATE FOR DATA PAdSSING

protocol countrySelectedDataDelegate: class {
    func userSelectedCountry(_ arrData: NSDictionary)
}

class countryDialCodeViewController: parentViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    weak var delegate: countrySelectedDataDelegate? = nil
    
    var arrAllCountryList: NSArray!
    var arrSearchData: NSArray!
    var  controller:UISearchController!
    //for table view
    @IBOutlet var tblCountryDialCode: UITableView!
    var resultSearchController = UISearchController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.navigationBarUI()
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = false
        arrSearchData = NSArray()
        
        self.resultSearchController = ({
            controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
           // controller.hidesNavigationBarDuringPresentation = false
           // self.definesPresentationContext = false

            return controller
        })()
        arrAllCountryList = DBManager.getSharedInstance().getAllCountryDialCodeList()
        tblCountryDialCode.reloadData()
        
        tblCountryDialCode.tableHeaderView = self.resultSearchController.searchBar
        
    }
    override func viewWillDisappear(_ animated: Bool)
     {
        
        self.view.endEditing(true)
    }
    //MARK: TABLE VIEW DATA SOURCE AND DELEGATE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.resultSearchController.isActive{
            //self.filterData()
            return arrSearchData.count
        }
        return arrAllCountryList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell!
        
        let lblCountryName = cell?.viewWithTag(10) as! UILabel
        
        if self.resultSearchController.isActive{
            
            lblCountryName.text = (arrSearchData.value(forKey: "countryName") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
        } else {
            lblCountryName.text = (arrAllCountryList.value(forKey: "countryName") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
        }
        lblCountryName.numberOfLines = 0;
        
        
        let lblCountryDialCode = cell?.viewWithTag(20) as! UILabel
        if self.resultSearchController.isActive{
            lblCountryDialCode.text = "+" + ((arrSearchData.value(forKey: "countryDialCode") as AnyObject).object(at: (indexPath as NSIndexPath).row) as! String)
        } else {
            lblCountryDialCode.text = "+" + ((arrAllCountryList.value(forKey: "countryDialCode") as AnyObject).object(at: (indexPath as NSIndexPath).row) as! String)
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
      
        if arrSearchData.count==0
        {
            delegate?.userSelectedCountry(arrAllCountryList.object(at: (indexPath as NSIndexPath).row) as! NSDictionary)
        }
        else
        {
            
            controller.searchBar.resignFirstResponder()
            //controller.searchBar.hidden=true;
            delegate?.userSelectedCountry(arrSearchData.object(at: (indexPath as NSIndexPath).row) as! NSDictionary)
            
        }
        self.definesPresentationContext = true
        controller.isActive = false
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK: SEARCH BAR DELEGATE METHODS
    //    func filterData(){
    //        //arrSearchData = NSArray()
    //
    //
    //        //tblCountryDialCode.reloadData()
    //    }
    
    //    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
    //        countrySearchController.active = true
    //        self.filterData()
    //    }
    //
    //    func activateSearch() {
    //        tblCountryDialCode.scrollRectToVisible(CGRectMake(0, 0, 1, 1), animated: false)
    //        countrySearchController.searchBar.becomeFirstResponder()
    //    }
    //    @IBAction func btnDisplaySearchBarClicked(sender: UIButton){
    //
    //        tblCountryDialCode.scrollRectToVisible(CGRectMake(0, 0, 1, 1), animated: true)
    //
    //        var delay: NSTimeInterval!
    //        if tblCountryDialCode.contentOffset.y > 1000 {
    //            delay = 0.4
    //        }
    //        else {
    //            delay = 0.1
    //        }
    //
    //        self.performSelector(#selector(activateSearch), withObject: nil, afterDelay: delay)
    //    }
    //
    //    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
    //        //self.tblViewFrame()
    //    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
     //   let predicate = NSPredicate(format:"countryName CONTAINS[c] '\(searchController.searchBar.text!)'")
         let predicate = NSPredicate(format:"countryName ==[c] '\(searchController.searchBar.text!)'")
        arrSearchData = arrAllCountryList.filtered(using: predicate) as NSArray!
        tblCountryDialCode.reloadData()
        
    }
    
}
