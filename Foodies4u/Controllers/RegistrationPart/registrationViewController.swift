//
//  registrationViewController.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 23/06/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import UIKit
import Foundation
import SpriteKit
import SwiftyJSON
import Alamofire
import NVActivityIndicatorView
import GoogleMaps
import GooglePlacePicker

/*if CLLocationManager.locationServicesEnabled() {
    switch(CLLocationManager.authorizationStatus()) {
    case .NotDetermined, .Restricted, .Denied:
        print("No access")
    case .AuthorizedAlways, .AuthorizedWhenInUse:
        print("Access")
    }
} else {
    print("Location services are not enabled")
}*/


class registrationViewController: parentViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,MIDatePickerDelegate,KACircleCropViewControllerDelegate,NVActivityIndicatorViewable,CLLocationManagerDelegate {
    
  
    
    @IBOutlet var tblList : UITableView!
    @IBOutlet var tblCreditCardList : UITableView!
    
    @IBOutlet var btnLogin : UIButton!
    @IBOutlet weak var btnProfilPic: UIButton!

    @IBOutlet weak var barButton: UIBarButtonItem!
    @IBOutlet var imgIcon : UIImageView!
    
    // for userdefaults
    var strWSURL: NSString!
    var strUSERID: AnyObject!
    var strMNumber: AnyObject!

    var imagePicker = UIImagePickerController()

    var lblYourPhoto: UILabel!
    
    var txtFirstname:UITextField!
    var txtLastname:UITextField!
    var txtField:UITextField!
    var strBase64:NSString!

    var actionSheetControllerIOS8: UIAlertController!
    var imageData = Data()
    var btnBD : UIButton!
    var btnIcon :UIButton!
    var btn:UIButton!

    var dateFormatter = DateFormatter()
    var cell : UITableViewCell!

    var  strFirstName:NSString!
    var  strLastName:NSString!
    var  strBD:NSString!
    var  strEmail:NSString!
    var  strMnumber:NSString!
    var  strCountry:NSString!
    var  strState:NSString!
    var  strZipCode:NSString!
    var userImage:UIImage!

    
    var Flag:Bool!
    var FlagImagepicker:Bool!
    var locationManager: CLLocationManager!
    var latitude: Double!
    var longitude: Double!
    

    var placePicker: GMSPlacePicker!
    var CCcell:Customcell!
    var datePicker = MIDatePicker.getFromNib()


    
    //for user basic details
    let arrList = ["Name", "Date of Birth (MM/DD/YYYY)", "Email", "Mobile Number", "Home Address","Work Address","Address"]
    let arrIconList = ["ProfileIcon.png", "CalendarIcon", "EmailIcon", "MobileIcon", "Home-Address.png", "Work-Address.png","AddressIcon.png"]
    
    //for credit card
    var someInts:[String] =  ["Credit Card Number", "Name on Card", "Expiry Date(MM/YY)","CVV"]
    var arrIcon:[String] =  ["CardIcon-1.png", "ProfileIcon.png", "CalendarIcon","CardIcon-2.png"]
    
    
    //for home and work address
    
    var strHomeAddressText: String! = ""
    var HomeLat: Double! = 0.0
    var HomeLong: Double! = 0.0
    var WorkLat: Double! = 0.0
    var WorkLong: Double! = 0.0
    
    var strWorkddressText: String! = ""
    var strDOBText: String! = ""
    
    var addHomedict:NSMutableDictionary!
    var addWorkdict:NSMutableDictionary!
    var addBothdict:NSMutableArray!
    
    
    //MARK: VIEW LYFE CYCLES METHODS

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
       // definesPresentationContext = true
        
        imagePicker.delegate = self
        Flag=true;
        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!
        strMNumber = UserDefaults.standard.value(forKey: "MNUMBER") as AnyObject!
        
         setupDatePicker()
        // moving backgound image
        /*let skView = self.view as! SKView
        let myScene = GameScene(size: skView.frame.size)
        skView.presentScene(myScene)*/
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationBarUI()
         navigationItem.rightBarButtonItem = UIBarButtonItem(title:"Skip", style: .plain, target: self, action: #selector(goToNextView))
        navigationItem.rightBarButtonItem!.setTitleTextAttributes([ NSFontAttributeName: UIFont (name: "OpenSans-Bold", size: 14)!], for: UIControlState())

        //dismis keyboard any where you tap on the screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
    }
    func userAddress(_ arrData: String, Latitude: Double, Longitude: Double)
    {
        let indexPath1 = IndexPath(row: 4, section: 0)
        CCcell = tblList.cellForRow(at: indexPath1) as! Customcell!
        CCcell.txtField.text = arrData
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
      
          view.endEditing(true)
        

    }
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]){
        // 1
        let location:CLLocation = locations.last!
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        
        
        
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: MIDATEPICKER METHODS

    fileprivate func setupDatePicker() {
        
        datePicker.delegate = self
        
        datePicker.config.startDate = Date()
        
        datePicker.config.animationDuration = 0.25
        
        datePicker.config.cancelButtonTitle = "Cancel"
        datePicker.config.confirmButtonTitle = "Confirm"
        
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor(red: 42/255.0, green: 18/255.0, blue: 15/255.0, alpha: 1)
        datePicker.config.confirmButtonColor = UIColor.white
        datePicker.config.cancelButtonColor = UIColor.white
        
    }
    
    func miDatePicker(_ amDatePicker: MIDatePicker, didSelect date: Date) {
        
        let indexPath1 = IndexPath(row: 1, section: 0)
        CCcell = tblList.cellForRow(at: indexPath1) as! Customcell!
        dateFormatter.dateFormat = "MM/dd/yyyy"
        CCcell.txtField.text=dateFormatter.string(from: date)
        strBD = CCcell.txtField.text as NSString!

    }
    func miDatePickerDidCancelSelection(_ amDatePicker: MIDatePicker) {
      
    }
    
    //MARK:  IMAGEPICKER CONTROLLER METHODS
    
    
    @IBAction func loadImageButtonTapped(_ sender: UIButton)
    {
        self.perform(#selector(self.openactionsheet), with: nil, afterDelay: 0.1)
    }
    
    
    func openactionsheet()
    {
               self.actionSheetControllerIOS8 = UIAlertController(title: "Please select", message: "image from", preferredStyle: .actionSheet)
        
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            
           // self.parentViewController = registrationViewController
            self.FlagImagepicker = false

        }
        
        self.actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let CameraActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            self.openCamera()
        }
        self.actionSheetControllerIOS8.addAction(CameraActionButton)
        
        let GalleryActionButton: UIAlertAction = UIAlertAction(title: "Gallery", style: .default)
        {
            action -> Void in
            self.openGallary()
        }
        self.actionSheetControllerIOS8.addAction(GalleryActionButton)
        print("bhumika")

      _ = navigationController?.present(self.actionSheetControllerIOS8, animated: true, completion: nil)
    
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        /*else
        {
            openGallary()
        }*/
    }
    func openGallary()
    {
        
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.imagePicker.allowsEditing = false

        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
           

            dismiss(animated: true, completion: nil)

            let circleCropController = KACircleCropViewController(withImage: pickedImage)
            circleCropController.delegate = self
            present(circleCropController, animated: false, completion: nil)
        }


    }
    
    //MARK: KACIRCLEVIEWCONTROLLER METHODS
    
    func circleCropDidCancel() {
        //Basic dismiss
        dismiss(animated: false, completion: nil)
    }
    
    func circleCropDidCropImage(_ image: UIImage)
    {
        //Same as dismiss but we also return the image
        btnProfilPic.layer.borderColor = UIColor.clear.cgColor
        btnProfilPic.layer.borderWidth = 0.0;
        btnProfilPic.imageView?.clipsToBounds = true
        btnProfilPic.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        btnProfilPic.setImage(UIImage(named: " "),  for:UIControlState())
        btnProfilPic.setBackgroundImage(image, for: UIControlState())
        lblYourPhoto.isHidden=true
        dismiss(animated: false, completion: nil)
        
        userImage = image
        

        
        let imageData = UIImagePNGRepresentation(image)
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let imageURL = documentsURL.appendingPathComponent("cached.png")
        
        if !((try? imageData!.write(to: imageURL, options: [])) != nil)
        {
            print("not saved")
        } else {
            print("saved")
        }
        
         strBase64 = imageData!.base64EncodedString(options: .lineLength64Characters) as NSString!
    }
    
    
    
    
    //MARK: TEXTFIELD DATA SOURCE AND DELEGATE
    
    func textFieldDidBeginEditing(_ tF: UITextField) {
        // print("keyboard %f",keyboardSize.height);
        
        if tF==txtFirstname || tF==txtLastname
        {
            tF.autocapitalizationType = .words
            tF.returnKeyType = .next
            if (DeviceType.IS_IPHONE_4_OR_LESS)
            {
                animateViewMoving(true, moveValue: 190)

                
            }
            

        }
        else
        
        {
            if tF.tag==5
            {
                tF.returnKeyType = .done

            }
            else
            {
            tF.returnKeyType = .next
            }
            
            

            animateViewMoving(true, moveValue: 215)

        }


        
       
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField==txtFirstname
        {
            strFirstName=textField.text as NSString!
        }
        else if textField==txtLastname
        {
            strLastName=textField.text as NSString!
        }
            
        if (DeviceType.IS_IPHONE_4_OR_LESS)
        {
            animateViewMoving(false, moveValue: 190)
            
            
        }

        else
        
        {
            if textField.tag == 2
                
            {
                strEmail=textField.text as NSString!
                print(strEmail)
            }
           else if textField.tag == 3
                
            {
                strMnumber=textField.text as NSString!
                print(strMnumber)
            }
            else  if textField.tag == 4
            {
                
                strHomeAddressText=textField.text
                print(strHomeAddressText)
            }
            
            if textField.tag == 5

            {
                
                strWorkddressText=textField.text
                print(strWorkddressText)
            }
            
            if(self.view.frame.origin.y != 0)
            {
            animateViewMoving(false, moveValue: 215)
            }

        }
        
        
    }
    
    override func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.txtFirstname
        {
            self.txtLastname.becomeFirstResponder()
        }
            else if textField == self.txtLastname
        {
           textField.resignFirstResponder()

            view.endEditing(true)
             self .handleTap()
        }
        else
        {
            let nextTag: NSInteger = textField.tag + 1;
            // Try to find next responder
            if let nextResponder: UIResponder? = CCcell.txtField.superview?.superview?.superview?.viewWithTag(nextTag) as UIResponder! {
                nextResponder?.becomeFirstResponder()
            }
            else
                {                // Not found, so remove keyboard.
                    textField.resignFirstResponder()
            }
            return false // We do not want UITextField to insert line-breaks.
            
            
            
            //return true
        }
        
        return true

    }
  
   

    
    // MARK: TABLE VIEW DATA SOURCE AND DELEGATE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
      
            if (indexPath as NSIndexPath).row == 0 {
                return 90
            }
          return 60;
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
      
            return arrList.count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
                  if (indexPath as NSIndexPath).row == 0
            {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "nameTextfieldCell")! as UITableViewCell
                
                ////// for icon
                
                btnProfilPic = cell.viewWithTag(20) as! UIButton
                btnProfilPic.layer.borderWidth = 1.0
                btnProfilPic.layer.borderColor = UIColor.lightGray.cgColor
                btnProfilPic.layer.cornerRadius = btnProfilPic.frame.size.width/2
                
                
                ////// for label
                
                lblYourPhoto = cell.viewWithTag(50) as! UILabel
                lblYourPhoto.text = "Add Your Photo"

                
                ////// for textfield view
                let viewForBack = cell.viewWithTag(10)! as UIView
                viewForBack.backgroundColor = UIColor.lightGray
                viewForBack.layer.borderWidth = 1.0
                viewForBack.layer.borderColor = UIColor.lightGray.cgColor
                viewForBack.layer.cornerRadius = 5.0
                
                ////// for firstname textfield
                //for left padding text
                
                txtFirstname = cell.viewWithTag(30) as! UITextField
                let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 25, height: 30))
                txtFirstname.leftView=paddingView;
                txtFirstname.leftViewMode = UITextFieldViewMode.always
                txtFirstname.delegate = self
                txtFirstname.keyboardType = .default

                //for making textfield placeholder color black
                
                txtFirstname.attributedPlaceholder = NSAttributedString(string:txtFirstname.placeholder!, attributes:[NSForegroundColorAttributeName: UIColor.black])
                
                
                ////// for lastname textfield
                //for left padding text
                
                txtLastname = cell.viewWithTag(40) as! UITextField
                let paddingView2 = UIView(frame:CGRect(x: 0, y: 0, width: 25, height: 30))
                txtLastname.leftView=paddingView2;
                txtLastname.leftViewMode = UITextFieldViewMode.always
                txtLastname.delegate = self
                txtFirstname.keyboardType = .default


                //for making textfield placeholder color black
                
                txtLastname.attributedPlaceholder = NSAttributedString(string:txtLastname.placeholder!, attributes:[NSForegroundColorAttributeName: UIColor.black])
                
            }
           
            else if (indexPath as NSIndexPath).row == arrList.count-1
            {
                
                //subimt button cell
                
                cell = tableView.dequeueReusableCell(withIdentifier: "submitBtnCell")! as UITableViewCell
                
                let btnNextStep = cell.viewWithTag(10) as! UIButton
                btnNextStep.titleLabel?.text = "NEXT STEP "
                btnNextStep.layer.borderColor = UIColor.lightGray.cgColor
                btnNextStep.layer.borderWidth = 1.0
                btnNextStep.layer.cornerRadius = 5.0
                
                // btnNextStep.addTarget(self, action: #selector(btnNextStepClicked(btnNextStep)), forControlEvents: UIControlEvents.TouchUpInside)
            }
            else
            {
                CCcell  = tableView.dequeueReusableCell(withIdentifier: "textfieldCell")! as! Customcell
                
                
                print((indexPath as NSIndexPath).row)
                
                
                if( arrList[(indexPath as NSIndexPath).row] == "Date of Birth (MM/DD/YYYY)" )
                {
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(registrationViewController.handleTap))
                    tap.numberOfTapsRequired = 1
                    CCcell.addGestureRecognizer(tap)
                    CCcell.txtField.isUserInteractionEnabled=false
                    
                  
                }
                 else if (indexPath as NSIndexPath).row==2
                {
                    CCcell.txtField.keyboardType = UIKeyboardType.emailAddress

                }
                else if (indexPath as NSIndexPath).row==3
                {
                    CCcell.txtField.keyboardType = UIKeyboardType.phonePad
                    CCcell.txtField.text = strMNumber as? String
                    CCcell.txtField.isUserInteractionEnabled = false
                    //CCcell.selectionStyle = UITableViewCellSelectionStyle.Gray
                    CCcell.selectionStyle = .none
                    CCcell.txtField.alpha=0.5


                    
                    
                }
                else
                
                {
                    CCcell.txtField.isUserInteractionEnabled=true
                    
                    
                    btn = UIButton(frame: CGRect(x: self.view.frame.size.width-70, y:15, width:30, height:30));
                    btn.setBackgroundImage(UIImage(named: "MapDirection.png"), for: UIControlState())
                    btn.addTarget(self, action: #selector(registrationViewController.ChooseAddress(_:)), for: UIControlEvents.touchUpInside)
                    btn.tag = (indexPath as NSIndexPath).row
                    let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 30, height: 30))
                    CCcell.txtField.rightView=paddingView;
                    CCcell.txtField.rightViewMode = UITextFieldViewMode.always
                    CCcell.contentView.addSubview(btn)
                    
                        
                    //}

                
                }
                // self.performSelector(#selector(self.formatTextfield(_:)), withObject: txtField, afterDelay: 0.1)
                
                //for adding border and corner radious
                CCcell.txtField.layer.borderColor = UIColor.lightGray.cgColor
                CCcell.txtField.layer.borderWidth = 1.0
                CCcell.txtField.layer.cornerRadius = 5.0
                CCcell.txtField.delegate = self
                CCcell.txtField.tag=(indexPath as NSIndexPath).row
                
                //for left padding text
                let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 55, height: 30))
                CCcell.txtField.leftView=paddingView;
                CCcell.txtField.leftViewMode = UITextFieldViewMode.always
                
                //for making textfield placeholder color black
                
                let strPlaceholder = arrList[(indexPath as NSIndexPath).row];
                CCcell.txtField.attributedPlaceholder = NSAttributedString(string:strPlaceholder, attributes:[NSForegroundColorAttributeName: UIColor.black])
                
                
                ////// for icon
                let strImg = arrIconList[(indexPath as NSIndexPath).row]
                
                CCcell.btnIcon.setImage(UIImage(named: strImg), for: UIControlState())
                
                
                return CCcell

            }
            
            
      
        cell.backgroundColor = UIColor.clear;
        
        
        return cell
    }
    
    func handleTap() {
        view.endEditing(true)

        datePicker.show(inVC: self)
        // Your code here...
    }
    
    
    //MARK: CHOOSE ADDRESS WORK AND HOME
    
    func ChooseAddress(_ sender:UIButton!)
    {
        
        let lat  = UserDefaults.standard.double(forKey: "LATITUDE")
        let long = UserDefaults.standard.double(forKey: "LONGITUDE")

        let center = CLLocationCoordinate2DMake(22.307159, 73.181219)
        let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
        let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        self.placePicker = GMSPlacePicker(config: config)
        
        
    placePicker.pickPlace
      {
        
            (place , error)   in
            
            if let error = error {
                print("Error occurred: \(error.localizedDescription)")
                return
            }
            // 3
            if let place = place {
                let coordinates = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
                let marker = GMSMarker(position: coordinates)
                marker.title = place.name
                self.latitude =  place.coordinate.latitude
                self.longitude = place.coordinate.longitude
                
                
                if (sender.tag == 4)
                {
                    
                    self.strHomeAddressText = place.formattedAddress!
                    self.HomeLat = self.latitude
                    self.HomeLong = self.longitude
                    let indexPath = IndexPath(row: 4, section: 0)
                    self.CCcell = self.tblList.cellForRow(at: indexPath) as! Customcell!
                    self.CCcell.txtField.text = place.formattedAddress!
                    self.CCcell.txtField.placeholder = " "
                    self.HomeLat = self.latitude
                    self.HomeLong = self.longitude
                    
                    
                    
                }
                else
                {
                    self.strWorkddressText =  place.formattedAddress!
                    let indexPath = IndexPath(row: 5, section: 0)
                    self.CCcell =  self.tblList.cellForRow(at: indexPath) as! Customcell!
                    self.CCcell.txtField.text =  place.formattedAddress!
                    self.CCcell.txtField.placeholder = " "
                    self.WorkLat = self.latitude
                    self.WorkLong = self.longitude
                    
                    
                    
                    
                }
                
            }
        }
        // 2
        
        
       /* var center = CLLocationCoordinate2DMake(51.5108396, -0.0922251)
        var northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
        var southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
        var viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        var config = GMSPlacePickerConfig(viewport: viewport)
        self.placePicker = GMSPlacePicker(config: config)
        
        placePicker.pickPlace { (place, error) in
            
            if error != nil {
                print("Pick Place error \(error?.localizedDescription)")
                return
            }
            if place != nil {
                print("Place name \(place?.name)")
                print("Place address \(place?.formattedAddress)")
                print("Place attributions \(place?.attributions.string)")
            }
            else {
            }
 
        }*/
        
     /*   placePicker.pickPlace(callback: {(_ place: GMSPlace, _ error: Error) -> Void in
            if error != nil {
                print("Pick Place error \(error.localizedDescription)")
                return
            }
            if place != nil {
                print("Place name \(place.name)")
                print("Place address \(place.formattedAddress)")
                print("Place attributions \(place.attributions.string)")
            }
            else {
            }
    })*/
    }
    
    // MARK: button clicks
    
    
    func goToNextView()
    {
        let CCViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "CreditCardViewController") as? CreditCardViewController
        self.navigationController?.pushViewController(CCViewControllerObj!, animated: true)
        
    }
    
    
    
    
    override func isValidEmail(_ testStr:String) -> Bool
    {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }

    @IBAction func btnNextStepClicked(_ sender: UIButton)
    {
        view.endEditing(true)

        
        if  (strFirstName == nil)
        {
            
            strFirstName="";
        }
         if  (strLastName == nil)
        {
            strLastName="";

        }
         if  (strEmail == nil)
        {
            strEmail="";

        }
         if  (strHomeAddressText == nil )
        {
            strHomeAddressText="";

        }
        
        if  (strWorkddressText == nil )
           
        {
            strWorkddressText = "";
            
        }
        
        
            
         if  (strBD == nil )
        {
            strBD="";

        }
         if  (strBase64 == nil )
        {
            strBase64="";
            
        }
        if strEmail.length>0
        {
            if !isValidEmail(strEmail as String)
            {
                
               
               
               let dialog = ZAlertView(title: "Foodies4u",
                                    message:GlobalConstants.Str_Valid_EmailId,
                                    closeButtonText: "Ok",
                                    closeButtonHandler: { (alertView) -> () in
                                        
                                        alertView.dismissAlertView()

                    }
                )
                dialog.show()
                dialog.allowTouchOutsideToDismiss = true
                Flag = false
            }
            else
            
            {
                Flag = true

            }
        }
        
        if  (Flag == true)
        {
            
            if(HomeLat == nil)
            {
                HomeLat = 0.0
            }
            if(HomeLong == nil)
            {
                HomeLong = 0.0
                
            }
            
            if(WorkLat == nil)
            {
                WorkLat = 0.0
            }
            if(WorkLong == nil)
            {
                WorkLong = 0.0
                
            }
            
            addHomedict =
                [ "AddressId" : 1, "Address":strHomeAddressText! ,"Latitude":String(describing: HomeLat!) ,"Longitude": String(describing: HomeLong!)]
            
            addWorkdict = [ "AddressId" : 2, "Address":strWorkddressText! ,"Latitude":String(describing: WorkLat!) ,"Longitude": String(describing: WorkLong!)]
            
            addBothdict = NSMutableArray()
            addBothdict.add(addHomedict)
            addBothdict.add(addWorkdict)
            
            //set userdefault to store home address and work address for map view
            
           // NSUserDefaults.standardUserDefaults().setObject(addBothdict, forKey: "USERADDRESS")
            
           // print( NSUserDefaults.standardUserDefaults().objectForKey("USERADDRESS"))
            
            
           // WEBSERVICES FOR REGISTRATION
            
            strWSURL=GETWSURL()
            
            let param : [ String : AnyObject] = [
                "Id":strUSERID,
                "FirstName": strFirstName,
                "LastName": strLastName,
                "Email": strEmail,
                "Address": addBothdict,
                "Country": "1" as AnyObject,
                "State":"2" as AnyObject,
                "City":"Newyork" as AnyObject,
                "DateOfBirth":strBD,
                "PostalCode":"390011" as AnyObject,
                "ImageName":"cached" as AnyObject,
                "pictureBinary":strBase64,
                "mimeType": ".png" as AnyObject
                
            ]
            
            
            UserDefaults.standard.set(strFirstName , forKey: "USERNAME")
            UserDefaults.standard.set(strEmail, forKey: "EMAILID")

            
            print(param)
            
            let size = CGSize(width: 80, height:80)
            
            startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
            Alamofire.request(strWSURL as String + "UpdateCustomerData?",method: .post, parameters: param,  encoding: JSONEncoding.default, headers: nil).responseJSON
                { response in switch response.result {
                    
                    
                case .success(let JSON):
                    
                    print("Success with JSON: \(JSON)")
                    
                    let response = JSON as! NSDictionary
                    
                    
                    self.stopActivityAnimating()

                    
                    if ((response.value(forKey: "Success") as? String) == "1")
                    {
                        if(self.userImage != nil)
                        {
                       UserDefaults.standard.set(UIImagePNGRepresentation(self.userImage), forKey: "USERIMAGE")
                        }
                        let CCViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "CreditCardViewController") as? CreditCardViewController
                        self.navigationController?.pushViewController(CCViewControllerObj!, animated: true)
                      
                    }
                    
                
                case .failure(let error):
                    self.stopActivityAnimating()
                    
                    print("Request failed with error: \(error)")
                    }
            }

        }
        
    }
    
    
    
    @IBAction func btnBasicDetailsClicked(_ sender: UIButton)  {
        
        if tblList.isHidden {
            
            let transition = CATransition()
            transition.duration = 0.5
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromLeft
            
            tblList.isHidden = false
            self.tblList.layer.add(transition, forKey: nil)
            
            tblCreditCardList.isHidden = true
        }
    }
    
    @IBAction func btnCreditCardDetailsClicked(_ sender: UIButton)  {
        
        if tblCreditCardList.isHidden {
            
            let transition = CATransition()
            transition.duration = 0.5
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
            
            tblCreditCardList.isHidden = false
            self.tblCreditCardList.layer.add(transition, forKey: nil)
            
            tblList.isHidden = true
        }
    }
    
    
    

}
    


