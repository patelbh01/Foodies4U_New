//
//  ViewController.swift
//  SwiftEstimoteBeacons
//
//  Created by Michael Kane on 2/18/15.
//  Copyright (c) 2015 ArilogicApps. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ESTBeaconManagerDelegate {
    
    @IBOutlet var beaconLabel: UILabel!
    
    let webView = UIWebView()
    var buttonWeb = UIBarButtonItem()
    let beaconManager : ESTBeaconManager = ESTBeaconManager();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set beacon manager delegate
        beaconManager.delegate = self;
        
        //create the beacon region
        let beaconRegion : ESTBeaconRegion = ESTBeaconRegion(proximityUUID: UUID(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, major: 15530, minor: 62687, identifier: "dcc3f4df3caa")
        
        //Opt in to be notified upon entering and exiting region
        beaconRegion.notifyOnEntry = true
        beaconRegion.notifyOnExit = true
        
        //beacon manager asks permission from user
        beaconManager.startRangingBeacons(in: beaconRegion)
        beaconManager.startMonitoring(for: beaconRegion)
        beaconManager.requestAlwaysAuthorization()
    }
    
    func beaconManager(_ manager: ESTBeaconManager!, didRangeBeacons beacons: [AnyObject]!, in region: ESTBeaconRegion!) {
        
        if beacons.count > 0 {
            let firstBeacon : ESTBeacon = beacons.first! as! ESTBeacon
            
            self.beaconLabel.text = ("\(textForPromimity(firstBeacon.proximity))")
            
        }
    }
    
    func textForPromimity(_ proximity:CLProximity) -> (NSString)
    {
        var distance : NSString!
        
        switch(proximity)
        {
        case .far:
            print("Far")
            distance = "far"
            beaconLabel.textColor = UIColor.red
            return distance
        case .near:
            print("Near")
            distance = "Near"
            beaconLabel.textColor = UIColor.purple
            return distance
        case .immediate:
            print("Immediate")
            distance = "Immediate"
            beaconLabel.textColor = UIColor.green
            return distance
        case .unknown:
            print("Unknown")
            distance = "Unknown"
            return distance
        default:
            break;
        }
        return distance
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //check for region failure
    func beaconManager(_ manager: ESTBeaconManager!, monitoringDidFailFor region: ESTBeaconRegion!, withError error: NSError!) {
        print("Region did fail: \(manager) \(region) \(error)")
    }
    
    //checks permission status
    func beaconManager(_ manager: ESTBeaconManager!, didChange status: CLAuthorizationStatus) {
        print("Status: \(status)")
    }
    
    //beacon entered region
    func beaconManager(_ manager: ESTBeaconManager!, didEnter region: ESTBeaconRegion!) {
        
        let notification : UILocalNotification = UILocalNotification()
        notification.alertBody = "Youve done it!"
        notification.soundName = "Default.mp3"
        print("Youve entered")
        UIApplication.shared.presentLocalNotificationNow(notification)
    }
    
    //beacon exited region
    func beaconManager(_ manager: ESTBeaconManager!, didExitRegion region: ESTBeaconRegion!) {
        
        let notification : UILocalNotification = UILocalNotification()
        notification.alertBody = "Youve exited!"
        notification.soundName = "Default.mp3"
        print("Youve exited")
        UIApplication.shared.presentLocalNotificationNow(notification)
    }
    
    @IBAction func getConnected(_ sender: AnyObject) {
        
        webView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(webView)
        
        let url = URL(string: "http://www.estimote.com")
        let request = URLRequest(url:url!)
        webView.loadRequest(request)
        
        buttonWeb = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(ViewController.finished))
        navigationItem.setRightBarButton(buttonWeb, animated: true)
        
    }
    
    func finished()
    {
        webView.removeFromSuperview()
        let b = UIBarButtonItem()
        navigationItem.setRightBarButton(b, animated: true)
    }
    
}

