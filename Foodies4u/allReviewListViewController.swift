//
//  allReviewListViewController.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 13/09/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON

class allReviewListViewController: parentViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {
    
    
    @IBOutlet weak var tblReviewList:UITableView!
    
    var strWSURL: NSString!
    var strRestaurantID: String? = ""
    var strMenuID: String! = ""

    
    var arrAllReviewList: NSArray! = []
    
    
    override func viewDidLoad()
    {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        simplenavigationBarUI()
        
        self.title = "All Reviews"
        
        let img = UIImage(named: "BackArrow")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:img, style: .plain, target:self, action: #selector(self.btnBackClicked(_:)))
        self.navigationController?.navigationBar.isHidden = false
        
        tblReviewList.estimatedRowHeight = 90
        
        tblReviewList.rowHeight = UITableViewAutomaticDimension
        
        self.getAllReviews()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    //MARK: GET ALL REVIEWS FUNCITON
    func getAllReviewsList(_ completionHandler: @escaping (AnyObject?, NSError?) -> ())
    {
        let size = CGSize(width: 80, height:80)
        
        self.startActivityAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 1)!)
        
        strWSURL=GETWSURL()
        
       // print(strRestaurantID)
        
        if (strRestaurantID?.characters.count == 0)
        {
            
            let check:String? = (strMenuID! as AnyObject) as? String

            
            let param1 : [ String : AnyObject] = [
                "MenuId":check! as AnyObject
            ]
            
           makeCall(section: strURL as String + "GetMenuReview?",param:param1,completionHandler: completionHandler)
        }
        else
        {
        
        
            print("today is thursday" , strRestaurantID)
        let param1 : [ String : AnyObject] = [
            "RestaurantId":strRestaurantID! as AnyObject
        ]
        
       makeCall(section: strURL as String + "GetRestaurantReview?",param:param1 as [String : AnyObject],completionHandler: completionHandler)
        }
    }
    
    func getAllReviews() {
        
        if InternetConnection.isConnectedToNetwork()
        {
            
            getAllReviewsList() { responseObject, error in
                
                if (responseObject != nil)
                {
                
                let json = JSON(responseObject!)
                if let string = json.rawString()
                {
                    //Do something you want
                    //print(string)
                    self.stopActivityAnimating()
                    
                    if let data = string.data(using: String.Encoding.utf8) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                            print("json ",json)
                            
                            let successVal:String = json!["Success"] as! String
                            if (successVal == "1") {
                            
                                //var arrData: NSArray!
                                self.arrAllReviewList = json!["Data"] as! NSArray
                                print(self.arrAllReviewList)
                                
                                self.tblReviewList.reloadData()
      
                            }else {
                                
                                let dialog = ZAlertView(title: "Foodies4u",
                                                         message: "No Record Found" /*(json!["Data"]?.object(0).valueForKey("status") .stringValue)*/,
                                                         closeButtonText: "Ok",
                                                         closeButtonHandler: { (alertView) -> () in
                                                            
                                                            alertView.dismissAlertView()

                                                            self.navigationController!.popViewController(animated: false)
                                    }
                                )
                               dialog.show()
                                dialog.allowTouchOutsideToDismiss = true
                               
                            }
                            
                            
                            
                        } catch {
                            print("Something went wrong")
                        }
                    }
                    
                }
                }
                else
                {
                    self.stopActivityAnimating()

                    self.AlertMethod()
                }
                
            }
        }
    }
    
    //MARK: - BUTTON CLICKS
    @IBAction func btnBackClicked(_ sender: UIButton){
        _ = navigationController?.popViewController(animated: true)
    }
    // MARK: - TABLEVIEW DATASOURCE AND DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrAllReviewList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell: Customcell = tableView.dequeueReusableCell(withIdentifier: "reviewContentCell") as! Customcell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let viewForBackground: UIView = cell.viewWithTag(10)! as UIView
        viewForBackground.layer.cornerRadius = 5.0
        viewForBackground.layer.borderColor = UIColor.lightGray.cgColor
        viewForBackground.layer.borderWidth = 0.8
        
        //for user profile image
        //            let imgIcon: UIImageView = viewForBackground.viewWithTag(20) as! UIImageView
        //            imgIcon.image = UIImage(named: arrImages[indexPath.row])
        
        //            //for user name
        let lblUserName: UILabel = viewForBackground.viewWithTag(30) as! UILabel
        lblUserName.text = (arrAllReviewList.value(forKey: "CustomerName") as AnyObject).object(at: (indexPath as NSIndexPath).row) as? String
        
        //for rating
        let strRating = (arrAllReviewList.value(forKey: "Rating")as AnyObject).object(at: (indexPath as NSIndexPath).row) as! NSString
        let lblRatingText: UILabel = viewForBackground.viewWithTag(40) as! UILabel
        lblRatingText.text = "Rated       \(strRating)" // fix 7 space inbetween for UI formatting
        
        //for description
        let strDesc = ((arrAllReviewList.value(forKey: "Comment") ) as AnyObject).object(at: (indexPath as NSIndexPath).row) as! NSString
        let lblDescriptionText: UILabel = viewForBackground.viewWithTag(50) as! UILabel
        lblDescriptionText.text = strDesc as String
        
        return cell
    }
}
