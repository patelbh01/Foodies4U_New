//
//  DBManager.h
//  studentDemo
//
//  Created by Bhavi Mistry on 9/17/13.
//  Copyright (c) 2013 . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBManager : NSObject {
    NSString *databasePath;
    
}
+(DBManager*)getSharedInstance;
-(BOOL)createDB;


// for add user check inout entry
//-(BOOL)addUserEntry: (NSString *)username datetime:(NSString *)dTime latlong:(NSString *)latlng punchinout:(NSString *)punchInout type:(NSString *)type;

- (NSMutableArray*) getAllCountryDialCodeList ;

//-(BOOL) deleteAllUserCheckInOutHistory;

@end
