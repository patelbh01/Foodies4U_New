//
//  DBManager.m
//  studentDemo
//
//  Created by Bhavi Mistry on 9/17/13.
//  Copyright (c) 2013 . All rights reserved.
//

#import "DBManager.h"

static DBManager *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DBManager

+(DBManager*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
       // [sharedInstance createDB];
    }
    
    return sharedInstance;
}
#pragma mark - create DB
-(BOOL)createDB{
    NSLog(@"createDB");
    
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    //NSLog(@"docs dir = %@",docsDir);
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"DBCountryDialCode.sqlite"]];
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        // NSLog(@"first if");
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            // NSLog(@"second if");
            char *errMsg;
            
            const char *sql_stmt ="create table if not exists tblCheckInOut (id integer primary key AUTOINCREMENT, username text, datetime text, latlong text,punchinout text,type text)";
            
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            
            sqlite3_close(database);
            return  isSuccess;
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    return isSuccess;
}


-(NSMutableArray *)getAllCountryDialCodeList {
    
   // NSLog(@"in getAllCountryDialCodeList");
    
    @try {
        
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *dbPath = [[[NSBundle mainBundle] resourcePath ]stringByAppendingPathComponent:@"DBCountryDialCode.sqlite"];
        BOOL success = [fileMgr fileExistsAtPath:dbPath];
        if(!success)
        {
            //NSLog(@"Cannot locate database file '%@'.", dbPath);
        }
        if(!(sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK))
        {
            //NSLog(@"An error has occured.");
        }
        
        //const char *dbpath = [databasePath UTF8String];
        
        
       // if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
            
            NSString *querySQL = [NSString stringWithFormat: @"SELECT id, countryName, countryCode, countryDialCode from tblCountryDialCode order by countryName"];
            
            const char *query_stmt = [querySQL UTF8String];
            NSMutableArray *resultArray = [[NSMutableArray alloc]init];
            
            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    
                    while(sqlite3_step(statement) <= SQLITE_ROW) {
                        
                        
                        NSString *rowid = [[NSString alloc]initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 0)];
                        
                        NSString *strCountryName = [[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 1)];
                        NSString *strCountryCode = [[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 2)];
                        NSString *strCountryDialCode = [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 3)];
                        
                        NSMutableDictionary *dictTemp = [NSMutableDictionary dictionary];
                        
                        [dictTemp setObject: rowid forKey: @"id"];
                        [dictTemp setObject: strCountryName forKey: @"countryName"];
                        [dictTemp setObject: strCountryCode forKey: @"countryCode"];
                        [dictTemp setObject: strCountryDialCode forKey: @"countryDialCode"];
                        
                        [resultArray addObject:dictTemp];
                        
                        //NSLog(@"all values = %@ :: %@ :: %@ :: %@", rowid,strCountryName, strCountryCode, strCountryDialCode);
                        
                    }
                    
                    return resultArray;
                }else {
                    NSLog(@"select error = %s",sqlite3_errmsg(database));
                    return resultArray;
                }
            }else {
                NSLog(@"select error = %s",sqlite3_errmsg(database));
                return resultArray;
            }
        //}
    }
    @catch (NSException *exception) {
        NSLog(@"An exception occured: %@", [exception reason]);
    }
    @finally {
        //return wineArray;
    }
    return nil;
}


@end
