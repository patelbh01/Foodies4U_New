//
//  Reachability.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 21/07/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration


open class InternetConnection
{
    class func isConnectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags)
        {
          
            
            
            
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        
        if isReachable == false
        {
        let dialog = ZAlertView(title: "Foodies4u",
                                message: GlobalConstants.Str_Internet,
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                                    
                                    if #available(iOS 9.0, *) {
                                        let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                                        if let url = settingsUrl {
                                            UIApplication.shared.openURL(url as URL)
                                        }
                                    }
                                     else {
                                        /*guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                                            return
                                        }
                                        if UIApplication.shared.canOpenURL(settingsUrl) {
                                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                                print("Settings opened: \(success)") // Prints true
                                            })
                                        }*/
                                        UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)! as URL);

                                    }
                                    
           
            }
        )
        
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true
        }
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}
