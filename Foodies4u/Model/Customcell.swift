//
//  Customcell.swift
//  Foodie
//
//  Created by Bhumika Patel on 29/06/16.
//  Copyright © 2016 Bhumika Patel. All rights reserved.
//

import Foundation
import UIKit

class Customcell: UITableViewCell {
    
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var ImgViewicon: UIImageView!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnBackToScan: UIButton!
    @IBOutlet weak var btnIcon: UIButton!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var imgCardImage: UIImageView!
    

    //for restaurat details
    @IBOutlet weak var btnViewAllReview: UIButton!
    @IBOutlet weak var imgReview: UIImageView!
    @IBOutlet weak var lblReview: UILabel!


    //for My Account Fav details
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblRestName: UILabel!
    @IBOutlet weak var lblRestAdd: UILabel!
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet weak var imgRestIcon: UIImageView!
    
    
    //for food item details
    
    @IBOutlet weak var imgVegNonveg: UIImageView!
    @IBOutlet weak var lblFoodItem: UILabel!
    @IBOutlet weak var lblFoodPrice: UILabel!
    @IBOutlet weak var lblFoodDesc: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var imgSpicy: UIImageView!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var btnCancelCart: UIButton!
    @IBOutlet weak var btnReseverAgain: UIButton!
    @IBOutlet weak var btnRequestReceipe: UIButton!
    @IBOutlet weak var btnWriteReview: UIButton!

    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnSelRow: UIButton!

    @IBOutlet weak var txtComments: UITextView!
    @IBOutlet weak var viewPager: ViewPager!
    
    
    //for Credit Card
    
    @IBOutlet weak var lblCardType: UILabel!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var imgViewDefault: UIImageView!
    @IBOutlet weak var imgVIewCardType: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var imgVIewCredtiCard: UIView!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblMarkAsDefault: UILabel!
    @IBOutlet weak var btnMarkAsDefault : UIButton!
    @IBOutlet weak var btnRateFoodItem : UIButton!




    //For restaurant review
    @IBOutlet weak var currReview: FloatRatingView!
    @IBOutlet weak var imgRestaurantImage: UIImageView!
    @IBOutlet weak var lblRestauratnName: UILabel!
    @IBOutlet weak var lblRates: UILabel!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var lblReviewName: UILabel!
    @IBOutlet weak var vwContainerOfRateButtons: UIView!
    @IBOutlet weak var txtReviewComments: UITextView! {
        didSet{
            txtReviewComments.layer.borderColor = UIColor.lightGray.cgColor
            txtReviewComments.layer.borderWidth = 1.0
        }
    }
    
    @IBOutlet weak var btnSubmitReview: UIButton! {
        didSet{
            btnSubmitReview.layer.cornerRadius = 5.0
        }
    }
    
    //For place order review
    
    @IBOutlet weak var lblDateandTime: UILabel!
    @IBOutlet weak var lblFoodItemName: UILabel!
    @IBOutlet weak var lblFoodQty: UILabel!
    @IBOutlet weak var btnIncreaseQty: UIButton!
    @IBOutlet weak var btnDecreaseQty: UIButton!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lblTax: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblOrderID: UILabel!

    @IBOutlet weak var viewCopoun: UIView!
    @IBOutlet weak var txtfieldCopoun: UITextField!
    @IBOutlet weak var btnapply: UIButton!
    @IBOutlet weak var btnPlaceOrder: UIButton!
    @IBOutlet weak var btnAddMoreItem: UIButton!
    @IBOutlet weak var lblGrandTotalAmount: UILabel!
    @IBOutlet weak var btnBuzz: UIButton!




    override func awakeFromNib() {
        super.awakeFromNib()
        
       // btnRegister.layer.cornerRadius=5.0
       // btnBackToScan.layer.cornerRadius=5.0

        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
