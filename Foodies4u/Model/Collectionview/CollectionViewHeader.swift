//
//  CollectionViewHeader.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 11/08/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation

class CollectionViewHeader:UICollectionReusableView
{
    @IBOutlet weak var lblSection: UILabel!
}