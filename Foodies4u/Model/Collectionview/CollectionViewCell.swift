//
//  CollectionViewCell.swift
//  UICollectionView
//
//  Created by Brian Coleman on 2014-09-04.
//  Copyright (c) 2014 Brian Coleman. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    
    //for sorting in homeview
    
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var lblName: UILabel!

    @IBOutlet var ViewSepratorLine: UIView!

    //for menu
    
    @IBOutlet var lblMenu: UILabel!
    @IBOutlet var btnMenu: UIButton!
    @IBOutlet var btnAddtoCart: UIButton!
    @IBOutlet var viewSelected: UIView!
    @IBOutlet var viewUnerline: UIView!
    @IBOutlet var btnFavItem: UIButton!
    @IBOutlet var btnFoodItemImage: UIImageView!
    @IBOutlet var btnDiscount: UIButton!
    @IBOutlet var imgisspecial: UIImageView!

    @IBOutlet var lblprice: UILabel!
    
    
    // for my preferences
    
    @IBOutlet var btnRadio: UIButton!
    @IBOutlet var btnPrefName: UIButton!
    @IBOutlet var btnSelCell: UIButton!
    @IBOutlet var btnReview: UIButton!
    @IBOutlet var txtfieldTip: UITextField!





    
    
}
