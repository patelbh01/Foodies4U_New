//
//  Global.h
//  Foodies4u
//
//  Created by Bhavi Mistry on 21/06/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

#ifndef Global_h
#define Global_h


#define MAIN_BROWN_COLOR  UIColor.init(red: 42.0/255.0, green: 18.0/255.0, blue: 15.0/255.0, alpha: 1.0)
#define MENU_BG_COLOR  UIColor.init(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0) //238
#define MENU_SELECTED_BG_COLOR  UIColor.init(red: 183.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0) //183 51 51
#define MENU_BORDER_COLOR  UIColor.init(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0) // 204

/*
 Navigation Menu Bg color: #eeeeee;
 Navigation Menu Font Color: #2a120f;
 Navigation Mneu Selected Font Color: #b73333;
 Border Color: #cccccc;
 */
#endif /* Global_h */
