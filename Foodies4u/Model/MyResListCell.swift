//
//  MyResListCell.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 08/08/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import UIKit


class MyResListCell: UITableViewCell
{
    //for restlist,restdetails and search screen

    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var ImgViewRest: UIImageView!
  
    @IBOutlet weak var btnFiltersName: UIButton!
    @IBOutlet weak var btnIcon: UIButton!
    @IBOutlet weak var lblRestName: UILabel!
    @IBOutlet weak var lblRestAddress: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblFavourite: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var imgRestAddress:  UIImageView!
    @IBOutlet weak var imgFav:  UIImageView!
    @IBOutlet weak var imgVeg:  UIImageView!
    @IBOutlet weak var imgNonVeg:  UIImageView!
    @IBOutlet weak var imgLocation:  UIImageView!
    @IBOutlet weak var txtfieldLocationAddress:  UITextField!
    @IBOutlet weak var btnChooseLocation:  UIButton!


    @IBOutlet weak var imgDist:  UIImageView!
    @IBOutlet var floatRatingView: FloatRatingView!
    
    
    //for filters screen
    @IBOutlet weak var btnFilterCatName: UIButton!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var btnFilters: UIButton!
    @IBOutlet weak var btnRadioBtn: UIButton!
    @IBOutlet weak var btnClickRow: UIButton!
    @IBOutlet weak var sliderDistance: UISlider!
    @IBOutlet weak var lblMinDistance: UILabel!
    @IBOutlet weak var lblMaxDistance: UILabel!

    @IBOutlet weak var txtFieldPrice: UITextField!
    @IBOutlet weak var viewLine: UIView!


    //for make reservation screen
    @IBOutlet weak var txtfieldreservation: UITextField!
    @IBOutlet weak var txtfieldAdult: UITextField!
    @IBOutlet weak var txtfieldChildren: UITextField!
    @IBOutlet weak var btnReservation: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ImgViewicon: UIImageView!
    @IBOutlet weak var txtRequest: UITextView!
    @IBOutlet weak var ImgChildren: UIImageView!
    @IBOutlet weak var ImgMadentory: UIImageView!

    
    //for discounts
    
    @IBOutlet var lblCouponCode: UILabel!
    @IBOutlet var lblValidDate: UILabel!
    @IBOutlet var btnSelectCoupon: UIButton!
    
    // for my order details
    
    @IBOutlet var lblOrderId: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var btnPrice: UIButton!
    @IBOutlet var btnOrderStatus: UIButton!
    @IBOutlet var lblSpecialRequest: UILabel!

    //for track order
    
    @IBOutlet weak var viewOrderStatus: UIView!


    
   

}
