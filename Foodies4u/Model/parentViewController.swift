//
//  parentViewController.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 17/06/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

/// refereal form on gretyhr

class parentViewController: UIViewController
{
  //  var DynamicView:UIView! = nil

   // var activityIndicatorView : DGActivityIndicatorView = DGActivityIndicatorView()
    var myDict: NSDictionary!
    var strURL: NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
       // self.navigationBarUI()
    }
    
    
    //MARK - NAVIGATION BAR
    func navigationBarUI() {
        //add logo in title bar
        let imgLogo = UIImage(named: "FoodiesLogo.png")
        let imageview = UIImageView(image: imgLogo)
        self.navigationItem.titleView = imageview
        
        //self.navigationItem.setHidesBackButton(true, animated:true);

        UIApplication.shared.statusBarStyle = .lightContent

        self.navigationController?.navigationBar.backgroundColor = UIColor.black
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 42.0/255.0, green: 18.0/255.0, blue: 15.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
    }
    
     func AlertMethod() {
        let dialog:ZAlertView = ZAlertView(title: "Foodies4u",
                                message: "Something went wrong. Please try  after sometime."
            ,
                                closeButtonText: "Ok",
                                closeButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                                    
                                    
            }
            
        )
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true
    }
    
    //MARK - NAVIGATION BAR
    func simplenavigationBarUI()
    {
        
        UIApplication.shared.statusBarStyle = .lightContent

        self.navigationController?.navigationBar.backgroundColor = UIColor.black
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 42.0/255.0, green: 18.0/255.0, blue: 15.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName : UIColor.white,
            NSFontAttributeName : UIFont (name: "Aileron-Light", size: 22)!
        ]
        
    }
    
    
    
    
    //MARK - CUSTOM NAVIGATION BAR

   /* func customnavigationBarUI()
    {
        
        let deviceType = UIDevice.currentDevice().deviceType
        print(deviceType)
        switch deviceType
        {
        case .IPhone4:
                 DynamicView=UIView(frame: CGRectMake(0, 20, self.view.frame.size.width, 50))
        case .IPhone5:
  DynamicView=UIView(frame: CGRectMake(0, 20, self.view.frame.size.width, 50))
            
        case .IPhone5S:
  DynamicView=UIView(frame: CGRectMake(0, 20, self.view.frame.size.width, 50))
        case .IPhone6:
  DynamicView=UIView(frame: CGRectMake(0, 20, self.view.frame.size.width, 60))
        case .IPhone6Plus:
  DynamicView=UIView(frame: CGRectMake(0, 20, self.view.frame.size.width, 60))
        case .IPhone6S:
  DynamicView=UIView(frame: CGRectMake(0, 20, self.view.frame.size.width, 60))
        case .IPhone6SPlus:
  DynamicView=UIView(frame: CGRectMake(0, 20, self.view.frame.size.width, 60))
            
        default:
            DynamicView=UIView(frame: CGRectMake(0, 20, self.view.frame.size.width, 50))

        }

       
        DynamicView.backgroundColor=UIColor.init(red: 42.0/255.0, green: 18.0/255.0, blue: 15.0/255.0, alpha: 1.0)
        
        let imageview=UIImageView(frame:CGRectMake(self.view.frame.size.width/3, 5, 135, 45))
        let imgLogo = UIImage(named: "FoodiesLogo.png")
        imageview.image = imgLogo
        
        let button = UIButton(frame: CGRect(x: 10, y: 25, width: 16, height: 16))
        let imgLogo1 = UIImage(named: "MenuBar.png")
        button.setImage(imgLogo1, forState: .Normal)
        button.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: .TouchUpInside)
        
        self.view.addSubview(DynamicView)
        DynamicView.addSubview(imageview)
        DynamicView.addSubview(button)
        
    }*/

    
    
    //MARK - UITEXTFIELD FORMAT
    func formatTextfield(_ txtTemp: UITextField) {
        
        //let txtTemp : UITextField = UITextField()
        //for left padding
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 40, height: 30))
        txtTemp.leftView=paddingView;
        txtTemp.leftViewMode = UITextFieldViewMode.always
        
        //for bottom border
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.black.cgColor
        border.frame = CGRect(x: 0, y: txtTemp.frame.size.height - width, width:  txtTemp.frame.size.width, height: txtTemp.frame.size.height)
        
        border.borderWidth = width
        txtTemp.layer.addSublayer(border)
        txtTemp.layer.masksToBounds = true
        
        //for making textfield placeholder color black
        txtTemp.attributedPlaceholder = NSAttributedString(string:txtTemp.placeholder!, attributes:[NSForegroundColorAttributeName: UIColor.black])
        
        //return txtTemp
    }
    func formatTextfieldWithoutBottomLine(_ txtTemp: UITextField) {
        
        //let txtTemp : UITextField = UITextField()
        //for left padding
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 40, height: 30))
        txtTemp.leftView=paddingView;
        txtTemp.leftViewMode = UITextFieldViewMode.always
        
        txtTemp.layer.borderColor = UIColor.lightGray.cgColor
        txtTemp.layer.borderWidth = 1.0
        txtTemp.layer.cornerRadius = 5.0
        
        //for making textfield placeholder color black
        //txtTemp.attributedPlaceholder = NSAttributedString(string:txtTemp.placeholder!, attributes:[NSForegroundColorAttributeName: UIColor.blackColor()])
        
        //return txtTemp
    }
    
    
    
    //MARK: validate email id
    func isValidEmail(_ testStr:String) -> Bool
    {
       
            print("validate calendar: \(testStr)")
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: testStr)
        
    }
    
    //MARK:Webservice function
    
    func makeCall(section: String,param:[ String : AnyObject], completionHandler: @escaping (AnyObject?, NSError?) -> ()) {
        print(param);
        // let params = ["user":"mistrybh01" ]
        
        Alamofire.request(section, method: .post , parameters: param)
            .responseString { response in
                switch response.result {
                case .success(let value1):
                    
                    
                    // print("bhumika",value1)
                    completionHandler(value1 as AnyObject?, nil)
                case .failure(let error):
                    completionHandler(nil, error as NSError?)
                }
        }
    }
    func makeCall1(section: String,param:[ String : String], completionHandler: @escaping (AnyObject?, NSError?) -> ()) {
        print(param);
        // let params = ["user":"mistrybh01" ]
        
        Alamofire.request(section, method: .post , parameters: param)
            .responseString { response in
                switch response.result {
                case .success(let value1):
                    
                    
                    // print("bhumika",value1)
                    completionHandler(value1 as AnyObject?, nil)
                case .failure(let error):
                    completionHandler(nil, error as NSError?)
                }
        }
    }
    
  
    
    //MARK: alertview

    func showAlertMsg(_ strMsg: NSString) {
        let alert = UIAlertController(title: "", message: strMsg as String, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: alertview
    
    func GETWSURL() -> NSString
    {
        if let path = Bundle.main.path(forResource: "WSURL", ofType: "plist")
        {
            myDict = NSDictionary(contentsOfFile: path)
            strURL = myDict.value(forKey: "URL") as? String as NSString!
        }
        
        return strURL
    }

    //MARK: animate view up-dwn
    func animateViewMoving (_ up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    
    //MARK - ACTIVITY INDICATOR
    func startIndicatorCircleAnimating() {
        
      //  let activityTypes:[DGActivityIndicatorAnimationType] = [(DGActivityIndicatorAnimationType.NineDots)]
        
     //   print("\(activityTypes)")
        
        //        for i in 0 ..< activityTypes.count {
        //
        //            //print("\(activityTypes[i].rawValue)")
        //
        //            //var indexx = activityTypes[i].rawValue;
        //
        //            activityIndicatorView = DGActivityIndicatorView(type: (Int(activityTypes[i])! as! DGActivityIndicatorAnimationType), tintColor: UIColor.blackColor())
        //
        //            //activityIndicatorView = DGActivityIndicatorView(type: (DGActivityIndicatorAnimationType)[0], tintColor: UIColor.blackColor())
        //        }
        
        
      /*  for i in 0 ..< activityTypes.count {
            
            activityIndicatorView = DGActivityIndicatorView(type: (activityTypes[i] ), tintColor: UIColor.blackColor())
            let width: CGFloat = self.view.bounds.size.width
            let height: CGFloat = self.view.bounds.size.height
            let x: CGFloat = (self.view.frame.size.width / 2) - (width / 2)
            let y: CGFloat = (self.view.frame.size.height / 2) - (height / 2)
            //activityIndicatorView.frame = CGRectMake(width * (i % 7), height * (int)(i / 7)+50, width, height);
            activityIndicatorView.frame = CGRectMake(x, y, width, height)
            self.view!.addSubview(activityIndicatorView)
            activityIndicatorView.startAnimating()
        }*/
        
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func stopIndicatorCircleAnimating() {
        
        //activityIndicatorView.startAnimating()
       // activityIndicatorView.removeFromSuperview()
        // activityIndicatorView.stopAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    internal func ShowAlert(_ title:String, Msg: String, action:()->Void)
    
    {
        
       
    }
    
    
}
