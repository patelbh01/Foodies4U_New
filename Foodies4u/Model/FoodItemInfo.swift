//
//  FoodItemInfo.swift
//  DataBaseDemo
//
//  Created by Krupa-iMac on 05/08/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

class FoodItemInfo: NSObject {
    
    var strMenuId: String = String()
    var streQuantity: String = String()
    var strUnitPrice: String = String()
    var strComment: String = String()
    var strTotalPrice:String = String()
    var strMenuName:String = String()
    var strBadge:String = String()
    var strRestName:String = String()
    var strRestId:String = String()
    var strFoodDiscount:String = String()

}
