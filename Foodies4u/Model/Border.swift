//
//  Border.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 03/11/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation
extension UIView {
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width )
        

        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        if DeviceType.IS_IPHONE_6
        {
            border.frame = CGRect(x:358 - width,y: 0, width: width,  height:self.frame.size.height)

        }
        else if DeviceType.IS_IPHONE_6P
        {
            border.frame = CGRect(x:398 - width,y: 0, width: width,  height:self.frame.size.height)

        }
        else
        {
            border.frame = CGRect(x:self.frame.size.width - width,y: 0, width: width,  height:self.frame.size.height)

        }
        
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        
        if DeviceType.IS_IPHONE_6
        {
            border.frame = CGRect(x:0, y: self.frame.size.height - width, width: 358,   height: width)
            
        }
        else if DeviceType.IS_IPHONE_6P
        {
            border.frame = CGRect(x:0, y: self.frame.size.height - width, width: 398,   height: width)
            
        }
        else
        {
            border.frame = CGRect(x:0, y: self.frame.size.height - width, width: self.frame.size.width,   height: width)
            
        }
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame =  CGRect(x:0,y: 0,width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
}
