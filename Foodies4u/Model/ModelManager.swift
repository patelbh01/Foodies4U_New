//
//  ModelManager.swift
//  DataBaseDemo
//
//  Created by Krupa-iMac on 05/08/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

let sharedInstance = ModelManager()

class ModelManager: NSObject {
    
    var database: FMDatabase? = nil

    class func getInstance() -> ModelManager
    {
        if(sharedInstance.database == nil)
        {
            sharedInstance.database = FMDatabase(path: Util.getPath("FoodItemInfo.sqlite"))
        }
        return sharedInstance
    }
    
    func addStudentData(_ FoodItemInfo: FoodItemInfo) -> Bool {
        sharedInstance.database!.open()
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO FoodItemDetail (MenuId, Quantity, UnitPrice, Comment, TotalPrice, MenuName, BadgeCount, RestName, RestId, Discount) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", withArgumentsIn: [FoodItemInfo.strMenuId, FoodItemInfo.streQuantity, FoodItemInfo.strUnitPrice, FoodItemInfo.strComment, FoodItemInfo.strTotalPrice, FoodItemInfo.strMenuName, FoodItemInfo.strBadge , FoodItemInfo.strRestName, FoodItemInfo.strRestId, FoodItemInfo.strFoodDiscount])
        sharedInstance.database!.close()
        return isInserted
    }
   
    func updateStudentData(_ FoodItemInfo: FoodItemInfo) -> Bool {
        sharedInstance.database!.open()
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE FoodItemDetail SET Quantity=?, Comment=?, TotalPrice=?,BadgeCount=?,Discount=? WHERE MenuId=?", withArgumentsIn: [FoodItemInfo.streQuantity,  FoodItemInfo.strComment, FoodItemInfo.strTotalPrice,FoodItemInfo.strBadge,FoodItemInfo.strFoodDiscount,FoodItemInfo.strMenuId])
        sharedInstance.database!.close()
        return isUpdated
    }
    
    func updateBadgeData(_ FoodItemInfo: FoodItemInfo) -> Bool {
        sharedInstance.database!.open()
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE FoodItemDetail SET BadgeCount=? WHERE MenuId=?", withArgumentsIn: [FoodItemInfo.strBadge,FoodItemInfo.strMenuId])
        sharedInstance.database!.close()
        return isUpdated
    }
    
    func deleteStudentData(_ FoodItemInfo: FoodItemInfo) -> Bool {
        sharedInstance.database!.open()
        let isDeleted = sharedInstance.database!.executeUpdate("DELETE FROM FoodItemDetail WHERE MenuId=?", withArgumentsIn: [FoodItemInfo.strMenuId])
        sharedInstance.database!.close()
        return isDeleted
    }
    
    func deleteAllStudentData(_ FoodItemInfo: FoodItemInfo) -> Bool {
        sharedInstance.database!.open()
        let isDeleted = sharedInstance.database!.executeUpdate("DELETE FROM FoodItemDetail ", withArgumentsIn: nil)
        sharedInstance.database!.close()
        return isDeleted
    }
    
    func getStudentData(_ FoodItemInfo: FoodItemInfo) -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT MenuId FROM FoodItemDetail ", withArgumentsIn: [FoodItemInfo.strMenuId])
        let marrFoodItemInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            let FoodItemInfo1 : FoodItemInfo = FoodItemInfo
            FoodItemInfo1.strBadge = resultSet.string(forColumn: "BadgeCount")

            marrFoodItemInfo.add(FoodItemInfo1)
            
            print(marrFoodItemInfo)

        }

        sharedInstance.database!.close()
        return marrFoodItemInfo

    }

    func getAllStudentData() -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM FoodItemDetail", withArgumentsIn: nil)
        let marrFoodItemInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let FoodItemInfo1 : FoodItemInfo = FoodItemInfo()
                FoodItemInfo1.strMenuId = resultSet.string(forColumn: "MenuId")
               FoodItemInfo1.strMenuName = resultSet.string(forColumn: "MenuName")
                FoodItemInfo1.strComment = resultSet.string(forColumn: "Comment")
                FoodItemInfo1.streQuantity = resultSet.string(forColumn: "Quantity")
                FoodItemInfo1.strTotalPrice = resultSet.string(forColumn: "TotalPrice")
                FoodItemInfo1.strUnitPrice = resultSet.string(forColumn: "UnitPrice")
                FoodItemInfo1.strBadge = resultSet.string(forColumn: "BadgeCount")
                FoodItemInfo1.strRestName = resultSet.string(forColumn: "RestName")
                FoodItemInfo1.strRestId = resultSet.string(forColumn: "RestId")
                FoodItemInfo1.strFoodDiscount = resultSet.string(forColumn: "Discount")

                marrFoodItemInfo.add(FoodItemInfo1)
            }
        }
        sharedInstance.database!.close()
        return marrFoodItemInfo
    }
}
