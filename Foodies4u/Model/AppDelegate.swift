

//
//  AppDelegate.swift
//  Foodies4u
//
//  Created by Bhavi Mistry on 17/06/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import UIKit
import GoogleMaps
import Fabric
import Crashlytics
import GooglePlacePicker
import UserNotifications
import FirebaseAnalytics
import FirebaseInstanceID
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON


import FirebaseMessaging


@UIApplicationMain


class AppDelegate: UIResponder, UIApplicationDelegate,SWRevealViewControllerDelegate,ESTBeaconManagerDelegate,CLLocationManagerDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate,ProximityContentManagerDelegate, NVActivityIndicatorViewable {
    
    var window: UIWindow?
    
    var strWSURL: NSString!
    var strUSERID: AnyObject!

    
    var locationManager: CLLocationManager!

    var proximityContentManager: ProximityContentManager!

    // Add a property to hold the beacon manager and instantiate it
    
    let beaconManager = ESTBeaconManager()
    var beaconEnabled : Bool = true
    var strBeaconId : String = ""

    

    
    //MARK: APPLICATIONS STATES METHODS
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        
        
        //for registration of estimote beacon
        
        ESTConfig.setupAppID("intransure-technologies-pv-429", andAppToken: "813170191e8a31b8f23bced2f8fa945f")
        let beaconID = NSUUID(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")
        let regionIdentifier = "intransure-technologies-pv-429"
        let beaconRegion = CLBeaconRegion(proximityUUID: beaconID! as UUID, identifier: regionIdentifier)
        
        
        //Notification registration
       
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        
        FIRApp.configure()
        
        
        // Add observer for InstanceID token refresh callback.
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)

        
        //for database
        
        Util.copyFile("FoodItemInfo.sqlite")

        
        //for fabrics
        
        Fabric.with([Crashlytics.self])
        GMSServices.provideAPIKey("AIzaSyBL1Fk98RMAhS9PNQkMQM3O2gOjpNL0w0I")
        GMSPlacesClient.provideAPIKey("AIzaSyBL1Fk98RMAhS9PNQkMQM3O2gOjpNL0w0I")
        
        //for location manager

        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            
            
            // UserDefaults.standard .setValue(40.720166, forKey: "LATITUDE")
            //   UserDefaults.standard .setValue(-74.08787619999998, forKey: "LONGITUDE")
        }
        
        locationManager.startMonitoring(for: beaconRegion)
        locationManager.startRangingBeacons(in: beaconRegion)
        locationManager.startUpdatingLocation()
        
        //this nsuserdefaults are used for filters screen in rightviewcontroller
        
        
        //check if user is registered or not
        
        let session = UserDefaults.standard.string(forKey: "REGISTERED")
        
        if (session != nil)
        {
            //this  is used for current location in home screen
            
            
            
            UserDefaults.standard .setValue("YES", forKey: "HOME")
            UserDefaults.standard .setValue("NO", forKey: "SHOWRESTLIST")
            UserDefaults.standard.set(false, forKey:"SEARCHFORFILTERS")
            UserDefaults.standard .set(false, forKey: "SEARCHDONEANDAPPLYFILETR")
            UserDefaults.standard.setValue("", forKey: "SEARCHTEXT")
            UserDefaults.standard.setValue("", forKey: "SEARCHTEXTCATEGORY")
            UserDefaults.standard.set("NO", forKey: "ZOOMED")

            //side menu
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let storyboard: UIStoryboard = UIStoryboard(name: "MainSW", bundle: nil)
            let frontViewController =  storyboard.instantiateViewController(withIdentifier: "HomeViewController")
            let rearViewController=storyboard.instantiateViewController(withIdentifier: "MenuController")
            let rearrightViewController=storyboard.instantiateViewController(withIdentifier: "RightMenuController")
            let frontNavigationController: UINavigationController = UINavigationController(rootViewController: frontViewController)
            let rearNavigationController: UINavigationController = UINavigationController(rootViewController: rearViewController)
            let rearrightNavigationController: UINavigationController = UINavigationController(rootViewController: rearrightViewController)
            let mainRevealController: SWRevealViewController = SWRevealViewController( rearViewController: rearNavigationController, frontViewController: frontNavigationController)
            mainRevealController.rightViewController = rearrightNavigationController
            mainRevealController.delegate = self
            rearNavigationController.isNavigationBarHidden = true
            appDelegate.window!.rootViewController = mainRevealController
            appDelegate.window!.makeKeyAndVisible()
    
        }
        
        // Configure tracker from GoogleService-Info.plist.
        
        var configureError:NSError?
      /*  SSLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")*/
        
        // Optional: configure GAI options.
        /*9 let gai = GAI.sharedInstance()
         gai.trackUncaughtExceptions = true
         // FIRAnalyticsConfiguration.sharedInstance().setAnalyticsCollectionEnabled(true)// report uncaught exceptions
         gai.logger.logLevel = GAILogLevel.Verbose */ // remove before app release
        
        
        //Crashlytics.sharedInstance().crash()
        
        
        
        
        
        
      
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
        
    }
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func proximityContentManager(_ proximityContentManager: ProximityContentManager, didUpdateContent content: AnyObject?) {
        
        if let beaconDetails = content as? BeaconDetails {
           /* self.view.backgroundColor = beaconDetails.backgroundColor
            self.label.text = "You're in \(beaconDetails.beaconName)'s range!"
            self.image.isHidden = false*/
            print("Youre in",beaconDetails.beaconName)
        } else {
            print("no neacone")

        }
    }
    
    //MARK: GET FCM TOKEN FOR NOTIFICATION

    
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            
            UserDefaults.standard.setValue(refreshedToken, forKey: "DEVICETOEKN")



            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    // Receive displayed notifications for iOS 10 devices.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        
        // Print full message.
        print("%@", userInfo)
    }
    
    
    
    
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("%@", remoteMessage.appData)
    }

    
    
    //MARK: GET USER LOCATION
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        let location = locations.last! as CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
       
        UserDefaults.standard.set((locationManager.location?.coordinate.latitude)!, forKey: "LATITUDE")
        UserDefaults.standard.set((locationManager.location?.coordinate.longitude)!, forKey: "LONGITUDE")
        
        
        UserDefaults.standard.set((locationManager.location?.coordinate.latitude)!, forKey: "CLATITUDE")
        UserDefaults.standard.set((locationManager.location?.coordinate.longitude)!, forKey: "CLONGITUDE")
        
        locationManager.stopUpdatingLocation()
        
       // print("LOCARION",UserDefaults.standard.double(forKey: "LATITUDE"))
        
    }
    
    
    
    
    
    //MARK: SET STATUS BAR METHOD
    
    
    func setStatusBarBackgroundColor(_ color: UIColor)
    {
        
        guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
            return
        }
        
        statusBar.backgroundColor = color
    }
    
    
    //MARK: ESTIMOTE BEACONS DELEGATES
    
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
       if beaconEnabled {
            
            let knownBeacon = beacons.filter{ $0.proximity != CLProximity.unknown }
            if knownBeacon.count > 0 {
                let nearestBeacon = knownBeacon[0] as CLBeacon
                
                switch nearestBeacon.proximity {
                case CLProximity.far:
                    print("You are far away from the beacon")
                    
                case CLProximity.near:
                    print("You are near the beacon")
                    
                case CLProximity.immediate:
                    print("You are in the immediate proximity of the beacon")
                    if nearestBeacon.minor.intValue < 10000000
                    {
                        /* let notification = UILocalNotification()
                         notification.alertBody =
                         "Your gate closes in 47 minutes. " +
                         "Current security wait time is 15 minutes, " +
                         "and it's a 5 minute walk from security to the gate. " +
                         "Looks like you've got plenty of time!"
                         UIApplication.shared.presentLocalNotificationNow(notification)*/
                        
                        
                        print("nearestBeacon.beaconID" ,nearestBeacon.beaconID)
                        
                        
                        strBeaconId = String(describing: nearestBeacon.beaconID)
                        strUSERID = UserDefaults.standard.value(forKey: "USERID") as AnyObject!
                        if(strUSERID != nil)
                        {
                            self.WSgetBeaconDetection()
                        }
                        
                    }
                
                    
                case CLProximity.unknown:
                    print("The proximity of the beacon is Unknown")
                    
                }
       }
        
                
        else
            {
                print("bhumica")

        }
        }
        else
    
        {
            locationManager.stopMonitoring(for: region)
            locationManager.stopRangingBeacons(in: region)

        }
    
        
        
    }
    
    
    
    
    //MARK: DETECT BEACON

    
    func WSgetBeaconDetection()
    {
        
        
        // get user-id
        
        
        let param2 : [ String : AnyObject] = [
            "Beacon":strBeaconId as AnyObject,
            "CustomerId":strUSERID as AnyObject
            
        ]
        
        print(param2)
        
        Alamofire.request("http://qafoodies4you.intransure.com:86/Api/Foodies/DetectBeacon?", method: .post , parameters: param2)
            .responseString { response in
                switch response.result {
                case .success(let value1):
                    

                
                    let json = JSON(value1)

                    if let string = json.rawString()
                    {
                        //Do something you want
                        print(string)
                        
                        
                        let encodedString : Data = (string as NSString).data(using: String.Encoding.utf8.rawValue)!
                        let finalJSON = JSON(data: encodedString)
                        print("response.result",finalJSON)

                        
                        let name = finalJSON["Success"]
                        
                        if name == "1"
                        {
                            
                            

                            self.beaconEnabled  = false
                            let storyboard: UIStoryboard = UIStoryboard(name: "Main_GetInLine", bundle: nil)
                            let WLControllerObj = storyboard.instantiateViewController(withIdentifier: "waitListViewController") as? waitListViewController
                            WLControllerObj?.strEstimateTime = String(describing:finalJSON["Data"][0]["WaitTime"])

                            UserDefaults.standard.setValue(String(describing: finalJSON["Data"][0]["Id"]), forKey: "RESERVATIONID")

                            UserDefaults.standard.setValue(String(describing: finalJSON["Data"][0]["RestaurantId"]), forKey: "RESTID")
                            let navigationController = UINavigationController(rootViewController: WLControllerObj!)
                            self.window?.rootViewController = navigationController
                            self.window?.makeKeyAndVisible()

                    
                        }
                        if name == "2"
                        {
                            let dict = finalJSON["Data"]
                            print(dict)

                            self.beaconEnabled  = false
                            let storyboard: UIStoryboard = UIStoryboard(name: "Main_GetInLine", bundle: nil)
                            let WLControllerObj = storyboard.instantiateViewController(withIdentifier: "getInLineViewController") as? getInLineViewController
                            
                            UserDefaults.standard.setValue(String(describing: finalJSON["Data"]["RestaurantId"]), forKey: "RESTID")
                            let navigationController = UINavigationController(rootViewController: WLControllerObj!)
                            self.window?.rootViewController = navigationController
                            self.window?.makeKeyAndVisible()
                            
                            
                        }
                    }
                   
                    break

                case . failure(let _):
                        break
                }
        }
     
    }
    
    

}










