//
//  Globsl_Constant.swift
//  Foodies4u
//
//  Created by Bhumika Patel on 16/08/16.
//  Copyright © 2016 Bhavi Mistry. All rights reserved.
//

import Foundation

struct GlobalConstants
{
    //  COLOR CONSTANT
    
    static let Theme_Color_Seperator: UIColor = UIColor(red: 42.0/255.0, green: 18.0/255.0, blue: 15.0/255.0, alpha: 1.0)
    
    static let Str_Internet:String = "Please check your Internet connection"
    
    static let Str_Valid_MNumber:String = "Invalid mobile number"
    
    static let Str_Valid_OTP:String = "Please enter valid OTP"
    
    static let Str_Valid_EmailId = "Please enter valid Email-Id"
    
    static let Str_OTP:String = "Please enter  OTP"
    
    static let Str_CardNumber:String = "Please enter card number"
    
    static let Str_Valid_CardNumber:String = "Please enter valid card name"
    
    static let Str_Valid_ExpiryDate:String = "Please enter valid expiry date"
    
    static let Str_Valid_CVV:String = "Please enter valid CVV number"
    
    static let Str_Success:String = "Account Updated successfully"
    
    static let Str_Name:String = "Name cannot be empty"
    
    static let Str_Adults:String = "Please enter Total Adults"
    
    static let Str_Childrens:String = "Please enter Total Children"
    
    static let Str_DateTime:String = "Reservation DateTime cannot be empty"
    
    static let Str_RequestReservationSuccess:String = "Thanks, your booking request is waiting to be confirmed. Updates will be sent to the email address and the registered mobile number you provided."

    
    static let MAIN_BROWN_COLOR = UIColor.init(red: 42.0/255.0, green: 18.0/255.0, blue: 15.0/255.0, alpha: 1.0)
    static let MENU_BG_COLOR = UIColor.init(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0) //238
    static let MENU_SELECTED_BG_COLOR = UIColor.init(red: 183.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0) //183 51 51
    static let MENU_BORDER_COLOR = UIColor.init(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0) // 204
    
    
}
struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && UIScreen.main.bounds.size.height < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && UIScreen.main.bounds.size.height == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && UIScreen.main.bounds.size.height == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && UIScreen.main.bounds.size.height == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && UIScreen.main.bounds.size.height == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && UIScreen.main.bounds.size.height == 1366.0
}
